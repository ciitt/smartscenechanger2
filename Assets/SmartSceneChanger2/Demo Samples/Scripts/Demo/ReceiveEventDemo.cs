using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test receiving events
    /// </summary>
    public class ReceiveEventDemo : EventReceiverTemplate<SendEventDemo.EventData>
    {

        // ----------------------------------------------------------------------------------------------------
        public override void Receive(SendEventDemo.EventData eventData)
        {

            if (eventData != null)
            {
                Debug.LogFormat("{0} : {1}", eventData.valA, this.name);
            }

        }

    }

}
