using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;
using System.Text;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to send language event
    /// </summary>
    public class LanguageSendDemo : MonoBehaviour
    {

        /// <summary>
        /// Language info
        /// </summary>
        public class LanguageInfo
        {

            /// <summary>
            /// Event state
            /// </summary>
            public enum EventState
            {
                Unknown,
                Changed,
            }

            /// <summary>
            /// Current language
            /// </summary>
            public string currentLanguage = "";

            /// <summary>
            /// Dictionary
            /// </summary>
            Dictionary<(string, string), string> dict = new Dictionary<(string, string), string>();

            /// <summary>
            /// Event state
            /// </summary>
            public EventState eventState { get; set; } = EventState.Unknown;

            /// <summary>
            /// Read the csv text
            /// </summary>
            /// <param name="csv">csv</param>
            public bool ReadCsv(string csv)
            {
                return CsvParser.Parse(csv, this.dict);
            }

            /// <summary>
            /// Get text
            /// </summary>
            /// <param name="language">language</param>
            /// <param name="key">key</param>
            /// <returns>text</returns>
            public string GetText(
                string language,
                string key
                )
            {

                (string, string) dictKey = (language, key);

                string ret = "";

                if (dict.ContainsKey(dictKey))
                {
                    ret = dict[dictKey];
                }

                else
                {
                    Debug.LogErrorFormat("{0} not found", dictKey);
                }

                return ret;

            }

        }

        // ----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to Csv
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Csv")]
        TextAsset m_refCsv = null;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

            this.ReadCsv();

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCsv)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCsv), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Read the csv
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void ReadCsv()
        {

            if (!this.m_refCsv)
            {
                return;
            }

            // -----------------------

            //
            {

                if (!EventManager<LanguageInfo>.Instance.value.ReadCsv(this.m_refCsv.text))
                {
                    Debug.LogErrorFormat("Failed to parse {0}", this.m_refCsv.name);
                }

            }

        }

        /// <summary>
        /// Send event
        /// </summary>
        /// <param name="language"></param>
        // ----------------------------------------------------------------------------------------------------
        public void SendEvent(string language)
        {

            EventManager<LanguageInfo> languageInfo = EventManager<LanguageInfo>.Instance;

            languageInfo.value.eventState = LanguageInfo.EventState.Changed;
            languageInfo.value.currentLanguage = language;

            EventManager<LanguageInfo>.Instance.SendEvent();

        }

    }

}
