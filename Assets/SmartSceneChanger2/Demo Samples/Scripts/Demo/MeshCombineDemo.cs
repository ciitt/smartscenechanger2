using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test combining meshes
    /// </summary>
    public class MeshCombineDemo : MonoBehaviour
    {

        /// <summary>
        /// Combine type
        /// </summary>
        enum CombineType
        {
            Combine,
            CombineAndMerge,
            CombineAndSmoothNormal
        }

        /// <summary>
        /// Reference to mesh asset A
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset A")]
        Mesh m_refMeshA = null;

        /// <summary>
        /// Reference to mesh asset B
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset B")]
        Mesh m_refMeshB = null;

        /// <summary>
        /// Reference to mesh asset C
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset C")]
        Mesh m_refMeshC = null;

        /// <summary>
        /// Reference to mesh asset C
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset C")]
        MeshFilter m_refMeshFilter = null;

        /// <summary>
        /// CombineType
        /// </summary>
        [SerializeField]
        [Tooltip("CombineType")]
        CombineType m_combineType = CombineType.Combine;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

            // CombineMeshes
            {
                this.CombineMeshes();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refMeshA)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshA), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMeshB)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshB), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMeshC)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshC), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMeshFilter)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshFilter), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void OnDestroy()
        {
            
            if (this.m_refMeshFilter)
            {
                Destroy(this.m_refMeshFilter.mesh);
            }

        }

        /// <summary>
        /// Combine meshes
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void CombineMeshes()
        {

            if (
                !this.m_refMeshA ||
                !this.m_refMeshB ||
                !this.m_refMeshC ||
                !this.m_refMeshFilter
                )
            {
                return;
            }

            if (
                !MeshFuncs.CheckMeshReadable(this.m_refMeshA) ||
                !MeshFuncs.CheckMeshReadable(this.m_refMeshB) ||
                !MeshFuncs.CheckMeshReadable(this.m_refMeshC)
                )
            {
                return;
            }

            // -----------------------------

            Mesh[] meshes = new Mesh[]
            {
                this.m_refMeshA,
                this.m_refMeshB,
                this.m_refMeshC,
            };

            Mesh mesh = new Mesh();

            // -----------------------------

            //
            {

                mesh.name = "Combined";

                MeshFuncs.CombineMeshes(
                    mesh,
                    meshes
                    );

                this.m_refMeshFilter.mesh = mesh;


            }

            // MergeVertices
            {

                if (this.m_combineType == CombineType.Combine)
                {
                    Debug.LogFormat("VertexCount after combine == {0} : {1}", mesh.vertexCount, Funcs.CreateHierarchyPath(this.transform));
                }

                else if (this.m_combineType == CombineType.CombineAndMerge)
                {
                    MeshFuncs.MergeVertices(mesh, mesh, 0.01f, 180.0f);
                    Debug.LogFormat("VertexCount after merge == {0} : {1}", mesh.vertexCount, Funcs.CreateHierarchyPath(this.transform));
                }

                else if (this.m_combineType == CombineType.CombineAndSmoothNormal)
                {
                    MeshFuncs.SmoothNormals(mesh, 0.01f, 180.0f);
                    Debug.LogFormat("VertexCount after smooth == {0} : {1}", mesh.vertexCount, Funcs.CreateHierarchyPath(this.transform));
                }

            }

        }

    }

}
