using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2Sample
{

    /// <summary>
    /// Startup
    /// </summary>
    public class StartupErrorDemo : StartupTemplate
    {

        /// <summary>
        /// Scene name
        /// </summary>
        [SerializeField]
        [Tooltip("Scene name")]
        string m_titleSceneName = "Title Scene";

        /// <summary>
        /// UiPopupGroup
        /// </summary>
        [SerializeField]
        [Tooltip("UiPopupGroup")]
        UiPopupGroup m_refUiPopupGroup = null;

        // ----------------------------------------------------------------------------------------------------
        protected override IEnumerator StartupFunction()
        {

            yield return new WaitForSeconds(1.0f);
            
            if (
                this.m_refUiPopupGroup &&
                this.m_refUiPopupGroup.refUiPopupOk &&
                this.m_refUiPopupGroup.refUiPopupOk.refShowOrHideObject
                )
            {

                yield return this.m_refUiPopupGroup.refUiPopupOk.ShowPopupIE(
                    "Back to the title scene",
                    true,
                    false,
                    UiPopupOk.SelectableType.Ok
                    );

                while (this.m_refUiPopupGroup.refUiPopupOk.refShowOrHideObject.showingState != ShowOrHideObject.ShowHideState.Hide)
                {
                    yield return null;
                }

            }

            SceneChanger.Instance.LoadNextScene(this.m_titleSceneName);

        }

        // ----------------------------------------------------------------------------------------------------
        public override void Receive(SceneChanger.SceneChangeEvent eventData)
        {
            Debug.LogFormat("{0} : {1}", eventData.eventState, Funcs.CreateHierarchyPath(this.transform));
        }

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        protected override void Start()
        {

            // base
            {
                base.Start();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refUiPopupGroup)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refUiPopupGroup), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
