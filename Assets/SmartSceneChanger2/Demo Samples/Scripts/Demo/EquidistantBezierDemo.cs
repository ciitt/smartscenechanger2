using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test EquidistantBezier
    /// </summary>
    public class EquidistantBezierDemo : MonoBehaviour
    {

        /// <summary>
        /// Point 1
        /// </summary>
        [SerializeField]
        [Tooltip("Point 1")]
        Transform m_refPoint1 = null;

        /// <summary>
        /// Point 2
        /// </summary>
        [SerializeField]
        [Tooltip("Point 2")]
        Transform m_refPoint2 = null;

        /// <summary>
        /// Point 3
        /// </summary>
        [SerializeField]
        [Tooltip("Point 3")]
        Transform m_refPoint3 = null;

        /// <summary>
        /// Point 4
        /// </summary>
        [SerializeField]
        [Tooltip("Point 4")]
        Transform m_refPoint4 = null;

        /// <summary>
        /// Unit meter to sample
        /// </summary>
        [SerializeField]
        [Tooltip("Unit meter to sample")]
        float m_samplingUnitMeter = 1.0f;

#if UNITY_EDITOR

        /// <summary>
        /// Handle position (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Handle position (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_handlePosEditorOnly = 0.0f;

#endif

        /// <summary>
        /// EquidistantBezierCurve
        /// </summary>
        EquidistantBezierCurve m_bezier = new EquidistantBezierCurve();

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {
            
            this.Sample();

#if UNITY_EDITOR

            //
            {

                if (!this.m_refPoint1)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint1), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint2)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint2), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint3)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint3), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint4)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint4), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Sample
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Sample()
        {

            // check
            {

                if (
                    !this.m_refPoint1 ||
                    !this.m_refPoint2 ||
                    !this.m_refPoint3 ||
                    !this.m_refPoint4
                    )
                {
                    return;
                }

            }

            // update
            {
                this.m_bezier.Update(
                    this.m_refPoint1.position,
                    this.m_refPoint2.position,
                    this.m_refPoint3.position,
                    this.m_refPoint4.position,
                    this.m_samplingUnitMeter,
                    this.m_refPoint1.up,
                    this.m_refPoint4.up
                    );
            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

            if (Application.isPlaying)
            {
                this.m_bezier.DrawBezierLineEditorOnly(Color.green, 3.0f);
                this.m_bezier.DrawHandleOnBezierEditorOnly(this.m_handlePosEditorOnly, true, 5.0f);
            }

        }

#endif

    }

}
