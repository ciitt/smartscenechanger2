using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;
using System.Text;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test parsing csv
    /// </summary>
    public class CsvDemo : MonoBehaviour
    {

        /// <summary>
        /// Reference to Csv
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Csv")]
        TextAsset m_refCsv = null;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

            this.ReadCsv();

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCsv)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCsv), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Read the csv
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void ReadCsv()
        {

            if (!this.m_refCsv)
            {
                return;
            }

            // -----------------------

            List<List<string>> cellList = new List<List<string>>();
            Dictionary<string, List<string>> headerDict= new Dictionary<string, List<string>>();

            // -----------------------

            //
            {

                bool success = CsvParser.Parse(this.m_refCsv.text, cellList);

                if (!success)
                {
                    Debug.LogErrorFormat("Failed to parse {0}", this.m_refCsv.name);
                    return;
                }

                else
                {

                    Debug.LogFormat("Row : Col == {0} : {1}", cellList.Count, (cellList.Count > 0 ? cellList[0].Count : 0));

                    StringBuilder sb = new StringBuilder();

                    foreach (var row in cellList)
                    {

                        sb.Length = 0;

                        foreach (var val in row)
                        {
                            sb.AppendFormat("{0} ", val);
                        }

                        print(sb.ToString());

                    }

                }

            }

            //
            {

                bool success = CsvParser.Parse(this.m_refCsv.text, headerDict);

                if (!success)
                {
                    Debug.LogErrorFormat("Failed to parse {0}", this.m_refCsv.name);
                    return;
                }

                else
                {

                    Debug.LogFormat("Header count == {0}", headerDict.Count);

                    foreach (var col in headerDict)
                    {

                        Debug.LogFormat("Header name == {0}", col.Key);

                        foreach (var val in col.Value)
                        {
                            print(val);
                        }

                        print("==================================");

                    }

                }

            }

        }

    }

}
