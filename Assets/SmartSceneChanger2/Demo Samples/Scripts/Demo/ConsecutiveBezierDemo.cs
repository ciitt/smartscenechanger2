using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test ConsecutiveEquidistantBezierCurves
    /// </summary>
    public class ConsecutiveBezierDemo : MonoBehaviour
    {

        /// <summary>
        /// Point list
        /// </summary>
        [SerializeField]
        [Tooltip("Point list")]
        Transform[] m_refPointList = new Transform[0];

        /// <summary>
        /// Unit meter to sample
        /// </summary>
        [SerializeField]
        [Tooltip("Unit meter to sample")]
        float m_samplingUnitMeter = 1.0f;

        /// <summary>
        /// Tangent scale
        /// </summary>
        [SerializeField]
        [Tooltip("Tangent scale")]
        float m_tangentScale = 1.0f;

#if UNITY_EDITOR

        /// <summary>
        /// Handle position (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Handle position (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_handlePosEditorOnly = 0.0f;

#endif

        /// <summary>
        /// ConsecutiveEquidistantBezierCurves
        /// </summary>
        ConsecutiveEquidistantBezierCurves m_cBezier = new ConsecutiveEquidistantBezierCurves();

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {
            
            this.Sample();

#if UNITY_EDITOR

            //
            {

                if (this.m_refPointList.Length <= 1)
                {
                    Debug.LogWarningFormat("{0}.Length <= 1 : {1}", nameof(this.m_refPointList), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Sample
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Sample()
        {

            // SetPointList
            {
                this.m_cBezier.SetPointList(
                    this.m_refPointList,
                    this.m_samplingUnitMeter,
                    false,
                    this.m_tangentScale
                    );
            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

            if (Application.isPlaying)
            {
                this.m_cBezier.DrawBezierLineEditorOnly(Color.green, 3.0f);
                this.m_cBezier.DrawHandleOnBezierEditorOnly(this.m_handlePosEditorOnly, true, 5.0f);
            }

        }

#endif

    }

}
