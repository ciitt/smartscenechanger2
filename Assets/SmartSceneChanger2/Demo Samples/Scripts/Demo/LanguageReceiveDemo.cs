using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;
using TMPro;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to receive language event
    /// </summary>
    public class LanguageReceiveDemo : EventReceiverTemplate<LanguageSendDemo.LanguageInfo>
    {

        /// <summary>
        /// Reference to Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        TextMeshProUGUI m_refText = null;

        /// <summary>
        /// Key
        /// </summary>
        [SerializeField]
        [Tooltip("Key")]
        string m_key = "Sample A";

        // ----------------------------------------------------------------------------------------------------
        public override void Receive(LanguageSendDemo.LanguageInfo eventData)
        {

            if (
                this.m_refText &&
                eventData != null &&
                eventData.eventState == LanguageSendDemo.LanguageInfo.EventState.Changed
                )
            {

                string text = eventData.GetText(eventData.currentLanguage, this.m_key);

                if (!string.IsNullOrEmpty(text))
                {
                    this.m_refText.text = text;
                }
                
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refText), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
