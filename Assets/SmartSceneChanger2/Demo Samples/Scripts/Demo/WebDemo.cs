using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;
using System;
using UnityEngine.Networking;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test web functions
    /// </summary>
    public class WebDemo : MonoBehaviour
    {

        /// <summary>
        /// Url to get
        /// </summary>
        [SerializeField]
        [Tooltip("Url to get")]
        string m_urlGet = "http://localhost:50001/sample.png";

        /// <summary>
        /// Url to post
        /// </summary>
        [SerializeField]
        [Tooltip("Url to post")]
        string m_urlPost = "http://localhost:50001/post";

        /// <summary>
        /// Start
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ----------------------------------------------------------------------------------------------------
        IEnumerator Start()
        {

            yield return this.TestHttpGet();

        }

        /// <summary>
        /// Test HttpGet
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ----------------------------------------------------------------------------------------------------
        IEnumerator TestHttpGet()
        {

            void successCallback(DownloadHandler downloadHandler)
            {
                Debug.LogFormat("Downloaded byte[{0}]", downloadHandler.data.Length);
            }

            void errorCallback(long responseCode, string error)
            {
                Debug.LogWarningFormat("{0} : {1}", responseCode, error);
            }

            yield return WebFuncs.HttpGet(
                this.m_urlGet,
                successCallback,
                errorCallback,
                0
                );

            yield return WebFuncs.HttpPost(
                this.m_urlPost,
                "Sample Post Text",
                successCallback,
                errorCallback,
                0
                );

        }

    }

}
