using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test popup ui
    /// </summary>
    public class PopupUiDemo : MonoBehaviour
    {

        /// <summary>
        /// Reference to UiPopupGroup
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiPopupGroup")]
        UiPopupGroup m_refUiPopupGroup = null;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refUiPopupGroup)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refUiPopupGroup), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Show the popup of ok A
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        public void ShowPopupOk_A()
        {

            void OnClick()
            {
                print("Button Pressed");
            }

            this.m_refUiPopupGroup?.refUiPopupOk?.ShowPopup(
                "Sample Message",
                OnClick,
                true,
                false,
                UiPopupOk.SelectableType.Ok
                );

        }

        /// <summary>
        /// Show the popup of ok B
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        public void ShowPopupOk_B()
        {

            void OnClickA()
            {
                print("Ok Pressed");
            }

            void OnClickB()
            {
                this.m_refUiPopupGroup?.refUiPopupOk?.ShowPopup(
                    "Next Sample Message",
                    OnClickA,
                    true,
                    true,
                    UiPopupOk.SelectableType.Ok
                    );
            }

            this.m_refUiPopupGroup?.refUiPopupOk?.ShowPopup(
                "Sample Message",
                OnClickB,
                false,
                false,
                UiPopupOk.SelectableType.Ok
                );

        }

        /// <summary>
        /// Show the popup of ok
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        public void ShowPopupYesNo()
        {

            void Yes()
            {
                print("Yes Pressed");
            }

            void No()
            {
                print("No Pressed");
            }

            this.m_refUiPopupGroup?.refUiPopupYesNo?.ShowPopup(
                "Sample Message",
                Yes,
                No,
                true,
                false,
                UiPopupYesNo.SelectableType.Yes
                );

        }

        /// <summary>
        /// Show the popup of ok
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        public void ShowPopupMessage()
        {

            this.m_refUiPopupGroup?.refUiPopupMessage?.ShowPopup(
                "Sample Message",
                1.5f
                );

        }

    }

}
