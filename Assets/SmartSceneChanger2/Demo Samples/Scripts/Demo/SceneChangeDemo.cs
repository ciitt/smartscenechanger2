using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to change scenes
    /// </summary>
    public class SceneChangeDemo : MonoBehaviour
    {

        /// <summary>
        /// Scene name to load
        /// </summary>
        [SerializeField]
        [Tooltip("Scene name to load")]
        protected string m_sceneNameToLoad = "Sample Scene 1";

        /// <summary>
        /// Load next scene
        /// </summary>
        // -------------------------------------------------------------------
        public void Load()
        {
            SceneChanger.Instance.LoadNextScene(this.m_sceneNameToLoad);
        }

    }

}
