using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2Sample
{

    /// <summary>
    /// User progress demo
    /// </summary>
    public class UserProgressDemo : MonoBehaviour
    {

        /// <summary>
        /// Key
        /// </summary>
        string m_key = "Data";

        /// <summary>
        /// Save
        /// </summary>
        // ------------------------------------------------------------------------------------
        public void Save()
        {

            EventManager<UserProgressData> em = EventManager<UserProgressData>.Instance;

            // clear
            {
                em.value.Clear();
            }

            // SendEvent
            {
                em.value.eventState = UserProgressData.EventState.Save;
                em.SendEvent();
            }

            // PlayerPrefs
            {

                string json = em.value.ConvertToJson(DateTime.Now);

                //
                {

                    //byte[] encryptedBytes = CryptoFuncs.ConvertE(
                    //    json,
                    //    CryptoFuncs.SamplesEditorOnly.bytesG,
                    //    CryptoFuncs.SamplesEditorOnly.bytesH
                    //    );

                    //// encryptedBytes = Funcs.Compress(encryptedBytes);

                    //string encryptedStr = Funcs.ConvertBytesToStringAsItIs(encryptedBytes);

                    //PlayerPrefs.SetString(this.m_key, encryptedStr);

                }

                //
                {
                    PlayerPrefs.SetString(this.m_key, json);
                }

                PlayerPrefs.Save();

                print(json);

            }

            //
            {
                print("Saved");
            }

        }

        /// <summary>
        /// Load
        /// </summary>
        // ------------------------------------------------------------------------------------
        public void Load()
        {

            EventManager<UserProgressData> em = EventManager<UserProgressData>.Instance;

            // ---------------------------

            // PlayerPrefs
            {

                //
                {

                    //string error = "";

                    //string encryptedStr = PlayerPrefs.GetString(this.m_key, "");

                    //byte[] encryptedBytes = Funcs.ConvertStringAsItIsToBytes(encryptedStr, out error);

                    //if (!string.IsNullOrEmpty(error))
                    //{
                    //    Debug.LogError(error);
                    //}

                    //// encryptedBytes = Funcs.Decompress(encryptedBytes);

                    //string json = CryptoFuncs.ConvertD(
                    //    encryptedBytes,
                    //    CryptoFuncs.SamplesEditorOnly.bytesG,
                    //    CryptoFuncs.SamplesEditorOnly.bytesH
                    //    );

                    //em.value.OverwriteFromJson(json);

                }

                //
                {

                    string json = PlayerPrefs.GetString(this.m_key, "");

                    em.value.OverwriteFromJson(json);

                }

            }

            // SendEvent
            {
                em.value.eventState = UserProgressData.EventState.Load;
                em.SendEvent();
            }

            //
            {
                print("Loaded");
            }

        }

    }

}
