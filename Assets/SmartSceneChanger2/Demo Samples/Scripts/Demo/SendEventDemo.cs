using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test sending events
    /// </summary>
    public class SendEventDemo : MonoBehaviour
    {

        /// <summary>
        /// Event data
        /// </summary>
        public class EventData
        {

            public int valA = 10;

        }

        /// <summary>
        /// Start
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

            EventManager<EventData>.Instance.value.valA = 20;

        }

        /// <summary>
        /// Send the event
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        public void SendEvent()
        {

            EventManager<EventData>.Instance.SendEvent();

        }

    }

}
