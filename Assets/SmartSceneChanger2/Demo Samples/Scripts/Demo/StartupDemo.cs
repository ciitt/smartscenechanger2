using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2Sample
{

    /// <summary>
    /// Startup
    /// </summary>
    public class StartupDemo : StartupTemplate
    {

        /// <summary>
        /// Seconds to wait
        /// </summary>
        [SerializeField]
        [Tooltip("Seconds to wait")]
        float m_dummyWaitSeconds = 1.0f;

        // ----------------------------------------------------------------------------------------------------
        protected override IEnumerator StartupFunction()
        {

            yield return new WaitForSeconds(this.m_dummyWaitSeconds);

        }

        // ----------------------------------------------------------------------------------------------------
        public override void Receive(SSC2.SceneChanger.SceneChangeEvent eventData)
        {
            Debug.LogFormat("{0} : {1}", eventData.eventState, Funcs.CreateHierarchyPath(this.transform));
        }

    }

}
