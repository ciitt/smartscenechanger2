using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SSC2Sample
{

    /// <summary>
    /// Startup
    /// </summary>
    public class StartupAddSceneDemo : StartupTemplate
    {

        /// <summary>
        /// Scene name to add
        /// </summary>
        [SerializeField]
        [Tooltip("Scene name to add")]
        string m_sceneNameToAdd = "Additive Scene";

        // ----------------------------------------------------------------------------------------------------
        protected override IEnumerator StartupFunction()
        {

            yield return SceneManager.LoadSceneAsync(this.m_sceneNameToAdd, LoadSceneMode.Additive);

        }

        // ----------------------------------------------------------------------------------------------------
        public override void Receive(SceneChanger.SceneChangeEvent eventData)
        {
            
        }

    }

}
