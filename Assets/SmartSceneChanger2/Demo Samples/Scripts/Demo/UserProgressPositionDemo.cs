using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;
using System;

namespace SSC2Sample
{

    /// <summary>
    /// User progress demo
    /// </summary>
    public class UserProgressPositionDemo : EventReceiverTemplate<UserProgressData>
    {

        /// <summary>
        /// Saved data
        /// </summary>
        [Serializable]
        class SavedData
        {
            public Vector3 worldPosition = Vector3.zero;
        }

        /// <summary>
        /// Saved data
        /// </summary>
        SavedData m_savedData = new SavedData();

        /// <summary>
        /// Get the key
        /// </summary>
        /// <returns>key</returns>
        // ---------------------------------------------------------------------------------------------
        string GetKey()
        {
            return Funcs.CreateHierarchyPath(this.transform);
        }

        // ---------------------------------------------------------------------------------------------
        public override void Receive(UserProgressData eventData)
        {
            
            if (eventData != null)
            {

                string key = this.GetKey();

                if (eventData.eventState == UserProgressData.EventState.Save)
                {
                    this.m_savedData.worldPosition = this.transform.position;
                    eventData.AddData(key, JsonUtility.ToJson(this.m_savedData));
                }

                else if (eventData.eventState == UserProgressData.EventState.Load)
                {

                    string json = eventData.GetData(key);

                    if (!string.IsNullOrEmpty(json))
                    {
                        JsonUtility.FromJsonOverwrite(json, this.m_savedData);
                        this.transform.position = this.m_savedData.worldPosition;
                    }

                }

            }

        }

        /// <summary>
        /// Move to random position
        /// </summary>
        // ---------------------------------------------------------------------------------------------
        public void MoveToRandomPosition()
        {

            float min = -5.0f;
            float max = 5.0f;

            float x = UnityEngine.Random.Range(min, max);
            float y = UnityEngine.Random.Range(min, max);
            float z = UnityEngine.Random.Range(min, max);

            this.transform.position = new Vector3(x, y, x);

        }

    }

}
