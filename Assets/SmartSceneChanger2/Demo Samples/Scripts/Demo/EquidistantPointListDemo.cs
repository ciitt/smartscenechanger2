using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test EquidistantPointList
    /// </summary>
    public class EquidistantPointListDemo : MonoBehaviour
    {

        /// <summary>
        /// Point list to sample
        /// </summary>
        [SerializeField]
        [Tooltip("Point list to sample")]
        Transform[] m_refPointList = new Transform[0];

        /// <summary>
        /// Unit meter to sample
        /// </summary>
        [SerializeField]
        [Tooltip("Unit meter to sample")]
        float m_samplingUnitMeter = 1.0f;

        /// <summary>
        /// EquidistantPointList
        /// </summary>
        EquidistantPointList m_equidistantPointList = new EquidistantPointList();

        /// <summary>
        /// Position list
        /// </summary>
        List<Vector3> m_list = new List<Vector3>();

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            this.Sample();

#if UNITY_EDITOR

            //
            {

                if (this.m_refPointList.Length <= 1)
                {
                    Debug.LogWarningFormat("{0}.Length <= 1 : {1}", nameof(this.m_refPointList), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Sample
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Sample()
        {

            List<Vector3> list = new List<Vector3>();

            // list
            {

                foreach (var val in this.m_refPointList)
                {
                    if (val)
                    {
                        list.Add(val.position);
                    }
                }

            }

            // update
            {
                this.m_equidistantPointList.Update(
                    list,
                    this.m_samplingUnitMeter
                    );
            }

            // CreateSpheres
            {
                this.CreateSpheres();
            }

        }

        /// <summary>
        /// Create spheres
        /// </summary>
        // -----------------------------------------------------------------------------------
        void CreateSpheres()
        {

            // create
            {

                int max = 100;

                if (this.m_equidistantPointList.sampledPointCount <= max)
                {

                    GameObject sphere = null;

                    int index = 0;

                    foreach (var val in this.m_equidistantPointList.sampledPoints)
                    {

                        sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

                        sphere.name = string.Format("Sphere ({0})", ++index);
                        sphere.transform.parent = this.transform;
                        sphere.transform.position = val;
                        sphere.transform.localScale = Vector3.one * 0.1f;

                    }

                }

                else
                {
                    Debug.LogWarningFormat("Too many points to create new objects: {0}", this.m_equidistantPointList.sampledPointCount);
                }

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

            if (Application.isPlaying)
            {
                this.m_equidistantPointList.DrawLinesEditorOnly(Color.green, 3.0f);
            }

        }

#endif

    }

}
