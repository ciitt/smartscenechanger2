using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test deforming mesh
    /// </summary>
    public class MeshDeformDemo : MonoBehaviour
    {

        /// <summary>
        /// Reference to mesh asset
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset")]
        Mesh m_refMesh = null;

        /// <summary>
        /// Reference to mesh asset C
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to mesh asset C")]
        MeshFilter m_refMeshFilter = null;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void Start()
        {

            // DeformMesh
            {
                this.DeformMesh();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refMesh)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMesh), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMeshFilter)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshFilter), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void OnDestroy()
        {
            
            if (this.m_refMeshFilter)
            {
                Destroy(this.m_refMeshFilter.mesh);
            }

        }

        /// <summary>
        /// Deform the mesh
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        void DeformMesh()
        {

            if (
                !this.m_refMesh ||
                !this.m_refMeshFilter
                )
            {
                return;
            }

            if (
                !MeshFuncs.CheckMeshReadable(this.m_refMesh)
                )
            {
                return;
            }

            // -----------------------------

            Mesh mesh = new Mesh();

            // -----------------------------

            //
            {

                mesh.name = "Deformed";

                Vector3 posNNN = new Vector3(0, 0, 0);
                Vector3 posPNN = new Vector3(1, 0, 0);
                Vector3 posNNP = new Vector3(0, 0, 1);
                Vector3 posPNP = new Vector3(1, 0, 1);

                Vector3 posNPN = new Vector3(0, 1, 0);
                Vector3 posPPN = new Vector3(2, 1, 0);
                Vector3 posNPP = new Vector3(0, 1, 2);
                Vector3 posPPP = new Vector3(2, 1, 2);

                MeshFuncs.DeformMesh(
                    this.m_refMesh,
                    mesh,
                    posNNN,
                    posPNN,
                    posNNP,
                    posPNP,
                    posNPN,
                    posPPN,
                    posNPP,
                    posPPP
                    );

                this.m_refMeshFilter.mesh = mesh;

            }

        }

    }

}
