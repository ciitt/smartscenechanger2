using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SSC2;

namespace SSC2Sample
{

    /// <summary>
    /// Demo to test EquidistantBezier
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshCollider))]
    public class EquidistantBezierRoadDemo : MonoBehaviour
    {

        /// <summary>
        /// Road type
        /// </summary>
        enum RoadType
        {
            Simple,
            Complex,
        }

        /// <summary>
        /// RoadType
        /// </summary>
        [SerializeField]
        [Tooltip("RoadType")]
        RoadType m_roadType = RoadType.Simple;

        /// <summary>
        /// Point 1
        /// </summary>
        [SerializeField]
        [Tooltip("Point 1")]
        Transform m_refPoint1 = null;

        /// <summary>
        /// Point 2
        /// </summary>
        [SerializeField]
        [Tooltip("Point 2")]
        Transform m_refPoint2 = null;

        /// <summary>
        /// Point 3
        /// </summary>
        [SerializeField]
        [Tooltip("Point 3")]
        Transform m_refPoint3 = null;

        /// <summary>
        /// Point 4
        /// </summary>
        [SerializeField]
        [Tooltip("Point 4")]
        Transform m_refPoint4 = null;

        /// <summary>
        /// Unit meter to sample
        /// </summary>
        [SerializeField]
        [Tooltip("Unit meter to sample")]
        float m_samplingUnitMeter = 1.0f;

#if UNITY_EDITOR

        /// <summary>
        /// Handle position (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Handle position (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_handlePosEditorOnly = 0.0f;

#endif

        /// <summary>
        /// EquidistantBezierCurve center
        /// </summary>
        EquidistantBezierCurve m_bezierC = new EquidistantBezierCurve();

        /// <summary>
        /// EquidistantBezierCurve left
        /// </summary>
        EquidistantBezierCurve m_bezierL = new EquidistantBezierCurve();

        /// <summary>
        /// EquidistantBezierCurve right
        /// </summary>
        EquidistantBezierCurve m_bezierR = new EquidistantBezierCurve();

        /// <summary>
        /// Reference to MeshFilter
        /// </summary>
        MeshFilter m_refMeshFilter = null;

        /// <summary>
        /// Reference to MeshCollider
        /// </summary>
        MeshCollider m_refMeshCollider = null;

        /// <summary>
        /// Mesh for visual
        /// </summary>
        Mesh m_visualMesh = null;

        /// <summary>
        /// Mesh for collider
        /// </summary>
        Mesh m_colliderMesh = null;

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            this.m_refMeshFilter = this.GetComponent<MeshFilter>();
            this.m_refMeshCollider = this.GetComponent<MeshCollider>();

            if (this.m_roadType == RoadType.Simple)
            {
                this.CreateSimpleRoad();
            }

            else
            {
                this.CreateComplexRoad();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refPoint1)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint1), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint2)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint2), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint3)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint3), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refPoint4)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refPoint4), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void OnDestroy()
        {
            Destroy(this.m_visualMesh); 
            Destroy(this.m_colliderMesh); 
        }

        /// <summary>
        /// Create a simple road
        /// </summary>
        // -----------------------------------------------------------------------------------
        void CreateSimpleRoad()
        {
            
            // check
            {

                if (
                    !this.m_refPoint1 ||
                    !this.m_refPoint2 ||
                    !this.m_refPoint3 ||
                    !this.m_refPoint4
                    )
                {
                    return;
                }

            }

            // m_bezierC
            {
                this.m_bezierC.Update(
                    this.m_refPoint1.position,
                    this.m_refPoint2.position,
                    this.m_refPoint3.position,
                    this.m_refPoint4.position,
                    this.m_samplingUnitMeter,
                    this.m_refPoint1.up,
                    this.m_refPoint4.up
                    );
            }

            // m_bezierB
            {

                Vector3 offset = new Vector3(3.0f, 0.0f, 0.0f);

                this.m_bezierC.CreateOffsetBezier(-offset, -offset, this.m_bezierL);
                this.m_bezierC.CreateOffsetBezier(offset, offset, this.m_bezierR);

            }

            // m_mesh
            {

                if (!this.m_visualMesh)
                {
                    this.m_visualMesh = new Mesh();
                    this.m_visualMesh.name = "Road";
                }

                if (!this.m_colliderMesh)
                {
                    this.m_colliderMesh = new Mesh();
                    this.m_colliderMesh.name = "Road Collider";
                }

            }

            //
            {

                EquidistantBezierCurve.CreateSimpleRoad(
                    this.m_bezierL,
                    this.m_bezierR,
                    0.0f,
                    this.m_bezierL.sampledTotalMeterLength,
                    0.0f,
                    this.m_bezierR.sampledTotalMeterLength,
                    3.0f,
                    this.m_visualMesh,
                    true,
                    Vector3.up,
                    this.transform
                    );

                EquidistantBezierCurve.CreateRoadCollider(
                    this.m_bezierL,
                    this.m_bezierR,
                    0.0f,
                    this.m_bezierL.sampledTotalMeterLength,
                    0.0f,
                    this.m_bezierR.sampledTotalMeterLength,
                    this.m_colliderMesh,
                    true,
                    Vector3.up,
                    this.transform
                    );

                this.m_refMeshFilter.sharedMesh = this.m_visualMesh;
                this.m_refMeshCollider.sharedMesh = null;
                this.m_refMeshCollider.sharedMesh = this.m_colliderMesh;

            }

        }

        /// <summary>
        /// Create a xomplex road
        /// </summary>
        // -----------------------------------------------------------------------------------
        void CreateComplexRoad()
        {

            // check
            {

                if (
                    !this.m_refPoint1 ||
                    !this.m_refPoint2 ||
                    !this.m_refPoint3 ||
                    !this.m_refPoint4
                    )
                {
                    return;
                }

            }

            // m_bezierC
            {
                this.m_bezierC.Update(
                    this.m_refPoint1.position,
                    this.m_refPoint2.position,
                    this.m_refPoint3.position,
                    this.m_refPoint4.position,
                    this.m_samplingUnitMeter,
                    this.m_refPoint1.up,
                    this.m_refPoint4.up
                    );
            }

            // m_bezierB
            {

                Vector3 offset = new Vector3(3.0f, 0.0f, 0.0f);

                this.m_bezierC.CreateOffsetBezier(-offset, -offset, this.m_bezierL);
                this.m_bezierC.CreateOffsetBezier(offset, offset, this.m_bezierR);

            }

            // m_mesh
            {

                if (!this.m_visualMesh)
                {
                    this.m_visualMesh = new Mesh();
                    this.m_visualMesh.name = "Road";
                }

                if (!this.m_colliderMesh)
                {
                    this.m_colliderMesh = new Mesh();
                    this.m_colliderMesh.name = "Road Collider";
                }

            }

            //
            {

                EquidistantBezierCurve.CreateComplexRoad2(
                    this.m_bezierL,
                    this.m_bezierR,
                    0.0f,
                    this.m_bezierL.sampledTotalMeterLength,
                    0.0f,
                    this.m_bezierR.sampledTotalMeterLength,
                    3.0f,
                    this.m_visualMesh,
                    true,
                    Vector3.up,
                    this.transform
                    );

                EquidistantBezierCurve.CreateRoadCollider(
                    this.m_bezierL,
                    this.m_bezierR,
                    0.0f,
                    this.m_bezierL.sampledTotalMeterLength,
                    0.0f,
                    this.m_bezierR.sampledTotalMeterLength,
                    this.m_colliderMesh,
                    true,
                    Vector3.up,
                    this.transform
                    );

                this.m_refMeshFilter.sharedMesh = this.m_visualMesh;
                this.m_refMeshCollider.sharedMesh = null;
                this.m_refMeshCollider.sharedMesh = this.m_colliderMesh;

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

            if (Application.isPlaying)
            {
                this.m_bezierC.DrawBezierLineEditorOnly(Color.green, 3.0f);
                this.m_bezierC.DrawHandleOnBezierEditorOnly(this.m_handlePosEditorOnly, true, 5.0f);
            }

        }

#endif

    }

}
