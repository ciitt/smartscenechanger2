using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2Sample
{

    /// <summary>
    /// Rotate
    /// </summary>
    public class Rotate : MonoBehaviour
    {

        /// <summary>
        /// Rotate
        /// </summary>
        [SerializeField]
        [Tooltip("Rotate")]
        Vector3 m_rotate = new Vector3(0, 0, 1);

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------
        void Update()
        {
            this.transform.Rotate(this.m_rotate);
        }

    }

}
