using SSC2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SSC2Sample
{

    /// <summary>
    /// Loading progress
    /// </summary>
    public class LoadingProgress : MonoBehaviour
    {

        /// <summary>
        /// Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Slider")]
        Slider m_refSlider = null;

        /// <summary>
        /// OnEnable
        /// </summary>
        // ------------------------------------------------------------------
        void OnEnable()
        {

            if (this.m_refSlider)
            {
                this.m_refSlider.value = 0.0f;
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------
        void Update()
        {

            if (this.m_refSlider)
            {

                if (SceneChanger.Instance.progress01 < 1.0f)
                {
                    this.m_refSlider.value = Mathf.Lerp(this.m_refSlider.value, SceneChanger.Instance.progress01, 0.1f);
                }

                else
                {
                    this.m_refSlider.value = 1.0f;
                }
                
            }

        }

    }

}
