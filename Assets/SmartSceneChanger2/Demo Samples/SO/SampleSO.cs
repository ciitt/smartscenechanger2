using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2Sample
{

    /// <summary>
    /// Sample so
    /// </summary>
    public class SampleSO : ScriptableObject
    {
        public float val = 5.0f;
        public string text = "Sample";
    }

}
