using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// StringAndString drawer
    /// </summary>
    [CustomPropertyDrawer(typeof(StringAndString))]
    public class StringAndStringDrawer : LRDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        // ------------------------------------------------------------------------------------------------------
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            this.OnGUILeftAndRight(0.3f, "key", "value", position, property, label);
        }

    }

}
