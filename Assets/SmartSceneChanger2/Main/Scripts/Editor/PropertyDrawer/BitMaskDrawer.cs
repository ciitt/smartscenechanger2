﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Bit mask PropertyDrawer
    /// </summary>
    [CustomPropertyDrawer(typeof(BitMaskAttribute))]
    public class BitMaskDrawer : PropertyDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            EditorGUI.BeginChangeCheck();

            EditorGUI.PropertyField(position, property, label);

            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
            }

        }

    }

}
