﻿using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace SSC2Editor
{

    /// <summary>
    /// Window to create new test runner objects
    /// </summary>
    public partial class CreateTerrainFromMeshWindow : EditorWindow
    {

        /// <summary>
        /// User data
        /// </summary>
        class UserData
        {
            public string folderPath = "";
        }

        /// <summary>
        /// Terrain height map resolution enum
        /// </summary>
        enum TerrainHeightMapResolutionEnum
        {
            _64 = 64,
            _128 = 128,
            _256 = 256,
            _512 = 512,
            _1024 = 1024,
            _2048 = 2048,
            _4096 = 4096,
            //_8192 = 8192,

        }

        /// <summary>
        /// Terrain unit type
        /// </summary>
        enum TerrainUnitType
        {
            FixedCount,
            Meter
        }

        /// <summary>
        /// Reference to target
        /// </summary>
        MeshCollider m_refTarget = null;

        /// <summary>
        /// Division X
        /// </summary>
        int m_divisionX = 1;

        /// <summary>
        /// Division Z
        /// </summary>
        int m_divisionZ = 1;

        /// <summary>
        /// Offset height
        /// </summary>
        float m_offsetHeight = 0.0f;

        /// <summary>
        /// Min height
        /// </summary>
        float m_minHeight = 100.0f;

        /// <summary>
        /// One terrain height map resolution
        /// </summary>
        TerrainHeightMapResolutionEnum m_oneTerrainHeightMapResolution = TerrainHeightMapResolutionEnum._128;

        /// <summary>
        /// TerrainUnitType
        /// </summary>
        TerrainUnitType m_terrainUnitType = TerrainUnitType.FixedCount;

        /// <summary>
        /// Terrain unit size XZ
        /// </summary>
        Vector2 m_terrainUnitSizeXZ = new Vector2(1000.0f, 1000.0f);

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// UserData
        /// </summary>
        static UserData userData = new UserData();

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (EditorApplication.isCompiling)
            {
                //this.Close();
                return;
            }

            // -----------------------

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // -----------------------

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "A tool to create terrains from a mesh",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            // set
            {

                this.m_refTarget = EditorGUILayout.ObjectField("Target MeshCollider", this.m_refTarget, typeof(MeshCollider), true) as MeshCollider;

                this.m_terrainUnitType = (TerrainUnitType)EditorGUILayout.EnumPopup("Type", this.m_terrainUnitType);

                if (this.m_terrainUnitType == TerrainUnitType.FixedCount)
                {
                    this.m_divisionX = EditorGUILayout.IntSlider("Division X", this.m_divisionX, 1, 10);
                    this.m_divisionZ = EditorGUILayout.IntSlider("Division Z", this.m_divisionZ, 1, 10);
                }

                else
                {

                    this.m_terrainUnitSizeXZ = EditorGUILayout.Vector2Field("Terrain Size XZ", this.m_terrainUnitSizeXZ);

                    this.CalcDivisionByTerrainUnitSize(out this.m_divisionX, out this.m_divisionZ);

                    GUI.enabled = false;

                    this.m_divisionX = EditorGUILayout.IntSlider("Division X", this.m_divisionX, 1, 10);
                    this.m_divisionZ = EditorGUILayout.IntSlider("Division Z", this.m_divisionZ, 1, 10);

                    GUI.enabled = true;

                }

                this.m_offsetHeight = EditorGUILayout.FloatField("Offset Height", this.m_offsetHeight);

                this.m_minHeight = EditorGUILayout.FloatField("Min Terrain Height", this.m_minHeight);

                this.m_oneTerrainHeightMapResolution = (TerrainHeightMapResolutionEnum)EditorGUILayout.EnumPopup("Height Map Resolution", this.m_oneTerrainHeightMapResolution);

                GUILayout.Space(30.0f);

                EditorGUILayout.LabelField(
                    string.Format("{0} x {1} = {2} terrains will be created.",
                    this.m_divisionX,
                    this.m_divisionZ,
                    this.m_divisionX * this.m_divisionZ
                    ));

                GUILayout.Space(30.0f);

            }

            // check
            {

                if (
                    this.m_refTarget &&
                    this.m_refTarget.gameObject.scene == null
                    )
                {
                    this.m_refTarget = null;
                }

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.IsAvailable();

                if (GUILayout.Button("Create", GUILayout.MinHeight(30.0f)))
                {

                    this.CreateTerrains();

                    GUI.FocusControl("");

                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------
        bool IsAvailable()
        {
            return
                this.m_refTarget &&
                this.m_divisionX > 0 &&
                this.m_divisionZ > 0
                ;
        }

        /// <summary>
        /// Create terrains
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void CreateTerrains()
        {

            if (!this.IsAvailable())
            {
                return;
            }

            if (!this.m_refTarget.gameObject.activeInHierarchy)
            {
                EditorUtility.DisplayDialog("Error", "Please activate MeshCollider GameObject", "OK");
                return;
            }

            if (!this.m_refTarget.sharedMesh)
            {
                EditorUtility.DisplayDialog("Error", "The mesh in MeshCollider is null", "OK");
                return;
            }

            // folderPath
            {

                string folderPath = EditorUtility.OpenFolderPanel("Folder path to save terrain data", userData.folderPath, "");

                if (string.IsNullOrEmpty(folderPath))
                {
                    return;
                }

                if (!folderPath.StartsWith(Application.dataPath))
                {
                    EditorUtility.DisplayDialog("Error", "Select a folder in Assets", "OK");
                    return;
                }

                if (!FuncsEditorOnly.CheckOverwriteAssetsIfNonEmptyFolder(folderPath))
                {
                    return;
                }

                userData.folderPath = FuncsEditorOnly.ChangeAbsolutePathToProjectPath(folderPath);

            }

            // ---------------------------

            GameObject root = new GameObject();

            Terrain[] terrains = terrains = new Terrain[this.m_divisionX * this.m_divisionZ];

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            // root
            {

                root.isStatic = true;
                root.name = GameObjectUtility.GetUniqueNameForSibling(null, string.Format("{0} Terrains (1)", this.m_refTarget.name));

                Undo.RegisterCreatedObjectUndo(root, "Created a Terrain Root");

            }

            // CreateTerrainFromMesh
            {

                int x = 0;
                int z = 0;

                int index = 0;

                if (this.m_terrainUnitType == TerrainUnitType.FixedCount)
                {

                    for (z = 0; z < this.m_divisionZ; z++)
                    {

                        for (x = 0; x < this.m_divisionX; x++)
                        {
                            terrains[index++] = this.CreateTerrainFromMesh(root.transform, x, z, userData.folderPath);
                        }

                    }

                }

                else
                {

                    Vector2 basePos = this.CalcBasePos();

                    Vector2 tempPosA = Vector2.zero;
                    Vector2 tempPosB = Vector2.zero;

                    for (z = 0; z < this.m_divisionZ; z++)
                    {

                        for (x = 0; x < this.m_divisionX; x++)
                        {

                            tempPosA = basePos + new Vector2(
                                this.m_terrainUnitSizeXZ.x * x,
                                this.m_terrainUnitSizeXZ.y * z
                                );

                            tempPosB = tempPosA + this.m_terrainUnitSizeXZ;

                            terrains[index++] = this.CreateTerrainFromMesh(
                                root.transform,
                                tempPosA,
                                tempPosB,
                                userData.folderPath
                                );

                        }

                    }


                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
            }

            // ClearProgressBar
            {
                EditorUtility.ClearProgressBar();
            }

            // SetNeighbors
            {

                Terrain target = null;

                Terrain left = null;
                Terrain right = null;
                Terrain top = null;
                Terrain bottom = null;

                for (int i = 0; i < terrains.Length; i++)
                {

                    target = terrains[i];

                    if (target)
                    {

                        left = (i % this.m_divisionX == 0) ? null : terrains[i - 1];
                        right = ((i + 1) % this.m_divisionX == 0) ? null : terrains[i + 1];
                        top = (i + this.m_divisionX >= terrains.Length) ? null : terrains[i + this.m_divisionX];
                        bottom = (i - this.m_divisionX < 0) ? null : terrains[i - this.m_divisionX];

                        target.SetNeighbors(left, top, right, bottom);

                        target.Flush();

                    }

                }

                Terrain.SetConnectivityDirty();

            }

            // Finish
            {
                EditorUtility.DisplayDialog("Confirmation", "Finished", "OK");
            }

        }

        /// <summary>
        /// Create a terrain from a mesh
        /// </summary>
        /// <param name="parent">parent</param>
        /// <param name="indexX">index X</param>
        /// <param name="indexZ">index Z</param>
        /// <param name="folderPath">folder path to save terrain data</param>
        /// <returns>created terrain</returns>
        // -------------------------------------------------------------------------------------------
        Terrain CreateTerrainFromMesh(
            Transform parent,
            int indexX,
            int indexZ,
            string folderPath
            )
        {

            // check
            {

                if (!this.IsAvailable())
                {
                    return null;
                }

            }

            // --------------------------------

            TerrainData terrainData = new TerrainData();

            terrainData.heightmapResolution = (int)this.m_oneTerrainHeightMapResolution;

            GameObject terrainGameObject = Terrain.CreateTerrainGameObject(terrainData);

            // --------------------------------

            // terrainData, terrainGameObject
            {

                terrainGameObject.name = GameObjectUtility.GetUniqueNameForSibling(parent, "Terrain (1)");
                terrainGameObject.transform.SetParent(parent);

                terrainData.name = terrainGameObject.name;

                terrainData.SetDetailResolution(1024, 32);

            }

            // RegisterCreatedObjectUndo
            {
                Undo.RegisterCreatedObjectUndo(terrainGameObject, "Created a Terrain");
            }

            //
            {

                int x = 0;
                int z = 0;

                Bounds bounds = this.m_refTarget.bounds;

                float[,] heights = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                float lerpValX = 0.0f;
                float lerpValZ = 0.0f;

                float maxDistance = (bounds.max.y - bounds.min.y) + 10.0f;

                float fromX01 = Mathf.Clamp01(indexX / (float)this.m_divisionX);
                float toX01 = Mathf.Clamp01((indexX + 1) / (float)this.m_divisionX);

                float fromZ01 = Mathf.Clamp01(indexZ / (float)this.m_divisionZ);
                float toZ01 = Mathf.Clamp01((indexZ + 1) / (float)this.m_divisionZ);

                float minY = bounds.min.y + this.m_offsetHeight;
                float maxY = Mathf.Max(bounds.max.y + this.m_offsetHeight, minY + this.m_minHeight);

                string progressTitle = "Creating";
                string progressMessage = string.Format("{0} / {1}", (indexZ * this.m_divisionX) + indexX + 1, this.m_divisionX * this.m_divisionZ);

                HashSet<(int, int)> errorListZX = new HashSet<(int, int)>();

                // heights
                {

                    for (z = 0; z < terrainData.heightmapResolution; z++)
                    {

                        lerpValZ = z / (float)(terrainData.heightmapResolution - 1);

                        EditorUtility.DisplayProgressBar(
                            progressTitle,
                            progressMessage,
                            lerpValZ
                            );

                        lerpValZ = Mathf.Lerp(fromZ01, toZ01, lerpValZ);

                        for (x = 0; x < terrainData.heightmapResolution; x++)
                        {

                            lerpValX = x / (float)(terrainData.heightmapResolution - 1);

                            lerpValX = Mathf.Lerp(fromX01, toX01, lerpValX);

                            ray.origin = new Vector3(
                                Mathf.Lerp(bounds.min.x, bounds.max.x, lerpValX),
                                bounds.max.y + 1.0f,
                                Mathf.Lerp(bounds.min.z, bounds.max.z, lerpValZ)
                                );

                            if (this.m_refTarget.Raycast(ray, out hit, maxDistance))
                            {
                                heights[z, x] = Mathf.InverseLerp(minY, maxY, hit.point.y + this.m_offsetHeight);
                            }

                            else
                            {
                                errorListZX.Add((z, x));
                                heights[z, x] = 0.0f;
                            }

                        }

                    }

                    // fix
                    {

                        //int indexZ0 = 0;
                        //int indexZ1 = 0;
                        //int indexX0 = 0;
                        //int indexX1 = 0;

                        //float temp = 0.0f;

                        //foreach (var val in errorListZX)
                        //{

                        //    indexZ0 = Mathf.Clamp(val.Item1 - 1, 0, terrainData.heightmapResolution - 1);
                        //    indexZ1 = Mathf.Clamp(val.Item1 + 1, 0, terrainData.heightmapResolution - 1);
                        //    indexX0 = Mathf.Clamp(val.Item2 - 1, 0, terrainData.heightmapResolution - 1);
                        //    indexX1 = Mathf.Clamp(val.Item2 + 1, 0, terrainData.heightmapResolution - 1);

                        //    temp =
                        //        heights[indexZ0, val.Item2] +
                        //        heights[indexZ1, val.Item2] +
                        //        heights[val.Item1, indexX0] +
                        //        heights[val.Item1, indexX1]
                        //        ;

                        //    heights[val.Item1, val.Item2] = temp / 4.0f;

                        //}

                    }

                }

                // set
                {

                    float scaleX = 1.0f / this.m_divisionX;
                    float scaleZ = 1.0f / this.m_divisionZ;

                    Vector3 size = new Vector3(bounds.size.x * scaleX, maxY - minY, bounds.size.z * scaleZ);
                    Vector3 pos = new Vector3(
                        Mathf.Lerp(bounds.min.x, bounds.max.x, fromX01),
                        minY,
                        Mathf.Lerp(bounds.min.z, bounds.max.z, fromZ01)
                        );

                    // terrainData
                    {
                        terrainData.size = size;
                        terrainData.SetHeights(0, 0, heights);
                    }

                    // terrainGameObject
                    {
                        terrainGameObject.transform.position = pos;
                    }
                    
                }

            }

            // CreateAsset
            {
                AssetDatabase.CreateAsset(terrainData, string.Format("{0}/{1}.asset", folderPath, terrainData.name));
            }

            return terrainGameObject.GetComponent<Terrain>();

        }

    }

}
