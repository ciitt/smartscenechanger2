using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace SSC2Editor
{

    public class EditorWindowMenus
    {

        ///<summary>
        /// Show Editor Scene Loader Window
        ///</summary>
        [MenuItem("Tools/SSC2/Editor Scene Loader Window", false, 0)]
        static void EditorSceneLoader()
        {
            EditorWindow.GetWindow(typeof(EditorSceneLoaderWindow)).Show();
        }

        ///<summary>
        /// Show CreateTerrainFromMeshWindow
        ///</summary>
        [MenuItem("Tools/SSC2/Terrain/Convert mesh to Terrain", false, 0)]
        static void ShowCreateTerrainFromMeshWindow()
        {
            EditorWindow.GetWindow(typeof(CreateTerrainFromMeshWindow)).Show();
        }

        ///<summary>
        /// Show TerrainLayerFillWindow
        ///</summary>
        [MenuItem("Tools/SSC2/Terrain/Fill Terrain", false, 0)]
        static void ShowTerrainLayerFillWindow()
        {
            EditorWindow.GetWindow(typeof(TerrainLayerFillWindow)).Show();
        }

        ///<summary>
        /// Show ChangeTerrainHeightSizeWindow
        ///</summary>
        [MenuItem("Tools/SSC2/Terrain/Change Terrain Height Size", false, 0)]
        static void ShowChangeTerrainHeightSizeWindow()
        {
            EditorWindow.GetWindow(typeof(ChangeTerrainHeightSizeWindow)).Show();
        }

        ///<summary>
        /// Show ChangeTerrainHeightByRaycastWindow
        ///</summary>
        [MenuItem("Tools/SSC2/Terrain/Change Terrain Heights by Raycast", false, 0)]
        static void ShowChangeTerrainHeightByRaycastWindow()
        {
            EditorWindow.GetWindow(typeof(ChangeTerrainHeightByRaycastWindow)).Show();
        }

        ///<summary>
        /// Show ObjectPositionSetterWindow
        ///</summary>
        [MenuItem("Tools/SSC2/Object/Move Object Positions by Raycast", false, 0)]
        static void ShowObjectPositionSetterWindow()
        {
            EditorWindow.GetWindow(typeof(ObjectPositionSetterWindow)).Show();
        }

    }

}
