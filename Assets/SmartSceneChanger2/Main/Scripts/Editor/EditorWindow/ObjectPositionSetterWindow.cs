using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SSC2Editor
{

    /// <summary>
    /// Object setter tool
    /// </summary>
    public class ObjectPositionSetterWindow : EditorWindow
    {

        /// <summary>
        /// Target LayerMask to include
        /// </summary>
        LayerMask m_includeLayerMask = 0;

        /// <summary>
        /// Target LayerMask to exclude
        /// </summary>
        LayerMask m_excludeLayerMask = 0;

        /// <summary>
        /// Parent
        /// </summary>
        GameObject m_parent = null;

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Raycast length
        /// </summary>
        float m_raycastLength = 1000.0f;

        /// <summary>
        /// Offst rom ground
        /// </summary>
        float m_offsetFromGround = 0.0f;

        /// <summary>
        /// Flag to rotate along surface
        /// </summary>
        bool m_rotateAlongSurface = false;

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (EditorApplication.isCompiling)
            {
                this.Close();
                return;
            }

            // -----------------------

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // -----------------------

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "A tool to put objects on the ground",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            // set
            {

                this.m_parent = EditorGUILayout.ObjectField("Parent", this.m_parent, typeof(GameObject), true) as GameObject;

                LayerMask temp = EditorGUILayout.MaskField("Target LayerMask", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(this.m_includeLayerMask), InternalEditorUtility.layers);
                this.m_includeLayerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(temp);

                temp = EditorGUILayout.MaskField("Exclude LayerMask", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(this.m_excludeLayerMask), InternalEditorUtility.layers);
                this.m_excludeLayerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(temp);

                this.m_rotateAlongSurface = EditorGUILayout.Toggle("Rotate along surface", this.m_rotateAlongSurface);
                this.m_raycastLength = EditorGUILayout.FloatField("Raycast length", this.m_raycastLength);
                this.m_offsetFromGround = EditorGUILayout.FloatField("Offset from ground", this.m_offsetFromGround);

                GUILayout.Space(30.0f);

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.IsAvailable();

                if (GUILayout.Button("Move", GUILayout.MinHeight(30.0f)))
                {

                    this.MoveObjects(
                        this.m_parent?.transform,
                        this.m_raycastLength,
                        this.m_offsetFromGround,
                        this.m_rotateAlongSurface,
                        this.m_includeLayerMask,
                        this.m_excludeLayerMask
                        );

                    GUI.FocusControl("");

                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------
        bool IsAvailable()
        {
            return
                this.m_includeLayerMask > 0
                ;
        }

        /// <summary>
        /// Move objects
        /// </summary>
        /// <param name="parent">parent</param>
        /// <param name="raycastLength">raycast length</param>
        /// <param name="offsetFromGround">offset from ground</param>
        /// <param name="rotateAlongSurface">rotate along surface</param>
        /// <param name="includeLayerMask">layer mask to include</param>
        /// <param name="excludeLayerMask">layer mask to exclude</param>
        // -------------------------------------------------------------------------------------------
        void MoveObjects(
            Transform parent,
            float raycastLength,
            float offsetFromGround,
            bool rotateAlongSurface,
            LayerMask includeLayerMask,
            LayerMask excludeLayerMask
            )
        {

            // check
            {

                if (
                    !parent ||
                    !this.IsAvailable()
                    )
                {
                    return;
                }

            }

            // --------------

            int groupID = Undo.GetCurrentGroup();

            // --------------

            // MoveObject
            {

                int count = 0;
                int maxCount = parent.childCount;

                string progressTitle = "Moving";
                string progressMessage = string.Format("{0} / {1}", count, maxCount);

                foreach (Transform child in parent)
                {

                    progressMessage = string.Format("{0} / {1}", count++, maxCount);

                    EditorUtility.DisplayProgressBar(
                        progressTitle,
                        progressMessage,
                        count / (float)maxCount
                        );

                    this.MoveObject(
                        child,
                        raycastLength,
                        offsetFromGround,
                        rotateAlongSurface,
                        includeLayerMask,
                        excludeLayerMask
                        );

                }

            }

            // ClearProgressBar
            {
                EditorUtility.ClearProgressBar();
            }

            //
            {

                if (parent.childCount > 0)
                {
                    Undo.CollapseUndoOperations(groupID);
                    Undo.SetCurrentGroupName("Moved objects");
                }

            }

            // Finish
            {
                EditorUtility.DisplayDialog("Confirmation", "Finished", "OK");
            }

        }

        /// <summary>
        /// Move an object
        /// </summary>
        /// <param name="transform">target</param>
        /// <param name="raycastLength">raycast length</param>
        /// <param name="offsetFromGround">offset from ground</param>
        /// <param name="rotateAlongSurface">rotate along surface</param>
        /// <param name="includeLayerMask">layer mask to include</param>
        /// <param name="excludeLayerMask">layer mask to exclude</param>
        // -------------------------------------------------------------------------------------------
        void MoveObject(
            Transform transform,
            float raycastLength,
            float offsetFromGround,
            bool rotateAlongSurface,
            LayerMask includeLayerMask,
            LayerMask excludeLayerMask
            )
        {

            if (transform)
            {

                Ray ray = new Ray(transform.position, -Vector3.up);

                RaycastHit hit = new RaycastHit();

                if (Physics.Raycast(ray, out hit, raycastLength, includeLayerMask))
                {

                    Vector3 pos = hit.point;
                    Quaternion rot =
                        (rotateAlongSurface) ?
                        Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation :
                        transform.rotation
                        ;

                    // ---------------------

                    if (
                        excludeLayerMask > 0 &&
                        Physics.Raycast(ray, out hit, raycastLength, excludeLayerMask)
                        )
                    {
                        return;
                    }

                    // ---------------------

                    Undo.RecordObject(transform, "Move");

                    transform.SetPositionAndRotation(pos, rot);

                }

            }

        }

    }

}
