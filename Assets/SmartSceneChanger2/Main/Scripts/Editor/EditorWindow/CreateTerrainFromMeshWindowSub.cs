using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace SSC2Editor
{

    public partial class CreateTerrainFromMeshWindow : EditorWindow
    {

        /// <summary>
        /// Calculate divisions by terrain unit size
        /// </summary>
        /// <param name="divisionX">destionation X</param>
        /// <param name="divisionZ">destionation Z</param>
        // -------------------------------------------------------------------------------------------
        void CalcDivisionByTerrainUnitSize(
            out int divisionX,
            out int divisionZ
            )
        {

            // check
            {

                if (
                    !this.m_refTarget ||
                    this.m_terrainUnitSizeXZ.x <= 0.0f ||
                    this.m_terrainUnitSizeXZ.y <= 0.0f
                    )
                {
                    divisionX = 1;
                    divisionZ = 1;
                    return;
                }

            }

            // calc
            {

                Bounds bounds = this.m_refTarget.bounds;

                divisionX = Mathf.CeilToInt(bounds.size.x / this.m_terrainUnitSizeXZ.x);
                divisionZ = Mathf.CeilToInt(bounds.size.z / this.m_terrainUnitSizeXZ.y);

                divisionX = Mathf.Clamp(divisionX, 1, 10);
                divisionZ = Mathf.Clamp(divisionZ, 1, 10);

            }

        }

        /// <summary>
        /// Calculate base position for terrains
        /// </summary>
        /// <returns>base pos</returns>
        // -------------------------------------------------------------------------------------------
        Vector2 CalcBasePos()
        {

            Vector2 ret = Vector2.zero;

            // check
            {

                if (
                    !this.m_refTarget ||
                    this.m_terrainUnitSizeXZ.x <= 0.0f ||
                    this.m_terrainUnitSizeXZ.y <= 0.0f
                    )
                {
                    return ret;
                }

            }

            // calc
            {

                float halfX = this.m_divisionX * this.m_terrainUnitSizeXZ.x * 0.5f;
                float halfZ = this.m_divisionZ * this.m_terrainUnitSizeXZ.y * 0.5f;

                Vector3 center = this.m_refTarget.bounds.center;

                ret = new Vector2(
                    center.x - halfX,
                    center.z - halfZ
                    );

            }

            return ret;

        }

        /// <summary>
        /// Create a terrain from a mesh
        /// </summary>
        /// <param name="parent">parent</param>
        /// <param name="fromXZ">position XZ as from</param>
        /// <param name="toXZ">position XZ as to</param>
        /// <param name="folderPath">folder path to save terrain data</param>
        /// <returns>created terrain</returns>
        // -------------------------------------------------------------------------------------------
        Terrain CreateTerrainFromMesh(
            Transform parent,
            Vector2 fromXZ,
            Vector2 toXZ,
            string folderPath
            )
        {

            // check
            {

                if (!this.IsAvailable())
                {
                    return null;
                }

            }

            // --------------------------------

            TerrainData terrainData = new TerrainData();

            terrainData.heightmapResolution = (int)this.m_oneTerrainHeightMapResolution;

            GameObject terrainGameObject = Terrain.CreateTerrainGameObject(terrainData);

            // --------------------------------

            // terrainData, terrainGameObject
            {

                terrainGameObject.name = GameObjectUtility.GetUniqueNameForSibling(parent, "Terrain (1)");
                terrainGameObject.transform.SetParent(parent);

                terrainData.name = terrainGameObject.name;

                terrainData.SetDetailResolution(1024, 32);

            }

            // RegisterCreatedObjectUndo
            {
                Undo.RegisterCreatedObjectUndo(terrainGameObject, "Created a Terrain");
            }

            //
            {

                int x = 0;
                int z = 0;

                Bounds bounds = this.m_refTarget.bounds;

                float[,] heights = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                float lerpValX = 0.0f;
                float lerpValZ = 0.0f;

                float maxDistance = (bounds.max.y - bounds.min.y) + 10.0f;

                float minY = bounds.min.y + this.m_offsetHeight;
                float maxY = Mathf.Max(bounds.max.y + this.m_offsetHeight, minY + this.m_minHeight);

                // heights
                {

                    //Vector3 origin = Vector3.zero;

                    for (z = 0; z < terrainData.heightmapResolution; z++)
                    {

                        lerpValZ = z / (float)(terrainData.heightmapResolution - 1);

                        for (x = 0; x < terrainData.heightmapResolution; x++)
                        {

                            lerpValX = x / (float)(terrainData.heightmapResolution - 1);

                            ray.origin = new Vector3(
                                Mathf.Lerp(fromXZ.x, toXZ.x, lerpValX),
                                bounds.max.y + 1.0f,
                                Mathf.Lerp(fromXZ.y, toXZ.y, lerpValZ)
                                );

                            if (this.m_refTarget.Raycast(ray, out hit, maxDistance))
                            {
                                heights[z, x] = Mathf.InverseLerp(minY, maxY, hit.point.y + this.m_offsetHeight);
                            }

                            else
                            {
                                heights[z, x] = 0.0f;
                            }

                        }

                    }

                }

                // set
                {

                    float scaleX = 1.0f / this.m_divisionX;
                    float scaleZ = 1.0f / this.m_divisionZ;

                    Vector3 size = new Vector3(
                        toXZ.x - fromXZ.x,
                        maxY - minY,
                        toXZ.y - fromXZ.y
                        );

                    Vector3 pos = new Vector3(
                        fromXZ.x,
                        minY,
                        fromXZ.y
                        );

                    // terrainData
                    {
                        terrainData.size = size;
                        terrainData.SetHeights(0, 0, heights);
                    }

                    // terrainGameObject
                    {
                        terrainGameObject.transform.position = pos;
                    }

                }

            }

            // CreateAsset
            {
                AssetDatabase.CreateAsset(terrainData, string.Format("{0}/{1}.asset", folderPath, terrainData.name));
            }

            return terrainGameObject.GetComponent<Terrain>();

        }


    }

}
