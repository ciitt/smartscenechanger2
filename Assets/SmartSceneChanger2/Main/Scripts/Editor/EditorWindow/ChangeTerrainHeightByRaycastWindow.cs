using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SSC2Editor
{

    /// <summary>
    /// Window to change the terrain height by raycast
    /// </summary>
    public class ChangeTerrainHeightByRaycastWindow : EditorWindow
    {

        /// <summary>
        /// Reference to target
        /// </summary>
        [SerializeField]
        [HideInInspector]
        Terrain[] m_refTerrainList = new Terrain[1];

        /// <summary>
        /// Target LayerMask
        /// </summary>
        LayerMask m_targetLayerMask = 0;

        /// <summary>
        /// Distance of raycast
        /// </summary>
        float m_raycastDistance = 1000.0f;

        /// <summary>
        /// Smooth length
        /// </summary>
        int m_smoothLength = 0;

        /// <summary>
        /// SerializedObject
        /// </summary>
        SerializedObject m_serializedObject = null;

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void Awake()
        {
            this.m_serializedObject = new SerializedObject(this);
        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (EditorApplication.isCompiling)
            {
                //this.Close();
                return;
            }

            // -----------------------

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // -----------------------

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "A tool to change terrain heights by raycast",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            // set
            {

                this.m_serializedObject.Update();
                EditorGUILayout.PropertyField(this.m_serializedObject.FindProperty(nameof(this.m_refTerrainList)), true);
                this.m_serializedObject.ApplyModifiedProperties();

                LayerMask temp = EditorGUILayout.MaskField("Target LayerMask", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(this.m_targetLayerMask), InternalEditorUtility.layers);
                this.m_targetLayerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(temp);

                this.m_raycastDistance = EditorGUILayout.Slider("Distance of Raycast", this.m_raycastDistance, 100.0f, 5000.0f);

                this.m_smoothLength = EditorGUILayout.IntSlider("Smooth length", this.m_smoothLength, 0, 100);

                GUILayout.Space(30.0f);

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.IsAvailable();

                if (GUILayout.Button("Raycast", GUILayout.MinHeight(30.0f)))
                {

                    this.ChangeTerrainHeights(this.m_raycastDistance);

                    GUI.FocusControl("");

                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------
        bool IsAvailable()
        {
            return
                this.m_refTerrainList.Length > 0 &&
                this.m_targetLayerMask > 0
                ;
        }

        /// <summary>
        /// Change terrain heights
        /// </summary>
        /// <param name="raycastDistance">distance of raycast</param>
        // -------------------------------------------------------------------------------------------
        void ChangeTerrainHeights(float raycastDistance)
        {

            // check
            {

                if (!this.IsAvailable())
                {
                    return;
                }

            }

            // --------------

            // ChangeTerrainHeight
            {

                int count = 0;
                int maxCount = this.m_refTerrainList.Length;

                string progressTitle = "Creating";
                string progressMessage = string.Format("{0} / {1}", count, maxCount);

                foreach (var val in this.m_refTerrainList)
                {

                    if (val)
                    {

                        progressMessage = string.Format("{0} / {1}", count++, maxCount);

                        EditorUtility.DisplayProgressBar(
                            progressTitle,
                            progressMessage,
                            count / (float)maxCount
                            );

                        this.ChangeTerrainHeight(val, raycastDistance);

                    }

                }

            }

            // ClearProgressBar
            {
                EditorUtility.ClearProgressBar();
            }

            // Finish
            {
                EditorUtility.DisplayDialog("Confirmation", "Finished", "OK");
            }

        }

        /// <summary>
        /// Change terrain height
        /// </summary>
        /// <param name="terrain">terrain</param>
        /// <param name="raycastDistance">distance of raycast</param>
        // -------------------------------------------------------------------------------------------
        void ChangeTerrainHeight(
            Terrain terrain,
            float raycastDistance
            )
        {

            // check
            {

                if (
                    !terrain ||
                    !this.IsAvailable()
                    )
                {
                    return;
                }

            }

            // --------------

            // ----------------------

            TerrainData terrainData = terrain.terrainData;

            int heightmapResolution = terrainData.heightmapResolution;

            float[,] heightMap = terrainData.GetHeights(0, 0, heightmapResolution, heightmapResolution);

            bool[,] isHitMap = new bool[heightmapResolution, heightmapResolution];
            //bool[,] alreadySmoothedMap = new bool[heightmapResolution, heightmapResolution];

            int x = 0;
            int z = 0;

            // ----------------------

            // undo
            {
                Undo.RecordObject(terrainData, "Modified Terrain");
            }

            // init
            {

                for (z = 0; z < heightmapResolution; z++)
                {

                    for (x = 0; x < heightmapResolution; x++)
                    {
                        isHitMap[z, x] = false;
                    }

                }

            }

            // heightMap
            {

                Vector3 from = terrain.transform.position;
                Vector3 to = from + terrainData.size;

                float lerpValX = 0.0f;
                float lerpValZ = 0.0f;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                float originZ = 0.0f;
                //float originY = 0.0f;

                for (z = 0; z < heightmapResolution; z++)
                {

                    lerpValZ = z / (float)(heightmapResolution - 1);

                    originZ = Mathf.Lerp(from.z, to.z, lerpValZ);

                    for (x = 0; x < heightmapResolution; x++)
                    {

                        lerpValX = x / (float)(heightmapResolution - 1);

                        ray.origin = new Vector3(
                            Mathf.Lerp(from.x, to.x, lerpValX),
                            Mathf.Lerp(from.y, to.y, heightMap[z, x]) + (raycastDistance),
                            originZ
                            );

                        if (Physics.Raycast(ray, out hit, raycastDistance * 2, this.m_targetLayerMask))
                        {
                            heightMap[z, x] = Mathf.InverseLerp(from.y, to.y, hit.point.y);
                            isHitMap[z, x] = true;
                        }

                    }

                }

            }

            // smooth
            {

                int tempZ = 0;
                int tempX = 0;

                //int foundCounter = 0;

                //float sum = 0.0f;

                int minX = -1;
                int maxX = heightmapResolution;
                int minZ = -1;
                int maxZ = heightmapResolution;

                float valX = 0.0f;
                float valZ = 0.0f;

                int smoothCounter = 0;

                for (z = 0; z < heightmapResolution; z++)
                {

                    for (x = 0; x < heightmapResolution; x++)
                    {

                        if (!isHitMap[z, x])
                        {

                            minX = -1;
                            maxX = heightmapResolution;
                            minZ = -1;
                            maxZ = heightmapResolution;

                            valX = -1.0f;
                            valZ = -1.0f;

                            // find
                            {

                                for (tempZ = z + 1, smoothCounter = 0; tempZ < heightmapResolution; tempZ++)
                                {

                                    if (isHitMap[tempZ, x])
                                    {
                                        maxZ = tempZ;
                                        break;
                                    }

                                    if (++smoothCounter >= this.m_smoothLength)
                                    {
                                        break;
                                    }

                                }

                                for (tempZ = z - 1, smoothCounter = 0; tempZ >= 0; tempZ--)
                                {

                                    if (isHitMap[tempZ, x])
                                    {
                                        minZ = tempZ;
                                        break;
                                    }

                                    if (++smoothCounter >= this.m_smoothLength)
                                    {
                                        break;
                                    }

                                }

                                for (tempX = x + 1, smoothCounter = 0; tempX < heightmapResolution; tempX++)
                                {

                                    if (isHitMap[z, tempX])
                                    {
                                        maxX = tempX;
                                        break;
                                    }

                                    if (++smoothCounter >= this.m_smoothLength)
                                    {
                                        break;
                                    }

                                }

                                for (tempX = x - 1, smoothCounter = 0; tempX >= 0; tempX--)
                                {

                                    if (isHitMap[z, tempX])
                                    {
                                        minX = tempX;
                                        break;
                                    }

                                    if (++smoothCounter >= this.m_smoothLength)
                                    {
                                        break;
                                    }

                                }

                            }

                            // valX
                            {

                                if (0 <= minX && maxX < heightmapResolution)
                                {
                                    valX = Mathf.Lerp(heightMap[z, minX], heightMap[z, maxX], Mathf.InverseLerp(minX, maxX, x));
                                }

                                else if (0 <= minX)
                                {
                                    valX = Mathf.Lerp(heightMap[z, minX], heightMap[z, x], (x - minX) / (float)(this.m_smoothLength + 1));
                                }

                                else if (maxX < heightmapResolution)
                                {
                                    valX = Mathf.Lerp(heightMap[z, maxX], heightMap[z, x], (maxX - x) / (float)(this.m_smoothLength + 1));
                                }

                            }

                            // valZ
                            {

                                if (0 <= minZ && maxZ < heightmapResolution)
                                {
                                    valZ = Mathf.Lerp(heightMap[minZ, x], heightMap[maxZ, x], Mathf.InverseLerp(minZ, maxZ, z));
                                }

                                else if (0 <= minZ)
                                {
                                    valZ = Mathf.Lerp(heightMap[minZ, x], heightMap[z, x], (z - minZ) / (float)(this.m_smoothLength + 1));
                                }

                                else if (maxZ < heightmapResolution)
                                {
                                    valZ = Mathf.Lerp(heightMap[maxZ, x], heightMap[z, x], (maxZ - z) / (float)(this.m_smoothLength + 1));
                                }

                            }

                            // update
                            {

                                if (
                                    valZ >= 0.0f &&
                                    valX >= 0.0f
                                    )
                                {
                                    heightMap[z, x] = Mathf.Clamp01((valZ + valX) * 0.5f);
                                }

                                else if (valZ >= 0.0f)
                                {
                                    heightMap[z, x] = valZ;
                                }

                                else if (valX >= 0.0f)
                                {
                                    heightMap[z, x] = valX;
                                }

                            }

                        }

                    }

                }

            }

            // SetHeights
            {
                terrainData.SetHeights(0, 0, heightMap);
            }

        }

    }

}
