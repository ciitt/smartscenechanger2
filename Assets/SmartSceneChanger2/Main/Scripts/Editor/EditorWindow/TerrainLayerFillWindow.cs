using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SSC2Editor
{

    /// <summary>
    /// Window to fill the terrain by TerrainLayer
    /// </summary>
    public class TerrainLayerFillWindow : EditorWindow
    {

        /// <summary>
        /// Reference to target
        /// </summary>
        [SerializeField]
        [HideInInspector]
        Terrain[] m_refTerrainList = new Terrain[1];

        /// <summary>
        /// Target LayerMask
        /// </summary>
        LayerMask m_targetLayerMask = 0;

        /// <summary>
        /// TerrainLayer to fill
        /// </summary>
        TerrainLayer m_refTerrainLayer = null;

        /// <summary>
        /// SerializedObject
        /// </summary>
        SerializedObject m_serializedObject = null;

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Blend level
        /// </summary>
        int m_blendLevel = 0;

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void Awake()
        {
            this.m_serializedObject = new SerializedObject(this);
        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (
                EditorApplication.isCompiling ||
                this.m_serializedObject == null
                )
            {
                this.Close();
                return;
            }

            // -----------------------

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // -----------------------

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "A tool to create terrains from a mesh",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            // set
            {

                this.m_serializedObject.Update();
                EditorGUILayout.PropertyField(this.m_serializedObject.FindProperty(nameof(this.m_refTerrainList)), true);
                this.m_serializedObject.ApplyModifiedProperties();

                this.m_refTerrainLayer = EditorGUILayout.ObjectField("Terrain Layer", this.m_refTerrainLayer, typeof(TerrainLayer), true) as TerrainLayer;
                
                LayerMask temp = EditorGUILayout.MaskField("Target LayerMask", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(this.m_targetLayerMask), InternalEditorUtility.layers);
                this.m_targetLayerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(temp);

                this.m_blendLevel = EditorGUILayout.IntSlider("Blend Level", this.m_blendLevel, 0, 5);

                GUILayout.Space(30.0f);

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.IsAvailable();

                if (GUILayout.Button("Fill", GUILayout.MinHeight(30.0f)))
                {

                    this.FillTerrains();

                    GUI.FocusControl("");

                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------
        bool IsAvailable()
        {
            return
                this.m_refTerrainList.Length > 0 &&
                this.m_refTerrainLayer &&
                this.m_targetLayerMask > 0
                ;
        }

        /// <summary>
        /// Fill terrains
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void FillTerrains()
        {

            // check
            {

                if (!this.IsAvailable())
                {
                    return;
                }

            }

            // --------------

            // FillTerrains
            {

                int count = 0;
                int maxCount = this.m_refTerrainList.Length;

                string progressTitle = "Creating";
                string progressMessage = string.Format("{0} / {1}", count, maxCount);

                foreach (var val in this.m_refTerrainList)
                {

                    if (val)
                    {

                        progressMessage = string.Format("{0} / {1}", count++, maxCount);

                        EditorUtility.DisplayProgressBar(
                            progressTitle,
                            progressMessage,
                            count / (float)maxCount
                            );

                        this.FillTerrains(val);

                    }

                }

            }

            // ClearProgressBar
            {
                EditorUtility.ClearProgressBar();
            }

            // Finish
            {
                EditorUtility.DisplayDialog("Confirmation", "Finished", "OK");
            }

        }

        /// <summary>
        /// Fill terrain
        /// </summary>
        /// <param name="terrain">terrain</param>
        // -------------------------------------------------------------------------------------------
        void FillTerrains(Terrain terrain)
        {

            // check
            {

                if (
                    !terrain ||
                    !this.IsAvailable()
                    )
                {
                    return;
                }

            }

            // --------------

            TerrainLayer[] terrainLayers = terrain.terrainData.terrainLayers;

            int terrainLayerIndex = Array.FindIndex(terrainLayers, x => x == this.m_refTerrainLayer);

            // --------------

            // check
            {

                if (terrainLayerIndex < 0)
                {
                    Debug.LogWarningFormat("Skipped {0} because the terrain doesn't have a target LayerMask.", terrain.name);
                    return;
                }

            }

            // undo
            {
                Undo.RecordObject(terrain, "Modified Terrain");
            }

            // ----------------------

            TerrainData terrainData = terrain.terrainData;
            
            float[,,] alphaMap = terrainData.GetAlphamaps(0, 0, terrainData.alphamapWidth, terrainData.alphamapHeight);

            bool[,] hits = new bool[terrainData.alphamapWidth, terrainData.alphamapHeight];

            // ----------------------

            // alphaMap
            {

                Vector3 from = terrain.transform.position;
                Vector3 to = from + terrainData.size;

                float halfOffsetY = 5.0f;

                float maxY = to.y + halfOffsetY;
                float raycastLength = to.y - from.y + (halfOffsetY * 2);

                int x = 0;
                int z = 0;
                int i = 0;

                float lerpValX = 0.0f;
                float lerpValZ = 0.0f;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                float originZ = 0.0f;

                for (z = 0; z < terrainData.alphamapHeight; z++)
                {

                    lerpValZ = z / (float)(terrainData.alphamapHeight - 1);

                    originZ = Mathf.Lerp(from.z, to.z, lerpValZ);

                    for (x = 0; x < terrainData.alphamapWidth; x++)
                    {

                        lerpValX = x / (float)(terrainData.alphamapWidth - 1);

                        ray.origin = new Vector3(
                            Mathf.Lerp(from.x, to.x, lerpValX),
                            maxY,
                            originZ
                            );

                        if (Physics.Raycast(ray, out hit, raycastLength, this.m_targetLayerMask))
                        {

                            for (i = 0; i < terrainData.alphamapLayers; i++)
                            {
                                alphaMap[z, x, i] = (i == terrainLayerIndex) ? 1.0f : 0.0f;
                            }

                            hits[z, x] = true;

                        }

                        else
                        {
                            hits[z, x] = false;
                        }

                    }

                }
                
            }

            //
            {

                if (this.m_blendLevel > 0)
                {

                    //float[,,] tempAlphaMap = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
                    float[,,] tempAlphaMap = alphaMap.Clone() as float[,,];
                    
                    // tempAlphaMap
                    {

                        int x = 0;
                        int z = 0;
                        int i = 0;

                        int tempX = 0;
                        int tempZ = 0;

                        int manLength = 0;

                        float targetWeight = 0.0f;
                        float addedWeight = 0.0f;
                        float scale = 0.0f;

                        float valA = .0f;

                        for (z = 0; z < terrainData.alphamapHeight; z++)
                        {

                            for (x = 0; x < terrainData.alphamapWidth; x++)
                            {

                                if (hits[z, x])
                                {

                                    for (tempZ = z - this.m_blendLevel; tempZ < z + this.m_blendLevel; tempZ++)
                                    {

                                        if (
                                            tempZ < 0 ||
                                            terrainData.alphamapHeight <= tempZ
                                            )
                                        {
                                            continue;
                                        }

                                        for (tempX = x - this.m_blendLevel; tempX < x + this.m_blendLevel; tempX++)
                                        {

                                            if (
                                                tempX < 0 ||
                                                terrainData.alphamapWidth <= tempX
                                                )
                                            {
                                                continue;
                                            }

                                            // ------------------

                                            if (!hits[tempZ, tempX])
                                            {

                                                manLength = Mathf.Abs(tempZ - z) + Mathf.Abs(tempX - x);

                                                targetWeight = 1.0f - (manLength / (float)(this.m_blendLevel + 1));

                                                if (
                                                    manLength > 0 &&
                                                    alphaMap[tempZ, tempX, terrainLayerIndex] < targetWeight
                                                    )
                                                {

                                                    addedWeight = targetWeight - alphaMap[tempZ, tempX, terrainLayerIndex];

                                                    valA = 1.0f - alphaMap[tempZ, tempX, terrainLayerIndex];

                                                    scale = (1.0f - targetWeight) / valA;

                                                    for (i = 0; i < terrainData.alphamapLayers; i++)
                                                    {

                                                        if (i == terrainLayerIndex)
                                                        {
                                                            tempAlphaMap[tempZ, tempX, i] = targetWeight;
                                                        }

                                                        else
                                                        {
                                                            tempAlphaMap[tempZ, tempX, i] = alphaMap[tempZ, tempX, i] * scale;
                                                        }

                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                    // alphaMap
                    {
                        Array.Copy(tempAlphaMap, 0, alphaMap, 0, alphaMap.Length);
                    }

                }

            }

            // SetAlphamaps
            {
                terrainData.SetAlphamaps(0, 0, alphaMap);
            }

        }

    }

}
