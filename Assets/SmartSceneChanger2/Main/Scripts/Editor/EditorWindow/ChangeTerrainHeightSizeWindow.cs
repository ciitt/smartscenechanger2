using SSC2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SSC2Editor
{

    /// <summary>
    /// Window to change height sizes of the terrains
    /// </summary>
    public class ChangeTerrainHeightSizeWindow : EditorWindow
    {

        /// <summary>
        /// Reference to target
        /// </summary>
        [SerializeField]
        [HideInInspector]
        Terrain[] m_refTerrainList = new Terrain[1];

        /// <summary>
        /// New height
        /// </summary>
        float m_newHeight = 100.0f;

        /// <summary>
        /// SerializedObject
        /// </summary>
        SerializedObject m_serializedObject = null;

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void Awake()
        {
            this.m_serializedObject = new SerializedObject(this);
        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (EditorApplication.isCompiling)
            {
                this.Close();
                return;
            }

            // -----------------------

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // -----------------------

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "A tool to change height sizes of the terrains",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            // set
            {

                this.m_newHeight = EditorGUILayout.FloatField("New Height", this.m_newHeight);

                GUILayout.Space(30.0f);

                this.m_serializedObject.Update();
                EditorGUILayout.PropertyField(this.m_serializedObject.FindProperty(nameof(this.m_refTerrainList)), true);
                this.m_serializedObject.ApplyModifiedProperties();

                GUILayout.Space(30.0f);

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.IsAvailable();

                if (GUILayout.Button("Change", GUILayout.MinHeight(30.0f)))
                {

                    this.ChangeHeights();

                    GUI.FocusControl("");

                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------
        bool IsAvailable()
        {
            return
                this.m_refTerrainList.Length > 0 &&
                !Array.Exists(this.m_refTerrainList, val => !val)
                ;
        }

        /// <summary>
        /// Create terrains
        /// </summary>
        // -------------------------------------------------------------------------------------------
        void ChangeHeights()
        {

            // check
            {

                if (!this.IsAvailable())
                {
                    return;
                }

            }

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            // update
            {

                float[,] heights = null;

                int heightmapResolution = 0;

                int x = 0;
                int y = 0;

                float oldMaxHeight = 0;
                float newMaxHeight = Mathf.Max(0, this.m_newHeight);

                float height = 0.0f;

                string progressTitle = "Changing";
                string progressMessage = "";

                int count = 0;

                foreach (var val in this.m_refTerrainList)
                {

                    progressMessage = string.Format("{0} / {1}", count + 1, this.m_refTerrainList.Length);

                    EditorUtility.DisplayProgressBar(
                        progressTitle,
                        progressMessage,
                        (count++ / (float)this.m_refTerrainList.Length)
                        );

                    if (val)
                    {

                        oldMaxHeight = val.terrainData.size.y;

                        heightmapResolution = val.terrainData.heightmapResolution;

                        heights = val.terrainData.GetHeights(0, 0, heightmapResolution, heightmapResolution);

                        for (x = 0; x < heightmapResolution; x++)
                        {

                            for (y = 0; y < heightmapResolution; y++)
                            {

                                height = oldMaxHeight * heights[x, y];

                                heights[x, y] = Mathf.InverseLerp(0, newMaxHeight, height);

                            }

                        }

                        Undo.RecordObject(val.terrainData, "Changed terrain data");

                        val.terrainData.size = new Vector3(val.terrainData.size.x, newMaxHeight, val.terrainData.size.z);
                        val.terrainData.SetHeights(0, 0, heights);

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
            }

            // ClearProgressBar
            {
                EditorUtility.ClearProgressBar();
            }

            // Finish
            {
                EditorUtility.DisplayDialog("Confirmation", "Finished", "OK");
            }

        }

    }

}
