using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for ShuffleChildren
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ShuffleChildren))]
    public class ShuffleChildrenInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => "Shuffle";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            //
            {

                ShuffleChildren script = null;

                List<Transform> list = new List<Transform>();

                int seed = 0;

                foreach (var val in this.targets)
                {

                    script = val as ShuffleChildren;

                    // clear
                    {
                        list.Clear();
                    }

                    // add
                    {

                        for (int i = 0; i < script.transform.childCount; i++)
                        {
                            list.Add(script.transform.GetChild(i));
                        }

                    }

                    // InitState
                    {
                        UnityEngine.Random.InitState(seed);
                    }

                    // sort
                    {
                        list.Sort((a, b) => UnityEngine.Random.Range(0.0f, 1.0f).CompareTo(UnityEngine.Random.Range(0.0f, 1.0f)));
                    }

                    // apply
                    {

                        Undo.RegisterChildrenOrderUndo(script.gameObject, "");

                        foreach (var child in list)
                        {
                            child.SetSiblingIndex(0);
                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Shuffle Children");
            }

            //
            {
                Debug.Log("Finished");
            }

        }

    }

}
