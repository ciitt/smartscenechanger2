using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for DistributeOnFaces
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(DistributeOnFaces))]
    public class DistributeOnFacesInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => "Distibute Children";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            Mesh mesh = null;

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            //
            {

                DistributeOnFaces script = null;

                foreach (var val in this.targets)
                {

                    script = val as DistributeOnFaces;


                    // check
                    {

                        mesh = script.GetComponent<MeshFilter>().sharedMesh;

                        if (!MeshFuncs.CheckMeshReadable(mesh))
                        {
                            continue;
                        }

                        if (script.transform.childCount <= 0)
                        {
                            Debug.LogWarning("No children");
                            continue;
                        }

                    }

                    // InitState
                    {
                        UnityEngine.Random.InitState(script.seed);
                    }

                    //
                    {

                        int[] triangles = mesh.triangles;
                        Vector3[] vertices = mesh.vertices;
                        Vector3[] normals = mesh.normals;

                        Matrix4x4 localToWorldMatrix = script.transform.localToWorldMatrix;

                        int triMaxIndex = triangles.Length / 3;

                        Vector3 pos0 = Vector3.zero;
                        Vector3 pos1 = Vector3.zero;
                        Vector3 pos2 = Vector3.zero;

                        Vector3 normal0 = Vector3.up;
                        Vector3 normal1 = Vector3.up;
                        Vector3 normal2 = Vector3.up;

                        float val0 = 0.0f;
                        float val1 = 0.0f;
                        float val2 = 0.0f;
                        float valSum = 0.0f;

                        float percentage0 = 0.0f;
                        float percentage1 = 0.0f;
                        float percentage2 = 0.0f;

                        Vector3 pos = Vector3.zero;
                        Vector3 normal = Vector3.up;

                        Transform child = null;

                        List<int> validTris = this.CalcValidTriFaceIndexes(normals, triangles, script.normalRangeY, localToWorldMatrix);

                        int validTrisIndex = -1;

                        DistributeOnFaces.DistributeType distributeType = script.distributeType;

                        if (validTris.Count <= 0)
                        {
                            Debug.Log("Nothing to do");
                            return;
                        }

                        // --------------------------

                        for (int i = 0; i < script.transform.childCount; i++)
                        {

                            // triIndex
                            {
                                validTrisIndex =
                                    (distributeType == DistributeOnFaces.DistributeType.Random) ?
                                    UnityEngine.Random.Range(0, validTris.Count) :
                                    (validTrisIndex + 1) % validTris.Count
                                    ;
                            }

                            // pos, normal
                            {

                                pos0 = localToWorldMatrix.MultiplyPoint3x4(vertices[triangles[(validTris[validTrisIndex] * 3) + 0]]);
                                pos1 = localToWorldMatrix.MultiplyPoint3x4(vertices[triangles[(validTris[validTrisIndex] * 3) + 1]]);
                                pos2 = localToWorldMatrix.MultiplyPoint3x4(vertices[triangles[(validTris[validTrisIndex] * 3) + 2]]);

                                normal0 = localToWorldMatrix.MultiplyVector((normals != null) ? normals[triangles[(validTris[validTrisIndex] * 3) + 0]] : Vector3.up);
                                normal1 = localToWorldMatrix.MultiplyVector((normals != null) ? normals[triangles[(validTris[validTrisIndex] * 3) + 1]] : Vector3.up);
                                normal2 = localToWorldMatrix.MultiplyVector((normals != null) ? normals[triangles[(validTris[validTrisIndex] * 3) + 2]] : Vector3.up);

                            }

                            // val, percentage
                            {

                                val0 = UnityEngine.Random.Range(0.0f, 1.0f);
                                val1 = UnityEngine.Random.Range(0.0f, 1.0f);
                                val2 = UnityEngine.Random.Range(0.0f, 1.0f);

                                valSum = val0 + val1 + val2;

                                if (valSum > 0.0f)
                                {
                                    percentage0 = val0 / valSum;
                                    percentage1 = val1 / valSum;
                                    percentage2 = val2 / valSum;
                                }

                                else
                                {
                                    percentage0 = 0.333f;
                                    percentage1 = 0.333f;
                                    percentage2 = 0.333f;
                                }

                            }

                            //
                            {

                                pos =
                                    (pos0 * percentage0) +
                                    (pos1 * percentage1) +
                                    (pos2 * percentage2)
                                    ;

                                normal =
                                    ((normal0 * percentage0) +
                                    (normal1 * percentage1) +
                                    (normal2 * percentage2)).normalized
                                    ;

                                if (script.offsetRangeY.use)
                                {
                                    pos += normal * (script.offsetRangeY.range.GetRandomValue());
                                }

                            }

                            //
                            {

                                child = script.transform.GetChild(i);

                                Undo.RecordObject(child, "");

                                child.position = pos;
                                child.up = normal;

                            }

                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Distribute on faces");
            }

            //
            {
                Debug.Log("Finished");
            }

        }

        /// <summary>
        /// Calculate valid tri index by using normals
        /// </summary>
        /// <param name="normals">normals</param>
        /// <param name="triangles">triangles</param>
        /// <param name="normalRangeY">normal range Y</param>
        /// <param name="localToWorldMatrix">localToWorldMatrix</param>
        /// <returns>list</returns>
        // ----------------------------------------------------------------------------------
        List<int> CalcValidTriFaceIndexes(
            Vector3[] normals,
            int[] triangles,
            MinMaxFloat normalRangeY,
            Matrix4x4 localToWorldMatrix
            )
        {

            List<int> ret = new List<int>();

            //
            {

                if (
                    normals != null &&
                    triangles.Length % 3 == 0
                    )
                {

                    int triFaceCount = triangles.Length / 3;

                    Vector3 normal0 = Vector3.up;
                    Vector3 normal1 = Vector3.up;
                    Vector3 normal2 = Vector3.up;

                    Vector3 normal = Vector3.up;

                    float dot = 0.0f;

                    for (int i = 0; i < triFaceCount; i++)
                    {

                        normal0 = localToWorldMatrix.MultiplyVector(normals[triangles[(i * 3) + 0]]);
                        normal1 = localToWorldMatrix.MultiplyVector(normals[triangles[(i * 3) + 1]]);
                        normal2 = localToWorldMatrix.MultiplyVector(normals[triangles[(i * 3) + 2]]);

                        normal = ((normal0 + normal1 + normal2) / 3.0f).normalized;

                        dot = Vector3.Dot(normal, Vector3.up); // == normal.y

                        if (normalRangeY.min <= dot && dot <= normalRangeY.max)
                        {
                            ret.Add(i);
                        }

                    }

                    //Debug.Log(countA + " : " + countB);

                }

            }

            return ret;

        }

    }

}
