using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for RenameChildren
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(RenameChildren))]
    public class RenameChildrenInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => "Rename";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            //
            {

                RenameChildren script = null;

                foreach (var val in this.targets)
                {

                    script = val as RenameChildren;

                    // check
                    {

                        if (string.IsNullOrEmpty(script.oldText))
                        {
                            continue;
                        }

                    }

                    // rename
                    {

                        foreach (Transform child in script.transform)
                        {
                            Undo.RegisterChildrenOrderUndo(child.gameObject, "");
                            child.name = child.name.Replace(script.oldText, script.newText);
                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Rename Children");
            }

            //
            {
                Debug.Log("Finished");
            }

        }

    }

}
