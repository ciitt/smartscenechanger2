using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for RaycastChildren
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(RaycastChildren))]
    public class RaycastChildrenInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => "Raycast";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            //
            {

                RaycastChildren script = null;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                float randomVal = 0.0f;

                Vector3 pos = Vector3.zero;
                Quaternion rot = Quaternion.identity;

                Vector3 direction = -Vector3.up;

                foreach (var val in this.targets)
                {

                    script = val as RaycastChildren;

                    // check
                    {

                        if (
                            direction.sqrMagnitude <= 0.0f ||
                            script.raycastDistance <= 0.0f
                            )
                        {
                            continue;
                        }

                        direction.Normalize();

                    }

                    // InitState
                    {
                        UnityEngine.Random.InitState(script.offsetFromSurfaceRange.seed);
                    }

                    // update
                    {

                        foreach (Transform child in script.transform)
                        {

                            ray.origin =
                                child.transform.position +
                                (-direction * script.raycastDistance * 0.5f)
                                ;

                            ray.direction = direction;

                            randomVal = script.offsetFromSurfaceRange.GetRandomValue();

                            if (Physics.Raycast(ray, out hit, script.raycastDistance, script.targetLayerMask))
                            {

                                pos =
                                    hit.point +
                                    (hit.normal * randomVal)
                                    ;

                                rot =
                                    (script.rotateAlongSurface) ?
                                    Quaternion.FromToRotation(child.transform.up, hit.normal) * child.transform.rotation :
                                    child.transform.rotation
                                    ;

                                Undo.RecordObject(child.transform, "");

                                child.transform.SetPositionAndRotation(pos, rot);

                            }

                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Raycast Children");
            }

            //
            {
                Debug.Log("Finished");
            }

        }

    }

}
