using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for MeshAlongLines
    /// </summary>
    [CustomEditor(typeof(MeshAlongLines))]
    public class MeshAlongLinesInspector : Editor
    {

        // ----------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            // DrawDefaultInspector
            {
                DrawDefaultInspector();
            }

            // Space
            {
                GUILayout.Space(20.0f);
            }

            // Button
            {

                MeshAlongLines script = this.target as MeshAlongLines;

                if (GUILayout.Button("Add Childen to List", GUILayout.Height(30.0f)))
                {
                    this.AddChildren();
                }

                GUI.enabled = script.source;

                if (GUILayout.Button("Create (Not Undoable)", GUILayout.Height(30.0f)))
                {
                    this.Create();
                }

                GUI.enabled = true;

            }

        }

        /// <summary>
        /// Add children
        /// </summary>
        // ----------------------------------------------------------------------------------
        void AddChildren()
        {

            MeshAlongLines script = this.target as MeshAlongLines;

            // Update
            {
                this.serializedObject.Update();
            }

            // set
            {

                SerializedProperty sp = this.serializedObject.FindProperty(nameof(script.positions));
                
                sp.arraySize = script.transform.childCount;

                for (int i = 0; i < sp.arraySize; i++)
                {
                    sp.GetArrayElementAtIndex(i).objectReferenceValue = script.transform.GetChild(i);
                }

            }

            // ApplyModifiedProperties
            {
                this.serializedObject.ApplyModifiedProperties();
            }

        }

        /// <summary>
        /// Create
        /// </summary>
        // ----------------------------------------------------------------------------------
        void Create()
        {

            MeshAlongLines script = this.target as MeshAlongLines;

            Transform[] positins = script.positions;

            // check
            {

                if (positins.Length < 2)
                {
                    Debug.LogError("Lack of positions");
                    return;
                }

                if (Array.Exists(positins, val => val == null))
                {
                    Debug.LogError("Contains null");
                    return;
                }

                if (!MeshFuncs.CheckMeshReadable(script.source))
                {
                    return;
                }

            }

            // -------------------

            Mesh dest = new Mesh();

            List<Vector3> list = new List<Vector3>();

            // -------------------

            // list
            {

                foreach (var val in positins)
                {
                    list.Add(val.position);
                }

            }

            // CopyMeshesAlongLines
            {

                dest.name = "Combined Mesh";

                MeshFuncs.CopyMeshesAlongLines(
                    list,
                    dest,
                    script.source,
                    script.source,
                    script.source,
                    Axis.Z,
                    0.0f,
                    0.0f,
                    0.0f,
                    script.transform.up,
                    script.transform.up,
                    Vector2.zero,
                    Vector2.zero,
                    Quaternion.Euler(script.rotation),
                    script.scale,
                    Mathf.Max(1, script.maxMeshLoopCount),
                    true,
                    script.raycastInfo.useRaycast ? script.raycastInfo : null,
                    script.transform,
                    script.close
                    );

            }

            // set
            {

                MeshFilter mf = script.GetComponent<MeshFilter>();

                Mesh sharedMesh = mf.sharedMesh;

                string sharedMeshFilePath = AssetDatabase.GetAssetPath(sharedMesh);

                if (string.IsNullOrEmpty(sharedMeshFilePath))
                {
                    DestroyImmediate(sharedMesh);
                }

                mf.sharedMesh = dest;

            }

            //
            {
                Debug.Log("Finished");
            }

        }

    }

}
