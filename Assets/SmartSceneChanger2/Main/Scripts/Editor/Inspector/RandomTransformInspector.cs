using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for RandomTransform
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(RandomTransform))]
    public class RandomTransformInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => "Randomise Children";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------
            
            //
            {

                RandomTransform script = null;

                foreach (var val in this.targets)
                {

                    script = val as RandomTransform;

                    // InitState
                    {
                        UnityEngine.Random.InitState(script.seed);
                    }

                    //
                    {

                        Transform child = null;

                        Vector3 temp = Vector3.zero;
                        Vector3 tempEuler = Vector3.zero;

                        for (int i = 0; i < script.transform.childCount; i++)
                        {

                            child = script.transform.GetChild(i);

                            Undo.RecordObject(child, "");

                            // pos
                            {

                                temp = script.positionRange.GetRandomValue();

                                if (script.positionRange.applyTargets != RandomTransform.ApplyTargets.None)
                                {

                                    if (!script.positionRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.X))
                                    {
                                        temp.x = child.localPosition.x;
                                    }

                                    if (!script.positionRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Y))
                                    {
                                        temp.y = child.localPosition.y;
                                    }

                                    if (!script.positionRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Z))
                                    {
                                        temp.z = child.localPosition.z;
                                    }

                                    child.localPosition = temp;

                                }

                            }

                            // rot
                            {

                                temp = script.rotationRange.GetRandomValue();

                                if (script.rotationRange.applyTargets != RandomTransform.ApplyTargets.None)
                                {

                                    tempEuler = child.localRotation.eulerAngles;

                                    if (!script.rotationRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.X))
                                    {
                                        temp.x = tempEuler.x;
                                    }

                                    if (!script.rotationRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Y))
                                    {
                                        temp.y = tempEuler.y;
                                    }

                                    if (!script.rotationRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Z))
                                    {
                                        temp.z = tempEuler.z;
                                    }

                                    child.localRotation = Quaternion.Euler(temp);

                                }

                            }

                            // scale
                            {

                                temp = script.scaleRange.GetRandomValue();

                                if (script.scaleRange.applyTargets != RandomTransform.ApplyTargets.None)
                                {

                                    if (script.scaleRange.unitScale)
                                    {
                                        child.localScale = Vector3.one * temp.x;
                                    }

                                    else
                                    {

                                        if (!script.scaleRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.X))
                                        {
                                            temp.x = child.localScale.x;
                                        }

                                        if (!script.scaleRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Y))
                                        {
                                            temp.y = child.localScale.y;
                                        }

                                        if (!script.scaleRange.applyTargets.HasFlag(RandomTransform.ApplyTargets.Z))
                                        {
                                            temp.z = child.localScale.z;
                                        }

                                        child.localScale = temp;

                                    }

                                }

                            }

                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Randomise");
            }

            //
            {
                Debug.Log("Finished");
            }

        }


    }

}
