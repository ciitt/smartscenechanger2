using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Custom Inspector with one button
    /// </summary>
    public abstract class SimpleButtonInspector : Editor
    {

        /// <summary>
        /// Button label
        /// </summary>
        protected abstract string buttonLabel { get; }

        /// <summary>
        /// Button function
        /// </summary>
        protected abstract void ButtonFunction();

        // ----------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            // DrawDefaultInspector
            {
                DrawDefaultInspector();
            }

            // Space
            {
                GUILayout.Space(20.0f);
            }

            // Button
            {

                if (GUILayout.Button(this.buttonLabel, GUILayout.Height(30.0f)))
                {
                    this.ButtonFunction();
                }

            }

        }

    }

}
