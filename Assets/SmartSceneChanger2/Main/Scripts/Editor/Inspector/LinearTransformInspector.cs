using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Inspector for LinearTransform
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(LinearTransform))]
    public class LinearTransformInspector : SimpleButtonInspector
    {

        protected override string buttonLabel => " Transform Children";

        // ----------------------------------------------------------------------------------
        protected override void ButtonFunction()
        {

            // ---------------------------

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------
            
            //
            {

                LinearTransform script = null;

                foreach (var val in this.targets)
                {

                    script = val as LinearTransform;

                    // InitState
                    {
                        UnityEngine.Random.InitState(script.seed);
                    }

                    //
                    {

                        Transform child = null;

                        Vector3 temp = Vector3.zero;
                        Vector3 tempAdd = Vector3.zero;
                        Vector3 tempEuler = Vector3.zero;

                        float lerpVal = 0.0f;

                        int childCount = script.transform.childCount;

                        for (int i = 0; i < childCount; i++)
                        {

                            child = script.transform.GetChild(i);

                            Undo.RecordObject(child, "");

                            lerpVal = (childCount >= 2) ? i / (float)(childCount - 1) : 0; // bad performance

                            // pos
                            {

                                temp = Vector3.Lerp(script.positionRange.min, script.positionRange.max, lerpVal);
                                tempAdd = script.additionalPositionRange.GetRandomValue();

                                temp += tempAdd;

                                if (script.positionRange.applyTargets != LinearTransform.ApplyTargets.None)
                                {

                                    if (!script.positionRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.X))
                                    {
                                        temp.x = child.localPosition.x;
                                    }

                                    if (!script.positionRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Y))
                                    {
                                        temp.y = child.localPosition.y;
                                    }

                                    if (!script.positionRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Z))
                                    {
                                        temp.z = child.localPosition.z;
                                    }

                                    child.localPosition = temp;

                                }

                            }

                            // rot
                            {

                                temp = Vector3.Lerp(script.rotationRange.min, script.rotationRange.max, lerpVal);
                                tempAdd = script.additionalRotationRange.GetRandomValue();

                                temp += tempAdd;

                                if (script.rotationRange.applyTargets != LinearTransform.ApplyTargets.None)
                                {

                                    tempEuler = child.localRotation.eulerAngles;

                                    if (!script.rotationRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.X))
                                    {
                                        temp.x = tempEuler.x;
                                    }

                                    if (!script.rotationRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Y))
                                    {
                                        temp.y = tempEuler.y;
                                    }

                                    if (!script.rotationRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Z))
                                    {
                                        temp.z = tempEuler.z;
                                    }

                                    child.localRotation = Quaternion.Euler(temp);

                                }

                            }

                            // scale
                            {

                                temp = Vector3.Lerp(script.scaleRange.min, script.scaleRange.max, lerpVal);
                                tempAdd = script.additionalScaleRange.GetRandomValue();

                                temp += tempAdd;

                                if (script.scaleRange.applyTargets != LinearTransform.ApplyTargets.None)
                                {

                                    if (script.scaleRange.unitScale)
                                    {
                                        child.localScale = Vector3.one * temp.x;
                                    }

                                    else
                                    {

                                        if (!script.scaleRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.X))
                                        {
                                            temp.x = child.localScale.x;
                                        }

                                        if (!script.scaleRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Y))
                                        {
                                            temp.y = child.localScale.y;
                                        }

                                        if (!script.scaleRange.applyTargets.HasFlag(LinearTransform.ApplyTargets.Z))
                                        {
                                            temp.z = child.localScale.z;
                                        }

                                        child.localScale = temp;

                                    }

                                }

                            }

                        }

                    }

                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Randomise");
            }

            //
            {
                Debug.Log("Finished");
            }

        }


    }

}
