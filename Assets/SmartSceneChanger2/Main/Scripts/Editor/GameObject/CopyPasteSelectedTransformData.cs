using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Copy and paste selected transform data
    /// </summary>
    public class CopyPasteSelectedTransformData
    {

        /// <summary>
        /// Transform info
        /// </summary>
        class TransformInfo
        {

            public Vector3 position = Vector3.zero;
            public Quaternion rotation = Quaternion.identity;
            public Vector3 scale = Vector3.one;

            public void Set(Transform transform)
            {

                if (transform)
                {
                    this.position = transform.position;
                    this.rotation = transform.rotation;
                    this.scale = transform.localScale;
                }

            }

            public void Apply(Transform transform)
            {

                if (transform)
                {
                    transform.position = this.position;
                    transform.rotation = this.rotation;
                    transform.localScale = this.scale;
                }

            }

        }

        /// <summary>
        /// TransformInfo list
        /// </summary>
        static List<TransformInfo> list = new List<TransformInfo>();

        [MenuItem("GameObject/SSC2/Copy Selected Transform Data")]
        static void CopySelectedTransformData()
        {

            Transform[] transforms = Selection.transforms;

            Funcs.RecycleList(list, transforms.Length);

            for (int i = 0; i < transforms.Length; i++)
            {
                list[i].Set(transforms[i]);
            }

        }

        [MenuItem("GameObject/SSC2/Copy Selected Transform Data", validate = true)]
        static bool CopySelectedTransformDataValidate()
        {

            return Selection.count >= 1;

        }

        [MenuItem("GameObject/SSC2/Paste Selected Transform Data")]
        static void PasteSelectedTransformData()
        {

            int groupID = Undo.GetCurrentGroup();

            // ---------------------------

            //
            {

                Transform[] transforms = Selection.transforms;

                int size = Mathf.Min(transforms.Length, list.Count);

                for (int i = 0; i < size; i++)
                {
                    Undo.RecordObject(transforms[i], "");
                    list[i].Apply(transforms[i]);
                }

            }

            //
            {
                Undo.CollapseUndoOperations(groupID);
                Undo.SetCurrentGroupName("Paste Selected Transform Data");
            }

        }

        [MenuItem("GameObject/SSC2/Paste Selected Transform Data", validate = true)]
        static bool PasteSelectedTransformDataValidate()
        {

            return
                Selection.count >= 1 &&
                Selection.transforms.Length == list.Count
                ;

        }

    }

}
