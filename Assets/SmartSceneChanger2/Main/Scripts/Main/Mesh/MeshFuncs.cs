﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static functions for mesh
    /// </summary>
    public static partial class MeshFuncs
    {

        /// <summary>
        /// Data lists for Mesh
        /// </summary>
        public class MeshDataLists
        {

            public List<Vector3> vertices = new List<Vector3>();

            public List<Vector4> uv = new List<Vector4>();
            public List<Vector4> uv2 = new List<Vector4>();
            public List<Vector4> uv3 = new List<Vector4>();
            public List<Vector4> uv4 = new List<Vector4>();

            public List<int> triangles = new List<int>();

            public List<Vector3> normals = new List<Vector3>();

            public List<Color32> color32 = new List<Color32>();

            /// <summary>
            /// Clear
            /// </summary>
            public MeshDataLists Clear()
            {

                this.vertices.Clear();
                this.uv.Clear();
                this.uv2.Clear();
                this.uv3.Clear();
                this.uv4.Clear();
                this.triangles.Clear();
                this.normals.Clear();
                this.color32.Clear();

                return this;

            }

            /// <summary>
            /// Clear
            /// </summary>
            public MeshDataLists Clear(
                int vertexCount,
                int triangleCount
                )
            {

                // clear
                {
                    this.Clear();
                }

                // capacity
                {

                    this.vertices.Capacity = vertexCount;
                    this.uv.Capacity = vertexCount;
                    this.uv2.Capacity = vertexCount;
                    this.uv3.Capacity = vertexCount;
                    this.uv4.Capacity = vertexCount;
                    this.normals.Capacity = vertexCount;
                    this.color32.Capacity = vertexCount;

                    this.triangles.Capacity = triangleCount;

                }

                // fill
                {

                    Color32 color = new Color32(0, 0, 0, 0);

                    for (int i = 0; i < vertexCount; i++)
                    {
                        this.vertices.Add(Vector3.zero);
                        this.uv.Add(Vector4.zero);
                        this.uv2.Add(Vector4.zero);
                        this.uv3.Add(Vector4.zero);
                        this.uv4.Add(Vector4.zero);
                        this.normals.Add(Vector3.zero);
                        this.color32.Add(color);
                    }

                    for (int i = 0; i < triangleCount; i++)
                    {
                        this.triangles.Add(0);
                    }

                }

                return this;

            }

            /// <summary>
            /// Copy values from source
            /// </summary>
            /// <param name="source">source</param>
            /// <param name="subMeshIndex">sub mesh index (if negative, all)</param>
            public MeshDataLists ClearAndSet(
                Mesh source,
                int subMeshIndex = -1
                )
            {

                // clear
                {
                    _ = this.Clear();
                }

                // set
                {

                    if (
                        source &&
                        CheckMeshReadable(source)
                        )
                    {

                        source.GetVertices(this.vertices);
                        source.GetUVs(0, this.uv);
                        source.GetUVs(1, this.uv2);
                        source.GetUVs(2, this.uv3);
                        source.GetUVs(3, this.uv4);

                        if (subMeshIndex < 0)
                        {
                            this.triangles.AddRange(source.triangles);
                        }

                        else
                        {
                            this.triangles.AddRange(source.GetTriangles(subMeshIndex));
                        }

                        source.GetNormals(this.normals);
                        source.GetColors(this.color32);

                    }

                }

                return this;

            }

            /// <summary>
            /// Apply
            /// </summary>
            /// <param name="dest">destination</param>
            /// <param name="markNoLongerReadable">markNoLongerReadable</param>
            public void Apply(
                Mesh dest,
                bool markNoLongerReadable
                )
            {

                if (dest)
                {

                    bool validNormal = this.normals.Count > 0;
                    bool validColor = this.color32.Count > 0;
                    bool validUv = this.uv.Count > 0;
                    bool validUv2 = this.uv2.Count > 0;
                    bool validUv3 = this.uv3.Count > 0;
                    bool validUv4 = this.uv4.Count > 0;

                    dest.Clear();

                    dest.SetVertices(this.vertices);

                    if (validUv) dest.SetUVs(0, this.uv);
                    if (validUv2) dest.SetUVs(1, this.uv2);
                    if (validUv3) dest.SetUVs(2, this.uv3);
                    if (validUv4) dest.SetUVs(3, this.uv4);

                    if (validNormal) dest.SetTriangles(this.triangles, 0);

                    if (validColor) dest.SetColors(this.color32);

                    dest.RecalculateBounds();
                    dest.RecalculateNormals();
                    dest.RecalculateTangents();

                    dest.UploadMeshData(markNoLongerReadable);

                }

            }

        }

        /// <summary>
        /// List for mesh list
        /// </summary>
        public class MeshListList
        {

            /// <summary>
            /// List
            /// </summary>
            public List<List<Mesh>> list = new List<List<Mesh>>();

            /// <summary>
            /// Clear
            /// </summary>
            /// <param name="destroy">destroy meshes</param>
            public void Clear(bool destroy)
            {

                foreach (var val in this.list)
                {

                    if (destroy)
                    {

                        foreach (var mesh in val)
                        {
                            Funcs.DestroyObject(mesh);
                        }

                    }

                    val.Clear();

                }

            }

            /// <summary>
            /// Clear and set min list count
            /// </summary>
            /// <param name="minCount">min list count</param>
            /// <param name="destroy">destroy meshes</param>
            public void ClearAndSetMinListCount(
                int minCount,
                bool destroy
                )
            {

                // clear
                {
                    this.Clear(destroy);
                }

                // add
                {

                    int add = minCount - this.list.Count;

                    for (int i = 0; i < add; i++)
                    {
                        this.list.Add(new List<Mesh>());
                    }

                }

            }

            /// <summary>
            /// Combine
            /// </summary>
            /// <param name="listIndex">list index</param>
            /// <param name="recalculateNormals">recalculateNormals</param>
            /// <param name="markNoLogerReadable">markNoLogerReadable</param>
            /// <param name="recalculateTangents">recalculateTangents</param>
            /// <returns>Mesh</returns>
            public Mesh Combine(
                int listIndex,
                bool recalculateNormals = true,
                bool markNoLogerReadable = false,
                bool recalculateTangents = true
                )
            {

                Mesh ret = null;

                if (listIndex < this.list.Count)
                {

                    ret = new Mesh();

                    ret.name = $"Mesh {listIndex}";

                    CombineMeshes(
                        ret,
                        this.list[listIndex],
                        recalculateNormals,
                        markNoLogerReadable,
                        recalculateTangents,
                        false
                        );

                }

                return ret;

            }

        }

        /// <summary>
        /// MeshDataLists
        /// </summary>
        static MeshDataLists _meshDataLists = null;

        /// <summary>
        /// MeshDataLists 2
        /// </summary>
        static MeshDataLists _meshDataLists2 = null;

        /// <summary>
        /// Index map
        /// </summary>
        static Dictionary<int, int> _indexMap = null;

        /// <summary>
        /// MeshListList
        /// </summary>
        static MeshListList _meshListList = null;

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// MeshDataLists
        /// </summary>
        public static MeshDataLists meshDataListsInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _meshDataLists); }
        }

        /// <summary>
        /// MeshDataLists 2
        /// </summary>
        public static MeshDataLists meshDataListsInstance2
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _meshDataLists2); }
        }

        /// <summary>
        /// Index map
        /// </summary>
        public static Dictionary<int, int> indexMapInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _indexMap); }
        }

        /// <summary>
        /// MeshListList
        /// </summary>
        public static MeshListList meshListListInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _meshListList); }
        }

        /// <summary>
        /// Remove data for collider
        /// </summary>
        /// <param name="sourceAndDest">aource and destination</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        // -------------------------------------------------------------------------------------------
        public static void RemoveMeshDataForCollider(
            Mesh sourceAndDest,
            bool markNoLogerReadable = false
            )
        {

            if (sourceAndDest)
            {

                sourceAndDest.uv = null;
                sourceAndDest.uv2 = null;
                sourceAndDest.uv3 = null;
                sourceAndDest.uv4 = null;
                sourceAndDest.uv5 = null;
                sourceAndDest.uv6 = null;
                sourceAndDest.uv7 = null;
                sourceAndDest.uv8 = null;

                sourceAndDest.normals = null;
                sourceAndDest.colors32 = null;

                sourceAndDest.UploadMeshData(markNoLogerReadable);

            }

        }

        /// <summary>
        /// Remove data for collider
        /// </summary>
        /// <param name="sourceAndDest">aource and destination</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshValues(
            Mesh source,
            Mesh dest,
            bool markNoLogerReadable = false
            )
        {

            if (
                source &&
                dest
                )
            {

                dest.uv = source.uv;
                dest.uv2 = source.uv2;
                dest.uv3 = source.uv3;
                dest.uv4 = source.uv4;
                dest.uv5 = source.uv5;
                dest.uv6 = source.uv6;
                dest.uv7 = source.uv7;
                dest.uv8 = source.uv8;

                dest.triangles = source.triangles;

                dest.normals = source.normals;
                dest.tangents = source.tangents;
                dest.colors32 = source.colors32;

                dest.RecalculateBounds();

                dest.UploadMeshData(markNoLogerReadable);

            }

        }

        /// <summary>
        /// Combine meshes
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="meshChunkList">mesh chunk list</param>
        /// <param name="recalculateNormals">use RecalculateNormals</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        /// <param name="recalculateTangents">use RecalculateTangents</param>
        /// <param name="listAsSubMesh">use list as submesh</param>
        // -------------------------------------------------------------------------------------------
        public static void CombineMeshes(
            Mesh dest,
            IList<Mesh> meshChunkList,
            bool recalculateNormals = true,
            bool markNoLogerReadable = false,
            bool recalculateTangents = true,
            bool listAsSubMesh = false
            )
        {

            int size = meshChunkList.Count;

            // ------------------

            // check
            {

                if (!dest)
                {
                    return;
                }

                if (size <= 0)
                {
                    dest.Clear();
                    return;
                }

                if (!CheckMeshReadable(dest))
                {
                    return;
                }

            }

            // ------------------

            int validSize = 0;

            bool isValid(Mesh mesh)
            {
                return
                    CheckMeshReadable(mesh) &&
                    mesh.vertexCount >= 3
                    ;
            }

            // ------------------

            // validSize
            {

                Mesh mesh = null;

                for (int i = 0; i < size; i++)
                {

                    mesh = meshChunkList[i];

                    if (isValid(mesh))
                    {
                        validSize += mesh.subMeshCount;
                    }

                }

                // ------------------

                if (validSize <= 0)
                {
                    dest.Clear();
                    return;
                }

            }

            // ------------------

            CombineInstance[] combine = new CombineInstance[validSize];

            // ------------------

            //
            {

                int index = 0;

                int i = 0;
                int j = 0;

                Mesh mesh = null;

                for (i = 0; i < size; i++)
                {

                    if (!isValid(meshChunkList[i]))
                    {
                        continue;
                    }

                    // -------------

                    mesh = meshChunkList[i];

                    // -------------

                    // set
                    {

                        for (j = 0; j < mesh.subMeshCount; j++)
                        {

                            combine[index].mesh = mesh;
                            combine[index].transform = Matrix4x4.identity;
                            combine[index].subMeshIndex = j;
                            index++;

                        }

                    }

                }

            }

            // CombineMeshes
            {

                dest.Clear();
                dest.CombineMeshes(combine, !listAsSubMesh);

                dest.RecalculateBounds();

                if (recalculateNormals)
                {
                    dest.RecalculateNormals();
                }

                if (recalculateTangents)
                {
                    dest.RecalculateTangents();
                }

            }

            // submesh
            {

                //if (
                //    listAsSubMesh &&
                //    validSize >= 2
                //    )
                //{

                //    // subMeshCount
                //    {
                //        dest.subMeshCount = validSize;
                //    }

                //    // SetSubMesh
                //    {

                //        int index = 0;

                //        int currentTotalTrianglesCount = 0;
                //        int trianglesCount = 0;

                //        for (int i = 0; i < size; i++)
                //        {

                //            if (!isValid(meshChunkList[i]))
                //            {
                //                continue;
                //            }

                //            trianglesCount = meshChunkList[i].triangles.Length; // TODO? triangles.Length
                //            dest.SetSubMesh(index++, new UnityEngine.Rendering.SubMeshDescriptor(currentTotalTrianglesCount, trianglesCount));
                //            currentTotalTrianglesCount += trianglesCount;

                //        }

                //    }

                //}

            }

            // UploadMeshData
            {
                dest.UploadMeshData(markNoLogerReadable);
            }

        }

        /// <summary>
        /// Combine meshes with keeping sub meshes
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="meshChunkList">mesh chunk list</param>
        /// <param name="recalculateNormals">use RecalculateNormals</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        /// <param name="recalculateTangents">use RecalculateTangents</param>
        /// <param name="listAsSubMesh">use list as submesh</param>
        // -------------------------------------------------------------------------------------------
        public static void CombineBasedOnSubMeshes(
            Mesh dest,
            IList<Mesh> meshChunkList,
            bool recalculateNormals = true,
            bool markNoLogerReadable = false,
            bool recalculateTangents = true
            )
        {

            int size = meshChunkList.Count;

            // ------------------

            // check
            {

                if (!dest)
                {
                    return;
                }

                if (size <= 0)
                {
                    dest.Clear();
                    return;
                }

                if (!CheckMeshReadable(dest))
                {
                    return;
                }

            }

            // ------------------

            bool isValid(Mesh mesh)
            {
                return
                    CheckMeshReadable(mesh) &&
                    mesh.vertexCount >= 3
                    ;
            }

            // ------------------

            MeshListList meshListList = meshListListInstance;

            int subMeshMax = 0;

            int i = 0;

            // ------------------

            // subMeshMax
            {

                foreach (var val in meshChunkList)
                {
                    subMeshMax = Mathf.Max(subMeshMax, val.subMeshCount);
                }

            }

            // init
            {
                meshListList.ClearAndSetMinListCount(subMeshMax, true);
            }

            // meshListList
            {

                List<Mesh> list = null;

                Matrix4x4 matrix4x4 = Matrix4x4.identity;

                for (i = 0; i < subMeshMax; i++)
                {

                    list = meshListList.list[i];

                    foreach (var mesh in meshChunkList)
                    {

                        if (
                            isValid(mesh) &&
                            i < mesh.subMeshCount
                            )
                        {
                            list.Add(ExtractAndCreateSubMesh(mesh, i, false, matrix4x4));
                        }

                    }

                }

            }

            // combine
            {

                CombineInstance[] combine = new CombineInstance[subMeshMax];

                // combine
                {

                    for (i = 0; i < subMeshMax; i++)
                    {

                        combine[i].mesh = meshListList.Combine(i, true, false, true);
                        combine[i].transform = Matrix4x4.identity;

                    }

                }

                // CombineMeshes
                {

                    dest.Clear();
                    dest.CombineMeshes(combine, false);

                    dest.RecalculateBounds();

                    if (recalculateNormals)
                    {
                        dest.RecalculateNormals();
                    }

                    if (recalculateTangents)
                    {
                        dest.RecalculateTangents();
                    }

                    dest.UploadMeshData(markNoLogerReadable);

                }

                // destroy
                {

                    foreach (var val in combine)
                    {
                        Funcs.DestroyObject(val.mesh);
                    }

                }

            }

            // clear
            {
                meshListList.Clear(true);
            }

        }

        /// <summary>
        /// Rotate mesh
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="rotation">rotation</param>
        // -------------------------------------------------------------------------------------------
        public static void RotateMesh(
            Mesh sourceAndDest,
            Quaternion rotation
            )
        {

            if (CheckMeshReadable(sourceAndDest))
            {

                MeshDataLists meshDataLists = meshDataListsInstance.Clear();

                // ---------------------

                List<Vector3> vertices = meshDataLists.vertices;

                // ---------------------

                // vertices
                {
                    sourceAndDest.GetVertices(vertices);
                }

                // rotate
                {

                    for (int i = 0; i < vertices.Count; i++)
                    {
                        vertices[i] = rotation * vertices[i];
                    }

                    sourceAndDest.SetVertices(vertices);

                    sourceAndDest.RecalculateBounds();

                }

                // clear
                {
                    vertices.Clear();
                }

            }

        }

        /// <summary>
        /// Deform the mesh
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="sourceVertices">source vertices</param>
        /// <param name="sourceBounds">source bounds</param>
        /// <param name="posNNN">pos at NNN</param>
        /// <param name="posPNN">pos at PNN</param>
        /// <param name="posNNP">pos at NNP</param>
        /// <param name="posPNP">pos at PNP</param>
        /// <param name="posNPN">pos at NPN</param>
        /// <param name="posPPN">pos at PPN</param>
        /// <param name="posNPP">pos at NPP</param>
        /// <param name="posPPP">pos at PPP</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="markNoLongerReadable">flag for UploadMeshData</param>
        // -------------------------------------------------------------------------------------------
        public static void DeformMesh(
            Mesh source,
            Mesh dest,
            Vector3 posNNN,
            Vector3 posPNN,
            Vector3 posNNP,
            Vector3 posPNP,
            Vector3 posNPN,
            Vector3 posPPN,
            Vector3 posNPP,
            Vector3 posPPP,
            Transform baseTransform = null,
            bool markNoLongerReadable = false
            )
        {

            if (
                !source ||
                !dest
                )
            {
                return;
            }

            // ---------------------

            MeshDataLists meshDataLists = meshDataListsInstance.Clear();

            List<Vector3> vertices = meshDataLists.vertices;

            // ---------------------

            // vertices
            {
                source.GetVertices(vertices);
            }

            // ---------------------

            Vector3 updatePos(
                Vector3 pos,
                Bounds sourceBounds,
                ref float lerpValX,
                ref float lerpValY,
                ref float lerpValZ,
                ref Vector3 temp1,
                ref Vector3 temp2,
                ref Vector3 temp3,
                ref Vector3 temp4
                )
            {

                lerpValX = Mathf.InverseLerp(sourceBounds.min.x, sourceBounds.max.x, pos.x);
                lerpValY = Mathf.InverseLerp(sourceBounds.min.y, sourceBounds.max.y, pos.y);
                lerpValZ = Mathf.InverseLerp(sourceBounds.min.z, sourceBounds.max.z, pos.z);

                temp1 = Vector3.Lerp(posNNN, posPNN, lerpValX);
                temp2 = Vector3.Lerp(posNNP, posPNP, lerpValX);
                temp3 = Vector3.Lerp(posNPN, posPPN, lerpValX);
                temp4 = Vector3.Lerp(posNPP, posPPP, lerpValX);

                temp1 = Vector3.Lerp(temp1, temp2, lerpValZ);
                temp2 = Vector3.Lerp(temp3, temp4, lerpValZ);

                return Vector3.Lerp(temp1, temp2, lerpValY);

            }

            // ---------------------

            // vertices
            {
                source.GetVertices(vertices);
            }

            //
            {

                Bounds sourceBounds = source.bounds;

                Vector3 pos = Vector3.zero;
                Vector4 uv0 = Vector4.zero;

                float lerpValX = 0.0f;
                float lerpValY = 0.0f;
                float lerpValZ = 0.0f;

                Vector3 temp1 = Vector3.zero;
                Vector3 temp2 = Vector3.zero;
                Vector3 temp3 = Vector3.zero;
                Vector3 temp4 = Vector3.zero;

                for (int i = 0; i < vertices.Count; i++)
                {

                    pos = updatePos(
                        vertices[i],
                        sourceBounds,
                        ref lerpValX,
                        ref lerpValY,
                        ref lerpValZ,
                        ref temp1,
                        ref temp2,
                        ref temp3,
                        ref temp4
                        );

                    vertices[i] = (baseTransform) ? baseTransform.InverseTransformPoint(pos) : pos;

                }

            }

            // set
            {

                dest.Clear();

                dest.SetVertices(vertices);

                dest.uv = source.uv;
                dest.uv2 = source.uv2;
                dest.uv3 = source.uv3;
                dest.uv4 = source.uv4;
                dest.uv5 = source.uv5;
                dest.uv6 = source.uv6;
                dest.uv7 = source.uv7;
                dest.uv8 = source.uv8;
                dest.triangles = source.triangles;
                dest.colors32 = source.colors32;

                dest.RecalculateBounds();
                dest.RecalculateNormals();
                dest.RecalculateTangents();

                dest.UploadMeshData(markNoLongerReadable);

            }

            // clear
            {
                vertices.Clear();
            }

        }

        /// <summary>
        /// Clear and prepare meshes
        /// </summary>
        /// <param name="meshList">mesh list</param>
        /// <param name="minSize">min size for list</param>
        // -------------------------------------------------------------------------------------------
        public static void ClearAndPrepareMeshes(
            List<Mesh> meshList,
            int minSize
            )
        {

            int size = Mathf.Max(meshList.Count, minSize);

            Funcs.RecycleAssetList(meshList, size, true);

            foreach (var val in meshList)
            {
                val.Clear();
            }

        }

        /// <summary>
        /// Check if the mesh is readable
        /// </summary>
        /// <param name="mesh">mesh</param>
        /// <param name="printWarningMessage">true to print warning message if not readable</param>
        /// <returns>readable</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CheckMeshReadable(
            Mesh mesh,
            bool printWarningMessage = true
            )
        {

            bool ret = false;

            if (mesh)
            {

                if (mesh.isReadable)
                {
                    ret = true;
                }

                else
                {
                    if (printWarningMessage)
                    {
                        Debug.LogWarningFormat("The mesh is not readable : {0}", mesh.name);
                    }
                }

            }

            return ret;

        }

        /// <summary>
        /// Bend the mesh along the points
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="worldPos0">world pos 0</param>
        /// <param name="worldPos1">world pos 1</param>
        /// <param name="worldPos2">world pos 2</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool BendMeshXZ(
            Mesh source,
            Mesh dest,
            Vector2 worldPos0,
            Vector2 worldPos1,
            Vector2 worldPos2
            )
        {

            // check
            {

                if (
                    !CheckMeshReadable(source) ||
                    !CheckMeshReadable(dest)
                    )
                {
                    return false;
                }

            }

            // ---------------------------

            Vector2 dir0to1 = (worldPos1 - worldPos0).normalized;
            Vector2 dir1to2 = (worldPos2 - worldPos1).normalized;

            Vector2 worldRight0 = Quaternion.Euler(0, 0, -90) * dir0to1;
            Vector2 worldRight2 = Quaternion.Euler(0, 0, -90) * dir1to2;

            Vector3[] vertices = source.vertices;

            Bounds bounds = source.bounds;

            Vector3 offset = bounds.center;
            Vector3 boundshalfSize = bounds.size * 0.5f;

            void calcNormalizedPosInBounds(
                Vector3 _boundsMin,
                Vector3 _boundsMax,
                Vector3 _pos,
                ref Vector3 _dest
                )
            {
                _dest.x = Mathf.InverseLerp(_boundsMin.x, _boundsMax.x, _pos.x);
                _dest.y = Mathf.InverseLerp(_boundsMin.y, _boundsMax.y, _pos.y);
                _dest.z = Mathf.InverseLerp(_boundsMin.z, _boundsMax.z, _pos.z);
            }

            // ---------------------------

            Vector2 offset0 = worldRight0 * boundshalfSize.x;
            Vector2 offset2 = worldRight2 * boundshalfSize.x;

            Vector2 pos0L = worldPos0 - offset0;
            Vector2 pos0R = worldPos0 + offset0;
            Vector2 pos2L = worldPos2 - offset2;
            Vector2 pos2R = worldPos2 + offset2;

            Vector2 pos1L = Vector2.zero;
            Vector2 pos1R = Vector2.zero;

            // ---------------------------

            // pos1L, pos1R
            {

                bool successA = VectorFuncs.CalcIntersectionPoint2D(
                    pos0L,
                    pos0L + dir0to1,
                    pos2L,
                    pos2L - dir1to2,
                    ref pos1L
                    );

                bool successB = VectorFuncs.CalcIntersectionPoint2D(
                    pos0R,
                    pos0R + dir0to1,
                    pos2R,
                    pos2R - dir1to2,
                    ref pos1R
                    );

                if (
                    !successA ||
                    !successB
                    )
                {
                    return false;
                }

            }

            // vertices
            {

                Vector3 boundsMin = bounds.min;
                Vector3 boundsMax = bounds.max;

                Vector3 normalizedPos = Vector3.zero;

                Vector3 pos = Vector3.zero;

                Vector2 tempL = Vector2.zero;
                Vector2 tempR = Vector2.zero;

                for (int i = 0; i < vertices.Length; i++)
                {

                    pos = vertices[i];

                    // normalizedPos
                    {

                        calcNormalizedPosInBounds(
                            boundsMin,
                            boundsMax,
                            vertices[i],
                            ref normalizedPos
                            );

                    }

                    // pos.x, pos.z
                    {

                        if (normalizedPos.z <= 0.5f)
                        {

                            normalizedPos.z *= 2;

                            tempL = Vector2.Lerp(pos0L, pos1L, normalizedPos.z);
                            tempR = Vector2.Lerp(pos0R, pos1R, normalizedPos.z);

                            pos.x = Mathf.Lerp(tempL.x, tempR.x, normalizedPos.x);
                            pos.z = Mathf.Lerp(tempL.y, tempR.y, normalizedPos.x);

                        }

                        else
                        {

                            normalizedPos.z = (normalizedPos.z - 0.5f) * 2;

                            tempL = Vector2.Lerp(pos1L, pos2L, normalizedPos.z);
                            tempR = Vector2.Lerp(pos1R, pos2R, normalizedPos.z);

                            pos.x = Mathf.Lerp(tempL.x, tempR.x, normalizedPos.x);
                            pos.z = Mathf.Lerp(tempL.y, tempR.y, normalizedPos.x);

                        }

                    }

                    // vertices
                    {
                        vertices[i] = pos;
                    }

                }

            }

            // clear
            {
                dest.Clear();
            }

            // set
            {

                dest.vertices = vertices;

                dest.uv = source.uv;
                dest.uv2 = source.uv2;
                dest.uv3 = source.uv3;
                dest.uv4 = source.uv4;
                dest.uv5 = source.uv5;
                dest.uv6 = source.uv6;
                dest.uv7 = source.uv7;
                dest.uv8 = source.uv8;

                dest.triangles = source.triangles;

                dest.colors = source.colors;

                dest.RecalculateBounds();
                dest.RecalculateNormals();

            }

            return true;

        }

        /// <summary>
        /// Bend the mesh along the points
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="worldPos0">world pos 0</param>
        /// <param name="worldPos1">world pos 1</param>
        /// <param name="worldPos2">world pos 2</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool BendMesh3D(
            Mesh source,
            Mesh dest,
            Vector2 worldPos0,
            Vector2 worldPos1,
            Vector2 worldPos2
            )
        {

            // check
            {

                if (
                    !CheckMeshReadable(source) ||
                    !CheckMeshReadable(dest)
                    )
                {
                    return false;
                }

            }

            // ---------------------------

            Vector2 dir0to1 = (worldPos1 - worldPos0).normalized;
            Vector2 dir1to2 = (worldPos2 - worldPos1).normalized;

            Vector2 worldRight0 = Quaternion.Euler(0, 0, -90) * dir0to1;
            Vector2 worldRight2 = Quaternion.Euler(0, 0, -90) * dir1to2;

            Vector3[] vertices = source.vertices;

            Bounds bounds = source.bounds;

            Vector3 boundshalfSize = bounds.size * 0.5f;

            void calcNormalizedPosInBounds(
                Vector3 _boundsMin,
                Vector3 _boundsMax,
                Vector3 _pos,
                ref Vector3 _dest
                )
            {
                _dest.x = Mathf.InverseLerp(_boundsMin.x, _boundsMax.x, _pos.x);
                _dest.y = Mathf.InverseLerp(_boundsMin.y, _boundsMax.y, _pos.y);
                _dest.z = Mathf.InverseLerp(_boundsMin.z, _boundsMax.z, _pos.z);
            }

            // ---------------------------

            Vector2 offset0 = worldRight0 * boundshalfSize.x;
            Vector2 offset2 = worldRight2 * boundshalfSize.x;

            Vector2 pos0L = worldPos0 - offset0;
            Vector2 pos0R = worldPos0 + offset0;
            Vector2 pos2L = worldPos2 - offset2;
            Vector2 pos2R = worldPos2 + offset2;

            Vector2 pos1L = Vector2.zero;
            Vector2 pos1R = Vector2.zero;

            // ---------------------------

            // pos1L, pos1R
            {

                bool successA = VectorFuncs.CalcIntersectionPoint2D(
                    pos0L,
                    pos0L + dir0to1,
                    pos2L,
                    pos2L - dir1to2,
                    ref pos1L
                    );

                bool successB = VectorFuncs.CalcIntersectionPoint2D(
                    pos0R,
                    pos0R + dir0to1,
                    pos2R,
                    pos2R - dir1to2,
                    ref pos1R
                    );

                if (
                    !successA ||
                    !successB
                    )
                {
                    return false;
                }

            }

            // vertices
            {

                Vector3 boundsMin = bounds.min;
                Vector3 boundsMax = bounds.max;

                Vector3 normalizedPos = Vector3.zero;

                Vector3 pos = Vector3.zero;

                Vector2 tempL = Vector2.zero;
                Vector2 tempR = Vector2.zero;

                for (int i = 0; i < vertices.Length; i++)
                {

                    pos = vertices[i];

                    // normalizedPos
                    {

                        calcNormalizedPosInBounds(
                            boundsMin,
                            boundsMax,
                            vertices[i],
                            ref normalizedPos
                            );

                    }

                    // pos.x, pos.z
                    {

                        if (normalizedPos.z <= 0.5f)
                        {

                            normalizedPos.z *= 2;

                            tempL = Vector2.Lerp(pos0L, pos1L, normalizedPos.z);
                            tempR = Vector2.Lerp(pos0R, pos1R, normalizedPos.z);

                            pos.x = Mathf.Lerp(tempL.x, tempR.x, normalizedPos.x);
                            pos.z = Mathf.Lerp(tempL.y, tempR.y, normalizedPos.x);

                        }

                        else
                        {

                            normalizedPos.z = (normalizedPos.z - 0.5f) * 2;

                            tempL = Vector2.Lerp(pos1L, pos2L, normalizedPos.z);
                            tempR = Vector2.Lerp(pos1R, pos2R, normalizedPos.z);

                            pos.x = Mathf.Lerp(tempL.x, tempR.x, normalizedPos.x);
                            pos.z = Mathf.Lerp(tempL.y, tempR.y, normalizedPos.x);

                        }

                    }

                    // vertices
                    {
                        vertices[i] = pos;
                    }

                }

            }

            // clear
            {
                dest.Clear();
            }

            // set
            {

                dest.vertices = vertices;

                dest.uv = source.uv;
                dest.uv2 = source.uv2;
                dest.uv3 = source.uv3;
                dest.uv4 = source.uv4;
                dest.uv5 = source.uv5;
                dest.uv6 = source.uv6;
                dest.uv7 = source.uv7;
                dest.uv8 = source.uv8;

                dest.triangles = source.triangles;

                dest.colors = source.colors;

                dest.RecalculateBounds();
                dest.RecalculateNormals();

            }

            return true;

        }

        /// <summary>
        /// Flip triangles
        /// </summary>
        /// <param name="triangles">triangles</param>
        // -------------------------------------------------------------------------------------------
        public static void FlipTriangles(int[] triangles)
        {

            int temp = 0;
            int size = triangles.Length;

            for (int i = 0; i < size / 2; i++)
            {

                temp = triangles[i];

                triangles[i] = triangles[size - i - 1];
                triangles[size - i - 1] = temp;

            }

        }


        /// <summary>
        /// Copy meshes along axis
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy along</param>
        /// <param name="targetMeter">target meter to calculate the count of loop</param>
        /// <param name="targetMeterForLoopCount">space meter at loop</param>
        /// <param name="maxLoopCount">max loop count</param>
        /// <param name="mergeSubMeshes">mergeSubMeshes</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongAxis(
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            float targetMeterForLoopCount,
            float spaceMeterAtLoop,
            int maxLoopCount = 20,
            bool mergeSubMeshes = true
            )
        {

            CopyMeshesAlongAxis(
                dest,
                start,
                loop,
                end,
                axis,
                targetMeterForLoopCount,
                spaceMeterAtLoop,
                Quaternion.identity,
                Vector3.one,
                maxLoopCount,
                mergeSubMeshes
                );

        }

        /// <summary>
        /// Copy meshes along axis
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy along</param>
        /// <param name="targetMeter">target meter to calculate the count of loop</param>
        /// <param name="targetMeterForLoopCount">space meter at loop</param>
        /// <param name="maxLoopCount">max loop count</param>
        /// <param name="rotation">rotation for meshes</param>
        /// <param name="scale">scale</param>
        /// <param name="mergeSubMeshes">mergeSubMeshes</param>
        /// <param name="rotation">rotation for meshes</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongAxis(
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            float targetMeterForLoopCount,
            float spaceMeterAtLoop,
            Quaternion rotation,
            Vector3 scale,
            int maxLoopCount = 20,
            bool mergeSubMeshes = true
            )
        {

            // check
            {

                if (
                    !dest ||
                    !start ||
                    !loop ||
                    !end
                    )
                {
                    return;
                }

            }

            // --------------------------

            Bounds startBounds = start.bounds;
            Bounds loopBounds = loop.bounds;
            Bounds endBounds = end.bounds;

            Vector3 startSize = rotation * startBounds.size;
            Vector3 loopSize = rotation * loopBounds.size;
            Vector3 endSize = rotation * endBounds.size;

            startSize = new Vector3(Mathf.Abs(startSize.x), Mathf.Abs(startSize.y), Mathf.Abs(startSize.z));
            loopSize = new Vector3(Mathf.Abs(loopSize.x), Mathf.Abs(loopSize.y), Mathf.Abs(loopSize.z));
            endSize = new Vector3(Mathf.Abs(endSize.x), Mathf.Abs(endSize.y), Mathf.Abs(endSize.z));

            startSize.Scale(scale);
            loopSize.Scale(scale);
            endSize.Scale(scale);

            // --------------------------

            int loopCount = 0;

            float startAxisSize = VectorFuncs.GetValueByAxis(startSize, axis);
            float loopAxisSize = VectorFuncs.GetValueByAxis(loopSize, axis);
            float endAxisSize = VectorFuncs.GetValueByAxis(endSize, axis);

            // --------------------------

            // loopCount
            {

                float temp =
                    targetMeterForLoopCount -
                    startAxisSize -
                    endAxisSize -
                    spaceMeterAtLoop
                    ;

                if (temp > 0.0f)
                {
                    loopCount = Mathf.RoundToInt(temp / (loopAxisSize + spaceMeterAtLoop));
                }

                loopCount = Mathf.Clamp(loopCount, 0, maxLoopCount);

            }

            // CopyMeshesAlongAxis
            {

                CopyMeshesAlongAxis(
                    dest,
                    start,
                    loop,
                    end,
                    axis,
                    loopCount,
                    rotation,
                    scale,
                    spaceMeterAtLoop,
                    mergeSubMeshes
                    );

            }

        }

        /// <summary>
        /// Copy meshes along axis
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy along</param>
        /// <param name="loopCount">count of loop mesh</param>
        /// <param name="spaceMeterAtLoop">space meter at loop</param>
        /// <param name="mergeSubMeshes">mergeSubMeshes</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongAxis(
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            int loopCount,
            float spaceMeterAtLoop,
            bool mergeSubMeshes = true
            )
        {

            CopyMeshesAlongAxis(
                dest,
                start,
                loop,
                end,
                axis,
                loopCount,
                Quaternion.identity,
                Vector3.one,
                spaceMeterAtLoop,
                mergeSubMeshes
                );

        }

        /// <summary>
        /// Copy meshes along axis
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy along</param>
        /// <param name="loopCount">count of loop mesh</param>
        /// <param name="rotation">rotation for meshes</param>
        /// <param name="scale">scale</param>
        /// <param name="spaceMeterAtLoop">space meter at loop</param>
        /// <param name="mergeSubMeshes">mergeSubMeshes</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongAxis(
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            int loopCount,
            Quaternion rotation,
            Vector3 scale,
            float spaceMeterAtLoop,
            bool mergeSubMeshes = true
            )
        {

            // check
            {

                if (
                    !dest ||
                    !CheckMeshReadable(dest)
                    )
                {
                    return;
                }

            }

            // clear
            {
                dest.Clear();
            }

            // check
            {

                // TODO?
                // loop == null is ok if loopCount is 0

                if (
                    !CheckMeshReadable(start) ||
                    !CheckMeshReadable(loop) ||
                    !CheckMeshReadable(end)
                    )
                {
                    return;
                }

            }

            // --------------------------

            Bounds startBounds = start.bounds;
            Bounds loopBounds = loop.bounds;
            Bounds endBounds = end.bounds;

            Vector3 startCenter = startBounds.center;
            Vector3 loopCenter = loopBounds.center;
            Vector3 endCenter = endBounds.center;

            Vector3 startSize = rotation * startBounds.size;
            Vector3 loopSize = rotation * loopBounds.size;
            Vector3 endSize = rotation * endBounds.size;

            startSize = new Vector3(Mathf.Abs(startSize.x), Mathf.Abs(startSize.y), Mathf.Abs(startSize.z));
            loopSize = new Vector3(Mathf.Abs(loopSize.x), Mathf.Abs(loopSize.y), Mathf.Abs(loopSize.z));
            endSize = new Vector3(Mathf.Abs(endSize.x), Mathf.Abs(endSize.y), Mathf.Abs(endSize.z));

            startSize.Scale(scale);
            loopSize.Scale(scale);
            endSize.Scale(scale);

            float startAxisSize = VectorFuncs.GetValueByAxis(startSize, axis);
            float loopAxisSize = VectorFuncs.GetValueByAxis(loopSize, axis);
            float endAxisSize = VectorFuncs.GetValueByAxis(endSize, axis);

            Vector3 dir = Vector3.forward;

            float currentSpace = 0.0f;

            int i = 0;
            int j = 0;

            // --------------------------

            // dir
            {

                switch (axis)
                {
                    case Axis.X: dir = Vector3.right; break;
                    case Axis.Y: dir = Vector3.up; break;
                    default: dir = Vector3.forward; break;
                }

            }

            if (mergeSubMeshes)
            {

                int combineIndex = 0;

                CombineInstance[] combine = new CombineInstance[
                    start.subMeshCount +
                    (loop.subMeshCount * loopCount) +
                    end.subMeshCount
                    ];

                // start
                {

                    float halfSize = startAxisSize * 0.5f;

                    currentSpace += halfSize;

                    Matrix4x4 matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - startCenter, rotation, scale);

                    for (j = 0; j < start.subMeshCount; j++)
                    {
                        combine[combineIndex].mesh = start;
                        combine[combineIndex].transform = matrix4x4;
                        combine[combineIndex].subMeshIndex = j;
                        combineIndex++;
                    }

                    currentSpace += halfSize;

                }

                // loop
                {

                    float halfSize = loopAxisSize * 0.5f;

                    Matrix4x4 matrix4x4;

                    for (i = 0; i < loopCount; i++)
                    {

                        currentSpace += spaceMeterAtLoop + halfSize;

                        matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - loopCenter, rotation, scale);

                        for (j = 0; j < loop.subMeshCount; j++)
                        {
                            combine[combineIndex].mesh = loop;
                            combine[combineIndex].transform = matrix4x4;
                            combine[combineIndex].subMeshIndex = j;
                            combineIndex++;

                        }

                        currentSpace += halfSize;

                    }

                    if (loopCount > 0)
                    {
                        currentSpace += spaceMeterAtLoop;
                    }

                }

                // last
                {

                    currentSpace += endAxisSize * 0.5f;

                    Matrix4x4 matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - endCenter, rotation, scale);

                    for (j = 0; j < end.subMeshCount; j++)
                    {
                        combine[combineIndex].mesh = end;
                        combine[combineIndex].transform = matrix4x4;
                        combine[combineIndex].subMeshIndex = j;
                        combineIndex++;
                    }

                }

                // CombineMeshes
                {
                    dest.CombineMeshes(combine);
                }

            }

            else
            {

                MeshListList meshListList = meshListListInstance;

                int subMeshMax = Mathf.Max(start.subMeshCount, loop.subMeshCount, end.subMeshCount);

                // init
                {
                    meshListList.ClearAndSetMinListCount(subMeshMax, true);
                }

                // meshListList
                {

                    List<Mesh> list = null;

                    float halfSize = 0.0f;

                    Matrix4x4 matrix4x4;

                    for (i = 0; i < subMeshMax; i++)
                    {

                        currentSpace = 0.0f;

                        list = meshListList.list[i];

                        // start
                        {

                            halfSize = startAxisSize * 0.5f;

                            currentSpace += halfSize;

                            matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - startCenter, rotation, scale);

                            if (i < start.subMeshCount)
                            {
                                list.Add(ExtractAndCreateSubMesh(start, i, false, matrix4x4));
                            }

                            currentSpace += halfSize;

                        }

                        // loop
                        {

                            halfSize = loopAxisSize * 0.5f;

                            for (j = 0; j < loopCount; j++)
                            {

                                currentSpace += spaceMeterAtLoop + halfSize;

                                matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - loopCenter, rotation, scale);

                                if (i < loop.subMeshCount)
                                {
                                    list.Add(ExtractAndCreateSubMesh(loop, i, false, matrix4x4));
                                }

                                currentSpace += halfSize;

                            }

                            if (loopCount > 0)
                            {
                                currentSpace += spaceMeterAtLoop;
                            }

                        }

                        // end
                        {

                            currentSpace += endAxisSize * 0.5f;

                            matrix4x4 = Matrix4x4.TRS((dir * currentSpace) - endCenter, rotation, scale);

                            if (i < end.subMeshCount)
                            {
                                list.Add(ExtractAndCreateSubMesh(end, i, false, matrix4x4));
                            }

                        }

                    }

                }

                // combine
                {

                    CombineInstance[] combine = new CombineInstance[subMeshMax];

                    // combine
                    {

                        for (i = 0; i < subMeshMax; i++)
                        {

                            combine[i].mesh = meshListList.Combine(i, true, false, true);
                            combine[i].transform = Matrix4x4.identity;

                        }

                        dest.CombineMeshes(combine, false);

                    }

                    // destroy
                    {

                        foreach (var val in combine)
                        {
                            Funcs.DestroyObject(val.mesh);
                        }

                    }

                }

                // clear
                {
                    meshListList.Clear(true);
                }

            }

        }

        /// <summary>
        /// Extract and create a mesh from a sub mesh
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="subMeshIndex">sub mesh index</param>
        /// <param name="markNoLongerReadable">markNoLongerReadable</param>
        /// <param name="matrix4x4">modify</param>
        /// <returns>mesh</returns>
        // -------------------------------------------------------------------------------------------
        public static Mesh ExtractAndCreateSubMesh(
            Mesh source,
            int subMeshIndex,
            bool markNoLongerReadable,
            Matrix4x4 matrix4x4
            )
        {

            // check
            {

                if (
                    !source ||
                    !CheckMeshReadable(source) ||
                    source.subMeshCount <= subMeshIndex
                    )
                {
                    return null;
                }

            }

            // -----------------

            Mesh ret = null;

            MeshDataLists oldInfo = meshDataListsInstance.ClearAndSet(source, subMeshIndex);
            MeshDataLists newInfo = meshDataListsInstance2.Clear();

            Dictionary<int, int> map = indexMapInstance;

            int i = 0;

            // -----------------

            // clear
            {
                map.Clear();
            }

            //
            {

                int oldVertexIndex = 0;
                int newIndex = 0;

                bool validNormal = oldInfo.normals.Count > 0;
                bool validColor = oldInfo.color32.Count > 0;
                bool validUv = oldInfo.uv.Count > 0;
                bool validUv2 = oldInfo.uv2.Count > 0;
                bool validUv3 = oldInfo.uv3.Count > 0;
                bool validUv4 = oldInfo.uv4.Count > 0;

                for (i = 0; i < oldInfo.triangles.Count; i++)
                {

                    oldVertexIndex = oldInfo.triangles[i];

                    if (!map.ContainsKey(oldVertexIndex))
                    {

                        newInfo.vertices.Add(oldInfo.vertices[oldVertexIndex]);

                        if (validNormal) newInfo.normals.Add(oldInfo.normals[oldVertexIndex]);
                        if (validColor) newInfo.color32.Add(oldInfo.color32[oldVertexIndex]);
                        if (validUv) newInfo.uv.Add(oldInfo.uv[oldVertexIndex]);
                        if (validUv2) newInfo.uv.Add(oldInfo.uv2[oldVertexIndex]);
                        if (validUv3) newInfo.uv.Add(oldInfo.uv3[oldVertexIndex]);
                        if (validUv4) newInfo.uv.Add(oldInfo.uv4[oldVertexIndex]);

                        map.Add(oldVertexIndex, newIndex);

                        newIndex++;

                    }

                }

                // triangles
                {

                    for (i = 0; i < oldInfo.triangles.Count; i++)
                    {
                        newInfo.triangles.Add(map[oldInfo.triangles[i]]);
                    }

                }

                // modify vertices
                {

                    if (matrix4x4 != Matrix4x4.identity)
                    {

                        for (i = 0; i < newInfo.vertices.Count; i++)
                        {
                            newInfo.vertices[i] = matrix4x4.MultiplyPoint(newInfo.vertices[i]);
                        }

                    }

                }

                // apply
                {
                    ret = new Mesh();
                    ret.name = $"SubMesh {subMeshIndex}";
                    newInfo.Apply(ret, markNoLongerReadable);
                }

            }

            // clear
            {
                oldInfo.Clear();
                newInfo.Clear();
                map.Clear();
            }

            return ret;

        }

        /// <summary>
        /// Copy meshes along a line
        /// </summary>
        /// <param name="startPos">position at start</param>
        /// <param name="endPos">position at end</param>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy</param>
        /// <param name="spaceMeterAtStart">space meter at start</param>
        /// <param name="spaceMeterAtLoop">space meter at loop</param>
        /// <param name="spaceMeterAtEnd">space meter at end</param>
        /// <param name="startUpwards">up direction at start</param>
        /// <param name="endUpwards">up direction at end</param>
        /// <param name="startNormalRight">right direction at start</param>
        /// <param name="endNormalRight">right direction at end</param>
        /// <param name="startOffset">offset at start</param>
        /// <param name="endOffset">offset at end</param>
        /// <param name="rotation">rotation</param>
        /// <param name="scale">scale</param>
        /// <param name="maxLoopCount">max loop count</param>
        /// <param name="mergeSubMeshes">flag to merge sub meshes</param>
        /// <param name="raycastInfo">RaycastInfo</param>
        /// <param name="baseTransform">base transform</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongLine(
            Vector3 startPos,
            Vector3 endPos,
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            float spaceMeterAtStart,
            float spaceMeterAtLoop,
            float spaceMeterAtEnd,
            Vector3 startUpwards,
            Vector3 endUpwards,
            Vector3 startNormalRight,
            Vector3 endNormalRight,
            Vector2 startOffset,
            Vector2 endOffset,
            Quaternion rotation,
            Vector3 scale,
            int maxLoopCount = 20,
            bool mergeSubMeshes = true,
            RaycastInfo raycastInfo = null,
            Transform baseTransform = null
            )
        {

            // check
            {

                if (
                    !dest ||
                    !CheckMeshReadable(dest)
                    )
                {
                    return;
                }

            }

            // clear
            {
                dest.Clear();
            }

            // check
            {

                // TODO?
                // loop == null is ok if loopCount is 0

                if (
                    !CheckMeshReadable(start) ||
                    !CheckMeshReadable(loop) ||
                    !CheckMeshReadable(end)
                    )
                {
                    return;
                }

            }

            // clamp
            {

                spaceMeterAtStart = Mathf.Max(0.0f, spaceMeterAtStart);
                spaceMeterAtLoop = Mathf.Max(0.0f, spaceMeterAtLoop);
                spaceMeterAtEnd = Mathf.Max(0.0f, spaceMeterAtEnd);

            }

            // -----------------

            float distance = Vector3.Distance(startPos, endPos);

            // -----------------

            // CopyMeshesAlongAxis
            {

                CopyMeshesAlongAxis(
                    dest,
                    start,
                    loop,
                    end,
                    axis,
                    distance,
                    spaceMeterAtLoop,
                    rotation,
                    scale,
                    maxLoopCount,
                    mergeSubMeshes
                    );

            }

            //
            {

                Vector3 _startPos = startPos;
                Vector3 _endPos = endPos;

                if (distance > 0.0f)
                {
                    startPos = Vector3.Lerp(_startPos, _endPos, spaceMeterAtStart / distance);
                    endPos = Vector3.Lerp(_endPos, _startPos, spaceMeterAtEnd / distance);
                }

            }

            // --------------------

            Vector3[] vertices = dest.vertices;

            // --------------------

            // vertices
            {

                Bounds bounds = dest.bounds;

                Vector3 boundsMin = bounds.min;
                Vector3 boundsMax = bounds.max;

                void func(
                    Vector3 vertex,
                    ref float _lerpValForward,
                    ref float _rightVal,
                    ref float _upVal
                    )
                {

                    if (axis == Axis.X)
                    {
                        _lerpValForward = Mathf.InverseLerp(boundsMin.x, boundsMax.x, vertex.x);
                        _rightVal = -vertex.z;
                        _upVal = vertex.y;
                    }

                    else if (axis == Axis.Y)
                    {
                        _lerpValForward = Mathf.InverseLerp(boundsMin.y, boundsMax.y, vertex.y);
                        _rightVal = vertex.x;
                        _upVal = -vertex.z;
                    }

                    else
                    {
                        _lerpValForward = Mathf.InverseLerp(boundsMin.z, boundsMax.z, vertex.z);
                        _rightVal = vertex.x;
                        _upVal = vertex.y;
                    }

                }

                Vector3 pos = Vector3.zero;
                Vector3 upDir = Vector3.up;
                Vector3 rightDir = Vector3.up;

                float lerpValForward = 0.0f;
                float rightVal = 0.0f;
                float upVal = 0.0f;

                Vector2 offset = Vector2.zero;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                for (int i = 0; i < vertices.Length; i++)
                {

                    // func
                    {
                        func(vertices[i], ref lerpValForward, ref rightVal, ref upVal);
                    }

                    // vertices
                    {

                        upDir = Vector3.Lerp(startUpwards, endUpwards, lerpValForward).normalized;
                        //rightDir = Vector3.Lerp(startNormalRight, endNormalRight, lerpValForward).normalized;
                        rightDir = Vector3.Lerp(startNormalRight, endNormalRight, lerpValForward);

                        pos = Vector3.Lerp(startPos, endPos, lerpValForward);

                        if (
                            raycastInfo != null &&
                            raycastInfo.raycastMaxDistance > 0.0f
                            )
                        {

                            ray.origin = pos + (upDir * raycastInfo.raycastMaxDistance);
                            ray.direction = -upDir;

                            if (Physics.Raycast(ray, out hit, raycastInfo.raycastMaxDistance * 2, raycastInfo.targetLayers))
                            {
                                pos = hit.point + (upDir * raycastInfo.offsetFromHitPosition);
                            }

                        }

                        offset = Vector2.Lerp(startOffset, endOffset, lerpValForward);

                        pos +=
                            (rightDir * (rightVal + offset.x)) +
                            (upDir * (upVal + offset.y))
                            ;

                        vertices[i] = (baseTransform) ? baseTransform.InverseTransformPoint(pos) : pos;

                    }

                }

            }

            // vertices
            {
                dest.vertices = vertices;
            }

        }

        /// <summary>
        /// Copy meshes along positions
        /// </summary>
        /// <param name="linePositionList">position list</param>
        /// <param name="dest">destination</param>
        /// <param name="start">mesh at start</param>
        /// <param name="loop">mesh at loop</param>
        /// <param name="end">mesh at end</param>
        /// <param name="axis">axis to copy</param>
        /// <param name="spaceMeterAtStart">space meter at start</param>
        /// <param name="spaceMeterAtLoop">space meter at loop</param>
        /// <param name="spaceMeterAtEnd">space meter at end</param>
        /// <param name="startUpwards">up direction at start</param>
        /// <param name="endUpwards">up direction at end</param>
        /// <param name="startOffset">offset at start</param>
        /// <param name="endOffset">offset at end</param>
        /// <param name="rotation">rotation</param>
        /// <param name="scale">scale</param>
        /// <param name="maxLoopCount">max loop count</param>
        /// <param name="mergeSubMeshes">flag to merge sub meshes</param>
        /// <param name="raycastInfo">RaycastInfo</param>
        /// <param name="baseTransform">base transform</param>
        /// <param name="close">flag to close</param>
        // -------------------------------------------------------------------------------------------
        public static void CopyMeshesAlongLines(
            IList<Vector3> linePositionList,
            Mesh dest,
            Mesh start,
            Mesh loop,
            Mesh end,
            Axis axis,
            float spaceMeterAtStart,
            float spaceMeterAtLoop,
            float spaceMeterAtEnd,
            Vector3 startUpwards,
            Vector3 endUpwards,
            Vector2 startOffset,
            Vector2 endOffset,
            Quaternion rotation,
            Vector3 scale,
            int maxLoopCount = 20,
            bool mergeSubMeshes = true,
            RaycastInfo raycastInfo = null,
            Transform baseTransform = null,
            bool close = false
            )
        {

            // check
            {

                if (
                    !dest ||
                    !CheckMeshReadable(dest)
                    )
                {
                    return;
                }

            }

            // clear
            {
                dest.Clear();
            }

            // -----------------

            int posCount = linePositionList.Count;

            // -----------------

            // check
            {

                // TODO?
                // loop == null is ok if loopCount is 0

                if (
                    !CheckMeshReadable(start) ||
                    !CheckMeshReadable(loop) ||
                    !CheckMeshReadable(end) ||
                    posCount <= 1
                    )
                {
                    return;
                }

            }

            //
            {
                close = close && (posCount > 2);
            }

            // -----------------

            Mesh[] meshes = new Mesh[close ? posCount : posCount - 1];

            float totalMeterLength = 0.0f;

            // -----------------

            // totalMeterLength
            {

                for (int i = 0; i < (close ? posCount : posCount - 1); i++)
                {
                    totalMeterLength += Vector3.Distance(linePositionList[i], linePositionList[(i + 1) % linePositionList.Count]);
                }

                if (totalMeterLength <= 0.0f)
                {
                    return;
                }

            }

            // meshes
            {

                for (int i = 0; i < meshes.Length; i++)
                {
                    meshes[i] = new Mesh();
                }

            }

            // meshes
            {

                Vector3 posA = linePositionList[0];
                Vector3 posB = linePositionList[1];
                Vector3 posC = linePositionList[linePositionList.Count - 1];

                float distanceAB = 0.0f;

                Vector3 upA = startUpwards;
                Vector3 upB = Vector3.up;

                Vector3 rightA = Vector3.Cross(startUpwards, (posB - posA).normalized);
                Vector3 rightB = rightA;

                Vector3 dirAB = (posB - posA).normalized;
                Vector3 dirBC = (posC - posB).normalized;

                Vector2 offsetA = startOffset;
                Vector2 offsetB = Vector2.right;

                float tempTotalMeterLength = 0.0f;

                float scaleA = 1.0f;
                float scaleB = 1.0f;

                Mesh meshA = start;
                Mesh meshB = null;

                float spaceMeterA = spaceMeterAtStart;
                float spaceMeterB = 0.0f;

                float lerpVal = 0.0f;

                float sign = 0.0f;

                int size = close ? posCount : posCount - 1;

                if (close)
                {

                    Vector3 dirCA = (posA - posC).normalized;

                    sign = Mathf.Sign(Vector3.SignedAngle(dirCA, dirAB, upA));

                    rightA = (dirAB - dirCA).normalized * ((sign >= 0.0f) ? 1.0f : -1.0f);

                    scaleA = Mathf.Cos(Mathf.Min(89.0f, Vector3.Angle(dirCA, dirAB) * 0.5f) * Mathf.Deg2Rad);

                }

                for (int i = 0; i < size; i++)
                {

                    posB = linePositionList[(i + 1) % linePositionList.Count];

                    distanceAB = Vector3.Distance(posA, posB);

                    // ----------------

                    if (distanceAB <= 0.0f)
                    {
                        continue;
                    }

                    // ----------------

                    tempTotalMeterLength += distanceAB;

                    lerpVal = tempTotalMeterLength / totalMeterLength;

                    upB = Vector3.Lerp(startUpwards, endUpwards, lerpVal);
                    offsetB = Vector2.Lerp(startOffset, endOffset, lerpVal);

                    if (
                        !close &&
                        i + 2 >= posCount
                        )
                    {
                        rightB = Vector3.Cross(endUpwards, (posB - posA).normalized);
                        scaleB = 1.0f;
                        spaceMeterB = spaceMeterAtEnd;
                        meshB = end;
                    }

                    else
                    {

                        posC = linePositionList[(i + 2) % linePositionList.Count];

                        dirAB = (posB - posA).normalized;
                        dirBC = (posC - posB).normalized;

                        sign = Mathf.Sign(Vector3.SignedAngle(dirAB, dirBC, upB));

                        rightB = (dirBC - dirAB).normalized * ((sign >= 0.0f) ? 1.0f : -1.0f);

                        if (rightB.sqrMagnitude <= 0.0f)
                        {
                            rightB = Vector3.Cross(upB, (posB - posA).normalized);
                        }

                        scaleB = Mathf.Cos(Mathf.Min(89.0f, Vector3.Angle(dirAB, dirBC) * 0.5f) * Mathf.Deg2Rad);
                        spaceMeterB = spaceMeterAtLoop * 0.5f;
                        meshB = loop;

                    }

                    CopyMeshesAlongLine(
                        posA,
                        posB,
                        meshes[i],
                        start,
                        loop,
                        end,
                        Axis.Z,
                        spaceMeterA,
                        spaceMeterAtLoop,
                        spaceMeterB,
                        upA,
                        upB,
                        rightA / scaleA,
                        rightB / scaleB,
                        offsetA,
                        offsetB,
                        rotation,
                        scale,
                        maxLoopCount,
                        mergeSubMeshes,
                        raycastInfo,
                        baseTransform
                        );

                    // swap
                    {
                        posA = posB;
                        upA = upB;
                        scaleA = scaleB;
                        rightA = rightB;
                        spaceMeterA = spaceMeterB;
                        offsetA = offsetB;
                    }

                }

            }

            // dest
            {
                MeshFuncs.CombineMeshes(dest, meshes);
            }

            // destroy
            {
                foreach (var val in meshes)
                {
                    Funcs.DestroyObject(val);
                }
            }

        }

    }

}
