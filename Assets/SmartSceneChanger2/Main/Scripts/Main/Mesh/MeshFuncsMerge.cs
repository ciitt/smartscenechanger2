using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    public static partial class MeshFuncs
    {

        /// <summary>
        /// Merge info
        /// </summary>
        struct MergeInfo
        {

            public int originalIndex;
            public int targetIndex;
            public int shift;

            public MergeInfo(
                int _originalIndex,
                int _targetIndex
                )
            {
                this.originalIndex = _originalIndex;
                this.targetIndex = _targetIndex;
                this.shift = 0;
            }

            public bool ShouldBeReplaced()
            {
                return (this.originalIndex != this.targetIndex);
            }

            public int FindTargetIndex(List<MergeInfo> list)
            {

                MergeInfo target = list[this.targetIndex];

                if (target.originalIndex == target.targetIndex)
                {
                    return target.originalIndex + target.shift;
                }

                else
                {
                    return target.FindTargetIndex(list);
                }

            }

        }

        /// <summary>
        /// Index and position
        /// </summary>
        struct MergeIndexAndPos
        {

            public int originalIndex;
            public Vector3 position;

            public MergeIndexAndPos(int _originalIndex, Vector3 _position)
            {
                this.originalIndex = _originalIndex;
                this.position = _position;
            }

        }

        /// <summary>
        /// Merge info
        /// </summary>
        class MergeVerticesInfo
        {

            public List<Vector3> tempMergeVertices = new List<Vector3>();
            public List<MergeIndexAndPos> tempMergeSortedVertices = new List<MergeIndexAndPos>();
            public List<Vector2> tempMergeUvs = new List<Vector2>();
            public List<int> tempMergeTriangles = new List<int>();
            public List<Vector3> tempMergeNormals = new List<Vector3>();
            public List<Color32> tempMergeColors = new List<Color32>();
            public List<MergeInfo> tempMergeInfoList = new List<MergeInfo>();

            public HashSet<int> tempAlreadyUsedIndexListForNormal = new HashSet<int>();
            public List<MergeIndexAndPos> tempMergeInfoListForNormal = new List<MergeIndexAndPos>();

            public void Clear()
            {

                this.tempMergeVertices.Clear();
                this.tempMergeSortedVertices.Clear();
                this.tempMergeUvs.Clear();
                this.tempMergeTriangles.Clear();
                this.tempMergeNormals.Clear();
                this.tempMergeColors.Clear();
                this.tempMergeInfoList.Clear();

                this.tempAlreadyUsedIndexListForNormal.Clear();
                this.tempMergeInfoListForNormal.Clear();

            }

        }

        /// <summary>
        /// MergeVerticesInfo
        /// </summary>
        static MergeVerticesInfo mergeVerticesInfo = null;

        /// <summary>
        /// Merge vertices
        /// </summary>
        /// <param name="sourceAndDest">source and dest</param>
        /// <param name="thresholdMeter">threshold meter to merge</param>
        /// <param name="thresholdDegree">threshold degree to merge</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool MergeVertices(
            Mesh source,
            Mesh dest,
            float thresholdMeter = 0.01f,
            float thresholdDegree = 60.0f
            )
        {

            if (
                !CheckMeshReadable(source) ||
                !CheckMeshReadable(dest) ||
                source.vertexCount < 3
                )
            {
                return false;
            }

            // ------------------------------

            // clear
            {

                if (mergeVerticesInfo == null)
                {
                    mergeVerticesInfo = new MergeVerticesInfo();
                }

                else
                {
                    mergeVerticesInfo.Clear();
                }

            }

            // ------------------------------

            source.GetVertices(mergeVerticesInfo.tempMergeVertices);
            source.GetUVs(0, mergeVerticesInfo.tempMergeUvs);
            source.GetNormals(mergeVerticesInfo.tempMergeNormals);
            source.GetTriangles(mergeVerticesInfo.tempMergeTriangles, 0);
            source.GetColors(mergeVerticesInfo.tempMergeColors);

            for (int i = 0; i < mergeVerticesInfo.tempMergeVertices.Count; i++)
            {
                mergeVerticesInfo.tempMergeSortedVertices.Add(new MergeIndexAndPos(i, mergeVerticesInfo.tempMergeVertices[i]));
                mergeVerticesInfo.tempMergeInfoList.Add(new MergeInfo());
            }

            bool useNormal = (mergeVerticesInfo.tempMergeNormals.Count > 0);
            bool useUv = (mergeVerticesInfo.tempMergeUvs.Count > 0);
            bool useColor = (mergeVerticesInfo.tempMergeColors.Count > 0);

            // ------------------------------

            // tempSortedMergeVertices
            {
                mergeVerticesInfo.tempMergeSortedVertices.Sort((x, y) => x.position.sqrMagnitude.CompareTo(y.position.sqrMagnitude));
            }

            // find first close point
            {

                int i = 0;
                int j = 0;

                int size = mergeVerticesInfo.tempMergeVertices.Count;

                Vector3 vertexA = Vector3.zero;
                Vector3 vertexB = Vector3.zero;
                Vector3 normalA = Vector3.zero;
                Vector3 normalB = Vector3.zero;

                float thresholdMeter2 = thresholdMeter * thresholdMeter;

                int targetIndex = 0;

                float sqrMagnitude = 0.0f;

                MergeIndexAndPos indexAndPos;

                for (i = 0; i < size; i++)
                {

                    indexAndPos = mergeVerticesInfo.tempMergeSortedVertices[i];

                    vertexA = indexAndPos.position;

                    if (useNormal)
                    {
                        normalA = mergeVerticesInfo.tempMergeNormals[indexAndPos.originalIndex];
                    }

                    targetIndex = indexAndPos.originalIndex;

                    for (j = i - 1; j >= 0; j--)
                    {

                        vertexB = mergeVerticesInfo.tempMergeSortedVertices[j].position;

                        if (useNormal)
                        {
                            normalB = mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeSortedVertices[j].originalIndex];
                        }

                        sqrMagnitude = (vertexA - vertexB).sqrMagnitude;

                        if (sqrMagnitude <= thresholdMeter2)
                        {

                            if (!useNormal || Vector3.Angle(normalA, normalB) <= thresholdDegree)
                            {
                                targetIndex = mergeVerticesInfo.tempMergeSortedVertices[j].originalIndex;
                                break;
                            }

                        }

                        // NOTE
                        // to be exact (vertexB.magnitude + thresholdMeter < vertexA.magnitude)
                        else if (vertexB.sqrMagnitude + thresholdMeter2 < vertexA.sqrMagnitude)
                        {
                            break;
                        }

                    }

                    mergeVerticesInfo.tempMergeInfoList[indexAndPos.originalIndex] = new MergeInfo(indexAndPos.originalIndex, targetIndex);

                }

            }

            // shift
            {

                MergeInfo temp;

                int shift = 0;

                for (int i = 0; i < mergeVerticesInfo.tempMergeInfoList.Count; i++)
                {

                    temp = mergeVerticesInfo.tempMergeInfoList[i];

                    if (temp.originalIndex != temp.targetIndex)
                    {
                        shift--;
                    }

                    temp.shift = shift;

                    mergeVerticesInfo.tempMergeInfoList[i] = temp;

                }

            }

            // vertex, uv, color
            {

                for (int i = mergeVerticesInfo.tempMergeVertices.Count - 1; i >= 0; i--)
                {

                    if (mergeVerticesInfo.tempMergeInfoList[i].ShouldBeReplaced())
                    {

                        mergeVerticesInfo.tempMergeVertices.RemoveAt(i);

                        if (useUv)
                        {
                            mergeVerticesInfo.tempMergeUvs.RemoveAt(i);
                        }

                        if (useNormal)
                        {
                            mergeVerticesInfo.tempMergeNormals.RemoveAt(i);
                        }

                        if (useColor)
                        {
                            mergeVerticesInfo.tempMergeColors.RemoveAt(i);
                        }

                    }

                }

            }

            // triangle
            {

                int val0 = 0;
                int val1 = 0;
                int val2 = 0;

                for (int i = mergeVerticesInfo.tempMergeTriangles.Count - 3; i >= 0; i -= 3)
                {

                    val0 = mergeVerticesInfo.tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 0]].FindTargetIndex(mergeVerticesInfo.tempMergeInfoList);
                    val1 = mergeVerticesInfo.tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 1]].FindTargetIndex(mergeVerticesInfo.tempMergeInfoList);
                    val2 = mergeVerticesInfo.tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 2]].FindTargetIndex(mergeVerticesInfo.tempMergeInfoList);

                    // 

                    if (
                        val0 == val1 ||
                        val0 == val2 ||
                        val1 == val2
                        )
                    {
                        mergeVerticesInfo.tempMergeTriangles.RemoveRange(i, 3);
                    }

                    else
                    {
                        mergeVerticesInfo.tempMergeTriangles[i + 0] = val0;
                        mergeVerticesInfo.tempMergeTriangles[i + 1] = val1;
                        mergeVerticesInfo.tempMergeTriangles[i + 2] = val2;
                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.SetVertices(mergeVerticesInfo.tempMergeVertices);
                dest.SetUVs(0, mergeVerticesInfo.tempMergeUvs);
                dest.SetNormals(mergeVerticesInfo.tempMergeNormals);
                dest.SetTriangles(mergeVerticesInfo.tempMergeTriangles, 0);
                dest.SetColors(mergeVerticesInfo.tempMergeColors);

                dest.RecalculateBounds();
                dest.RecalculateNormals();
                dest.RecalculateTangents();

                dest.UploadMeshData(false);

            }

            // clear
            {
                mergeVerticesInfo.Clear();
            }

            return true;

        }

        /// <summary>
        /// Smooth normals
        /// </summary>
        /// <param name="sourceAndDest">source and dest</param>
        /// <param name="thresholdMeter">threshold meter to merge</param>
        /// <param name="thresholdDegree">threshold degree to merge</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool SmoothNormals(
            Mesh sourceAndDest,
            float thresholdMeter = 0.01f,
            float thresholdDegree = 60.0f
            )
        {

            if (
                !sourceAndDest ||
                !sourceAndDest.isReadable ||
                sourceAndDest.vertexCount < 3
                )
            {
                return false;
            }

            // ------------------------------

            // clear
            {

                if (mergeVerticesInfo == null)
                {
                    mergeVerticesInfo = new MergeVerticesInfo();
                }

                else
                {
                    mergeVerticesInfo.Clear();
                }

            }

            // ------------------------------

            sourceAndDest.GetVertices(mergeVerticesInfo.tempMergeVertices);
            sourceAndDest.GetNormals(mergeVerticesInfo.tempMergeNormals);

            for (int i = 0; i < mergeVerticesInfo.tempMergeVertices.Count; i++)
            {
                mergeVerticesInfo.tempMergeSortedVertices.Add(new MergeIndexAndPos(i, mergeVerticesInfo.tempMergeVertices[i]));
            }

            // ------------------------------

            //
            {
                if (mergeVerticesInfo.tempMergeNormals.Count <= 0)
                {
                    return false;
                }
            }

            // tempSortedMergeVertices
            {
                mergeVerticesInfo.tempMergeSortedVertices.Sort((x, y) => x.position.sqrMagnitude.CompareTo(y.position.sqrMagnitude));
            }

            //
            {

                int i = 0;
                int j = 0;

                int size = mergeVerticesInfo.tempMergeVertices.Count;

                Vector3 normalA = Vector3.zero;
                Vector3 normalB = Vector3.zero;

                Vector3 averageNormal = Vector3.zero;

                MergeIndexAndPos valA;
                MergeIndexAndPos valB;

                float thresholdMeter2 = thresholdMeter * thresholdMeter;
                float sqrMagnitude = 0.0f;

                for (i = 0; i < size; i++)
                {

                    if (!mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Add(i))
                    {
                        continue;
                    }

                    // ---------------

                    valA = mergeVerticesInfo.tempMergeSortedVertices[i];

                    normalA = mergeVerticesInfo.tempMergeNormals[valA.originalIndex];

                    mergeVerticesInfo.tempMergeInfoListForNormal.Add(valA);

                    // ---------------

                    for (j = i + 1; j < size; j++)
                    {

                        if (mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Contains(j))
                        {
                            continue;
                        }

                        // ---------------

                        valB = mergeVerticesInfo.tempMergeSortedVertices[j];

                        normalB = mergeVerticesInfo.tempMergeNormals[valB.originalIndex];

                        sqrMagnitude = (valA.position - valB.position).sqrMagnitude;

                        // ---------------

                        if (
                            sqrMagnitude <= thresholdMeter2 &&
                            Vector3.Angle(normalA, normalB) <= thresholdDegree
                            )
                        {
                            mergeVerticesInfo.tempMergeInfoListForNormal.Add(valB);
                            mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Add(j);
                        }

                        // NOTE
                        // to be exact (valB.position.magnitude - valA.position.magnitude > thresholdMeter)
                        else if (valB.position.sqrMagnitude - valA.position.sqrMagnitude > thresholdMeter2)
                        {
                            break;
                        }

                    }

                    if (mergeVerticesInfo.tempMergeInfoListForNormal.Count > 1)
                    {

                        averageNormal = Vector3.zero;

                        for (j = 0; j < mergeVerticesInfo.tempMergeInfoListForNormal.Count; j++)
                        {
                            averageNormal += mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeInfoListForNormal[j].originalIndex];
                        }

                        averageNormal = (averageNormal / mergeVerticesInfo.tempMergeInfoListForNormal.Count).normalized;

                        for (j = 0; j < mergeVerticesInfo.tempMergeInfoListForNormal.Count; j++)
                        {
                            mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeInfoListForNormal[j].originalIndex] = averageNormal;
                        }

                    }

                    mergeVerticesInfo.tempMergeInfoListForNormal.Clear();

                }

            }

            // set
            {
                sourceAndDest.SetNormals(mergeVerticesInfo.tempMergeNormals);
                sourceAndDest.UploadMeshData(false);
            }

            // clear
            {
                mergeVerticesInfo.Clear();
            }

            return true;

        }

    }

}
