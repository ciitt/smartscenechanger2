using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    public static partial class MeshFuncs
    {

        /// <summary>
        /// MeshFilter list
        /// </summary>
        static List<MeshFilter> _meshFilters = null;

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// MeshFilter list
        /// </summary>
        static List<MeshFilter> meshFiltersInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _meshFilters); }
        }

        /// <summary>
        /// Calculate bounds
        /// </summary>
        /// <param name="root">root GameObject</param>
        /// <param name="considerTransformScale">consider the scale of Transform</param>
        /// <returns>Bounds</returns>
        // -------------------------------------------------------------------------------------------
        public static Bounds CalcGameObjectBounds(
            GameObject root,
            bool considerTransformScale
            )
        {

            Bounds ret = new Bounds();

            // ----------------------

            // check
            {

                if (!root)
                {
                    return ret;
                }

            }

            // ----------------------

            Vector3 min = Vector3.zero;
            Vector3 max = Vector3.zero;

            List<MeshFilter> meshFilters = meshFiltersInstance;

            Vector3 tempMin = new Vector3(100000f, 100000f, 100000f);
            Vector3 tempMax = new Vector3(-100000f, -100000f, -100000f);

            // ----------------------

            // meshFilters
            {

                // clear
                {
                    meshFilters.Clear();
                }

                // GetComponentsInChildren
                {
                    root.GetComponentsInChildren<MeshFilter>(true, meshFilters);
                }

            }

            // check
            {

                if (meshFilters.Count <= 0)
                {
                    return ret;
                }

            }

            // ----------------------

            // calc
            {

                foreach (var mf in meshFilters)
                {

                    tempMin = mf.sharedMesh.bounds.min;
                    tempMax = mf.sharedMesh.bounds.max;

                    if (considerTransformScale)
                    {

                        tempMin.x *= mf.transform.lossyScale.x;
                        tempMin.y *= mf.transform.lossyScale.y;
                        tempMin.z *= mf.transform.lossyScale.z;

                        tempMax.x *= mf.transform.lossyScale.x;
                        tempMax.y *= mf.transform.lossyScale.y;
                        tempMax.z *= mf.transform.lossyScale.z;

                    }

                    min.x = Mathf.Min(min.x, tempMin.x);
                    min.y = Mathf.Min(min.y, tempMin.y);
                    min.z = Mathf.Min(min.z, tempMin.z);

                    max.x = Mathf.Max(max.x, tempMax.x);
                    max.y = Mathf.Max(max.y, tempMax.y);
                    max.z = Mathf.Max(max.z, tempMax.z);

                }

            }

            // SetMinMax
            {
                ret.SetMinMax(min, max);
            }

            // clear
            {
                meshFilters.Clear();
            }

            return ret;

        }

    }

}
