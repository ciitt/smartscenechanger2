using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC2
{

    /// <summary>
    /// Collider holder
    /// </summary>
    public class ColliderHolder : MonoBehaviour
    {

#if UNITY_EDITOR

        /// <summary>
        /// Print debug info
        /// </summary>
        [SerializeField]
        [Tooltip("Print debug info")]
        bool m_printDebugInfoEditorOnly = false;

#endif

        /// <summary>
        /// LayerMask to deal with
        /// </summary>
        [SerializeField]
        [Tooltip("LayerMask to deal with")]
        protected LayerMask m_layerMask = 0;

        /// <summary>
        /// Events called when entered any object
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when entered any object")]
        protected UnityEvent<ColliderHolder, Collision> m_enteredEvent = null;

        /// <summary>
        /// Events called when exited any object
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when exited any object")]
        protected UnityEvent<ColliderHolder, Collider> m_exitedEvent = null;

        /// <summary>
        /// Events called when the list became empty
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when the list became empty")]
        protected UnityEvent<ColliderHolder> m_becameEmptyEvent = null;

        /// <summary>
        /// Collider list
        /// </summary>
        protected HashSet<Collider> m_colliderList = new HashSet<Collider>();

        /// <summary>
        /// Reference to Collider
        /// </summary>
        protected Collider m_refCollider = null;

        /// <summary>
        /// Temporary list to remove non-active
        /// </summary>
        HashSet<Collider> m_tempRemoveList = new HashSet<Collider>();

        // -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Collider list
        /// </summary>
        public IReadOnlyCollection<Collider> colliderList
        {
            get { return this.m_colliderList; }
        }

        /// <summary>
        /// OnCollisionEnter
        /// </summary>
        /// <param name="collision">Collision</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnCollisionEnter(Collision collision)
        {

            // check
            {

                if (
                    collision == null ||
                    !collision.collider
                    )
                {
                    return;
                }

            }

            // -------------------


#if UNITY_EDITOR

            if (this.m_printDebugInfoEditorOnly)
            {
                Debug.LogFormat("OnCollisionEnter : {0}", Funcs.CreateHierarchyPath(collision.collider.transform));
            }

#endif

            if (Funcs.CheckLayerMatches(this.m_layerMask, collision.gameObject))
            {
                this.m_colliderList.Add(collision.collider);
                this.m_enteredEvent.Invoke(this, collision);
            }

        }

        /// <summary>
        /// OnCollisionStay
        /// </summary>
        /// <param name="collision">Collision</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnCollisionStay(Collision collision)
        {

            if (Funcs.CheckLayerMatches(this.m_layerMask, collision.gameObject))
            {
                this.m_colliderList.Add(collision.collider);
            }

        }

        /// <summary>
        /// OnCollisionExit
        /// </summary>
        /// <param name="collision">Collision</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnCollisionExit(Collision collision)
        {

            this.OnCollisionExit(collision.collider);

        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void Start()
        {
            // m_refCollider
            {
                this.m_refCollider = this.GetComponent<Collider>();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.GetComponent<Rigidbody>())
                {

                    if (!this.m_refCollider)
                    {
                        Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCollider), Funcs.CreateHierarchyPath(this.transform));
                    }

                    if (
                        this.m_refCollider &&
                        this.m_refCollider.isTrigger
                        )
                    {
                        Debug.LogWarningFormat("Collider.isTrigger must be off : {0}", Funcs.CreateHierarchyPath(this.transform));
                    }

                }

            }

#endif

        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {
            this.ClearList();
        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // RemoveNonActiveObject
            {
                this.RemoveNonActiveObject();
            }

            // ClearList
            {

                if (
                    this.m_refCollider &&
                    !this.m_refCollider.enabled
                    )
                {
                    this.ClearList();
                }

            }

            //
            {

                //if (Time.frameCount % 120 == 0)
                //{
                //    print(this.m_colliderList.Count);
                //}

            }

        }

        /// <summary>
        /// OnCollisionExit
        /// </summary>
        /// <param name="collider">Collider</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnCollisionExit(Collider collider)
        {

            // check
            {
                if (!collider)
                {
                    return;
                }
            }

            // -------------------

#if UNITY_EDITOR

            if (this.m_printDebugInfoEditorOnly)
            {
                Debug.LogFormat("OnCollisionExit : {0}", Funcs.CreateHierarchyPath(collider.transform));
            }

#endif

            this.m_colliderList.Remove(collider);

            if (Funcs.CheckLayerMatches(this.m_layerMask, collider.gameObject))
            {

                this.m_exitedEvent.Invoke(this, collider);

                if (this.m_colliderList.Count <= 0)
                {
                    this.m_becameEmptyEvent.Invoke(this);
                }

            }

        }

        /// <summary>
        /// Remove non-active collider
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        void RemoveNonActiveObject()
        {

            // clear
            {
                this.m_tempRemoveList.Clear();
            }

            // add
            {

                foreach (var val in this.m_colliderList)
                {

                    if (val)
                    {

                        if (
                            !val.gameObject.activeInHierarchy ||
                            !val.enabled
                            )
                        {
                            this.m_tempRemoveList.Add(val);
                        }

                    }

                }

            }

            // remove
            {

                foreach (var val in this.m_tempRemoveList)
                {
                    this.OnCollisionExit(val);
                }

            }

        }

        /// <summary>
        /// Clear list
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        void ClearList()
        {

            this.m_colliderList.Clear();

        }

        /// <summary>
        /// Does have any colliders
        /// </summary>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool HasAnyColliders()
        {
            return (this.m_colliderList.Count > 0);
        }

        /// <summary>
        /// Check if contains collider
        /// </summary>
        /// <param name="collider">collider</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool ContainsCollider(Collider collider)
        {

            return
                collider &&
                this.m_colliderList.Contains(collider)
                ;

        }

        /// <summary>
        /// Check if contains target layer mask
        /// </summary>
        /// <param name="layerMask">LayerMask</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool ContainsAnyLayerMask(LayerMask layerMask)
        {

            return this.FindAnyLayerMask(layerMask);

        }

        /// <summary>
        /// Find any GameObject that has the target layer mask
        /// </summary>
        /// <param name="layerMask">LayerMask</param>
        /// <returns>found or null</returns>
        // -------------------------------------------------------------------------------------------------
        public GameObject FindAnyLayerMask(LayerMask layerMask)
        {

            foreach (var val in this.m_colliderList)
            {

                if (
                    val &&
                    Funcs.CheckLayerMatches(layerMask, val.gameObject)
                    )
                {
                    return val.gameObject;
                }

            }

            return null;

        }

    }

}
