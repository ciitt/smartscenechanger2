using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace SSC2
{

    // NOTE
    // This script doesn't work if an opponent has MeshCollider and changed the mesh on runtime

    /// <summary>
    /// Trigger holder
    /// </summary>
    public class TriggerHolder : MonoBehaviour
    {

#if UNITY_EDITOR

        /// <summary>
        /// Print debug info
        /// </summary>
        [SerializeField]
        [Tooltip("Print debug info")]
        bool m_printDebugInfoEditorOnly = false;

#endif

        /// <summary>
        /// LayerMask to deal with
        /// </summary>
        [SerializeField]
        [Tooltip("LayerMask to deal with")]
        protected LayerMask m_layerMask = 0;

        /// <summary>
        /// Events called when entered any object
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when entered any object")]
        protected UnityEvent<TriggerHolder, Collider> m_enteredEvent = null;

        /// <summary>
        /// Events called when exited any object
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when exited any object")]
        [FormerlySerializedAs("m_exitesEvent")]
        protected UnityEvent<TriggerHolder, Collider> m_exitedEvent = null;

        /// <summary>
        /// Events called when the list became empty
        /// </summary>
        [SerializeField]
        [Tooltip("Events called when the list became empty")]
        protected UnityEvent<TriggerHolder> m_becameEmptyEvent = null;

        /// <summary>
        /// Object list
        /// </summary>
        protected HashSet<Collider> m_colliderList = new HashSet<Collider>();

        /// <summary>
        /// Reference to Collider
        /// </summary>
        protected Collider m_refCollider = null;

        /// <summary>
        /// Temporary list to remove non-active
        /// </summary>
        HashSet<Collider> m_tempRemoveList = new HashSet<Collider>();

        // -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Collider list
        /// </summary>
        public IReadOnlyCollection<Collider> colliderList
        {
            get { return this.m_colliderList; }
        }

        /// <summary>
        /// OnTriggerEnter
        /// </summary>
        /// <param name="other">Collider</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnTriggerEnter(Collider other)
        {

#if UNITY_EDITOR

            if (
                other &&
                this.m_printDebugInfoEditorOnly
                )
            {
                Debug.LogFormat("OnTriggerEnter : {0}", Funcs.CreateHierarchyPath(other.transform));
            }

#endif

            if (Funcs.CheckLayerMatches(this.m_layerMask, other.gameObject))
            {
                this.m_colliderList.Add(other);
                this.m_enteredEvent.Invoke(this, other);
            }

        }

        /// <summary>
        /// OnTriggerStay
        /// </summary>
        /// <param name="other">Collider</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnTriggerStay(Collider other)
        {

            if (!this.m_colliderList.Contains(other))
            {
                this.OnTriggerEnter(other);
            }

        }

        /// <summary>
        /// OnTriggerExit
        /// </summary>
        /// <param name="other">Collider</param>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnTriggerExit(Collider other)
        {

#if UNITY_EDITOR

            if (
                other &&
                this.m_printDebugInfoEditorOnly
                )
            {
                Debug.LogFormat("OnTriggerExit : {0}", Funcs.CreateHierarchyPath(other.transform));
            }

#endif

            this.m_colliderList.Remove(other);

            if (Funcs.CheckLayerMatches(this.m_layerMask, other.gameObject))
            {

                this.m_exitedEvent.Invoke(this, other);

                if (this.m_colliderList.Count <= 0)
                {
                    this.m_becameEmptyEvent.Invoke(this);
                }

            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_refCollider
            {
                this.m_refCollider = this.GetComponent<Collider>();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCollider)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCollider), Funcs.CreateHierarchyPath(this.transform));
                }

                if (
                    this.m_refCollider &&
                    !this.m_refCollider.isTrigger
                    )
                {
                    Debug.LogWarningFormat("Collider.isTrigger must be on : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {
            this.ClearList();
        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // RemoveNonActiveObject
            {
                this.RemoveNonActiveObject();
            }

            // ClearList
            {

                if (
                    this.m_refCollider &&
                    !this.m_refCollider.enabled
                    )
                {
                    this.ClearList();
                }

            }

            //
            {

                //if (Time.frameCount % 120 == 0)
                //{
                //    print(this.m_colliderList.Count);
                //}

            }

        }

        /// <summary>
        /// Remove non-active collider
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        void RemoveNonActiveObject()
        {

            // clear
            {
                this.m_tempRemoveList.Clear();
            }

            // add
            {

                foreach (var val in this.m_colliderList)
                {

                    if (val)
                    {

                        if (
                            !val.gameObject.activeInHierarchy ||
                            !val.enabled
                            )
                        {
                            this.m_tempRemoveList.Add(val);
                        }
                        
                    }

                }

            }

            // remove
            {

                foreach (var val in this.m_tempRemoveList)
                {
                    this.OnTriggerExit(val);
                }

            }

        }

        /// <summary>
        /// Clear list
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        void ClearList()
        {

            this.m_colliderList.Clear();

        }

        /// <summary>
        /// Does have any objects
        /// </summary>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool HasAnyObjects()
        {
            return (this.m_colliderList.Count > 0);
        }

        /// <summary>
        /// Check if contains collider
        /// </summary>
        /// <param name="collider">collider</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool ContainsCollider(Collider collider)
        {

            return
                collider &&
                this.m_colliderList.Contains(collider)
                ;

        }

        /// <summary>
        /// Check if contains target layer mask
        /// </summary>
        /// <param name="layerMask">LayerMask</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public bool ContainsAnyLayerMask(LayerMask layerMask)
        {

            return this.FindAnyLayerMask(layerMask);

        }

        /// <summary>
        /// Find any GameObject that has the target layer mask
        /// </summary>
        /// <param name="layerMask">LayerMask</param>
        /// <returns>found or null</returns>
        // -------------------------------------------------------------------------------------------------
        public GameObject FindAnyLayerMask(LayerMask layerMask)
        {

            foreach (var val in this.m_colliderList)
            {

                if (
                    val &&
                    Funcs.CheckLayerMatches(layerMask, val.gameObject)
                    )
                {
                    return val.gameObject;
                }

            }

            return null;

        }

    }

}
