﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC2
{

    /// <summary>
    /// UI to follow an object
    /// </summary>
    public class UiFollowObject : UiMonoBehaviour
    {

        /// <summary>
        /// Type of staying on the screen border
        /// </summary>
        protected enum StayOnScreenBorderType
        {
            None,
            Simple,
            Precise,
        }

        /// <summary>
        /// Reference to Camera
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Camera")]
        protected Camera m_refCamera = null;

        /// <summary>
        /// Reference to Transform to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Transform to follow")]
        protected Transform m_refFollowTarget = null;

        /// <summary>
        /// StayOnScreenBorderType
        /// </summary>
        [SerializeField]
        [Tooltip("StayOnScreenBorderType")]
        protected StayOnScreenBorderType m_stayType = StayOnScreenBorderType.None;

        /// <summary>
        /// Stay on screen border offset at left
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset at left")]
        protected float m_stayOnScreenBorderOffsetLeft = 100;

        /// <summary>
        /// Stay on screen border offset at right
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset at right")]
        protected float m_stayOnScreenBorderOffsetRight = 100;

        /// <summary>
        /// Stay on screen border offset at top
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset at top")]
        protected float m_stayOnScreenBorderOffsetTop = 100;

        /// <summary>
        /// Stay on screen border offset at bottom
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset at bottom")]
        protected float m_stayOnScreenBorderOffsetBottom = 100;

        /// <summary>
        /// Event called when the target got in front of the camera
        /// </summary>
        [SerializeField]
        [Tooltip("Event called when the target got in front of the camera")]
        protected UnityEvent m_inFrontOfCameraEvent = null;

        /// <summary>
        /// Event called when the target got behind the camera
        /// </summary>
        [SerializeField]
        [Tooltip("Event called when the target got behind the camera")]
        protected UnityEvent m_behindCameraEvent = null;

        /// <summary>
        /// Reference to Canvas RectTransform
        /// </summary>
        protected RectTransform m_refCanvasRectTransform = null;

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Target is behind the camera
        /// </summary>
        public bool isBehind { get; private set; } = false;

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_refCanvasRectTransform
            {
                this.m_refCanvasRectTransform = this.GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCamera)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCamera), Funcs.CreateHierarchyPath(this.transform));
                }

                if (
                    this.refRectTransform.anchorMin != Vector2.zero ||
                    this.refRectTransform.anchorMax != Vector2.zero
                    )
                {
                    Debug.LogWarningFormat("anchorMin and anchorMax must be Vector2.zero (Bottom Left) : ", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // UpdatePosition
            {
                this.UpdatePosition();
            }

        }

        /// <summary>
        /// Set target to follow
        /// </summary>
        /// <param name="target">target</param>
        // ------------------------------------------------------------------------------------------
        public virtual void SetFollowTarget(Transform target)
        {
            this.m_refFollowTarget = target;
        }

        /// <summary>
        /// Update position
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void UpdatePosition()
        {

            if (
                !this.m_refCamera ||
                !this.m_refFollowTarget
                )
            {
                return;
            }

            // -----------------------

            //
            {

                Vector2 center = new Vector2(0.5f, 0.5f);
                Vector3 viewportPoint3d = this.m_refCamera.WorldToViewportPoint(this.m_refFollowTarget.position);
                Vector2 shiftedViewportPoint2d = new Vector2(viewportPoint3d.x, viewportPoint3d.y) - center;

                Vector2 canvasSize = this.m_refCanvasRectTransform.rect.size;

                bool behind = viewportPoint3d.z < 0.0f;

                //
                {
                    if (behind)
                    {
                        viewportPoint3d.x *= -1;
                        viewportPoint3d.y *= -1;
                        shiftedViewportPoint2d.x *= -1;
                        shiftedViewportPoint2d.y *= -1;
                    }
                }

                // anchoredPosition
                {

                    float left01 = this.m_stayOnScreenBorderOffsetLeft / canvasSize.x;
                    float right01 = 1.0f - (this.m_stayOnScreenBorderOffsetRight / canvasSize.x);
                    float top01 = 1.0f - (this.m_stayOnScreenBorderOffsetTop / canvasSize.y);
                    float bottom01 = this.m_stayOnScreenBorderOffsetBottom / canvasSize.y;

                    float shiftedLeft01 = left01 - 0.5f;
                    float shiftedRight01 = right01 - 0.5f;
                    float shiftedTop01 = top01 - 0.5f;
                    float shiftedBottom01 = bottom01 - 0.5f;

                    bool stay = this.m_stayType != StayOnScreenBorderType.None;

                    if (stay)
                    {

                        if (
                            viewportPoint3d.x < left01 ||
                            right01 < viewportPoint3d.x ||
                            viewportPoint3d.y < bottom01 ||
                            top01 < viewportPoint3d.y
                            )
                        {

                            if (this.m_stayType == StayOnScreenBorderType.Simple)
                            {

                                viewportPoint3d.x = Mathf.Clamp(viewportPoint3d.x, left01, right01);
                                viewportPoint3d.y = Mathf.Clamp(viewportPoint3d.y, bottom01, top01);

                                this.refRectTransform.anchoredPosition = new Vector2(
                                    viewportPoint3d.x * canvasSize.x,
                                    viewportPoint3d.y * canvasSize.y
                                    );

                            }

                            else
                            {

                                float scale = 1.0f;

                                // scale
                                {

                                    float clockwiseAngle = Vector2.Angle(Vector2.up, shiftedViewportPoint2d);
                                    float clockwiseAngleTR = Vector2.SignedAngle(new Vector2(shiftedRight01, shiftedTop01), Vector2.up);
                                    float clockwiseAngleBR = Vector2.SignedAngle(new Vector2(shiftedRight01, shiftedBottom01), Vector2.up);
                                    float clockwiseAngleBL = Vector2.SignedAngle(new Vector2(shiftedLeft01, shiftedBottom01), Vector2.up);
                                    float clockwiseAngleTL = Vector2.SignedAngle(new Vector2(shiftedLeft01, shiftedTop01), Vector2.up);

                                    clockwiseAngleTR = (clockwiseAngleTR + 360) % 360.0f;
                                    clockwiseAngleBR = (clockwiseAngleBR + 360) % 360.0f;
                                    clockwiseAngleBL = (clockwiseAngleBL + 360) % 360.0f;
                                    clockwiseAngleTL = (clockwiseAngleTL + 360) % 360.0f;

                                    // error if division by zero

                                    if (clockwiseAngleTR <= clockwiseAngle && clockwiseAngle <= clockwiseAngleBR)
                                    {
                                        scale = Mathf.Abs(shiftedRight01 / shiftedViewportPoint2d.x);
                                    }

                                    else if (clockwiseAngleBR <= clockwiseAngle && clockwiseAngle <= clockwiseAngleBL)
                                    {
                                        scale = Mathf.Abs(shiftedBottom01 / shiftedViewportPoint2d.y);
                                    }

                                    else if (clockwiseAngleBL <= clockwiseAngle && clockwiseAngle <= clockwiseAngleTL)
                                    {
                                        scale = Mathf.Abs(shiftedLeft01 / shiftedViewportPoint2d.x);
                                    }

                                    else
                                    {
                                        scale = Mathf.Abs(shiftedTop01 / shiftedViewportPoint2d.y);
                                    }

                                }

                                shiftedViewportPoint2d = (shiftedViewportPoint2d * scale) + center;

                                this.refRectTransform.anchoredPosition = new Vector2(
                                    shiftedViewportPoint2d.x * canvasSize.x,
                                    shiftedViewportPoint2d.y * canvasSize.y
                                    );

                            }

                        }

                        else
                        {
                            stay = false;
                        }

                    }

                    if (!stay)
                    {

                        this.refRectTransform.anchoredPosition = new Vector2(
                            viewportPoint3d.x * canvasSize.x,
                            viewportPoint3d.y * canvasSize.y
                            );

                    }

                }

                // isBehind
                {

                    if (this.isBehind != behind)
                    {

                        this.isBehind = behind;

                        if (behind)
                        {
                            this.m_behindCameraEvent.Invoke();
                        }

                        else
                        {
                            this.m_inFrontOfCameraEvent.Invoke();
                        }
                        
                    }

                }

            }

        }

    }

}
