using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// UI MonoBehaviour
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class UiMonoBehaviour : MonoBehaviour
    {

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        RectTransform m_refRectTransform = null;

        /// <summary>
        /// Last frame called Canvas.ForceUpdateCanvases
        /// </summary>
        static int lastFrameCountForceUpdateCanvases = -1;

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        public RectTransform refRectTransform
        {
            get { return Funcs.GetComponentIfNeeded(this, ref this.m_refRectTransform); }
        }

        /// <summary>
        /// Call Canvas.ForceUpdateCanvases if needed
        /// </summary>
        public static void ForceUpdateCanvasesIfNeeded()
        {

            if (
                lastFrameCountForceUpdateCanvases != Time.frameCount &&
                Time.timeSinceLevelLoad <= 0.0f
                )
            {

                lastFrameCountForceUpdateCanvases = Time.frameCount;
                Canvas.ForceUpdateCanvases();

#if UNITY_EDITOR
                Debug.Log("Called Canvas.ForceUpdateCanvases to avoid RectTransform.rect's size become 0,0 at first frame");
#endif

            }

        }

    }

}
