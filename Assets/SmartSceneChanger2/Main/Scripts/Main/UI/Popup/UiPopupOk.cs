﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SSC2
{

    /// <summary>
    /// Ui popup with ok button
    /// </summary>
    public class UiPopupOk : MonoBehaviour
    {

        /// <summary>
        /// Slectable type
        /// </summary>
        public enum SelectableType
        {
            None,
            Ok
        }

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ShowOrHideObject")]
        protected ShowOrHideObject m_refShowOrHideObject = null;

        /// <summary>
        /// Reference to Ok button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Ok button")]
        protected Selectable m_refOkButton = null;

        /// <summary>
        /// Reference to message Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to message Text")]
        protected TextMeshProUGUI m_refMessageText = null;

        /// <summary>
        /// Reference to ok button Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ok button Text")]
        protected TextMeshProUGUI m_refOkButtonText = null;

        /// <summary>
        /// Current callback holder for ok button
        /// </summary>
        protected Action m_currentOkCallback = null;

        /// <summary>
        /// Flag to hide when clicked any buttons
        /// </summary>
        protected bool m_autoHide = false;

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        public ShowOrHideObject refShowOrHideObject
        {
            get { return this.m_refShowOrHideObject; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refShowOrHideObject)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refShowOrHideObject), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refOkButton)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refOkButton), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMessageText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMessageText), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refOkButtonText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refOkButtonText), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="messageText">message text</param>
        /// <param name="okCallback">called when clicked ok button</param>
        /// <param name="hideWhenButtonClick">hide the popup when clicked any buttons</param>
        /// <param name="quick">flag to show quickly</param>
        /// <param name="selectable">default selectable when show</param>
        /// <param name="okButtonText">ok button text</param>
        /// <param name="font">font</param>
        // -----------------------------------------------------------------------------------
        public virtual void ShowPopup(
            string messageText,
            Action okCallback,
            bool hideWhenButtonClick,
            bool quick,
            SelectableType selectable,
            string okButtonText = "OK",
            TMP_FontAsset font = null
            )
        {

            // m_refMessageText
            {

                if (this.m_refMessageText)
                {

                    this.m_refMessageText.text = messageText;
                    
                    if (font)
                    {
                        this.m_refMessageText.font = font;
                    }

                }

            }

            // m_refOkButtonText
            {

                if (this.m_refOkButtonText)
                {

                    this.m_refOkButtonText.text = okButtonText;

                    if (font)
                    {
                        this.m_refOkButtonText.font = font;
                    }

                }

            }

            // m_currentOkCallback
            {
                this.m_currentOkCallback = okCallback;
            }

            // m_autoHide
            {
                this.m_autoHide = hideWhenButtonClick;
            }

            // selectableType
            {

                if (
                    this.m_refOkButton &&
                    selectable == SelectableType.Ok
                    )
                {
                    this.m_refOkButton.Select();
                }

                else
                {

                    if (UnityEngine.EventSystems.EventSystem.current)
                    {
                        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
                    }

                }

            }

            // show
            {
                this.m_refShowOrHideObject?.ShowOrHide(true, quick);
            }

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="messageText">message text</param>
        /// <param name="hideWhenButtonClick">hide the popup when clicked any buttons</param>
        /// <param name="quick">flag to show quickly</param>
        /// <param name="selectable">default selectable when show</param>
        /// <param name="okButtonText">ok button text</param>
        /// <param name="font">font</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public virtual IEnumerator ShowPopupIE(
            string messageText,
            bool hideWhenButtonClick,
            bool quick,
            SelectableType selectable,
            string okButtonText = "OK",
            TMP_FontAsset font = null
            )
        {

            bool finished = false;

            this.ShowPopup(
                messageText,
                () => { finished = true; },
                hideWhenButtonClick,
                quick,
                selectable,
                okButtonText,
                font
                );

            while (!finished)
            {
                yield return null;
            }

        }

        /// <summary>
        /// OnClick for ok button
        /// </summary>
        // -----------------------------------------------------------------------------------
        public virtual void OnClickOk()
        {

            // hide
            {
                if (this.m_autoHide)
                {
                    this.Hide(false);
                }
            }

            // m_currentOkCallback
            {
                this.m_currentOkCallback?.Invoke();
                this.m_currentOkCallback = null;
            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        /// <param name="quick">flag to hide quickly</param>
        // -----------------------------------------------------------------------------------
        public virtual void Hide(bool quick)
        {
            this.m_refShowOrHideObject?.ShowOrHide(false, quick);
        }

    }

}
