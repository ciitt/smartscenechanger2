﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Ui popup group
    /// </summary>
    public class UiPopupGroup : MonoBehaviour
    {

        /// <summary>
        /// Reference to UiPopupMessage
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiPopupMessage")]
        protected UiPopupMessage m_refUiPopupMessage = null;

        /// <summary>
        /// Reference to UiPopupOk
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiPopupOk")]
        protected UiPopupOk m_refUiPopupOk = null;

        /// <summary>
        /// Reference to UiPopupYesNo
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiPopupYesNo")]
        protected UiPopupYesNo m_refUiPopupYesNo = null;

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// Reference to UiPopupMessage
        /// </summary>
        public UiPopupMessage refUiPopupMessage
        {
            get { return this.m_refUiPopupMessage; }
        }

        /// <summary>
        /// Reference to UiPopupOk
        /// </summary>
        public UiPopupOk refUiPopupOk
        {
            get { return this.m_refUiPopupOk; }
        }

        /// <summary>
        /// Reference to UiPopupYesNo
        /// </summary>
        public UiPopupYesNo refUiPopupYesNo
        {
            get { return this.m_refUiPopupYesNo; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refUiPopupMessage)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refUiPopupMessage == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refUiPopupOk)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refUiPopupOk == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refUiPopupYesNo)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refUiPopupYesNo == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
