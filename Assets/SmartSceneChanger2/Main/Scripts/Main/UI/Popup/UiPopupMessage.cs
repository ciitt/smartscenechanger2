﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SSC2
{

    /// <summary>
    /// Ui popup with message
    /// </summary>
    public class UiPopupMessage : MonoBehaviour
    {

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ShowOrHideObject")]
        protected ShowOrHideObject m_refShowOrHideObject = null;

        /// <summary>
        /// Reference to Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        protected TextMeshProUGUI m_refText = null;

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        public ShowOrHideObject refShowOrHideObject
        {
            get { return this.m_refShowOrHideObject; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refShowOrHideObject)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refShowOrHideObject), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refText), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="showingSeconds">seconds to be showing (call [Hide] function manually if negative vaue)</param>
        /// <param name="font">font</param>
        // -----------------------------------------------------------------------------------
        public virtual void ShowPopup(
            string text,
            float showingSeconds,
            TMP_FontAsset font = null
            )
        {

            if (!this.m_refText)
            {
                return;
            }

            // --------------------

            // Invoke
            {

                CancelInvoke("Hide");

                if (showingSeconds > 0.0f)
                {
                    Invoke("Hide", showingSeconds);
                }
                
            }

            // m_refText
            {

                this.m_refText.text = text;

                if (font)
                {
                    this.m_refText.font = font;
                }

            }

            // show
            {
                this.m_refShowOrHideObject?.ShowOrHide(true, false);
            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        // -----------------------------------------------------------------------------------
        public virtual void Hide()
        {
            this.m_refShowOrHideObject?.ShowOrHide(false, false);
        }

    }

}
