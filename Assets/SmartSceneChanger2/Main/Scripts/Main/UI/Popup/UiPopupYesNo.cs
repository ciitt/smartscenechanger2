﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SSC2
{

    /// <summary>
    /// Ui popup with yes and no button
    /// </summary>
    public class UiPopupYesNo : MonoBehaviour
    {

        /// <summary>
        /// Slectable type
        /// </summary>
        public enum SelectableType
        {
            None,
            Yes,
            No
        }

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ShowOrHideObject")]
        protected ShowOrHideObject m_refShowOrHideObject = null;

        /// <summary>
        /// Reference to Yes button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Yes button")]
        protected Selectable m_refYesButton = null;

        /// <summary>
        /// Reference to No button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to No button")]
        protected Selectable m_refNoButton = null;

        /// <summary>
        /// Reference to message Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to message Text")]
        protected TextMeshProUGUI m_refMessageText = null;

        /// <summary>
        /// Reference to yes button Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to yes button Text")]
        protected TextMeshProUGUI m_refYesButtonText = null;

        /// <summary>
        /// Reference to no button Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to no button Text")]
        protected TextMeshProUGUI m_refNoButtonText = null;

        /// <summary>
        /// Current callback holder for yes button
        /// </summary>
        protected Action m_currentYesCallback = null;

        /// <summary>
        /// Current callback holder for no button
        /// </summary>
        protected Action m_currentNoCallback = null;

        /// <summary>
        /// Flag to hide when clicked any buttons
        /// </summary>
        protected bool m_autoHide = false;

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// Reference to ShowOrHideObject
        /// </summary>
        public ShowOrHideObject refShowOrHideObject
        {
            get { return this.m_refShowOrHideObject; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {


                if (!this.m_refShowOrHideObject)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refShowOrHideObject), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refYesButton)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refYesButton), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refNoButton)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refNoButton), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refMessageText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMessageText), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refYesButtonText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refYesButtonText), Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refNoButtonText)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refNoButtonText), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="messageText">message text</param>
        /// <param name="yesCallback">called when clicked yes button</param>
        /// <param name="noCallback">called when clicked no button</param>
        /// <param name="hideWhenButtonClick">hide the popup when clicked any buttons</param>
        /// <param name="quick">flag to show quickly</param>
        /// <param name="selectable">default selectable when show</param>
        /// <param name="yesButtonText">yes button text</param>
        /// <param name="noButtonText">no button text</param>
        /// <param name="font">font</param>
        // -----------------------------------------------------------------------------------
        public virtual void ShowPopup(
            string messageText,
            Action yesCallback,
            Action noCallback,
            bool hideWhenButtonClick,
            bool quick,
            SelectableType selectable,
            string yesButtonText = "Yes",
            string noButtonText = "No",
            TMP_FontAsset font = null
            )
        {

            // m_refMessageText
            {

                if (this.m_refMessageText)
                {

                    this.m_refMessageText.text = messageText;
                    
                    if (font)
                    {
                        this.m_refMessageText.font = font;
                    }

                }

            }

            // m_refYesButtonText
            {

                if (this.m_refYesButtonText)
                {

                    this.m_refYesButtonText.text = yesButtonText;
                    
                    if (font)
                    {
                        this.m_refYesButtonText.font = font;
                    }

                }

            }

            // m_refNoButtonText
            {

                if (this.m_refNoButtonText)
                {

                    this.m_refNoButtonText.text = noButtonText;
                    
                    if (font)
                    {
                        this.m_refNoButtonText.font = font;
                    }

                }

            }

            // m_currentYesCallback, m_currentNoCallback
            {
                this.m_currentYesCallback = yesCallback;
                this.m_currentNoCallback = noCallback;
            }

            // m_autoHide
            {
                this.m_autoHide = hideWhenButtonClick;
            }

            // selectableType
            {

                if (
                    this.m_refYesButton &&
                    selectable == SelectableType.Yes
                    )
                {
                    this.m_refYesButton.Select();
                }

                else if (
                    this.m_refNoButton &&
                    selectable == SelectableType.No
                    )
                {
                    this.m_refNoButton.Select();
                }

                else
                {

                    if (UnityEngine.EventSystems.EventSystem.current)
                    {
                        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
                    }

                }

            }

            // show
            {
                this.m_refShowOrHideObject?.ShowOrHide(true, quick);
            }

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="messageText">message text</param>
        /// <param name="yesCallback">called when clicked yes button</param>
        /// <param name="noCallback">called when clicked no button</param>
        /// <param name="hideWhenButtonClick">hide the popup when clicked any buttons</param>
        /// <param name="quick">flag to show quickly</param>
        /// <param name="selectable">default selectable when show</param>
        /// <param name="yesButtonText">yes button text</param>
        /// <param name="noButtonText">no button text</param>
        /// <param name="font">font</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public virtual IEnumerator ShowPopupIE(
            string messageText,
            Action yesCallback,
            Action noCallback,
            bool hideWhenButtonClick,
            bool quick,
            SelectableType selectable,
            string yesButtonText = "Yes",
            string noButtonText = "No",
            TMP_FontAsset font = null
            )
        {

            bool yesPressed = false;
            bool noPressed = false;

            // show
            {

                this.ShowPopup(
                    messageText,
                    () => { yesPressed = true; },
                    () => { noPressed = true; },
                    hideWhenButtonClick,
                    quick,
                    selectable,
                    yesButtonText,
                    noButtonText,
                    font
                    );

            }

            // wait
            {

                while (
                    !yesPressed &&
                    !noPressed
                    )
                {
                    yield return null;
                }

            }

            // callback
            {

                if (yesPressed)
                {
                    yesCallback?.Invoke();
                }

                else
                {
                    noCallback?.Invoke();
                }

            }

        }

        /// <summary>
        /// OnClick for yes button
        /// </summary>
        // -----------------------------------------------------------------------------------
        public virtual void OnClickYes()
        {

            // hide
            {
                if (this.m_autoHide)
                {
                    this.Hide(false);
                }
            }

            // m_currentYesCallback
            {
                this.m_currentYesCallback?.Invoke();
                this.m_currentYesCallback = null;
            }

        }

        /// <summary>
        /// OnClick for no button
        /// </summary>
        // -----------------------------------------------------------------------------------
        public virtual void OnClickNo()
        {

            // hide
            {
                if (this.m_autoHide)
                {
                    this.Hide(false);
                }
            }

            // m_currentNoCallback
            {
                this.m_currentNoCallback?.Invoke();
                this.m_currentNoCallback = null;
            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        /// <param name="quick">flag to hide quickly</param>
        // -----------------------------------------------------------------------------------
        public virtual void Hide(bool quick)
        {
            this.m_refShowOrHideObject?.ShowOrHide(false, quick);
        }

    }

}
