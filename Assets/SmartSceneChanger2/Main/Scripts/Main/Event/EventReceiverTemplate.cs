using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Event receiver
    /// </summary>
    public abstract class EventReceiverTemplate<T> : MonoBehaviour, EventManager<T>.IReceiver where T : new()
    {

        /// <summary>
        /// Receive the event even if the instance is inactiv
        /// </summary>
        [SerializeField]
        [Tooltip("Receive the event even if the instance is inactive")]
        protected bool m_receiveEvenIfInactive = false;

        /// <summary>
        /// Receive
        /// </summary>
        /// <param name="eventData">data</param>
        public abstract void Receive(T eventData);

        /// <summary>
        /// OnEnable
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        protected virtual void OnEnable()
        {

            EventManager<T>.Instance.AddEventReceiver(this);

        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {

            if (!this.m_receiveEvenIfInactive)
            {
                EventManager<T>.Instance.RemoveEventReceiver(this);
            }

        }

    }

}
