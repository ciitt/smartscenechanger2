using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Event manager
    /// </summary>
    /// <typeparam name="T">T</typeparam>
    public class EventManager<T> : Singleton<EventManager<T>> where T : new()
    {

        /// <summary>
        /// Receiver interface
        /// </summary>
        public interface IReceiver
        {

            /// <summary>
            /// Receive
            /// </summary>
            /// <param name="eventData">data</param>
            void Receive(T eventData);

        }

        /// <summary>
        /// Event dictionary
        /// </summary>
        protected HashSet<IReceiver> m_eventList = new HashSet<IReceiver>();

        // -----------------------------------------------------------------------------------------

        /// <summary>
        /// Value
        /// </summary>
        public T value { get; set; } = new T();

        /// <summary>
        /// Add an event receiver
        /// </summary>
        /// <param name="receiver">receiver</param>
        // -----------------------------------------------------------------------------------------
        public virtual void AddEventReceiver(IReceiver receiver)
        {
            this.m_eventList.Add(receiver);
        }

        /// <summary>
        /// Remove an event receiver
        /// </summary>
        /// <param name="receiver">receiver</param>
        // -----------------------------------------------------------------------------------------
        public virtual void RemoveEventReceiver(IReceiver receiver)
        {
            this.m_eventList.Remove(receiver);
        }

        /// <summary>
        /// Send event
        /// </summary>
        /// <param name="val">val</param>
        // -----------------------------------------------------------------------------------------
        public virtual void SendEvent()
        {

            foreach (var receiver in this.m_eventList)
            {
                receiver.Receive(this.value);
            }

        }

    }

}
