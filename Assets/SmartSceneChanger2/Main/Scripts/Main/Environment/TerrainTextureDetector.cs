using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Terrain texture detector
    /// </summary>
    [RequireComponent(typeof(Terrain))]
    public class TerrainTextureDetector : ColliderTextureDetector
    {

        /// <summary>
        /// Reference to Terrain
        /// </summary>
        Terrain m_refTerrain = null;

        /// <summary>
        /// Reference to alpha map
        /// </summary>
        float[,,] m_refAlphaMap = null;

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to Terrain
        /// </summary>
        Terrain refTerrain
        {
            get { return (this.m_refTerrain) ? this.m_refTerrain : (this.m_refTerrain = this.GetComponent<Terrain>()); }
        }

        /// <summary>
        /// Start
        /// </summary>
        // --------------------------------------------------------------------------------------
        void Start()
        {
            _ = this.refTerrain;
            _ = this.GetAlphaMap();
        }

        /// <summary>
        /// Clear alpha map reference
        /// </summary>
        // --------------------------------------------------------------------------------------
        public void ClearAlphaMapReference()
        {
            this.m_refAlphaMap = null;
        }

        /// <summary>
        /// Get alpha map
        /// </summary>
        /// <returns>alpha map</returns>
        // --------------------------------------------------------------------------------------
        float[,,] GetAlphaMap()
        {

            if (this.m_refAlphaMap == null)
            {

                TerrainData terrainData = this.refTerrain.terrainData;

                this.m_refAlphaMap = terrainData.GetAlphamaps(0, 0, terrainData.alphamapWidth, terrainData.alphamapHeight);

            }

            return this.m_refAlphaMap;

        }

        // --------------------------------------------------------------------------------------
        public override Texture FindTexture(Vector3 pos)
        {

            Texture ret = null;

            TerrainLayer terrainLayer = this.GetTerrainLayer(pos);

            if (terrainLayer)
            {
                ret = terrainLayer.diffuseTexture;
            }

            return ret;

        }

        /// <summary>
        /// Find terrain
        /// </summary>
        /// <param name="pos">posiiton</param>
        /// <param name="terrain">terrain</param>
        /// <param name="x">x</param>
        /// <param name="z">z</param>
        /// <returns>found</returns>
        // --------------------------------------------------------------------------------------
        protected bool CalcCoordinate(
            Vector3 pos,
            ref int x,
            ref int z
            )
        {

            // calc
            {

                Terrain terrain = this.refTerrain;

                Vector3 from = Vector3.zero;
                Vector3 to = Vector3.zero;

                if (terrain.terrainData)
                {

                    from = terrain.transform.position;
                    to = from + terrain.terrainData.size;

                    if (
                        (from.x <= pos.x && pos.x <= to.x) &&
                        (from.z <= pos.z && pos.z <= to.z)
                        )
                    {
                        x = Mathf.RoundToInt(Mathf.InverseLerp(from.x, to.x, pos.x) * terrain.terrainData.alphamapWidth);
                        z = Mathf.RoundToInt(Mathf.InverseLerp(from.z, to.z, pos.z) * terrain.terrainData.alphamapHeight);
                        x = Mathf.Clamp(x, 0, terrain.terrainData.alphamapWidth - 1);
                        z = Mathf.Clamp(z, 0, terrain.terrainData.alphamapHeight - 1);
                        return true;
                    }

                }

            }

            return false;

        }

        /// <summary>
        /// Get a terrain layer
        /// </summary>
        /// <param name="pos">posiiton</param>
        /// <returns>found or null</returns>
        // --------------------------------------------------------------------------------------
        public TerrainLayer GetTerrainLayer(Vector3 pos)
        {

            TerrainLayer ret = null;

            // find
            {

                TerrainData terrainData = this.refTerrain.terrainData;

                int x = 0;
                int z = 0;

                if (this.CalcCoordinate(pos, ref x, ref z))
                {

                    float[,,] alphaMap = this.GetAlphaMap();

                    int size2 = alphaMap.GetLength(2);

                    if (size2 > 0)
                    {

                        int index = 0;
                        float maxWeight = -1.0f;
                        float tempWeight = 0.0f;

                        TerrainLayer[] terrainLayers = terrainData.terrainLayers;

                        for (int i = 0; i < size2; i++)
                        {

                            tempWeight = alphaMap[z, x, i];

                            if (
                                tempWeight < 0.0f ||
                                tempWeight > maxWeight
                                )
                            {
                                index = i;
                                maxWeight = tempWeight;
                            }

                        }

                        if (index < terrainLayers.Length)
                        {
                            ret = terrainLayers[index];
                        }

                    }

                }

            }

            return ret;

        }

    }

}
