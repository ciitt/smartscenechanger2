using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Simple texture detector
    /// </summary>
    public class SimpleTextureDetector : ColliderTextureDetector
    {

        /// <summary>
        /// Reference to MeshRenderer
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to MeshRenderer")]
        MeshRenderer m_refMeshRenderer = null;

        // --------------------------------------------------------------------------------------
        public override Texture FindTexture(Vector3 pos)
        {

            return
                this.m_refMeshRenderer && this.m_refMeshRenderer.sharedMaterial ?
                this.m_refMeshRenderer.sharedMaterial.mainTexture :
                null
                ;

        }

        /// <summary>
        /// Start
        /// </summary>
        // --------------------------------------------------------------------------------------
        void Start()
        {

#if UNITY_EDITOR

            // 
            {

                if (!this.m_refMeshRenderer)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refMeshRenderer), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
