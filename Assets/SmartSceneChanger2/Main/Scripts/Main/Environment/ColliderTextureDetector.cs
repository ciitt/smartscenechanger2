using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Ground texture detector
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public abstract class ColliderTextureDetector : MonoBehaviour
    {

        /// <summary>
        /// Priority
        /// </summary>
        [SerializeField]
        [Tooltip("Priority")]
        int m_priority = 0;

        /// <summary>
        /// Find texture at position
        /// </summary>
        /// <param name="pos">position</param>
        /// <returns>Texture</returns>
        public abstract Texture FindTexture(Vector3 pos);

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Priority
        /// </summary>
        public int priority { get { return this.m_priority; } }

    }

}
