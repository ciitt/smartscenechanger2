﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Attribute for readonly
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute
    {

    }

}
