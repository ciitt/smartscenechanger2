﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Attribute for bit mask
    /// </summary>
    public class BitMaskAttribute : PropertyAttribute
    {

    }

}
