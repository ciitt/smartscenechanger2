using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Axis
    /// </summary>
    public enum Axis
    {
        X,
        Y,
        Z
    }

}
