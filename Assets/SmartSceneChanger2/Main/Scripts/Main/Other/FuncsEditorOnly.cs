#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SSC2
{

    /// <summary>
    /// Static Functions (EditorOnly)
    /// </summary>
    public static class FuncsEditorOnly
    {

        /// <summary>
        /// Destroy the target if not stored on disk
        /// </summary>
        /// <param name="target">target</param>
        // -------------------------------------------------------------------------------------------
        public static void DestroyImmediateIfNotPersistent(UnityEngine.Object target)
        {

            if (
                target &&
                !Application.isPlaying &&
                !EditorUtility.IsPersistent(target)
                )
            {
                Undo.DestroyObjectImmediate(target);
            }

        }

        /// <summary>
        /// Check if the target exists in current scene
        /// </summary>
        /// <param name="target">target</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CheckTargetExistsInCurrentScene(GameObject target)
        {

            return
                target &&
                !string.IsNullOrEmpty(target.scene.name)
                ;

        }

        /// <summary>
        /// Create folder
        /// </summary>
        /// <param name="folderPath">folder path</param>
        /// <param name="showErrorDialog">show error dialog if error</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CreateFolder(
            string folderPath,
            bool showErrorDialog
            )
        {

            string success = "";

            // -----------------------

            // create
            {

                try
                {

                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                }

                catch (IOException e)
                {

                    Debug.LogError(e.Message);

                    success = e.Message;

                }

            }

            // show error
            {

                if (showErrorDialog && !string.IsNullOrEmpty(success))
                {

                    EditorUtility.DisplayDialog(
                        "Error",
                        string.Format("Failed to create a folder\n\n{0}", folderPath),
                        "OK"
                        );

                }

            }

            return string.IsNullOrEmpty(success);

        }

        /// <summary>
        /// Check if overwrite assets when already exist
        /// </summary>
        /// <param name="filePaths">file paths</param>
        /// <returns>true to overwrite or continue</returns>
        // -----------------------------------------------------------------------------------------
        public static bool CheckOverwriteAssets(params string[] filePaths)
        {

            StringBuilder sb = new StringBuilder();

            int lineLimit = 5;
            int overwriteCount = 0;

            // append
            {

                foreach (var val in filePaths)
                {

                    if (File.Exists(val))
                    {

                        overwriteCount++;

                        if (overwriteCount <= lineLimit)
                        {
                            sb.AppendFormat("{0}\n", val);
                        }

                    }

                }

                if (overwriteCount >= lineLimit)
                {
                    sb.Append("\n ...");
                }

            }

            // DisplayDialog
            {

                if (sb.Length > 0)
                {

                    string message = string.Format(
                        "Are you sure you want to overwrite the following {0} files?\n\n{1}",
                        overwriteCount,
                        sb.ToString()
                        );

                    return EditorUtility.DisplayDialog("Confirmation", message, "Yes", "Cancel");

                }

            }

            return true;

        }

        /// <summary>
        /// Check if overwrite assets in non-empty folder
        /// </summary>
        /// <param name="folderPath">folder path</param>
        /// <returns>true to overwrite or continue</returns>
        // -----------------------------------------------------------------------------------------
        public static bool CheckOverwriteAssetsIfNonEmptyFolder(string folderPath)
        {

            bool ret = true;

            try
            {

                if (
                    Directory.GetFiles(folderPath).Length > 0 ||
                    Directory.GetDirectories(folderPath).Length > 0
                    )
                {

                    ret = (
                        EditorUtility.DisplayDialog(
                            "Confirmation",
                            "The folder you selected is not empty.\nPerhaps some files would be overwritten.\n\nContinue?",
                            "Yes",
                            "No")
                            );

                }

            }

            catch (Exception e)
            {
                Debug.LogError(e.Message);
                ret = false;
            }

            return ret;

        }

        /// <summary>
        /// Check if in prefab mode
        /// </summary>
        /// <param name="target">target</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CheckIfPrefabmode(GameObject target)
        {
            return StageUtility.GetStage(target) is PrefabStage;
        }

        /// <summary>
        /// Check if in playing or prefab mode
        /// </summary>
        /// <param name="target">target</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------
        public static bool IsPlayingOrPrefabmode(GameObject target)
        {

            return
                Application.IsPlaying(target) ||
                CheckIfPrefabmode(target)
                ;

        }

        /// <summary>
        /// Save an asset to disk
        /// </summary>
        /// <param name="asset">asset</param>
        /// <param name="fileName"></param>
        /// <param name="extension"></param>
        /// <returns>saved</returns>
        // -------------------------------------------------------------------------------------------
        public static bool SaveAssetToDisk(
            UnityEngine.Object asset,
            string fileName,
            string extension
            )
        {

            string path = EditorUtility.SaveFilePanelInProject(
                "Save",
                fileName,
                extension,
                "Save"
                );

            bool ret = false;

            if (!string.IsNullOrEmpty(path))
            {

                AssetDatabase.CreateAsset(asset, path);
                AssetDatabase.SaveAssets();

                AssetDatabase.Refresh();

                ret = true;

            }

            return ret;

        }

        /// <summary>
        /// Save a texture to disk as png
        /// </summary>
        /// <returns>saved</returns>
        // -------------------------------------------------------------------------------------------
        public static bool SaveTextureAsPngToDisk(Texture2D texture)
        {

            bool ret = false;

            if (texture)
            {

                byte[] bytes = texture.EncodeToPNG();

                string path = UnityEditor.EditorUtility.SaveFilePanelInProject(
                    "Save",
                    texture.name,
                    "png",
                    "Save"
                    );

                if (
                    !string.IsNullOrEmpty(path) &&
                    path.StartsWith("Assets")
                    )
                {

                    path = ChangeProjectPathToAbsolutePath(path);
                    
                    try
                    {

                        System.IO.File.WriteAllBytes(path, bytes);

                        Debug.Log("Saved");

                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();

                        ret = true;

                    }

                    catch (System.Exception e)
                    {
                        Debug.LogError(e.Message);
                    }

                }

            }

            return ret;

        }

        /// <summary>
        /// Change absolute path to project path
        /// </summary>
        /// <param name="absolutePath">absolute path</param>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------------
        public static string ChangeAbsolutePathToProjectPath(string absolutePath)
        {

            string ret = absolutePath;

            if (
                !string.IsNullOrEmpty(absolutePath) &&
                absolutePath.StartsWith(Application.dataPath)
                )
            {

                string path = absolutePath.Replace(Application.dataPath, "");

                ret = string.Format("Assets{0}", path);

            }

            return ret;

        }

        /// <summary>
        /// Change project path to absolute path
        /// </summary>
        /// <param name="projectPath">project path</param>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------------
        public static string ChangeProjectPathToAbsolutePath(string projectPath)
        {

            string ret = projectPath;

            if (!string.IsNullOrEmpty(projectPath))
            {

                if (projectPath.StartsWith("Assets/"))
                {
                    ret = string.Format("{0}/{1}", Application.dataPath, projectPath.Substring(7));
                }

                else if (projectPath == "Assets")
                {
                    ret = Application.dataPath;
                }

            }

            return ret;

        }

    }

}

#endif
