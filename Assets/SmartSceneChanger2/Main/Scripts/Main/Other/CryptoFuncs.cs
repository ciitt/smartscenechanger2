using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static Functions for crypto
    /// </summary>
    public static class CryptoFuncs
    {

#if UNITY_EDITOR

        /// <summary>
        /// Samples (EditorOnly)
        /// </summary>
        public static class SamplesEditorOnly
        {

            /// <summary>
            /// _bytesA
            /// </summary>
            static byte[] _bytesA = new byte[8] { 250, 103, 53, 49, 8, 246, 187, 203, };

            /// <summary>
            /// _bytesB
            /// </summary>
            static byte[] _bytesB = new byte[8] { 68, 99, 211, 99, 160, 34, 32, 242, };

            /// <summary>
            /// _bytesC
            /// </summary>
            static byte[] _bytesC = new byte[8] { 129, 165, 127, 42, 26, 35, 107, 54, };

            /// <summary>
            /// _bytesD
            /// </summary>
            static byte[] _bytesD = new byte[8] { 217, 74, 235, 242, 170, 220, 44, 167, };

            /// <summary>
            /// _bytesE
            /// </summary>
            static byte[] _bytesE = new byte[8] { 180, 76, 213, 192, 17, 28, 9, 69, };

            /// <summary>
            /// _bytesF
            /// </summary>
            static byte[] _bytesF = new byte[8] { 14, 228, 38, 216, 98, 33, 44, 65, };

            /// <summary>
            /// _bytesG
            /// </summary>
            static byte[] _bytesG = null;

            /// <summary>
            /// _bytesH
            /// </summary>
            static byte[] _bytesH = null;

            /// <summary>
            /// bytesG (byte[32])
            /// </summary>
            public static byte[] bytesG
            {

                get
                {

                    if (_bytesG == null)
                    {

                        List<byte> list = new List<byte>();

                        list.AddRange(_bytesA);
                        list.AddRange(_bytesB);
                        list.AddRange(_bytesC);
                        list.AddRange(_bytesD);

                        _bytesG = list.ToArray();

                    }

                    return _bytesG;

                }

            }

            /// <summary>
            /// bytesH (byte[16])
            /// </summary>
            public static byte[] bytesH
            {

                get
                {

                    if (_bytesH == null)
                    {

                        List<byte> list = new List<byte>();

                        list.AddRange(_bytesE);
                        list.AddRange(_bytesF);

                        _bytesH = list.ToArray();

                    }

                    return _bytesH;

                }

            }

        }

#endif

        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="kff">kff</param>
        /// <param name="are">are</param>
        /// <param name="err">err</param>
        /// <returns>byte array</returns>
        public static byte[] ConvertE(
            string text,
            byte[] kff,
            byte[] are,
            out string err
            )
        {

            byte[] ret = null;

            err = "";

            try
            {

                using (Aes p27 = Aes.Create())
                {

                    p27.Key = kff;
                    p27.IV = are;

                    ICryptoTransform tor = p27.CreateEncryptor(p27.Key, p27.IV);

                    using (MemoryStream hdt = new MemoryStream())
                    {

                        using (CryptoStream nis = new CryptoStream(hdt, tor, CryptoStreamMode.Write))
                        {

                            using (StreamWriter mmm = new StreamWriter(nis))
                            {
                                mmm.Write(text);
                            }

                            ret = hdt.ToArray();
                            
                        }

                    }

                }

            }

            catch (Exception e)
            {
                ret = new byte[0];
                err = e.Message;
            }

            return ret;

        }

        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="data">data</param>
        /// <param name="v2f">v2f</param>
        /// <param name="ie">ie</param>
        /// <param name="err">err</param>
        /// <returns>string</returns>
        public static string ConvertD(
            byte[] data,
            byte[] v2f,
            byte[] ie,
            out string err
            )
        {

            string ret = "";

            err = "";

            try
            {

                using (Aes lol = Aes.Create())
                {

                    lol.Key = v2f;
                    lol.IV = ie;

                    ICryptoTransform det = lol.CreateDecryptor(lol.Key, lol.IV);

                    using (MemoryStream idr = new MemoryStream(data))
                    {

                        using (CryptoStream con = new CryptoStream(idr, det, CryptoStreamMode.Read))
                        {

                            using (StreamReader htp = new StreamReader(con))
                            {
                                ret = htp.ReadToEnd();
                            }

                        }

                    }

                }

            }

            catch (Exception e)
            {
                err = e.Message;
            }

            return ret;

        }

    }

}
