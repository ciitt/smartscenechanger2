using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static functions for Vector
    /// </summary>
    public static class VectorFuncs
    {

        /// <summary>
        /// Rotate a point around pivot
        /// </summary>
        /// <param name="point">point to rotate</param>
        /// <param name="pivot">pivot</param>
        /// <param name="quat">quat</param>
        /// <returns>new point</returns>
        // -------------------------------------------------------------------------------------------
        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion quat)
        {
            return (quat * (point - pivot)) + pivot;
        }

        /// <summary>
        /// Find closest position on a line
        /// </summary>
        /// <param name="position">input position</param>
        /// <param name="linePosA">line position to start</param>
        /// <param name="linePosB">line position to end</param>
        /// <param name="clamp">clamp</param>
        /// <returns>found position</returns>
        // ------------------------------------------------------------------------------------------------
        public static Vector3 FindClosestPositionOnLine(
            Vector3 position,
            Vector3 linePosA,
            Vector3 linePosB,
            bool clamp
            )
        {

            Vector3 ret = linePosA;

            Vector3 vecAtoB = linePosB - linePosA;
            Vector3 vecAtoP = position - linePosA;

            float sqrMagnitudeAtoB = vecAtoB.sqrMagnitude;

            if (sqrMagnitudeAtoB > 0.0f)
            {

                float dot = Vector3.Dot(vecAtoB, vecAtoP) / sqrMagnitudeAtoB;

                ret =
                    clamp ?
                    Vector3.Lerp(linePosA, linePosB, dot) :
                    Vector3.LerpUnclamped(linePosA, linePosB, dot)
                    ;

            }

            return ret;

        }

        /// <summary>
        /// Wedge product for Vector2
        /// </summary>
        /// <param name="lhs">left value</param>
        /// <param name="rhs">right value</param>
        /// <returns>wedge</returns>
        // ------------------------------------------------------------------------------------------------
        public static float Wedge(
            Vector2 lhs,
            Vector2 rhs
            )
        {
            return (lhs.x * rhs.y) - (rhs.x * lhs.y);
        }

        /// <summary>
        /// Wedge product for Vector3
        /// </summary>
        /// <param name="lhs">left value</param>
        /// <param name="rhs">right value</param>
        /// <returns>wedge</returns>
        // ------------------------------------------------------------------------------------------------
        public static float Wedge(
            Vector3 lhs,
            Vector3 rhs
            )
        {
            return
                (lhs.x * rhs.y) - (rhs.x * lhs.y) +
                (lhs.y * rhs.z) - (rhs.y * lhs.z) +
                (lhs.z * rhs.x) - (rhs.z * lhs.x)
                ;
        }

        /// <summary>
        /// Calculdate the intersection point
        /// </summary>
        /// <param name="_posA0">start position for the line A</param>
        /// <param name="_posA1">end position for the line A</param>
        /// <param name="_posB0">start position for the line B</param>
        /// <param name="_posB1">end position for the line B</param>
        /// <param name="dest">destination</param>
        /// <param name="thresholdAngle">value to check if parallel</param>
        /// <returns>found</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool CalcIntersectionPoint2D(
            Vector2 _posA0,
            Vector2 _posA1,
            Vector2 _posB0,
            Vector2 _posB1,
            ref Vector2 dest,
            float thresholdAngle = 0.01f
            )
        {

            bool ret = false;

            Vector2 vecA = _posA1 - _posA0;
            Vector2 vecB = _posB1 - _posB0;

            if (Vector2.Angle(vecA, vecB) > thresholdAngle)
            {

                float cross0 = Wedge(_posA1 - _posA0, _posB0 - _posA0);
                float cross1 = Wedge(_posA1 - _posA0, _posA0 - _posB1);

                if (
                    !Mathf.Approximately(cross0, 0.0f) &&
                    !Mathf.Approximately(cross1, 0.0f) &&
                    !Mathf.Approximately(cross0 + cross1, 0.0f)
                    )
                {

                    float t = cross0 / (cross0 + cross1);

                    dest = Vector2.LerpUnclamped(_posB0, _posB1, t);

                    ret = true;

                }

            }

            return ret;

        }

        /// <summary>
        /// Calculdate the intersection point
        /// </summary>
        /// <param name="_posA0">start position for the line A</param>
        /// <param name="_posA1">end position for the line A</param>
        /// <param name="_posB0">start position for the line B</param>
        /// <param name="_posB1">end position for the line B</param>
        /// <param name="dest">destination</param>
        /// <param name="thresholdMeter">value to check if close enough</param>
        /// <param name="thresholdAngle">value to check if parallel</param>
        /// <returns>found</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool CalcIntersectionPoint3D(
            Vector3 _posA0,
            Vector3 _posA1,
            Vector3 _posB0,
            Vector3 _posB1,
            ref Vector3 dest,
            float thresholdMeter = 0.001f,
            float thresholdAngle = 0.01f
            )
        {

            bool ret = false;

            Vector3 vecA = _posA1 - _posA0;
            Vector3 vecB = _posB1 - _posB0;

            if (Vector3.Angle(vecA, vecB) > thresholdAngle)
            {

                float cross0 = Wedge(_posA1 - _posA0, _posB0 - _posA0);
                float cross1 = Wedge(_posA1 - _posA0, _posA0 - _posB1);

                if (
                    !Mathf.Approximately(cross0, 0.0f) &&
                    !Mathf.Approximately(cross1, 0.0f) &&
                    !Mathf.Approximately(cross0 + cross1, 0.0f)
                    )
                {
                    
                    float t = cross0 / (cross0 + cross1);

                    Vector3 candidate = Vector3.LerpUnclamped(_posB0, _posB1, t);

                    Vector3 closest = FindClosestPositionOnLine(candidate, _posA0, _posA1, false);

                    float thresholdMeter2 = thresholdMeter * thresholdMeter;

                    if ((candidate - closest).sqrMagnitude <= thresholdMeter2)
                    {
                        dest = candidate;
                        ret = true;
                    }

                }

            }

            return ret;

        }

        /// <summary>
        /// Get value by axis
        /// </summary>
        /// <param name="val">input</param>
        /// <param name="axis">axis</param>
        /// <returns>value</returns>
        // ------------------------------------------------------------------------------------------------
        public static float GetValueByAxis(
            Vector3 val,
            Axis axis
            )
        {

            switch (axis)
            {
                case Axis.X: return val.x;
                case Axis.Y: return val.y;
                default: return val.z;
            }

        }

        /// <summary>
        /// Get direction by axis
        /// </summary>
        /// <param name="axis">axis</param>
        /// <returns>value</returns>
        // ------------------------------------------------------------------------------------------------
        public static Vector3 GetDirectionByAxis(Axis axis)
        {

            switch (axis)
            {
                case Axis.X: return Vector3.right;
                case Axis.Y: return Vector3.up;
                default: return Vector3.forward;
            }

        }

        /// <summary>
        /// Check if pass through
        /// </summary>
        /// <param name="targetPos">target position to check</param>
        /// <param name="targetDir">target direction on targetPos</param>
        /// <param name="posFrom">position from</param>
        /// <param name="posTo">position to</param>
        /// <param name="closeEnoughThreshold">threshold to be close enough</param>
        /// <returns>yes</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool CheckPassThrough(
            Vector3 targetPos,
            Vector3 targetDir,
            Vector3 posFrom,
            Vector3 posTo,
            float closeEnoughThreshold = 0.1f
            )
        {

            bool ret = false;

            closeEnoughThreshold *= closeEnoughThreshold;

            Vector3 valA = targetPos - posFrom;
            Vector3 valB = targetPos - posTo;

            if (
                valA.sqrMagnitude <= closeEnoughThreshold ||
                valB.sqrMagnitude <= closeEnoughThreshold
                )
            {
                ret = true;
            }

            else
            {

                float dotA = Vector3.Dot(valA, targetDir);
                float dotB = Vector3.Dot(valB, targetDir);

                ret = (dotA * dotB) <= 0.0f;

            }

            return ret;

        }

        /// <summary>
        /// Clamp rotation vector from 0 to 360
        /// </summary>
        /// <param name="input">input</param>
        /// <returns>rotation</returns>
        // ------------------------------------------------------------------------------------------------
        public static Vector3 ClampRotation0to360(Vector3 input)
        {

            float x = (input.x >= 0.0f) ? input.x % 360.0f : 360.0f + (input.x % 360.0f);
            float y = (input.y >= 0.0f) ? input.y % 360.0f : 360.0f + (input.y % 360.0f);
            float z = (input.z >= 0.0f) ? input.z % 360.0f : 360.0f + (input.z % 360.0f);

            return new Vector3(x, y, z);

        }

        /// <summary>
        /// Clamp rotation vector from -180 to 180
        /// </summary>
        /// <param name="input">input</param>
        /// <returns>rotation</returns>
        // ------------------------------------------------------------------------------------------------
        public static Vector3 ClampRotationN180toP180(Vector3 input)
        {

            Vector3 ret = ClampRotation0to360(input);

            if (ret.x >= 180.0f) { ret.x = ret.x - 360.0f; }
            if (ret.y >= 180.0f) { ret.y = ret.y - 360.0f; }
            if (ret.z >= 180.0f) { ret.z = ret.z - 360.0f; }

            return ret;

        }

    }

}
