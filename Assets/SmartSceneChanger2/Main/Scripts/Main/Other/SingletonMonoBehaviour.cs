﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Singleton MonoBehaviour
    /// </summary>
    /// <typeparam name="T">class</typeparam>
    public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
    {

        /// <summary>
        /// True to use DontDestroyOnLoad
        /// </summary>
        [SerializeField]
        [Tooltip("True to use DontDestroyOnLoad")]
        protected bool m_dontDestroyOnLoad = true;

        /// <summary>
        /// Singleton instance
        /// </summary>
        protected static T instance = null;

        /// <summary>
        /// Now quitting
        /// </summary>
        protected static bool isQuitting { get; set; } = false;

        //-----------------------------------------------------------------------

        /// <summary>
        /// static T instance
        /// </summary>
        public static T Instance
        {

            get
            {

                if (
                    instance == null &&
                    !isQuitting
                    )
                {

                    instance = FindObjectOfType<T>();

                    if (instance == null)
                    {
                        instance = CreateInstance();
                    }
                    
                }

                return instance;

            }

        }

        /// <summary>
        /// Awake()
        /// </summary>
        //-----------------------------------------------------------------------
        protected virtual void Awake()
        {

            if (instance == null)
            {

                instance = (T)this;

                if (this.m_dontDestroyOnLoad)
                {
                    DontDestroyOnLoad(this.transform.root.gameObject);
                }

            }

        }

        /// <summary>
        /// OnApplicationQuit
        /// </summary>
        //-----------------------------------------------------------------------
        protected virtual void OnApplicationQuit()
        {
            isQuitting = true;
        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        //-----------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if (instance == this)
            {
                instance = null;
            }

        }

        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <returns>instance</returns>
        //-----------------------------------------------------------------------
        protected static T CreateInstance()
        {

            GameObject go = new GameObject();

            go.name = typeof(T).Name;

#if UNITY_EDITOR

            Debug.LogFormat("Created an instance of [{0}]", go.name);

#endif

            return go.AddComponent<T>();

        }

    }

}
