﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace SSC2
{

    /// <summary>
    /// Static functions for web
    /// </summary>
    public static class WebFuncs
    {

        /// <summary>
        /// IMultipartFormSection list for http post
        /// </summary>
        static List<IMultipartFormSection> _postFormData = null;

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// IMultipartFormSection list for http post
        /// </summary>
        static List<IMultipartFormSection> postFormDataInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _postFormData); }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Http dummy fail for debug purpose (EditorOnly)
        /// </summary>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="dummyResponseCode">dummy response code</param>
        /// <param name="dummyErrorMessage">dummy error message</param>
        /// <returns></returns>
        // -------------------------------------------------------------------------------------------------------------------
        public static IEnumerator HttpDummyFailEditorOnly(
            Action<long, string> errorCallback,
            long dummyResponseCode,
            string dummyErrorMessage
            )
        {

            yield return null;

            if (errorCallback != null)
            {
                errorCallback(dummyResponseCode, dummyErrorMessage);
            }

        }

#endif

        /// <summary>
        /// Http post file data
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="data">data</param>
        /// <param name="fieldName">field name</param>
        /// <param name="fieldName">file name</param>
        /// <param name="contentType">content type</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        public static IEnumerator HttpPostFileData(
            string url,
            byte[] data,
            string fieldName,
            string fileName,
            string contentType,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            List<IMultipartFormSection> postFormData = postFormDataInstance;

            // clear
            {
                postFormData.Clear();
            }

            // add
            {
                postFormData.Add(new MultipartFormFileSection(fieldName, data, fileName, contentType));
            }

            // post
            {
                yield return HttpPost(url, postFormData, successCallback, errorCallback, timeoutSeconds, requestHeaders);
            }

            // clear
            {
                postFormData.Clear();
            }

        }

        /// <summary>
        /// Http post common
        /// </summary>
        /// <param name="uwr">UnityWebRequest</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns></returns>
        // -------------------------------------------------------------------------------------------------------------------
        static IEnumerator HttpGetCommonIE(
            UnityWebRequest uwr,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            if (uwr == null)
            {
                yield break;
            }

            // -------------------------------

            // set
            {

                uwr.timeout = timeoutSeconds;

                foreach ((string, string) val in requestHeaders)
                {
                    uwr.SetRequestHeader(val.Item1, val.Item2);
                }

            }

            // start and wait
            {

                UnityWebRequestAsyncOperation ao = uwr.SendWebRequest();

                while (!ao.isDone)
                {
                    yield return null;
                }

            }

            //
            {

                if (uwr.result == UnityWebRequest.Result.Success)
                {
                    if (successCallback != null)
                    {
                        successCallback(uwr.downloadHandler);
                    }
                }

                else
                {
                    if (errorCallback != null)
                    {
                        errorCallback(uwr.responseCode, uwr.error);
                    }
                }

            }

        }

        /// <summary>
        /// Http post common
        /// </summary>
        /// <param name="uwr">UnityWebRequest</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        static IEnumerator HttpPostCommonIE(
            UnityWebRequest uwr,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            if (uwr == null)
            {
                yield break;
            }

            // -------------------------------

            // set
            {

                uwr.timeout = timeoutSeconds;

                foreach ((string, string) val in requestHeaders)
                {
                    uwr.SetRequestHeader(val.Item1, val.Item2);
                }

            }

            // start and wait
            {

                UnityWebRequestAsyncOperation ao = uwr.SendWebRequest();

                while (!ao.isDone)
                {
                    yield return null;
                }
                
            }

            //
            {

                if (uwr.result == UnityWebRequest.Result.Success)
                {
                    if (successCallback != null)
                    {
                        successCallback(uwr.downloadHandler);
                    }
                }

                else
                {
                    if (errorCallback != null)
                    {
                        errorCallback(uwr.responseCode, uwr.error);
                    }
                }

            }

        }

        /// <summary>
        /// Http post
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="data">data</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        public static IEnumerator HttpPost(
            string url,
            string data,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            using (UnityWebRequest uwr = UnityWebRequest.PostWwwForm(url, data))
            {

                yield return HttpPostCommonIE(
                    uwr,
                    successCallback,
                    errorCallback,
                    timeoutSeconds,
                    requestHeaders
                    );

            }

        }

        /// <summary>
        /// Http post json
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="json">json</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        public static IEnumerator HttpPostJson(
            string url,
            string json,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            using (UnityWebRequest uwr = new UnityWebRequest(url, "POST"))
            {

                byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

                using (UploadHandlerRaw uploadHandler = new UploadHandlerRaw(jsonToSend))
                {

                    uwr.uploadHandler = uploadHandler;

                    yield return HttpPostCommonIE(
                        uwr,
                        successCallback,
                        errorCallback,
                        timeoutSeconds,
                        requestHeaders
                        );

                }

            }

        }

        /// <summary>
        /// Http post
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="postFormData">data</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        static IEnumerator HttpPost(
            string url,
            List<IMultipartFormSection> postFormData,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            using (UnityWebRequest uwr = UnityWebRequest.Post(url, postFormData))
            {

                yield return HttpPostCommonIE(
                    uwr,
                    successCallback,
                    errorCallback,
                    timeoutSeconds,
                    requestHeaders
                    );

            }

        }

        /// <summary>
        /// Http Get
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="successCallback">callback for success</param>
        /// <param name="errorCallback">callback for error (response code, error message)</param>
        /// <param name="timeoutSeconds">seconds to timeout</param>
        /// <param name="requestHeaders">request headers</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------------------------------
        public static IEnumerator HttpGet(
            string url,
            Action<DownloadHandler> successCallback,
            Action<long, string> errorCallback,
            int timeoutSeconds,
            params (string, string)[] requestHeaders
            )
        {

            // get
            {

                using (UnityWebRequest uwr = UnityWebRequest.Get(url))
                {

                    yield return HttpGetCommonIE(
                        uwr,
                        successCallback,
                        errorCallback,
                        timeoutSeconds,
                        requestHeaders
                        );

                }

            }

        }

    }

}
