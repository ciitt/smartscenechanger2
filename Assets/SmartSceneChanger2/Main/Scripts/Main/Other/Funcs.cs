﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static Functions
    /// </summary>
    public static class Funcs
    {

        /// <summary>
        /// StringBuilder
        /// </summary>
        static StringBuilder _sb = null;

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// StringBuilder
        /// </summary>
        static StringBuilder sbInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _sb); }
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Compress the data
        /// </summary>
        /// <param name="bytes">data</param>
        /// <param name="error">error</param>
        /// <returns>compressed data</returns>
        // -------------------------------------------------------------------------------------------
        public static byte[] Compress(
            byte[] data,
            out string error
            )
        {

            byte[] ret = null;

            error = "";

            try
            {

                using (MemoryStream ms = new MemoryStream())
                {

                    using (var zs = new GZipStream(ms, CompressionMode.Compress))
                    {
                        zs.Write(data, 0, data.Length);
                        zs.Close();
                        ret = ms.ToArray();
                    }

                }

            }

            catch (Exception e)
            {
                ret = new byte[0];
                error = e.Message;
            }

            return ret;

        }

        /// <summary>
        /// Decompress the data
        /// </summary>
        /// <param name="bytes">compressed data</param>
        /// <param name="error">error</param>
        /// <returns>decompress data</returns>
        public static byte[] Decompress(
            byte[] compressedData,
            out string error
            )
        {

            byte[] ret = null;
            
            error = "";

            try
            {

                using (MemoryStream msi = new MemoryStream(compressedData))
                {

                    using (var zs = new GZipStream(msi, CompressionMode.Decompress))
                    {

                        using (MemoryStream mso = new MemoryStream())
                        {
                            zs.CopyTo(mso);
                            zs.Close();
                            ret = mso.ToArray();
                        }

                    }

                }

            }

            catch (Exception e)
            {
                ret = new byte[0];
                error = e.Message;
            }

            return ret;

        }

        /// <summary>
        /// Create the instance if needed
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="dest">destination</param>
        /// <returns>instance</returns>
        // -------------------------------------------------------------------------------------------
        public static T CreateInstanceIfNeeded<T>(ref T dest) where T : new()
        {

            if (dest == null)
            {
                dest = new T();
            }

            return dest;

        }

        /// <summary>
        /// Get a component if needed
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="mb">mb</param>
        /// <param name="dest">destination</param>
        /// <returns>instance</returns>
        // -------------------------------------------------------------------------------------------
        public static T GetComponentIfNeeded<T>(MonoBehaviour mb, ref T dest) where T : Component
        {

            if (mb && dest == null)
            {
                dest = mb.GetComponent<T>();
            }

            return dest;

        }

        /// <summary>
        /// Create hierachy path
        /// </summary>
        /// <param name="trans">Transform</param>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------------
        public static string CreateHierarchyPath(Transform trans)
        {

            if (!trans)
            {
                return "";
            }

            // --------------------------

            StringBuilder sb = sbInstance;

            string ret = "";

            // --------------------------

            // clear
            {
                sb.Length = 0;
            }

            // set
            {

                sb.AppendFormat("/{0}", trans.name);

                Transform temp = trans.parent;

                while (temp != null)
                {
                    sb.Insert(0, string.Format("/{0}", temp.name));
                    temp = temp.parent;
                }

                ret = sb.ToString();

            }

            // clear
            {
                sb.Length = 0;
            }

            return ret;

        }

        /// <summary>
        /// Resize texture
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="compressHighQuality">compress with high quality</param>
        /// <param name="updateMipmaps">updateMipmaps</param>
        /// <param name="makeNoLongerReadable">makeNoLongerReadable</param>
        // ---------------------------------------------------------------------------
        public static void ResizeTexture(
            Texture2D source,
            Texture2D dest,
            bool compressHighQuality = true,
            bool updateMipmaps = true,
            bool makeNoLongerReadable = false
            )
        {

            if (
                !source ||
                !dest
                )
            {
                return;
            }

            // --------------------

            bool compressable = (dest.width % 4 == 0 && dest.height % 4 == 0);

            // --------------------

            if (!compressable)
            {
                Debug.LogWarningFormat(
                    "width and height should be multiples of 4 to be able to compress : {0}, {1}",
                    dest.width,
                    dest.height
                    );
            }

            // --------------------

            int newWidth = dest.width;
            int newHeight = dest.height;

            // --------------------

            RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
            rt.filterMode = source.filterMode;
            rt.wrapMode = source.wrapMode;
            RenderTexture.active = rt;
            Graphics.Blit(source, rt);

            dest.ReadPixels(new Rect(0, 0, newWidth, newHeight), 0, 0, updateMipmaps);

            if (compressable)
            {
                dest.Compress(compressHighQuality);
            }
            
            dest.Apply(updateMipmaps, makeNoLongerReadable);

            RenderTexture.active = null;
            RenderTexture.ReleaseTemporary(rt);

        }

        /// <summary>
        /// Resize texture
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="compressHighQuality">compress with high quality</param>
        /// <param name="updateMipmaps">updateMipmaps</param>
        /// <param name="makeNoLongerReadable">makeNoLongerReadable</param>
        /// <returns>IEnumerator</returns>
        // ---------------------------------------------------------------------------
        public static IEnumerator ResizeTextureIE(
            Texture2D source,
            Texture2D dest,
            bool compressHighQuality = true,
            bool updateMipmaps = true,
            bool makeNoLongerReadable = false
            )
        {

            if (
                !source ||
                !dest
                )
            {
                yield break;
            }

            if (dest.mipmapCount != 1)
            {
                Debug.LogWarning("mipChain of dest texture must be false");
                yield break;
            }

            // --------------------

            bool compressable = (dest.width % 4 == 0 && dest.height % 4 == 0);

            // --------------------

            if (!compressable)
            {
                Debug.LogWarningFormat(
                    "width and height should be multiples of 4 to be able to compress : {0}, {1}",
                    dest.width,
                    dest.height
                    );
            }

            // --------------------

            int newWidth = dest.width;
            int newHeight = dest.height;

            // --------------------

            RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
            rt.filterMode = source.filterMode;
            rt.wrapMode = source.wrapMode;
            RenderTexture.active = rt;
            Graphics.Blit(source, rt);

            UnityEngine.Rendering.AsyncGPUReadbackRequest request =
                UnityEngine.Rendering.AsyncGPUReadback.Request(rt, 0, dest.format);

            while (!request.done)
            {
                yield return null;
            }

            dest.LoadRawTextureData(request.GetData<Color32>());

            if (compressable)
            {
                dest.Compress(compressHighQuality);
            }

            dest.Apply(updateMipmaps, makeNoLongerReadable);

            RenderTexture.active = null;
            RenderTexture.ReleaseTemporary(rt);

        }

        /// <summary>
        /// Is position visible by vamera
        /// </summary>
        /// <param name="camera">camera</param>
        /// <param name="position">position to check</param>
        /// <param name="marginScreenXY">margin XY for WorldToViewportPoint</param>
        /// <param name="marginMeterZ">margin Z for WorldToViewportPoint</param>
        /// <returns>yes</returns>
        // ---------------------------------------------------------------------------
        public static bool IsPositionVisibleByCamera(
            Camera camera,
            Vector3 position,
            float marginScreenXY,
            float marginMeterZ
            )
        {

            if (!camera)
            {
                return false;
            }

            // -----------------------

            Vector3 viewPort = camera.WorldToViewportPoint(position);

            // -----------------------

            return
                -marginScreenXY <= viewPort.x && viewPort.x <= 1.0f + marginScreenXY &&
                -marginScreenXY <= viewPort.y && viewPort.y <= 1.0f + marginScreenXY &&
                -marginMeterZ <= viewPort.z && viewPort.z <= camera.farClipPlane + marginMeterZ
                ;

        }

        /// <summary>
        /// Wait seconds
        /// </summary>
        /// <param name="seconds">seconds to wait</param>
        /// <returns>IEnumerator</returns>
        // ---------------------------------------------------------------------------
        public static IEnumerator WaitSeconds(float seconds)
        {

            float timer = 0.0f;

            while (timer < seconds)
            {
                timer += Time.deltaTime;
                yield return null;
            }

        }

        /// <summary>
        /// Recycle list
        /// </summary>
        /// <typeparam name="T">class</typeparam>
        /// <param name="list">target list</param>
        /// <param name="size">size to resize</param>
        /// <param name="destroyIfRemoved">destroy the asset if removed from the list</param>
        // -------------------------------------------------------------------------------------------
        public static void RecycleAssetList<T>(
            List<T> list,
            int size,
            bool destroyIfRemoved
            ) where T : UnityEngine.Object, new()
        {

            size = Mathf.Max(0, size);

            // remove null
            {

                for (int i = list.Count - 1; i >= 0; i--)
                {

                    if (!list[i])
                    {
                        list.RemoveAt(i);
                    }

                }

            }

            // remove
            {

                for (int i = list.Count - 1; i >= size; i--)
                {

                    if (destroyIfRemoved)
                    {
                        DestroyObject(list[i] as UnityEngine.Object);
                    }

                    list.RemoveAt(i);

                }

            }

            // Capacity
            {
                list.Capacity = size;
            }

            // add
            {

                for (int i = list.Count; i < size; i++)
                {
                    list.Add(new T());
                }

            }

        }

        /// <summary>
        /// Recycle list
        /// </summary>
        /// <typeparam name="T">class</typeparam>
        /// <param name="list">target list</param>
        /// <param name="size">size to resize</param>
            // -------------------------------------------------------------------------------------------
        public static void RecycleList<T>(
            List<T> list,
            int size
            ) where T : new()
        {

            size = Mathf.Max(0, size);

            // remove
            {
                if (list.Count > size)
                {
                    list.RemoveRange(size, list.Count - size);
                }
            }

            // Capacity
            {
                list.Capacity = size;
            }

            // add
            {
                for (int i = list.Count; i < size; i++)
                {
                    list.Add(new T());
                }
            }

        }

        /// <summary>
        /// Recycle list
        /// </summary>
        /// <typeparam name="T">struct</typeparam>
        /// <param name="list">target list</param>
        /// <param name="value">value</param>
        /// <param name="size">size to resize</param>
        // -------------------------------------------------------------------------------------------
        public static void RecycleList<T>(
            List<T> list,
            T value,
            int size
            ) where T : struct
        {

            size = Mathf.Max(0, size);

            // remove
            {
                if (list.Count > size)
                {
                    list.RemoveRange(size, list.Count - size);
                }
            }

            // Capacity
            {
                list.Capacity = size;
            }

            // add
            {
                for (int i = list.Count; i < size; i++)
                {
                    list.Add(value);
                }
            }

        }

        /// <summary>
        /// Convert string to date time
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="dest">destination</param>
        /// <param name="format">format</param>
        /// <returns>error</returns>
        // -------------------------------------------------------------------------------------------
        public static string ConvertStringToDateTime(
            string text,
            ref DateTime dest,
            string format = "yyyyMMddHHmmss"
            )
        {

            return ConvertStringToDateTime(
                text,
                ref dest,
                CultureInfo.InvariantCulture,
                format
                );

        }

        /// <summary>
        /// Convert string to date time
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="dest">destination</param>
        /// <param name="formatProvider">IFormatProvider</param>
        /// <param name="format">format</param>
        /// <returns>error</returns>
        // -------------------------------------------------------------------------------------------
        public static string ConvertStringToDateTime(
            string text,
            ref DateTime dest,
            IFormatProvider formatProvider,
            string format = "yyyyMMddHHmmss"
            )
        {

            string error = "";

            try
            {
                dest = DateTime.ParseExact(text, format, formatProvider);
            }

            catch (Exception e)
            {
                error = e.Message;
            }

            return error;

        }

        /// <summary>
        /// Convert date time to string
        /// </summary>
        /// <param name="dateTime">date time</param>
        /// <param name="dest">destination</param>
        /// <param name="format">format</param>
        /// <returns>error</returns>
        // -------------------------------------------------------------------------------------------
        public static string ConvertDateTimeToString(
            DateTime dateTime,
            ref string dest,
            string format = "yyyyMMddHHmmss"
            )
        {

            string error = "";

            try
            {
                dest = dateTime.ToString(format);
            }

            catch (Exception e)
            {
                error = e.Message;
            }

            return error;

        }

        /// <summary>
        /// Check if the GameObject is in the scene of DontDestroyOnLoad
        /// </summary>
        /// <param name="gameObject">target to check</param>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CheckDontDestroy(GameObject gameObject)
        {

            return
                (gameObject) ?
                gameObject.scene.name == "DontDestroyOnLoad" :
                false
                ;

        }

        /// <summary>
        /// Convert string to byte array
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="encoding">encoding</param>
        /// <param name="error">error</param>
        /// <returns>byte array</returns>
        // -------------------------------------------------------------------------------------------
        public static byte[] ConvertStringToBytes(
            string text,
            Encoding encoding,
            out string error
            )
        {

            byte[] ret = null;

            error = "";

            try
            {
                ret = encoding.GetBytes(text);
            }

            catch (Exception e)
            {
                error = e.Message;
            }

            return ret;

        }

        /// <summary>
        /// Convert byte array to string
        /// </summary>
        /// <param name="bytes">byte array</param>
        /// <param name="encoding">encoding</param>
        /// <param name="error">error</param>
        /// <returns>string</returns>
        // -------------------------------------------------------------------------------------------
        public static string ConvertBytesToString(
            byte[] bytes,
            Encoding encoding,
            out string error
            )
        {

            string ret = "";

            error = "";

            try
            {
                ret = encoding.GetString(bytes);
            }

            catch (Exception e)
            {
                error = e.Message;
            }

            return ret;

        }

        /// <summary>
        /// Convert byte array to string
        /// </summary>
        /// <param name="bytes">byte array</param>
        /// <returns>string</returns>
        // -------------------------------------------------------------------------------------------
        public static string ConvertBytesToStringAsItIs(byte[] bytes)
        {

            StringBuilder sb = sbInstance;

            string ret = "";

            // --------------------------

            // clear
            {
                sb.Length = 0;
            }

            // set
            {

                if (bytes.Length > 0)
                {
                    sb.Append(bytes[0]);
                }

                for (int i = 1; i < bytes.Length; i++)
                {
                    sb.AppendFormat(",{0}", bytes[i]);
                }

                ret = sb.ToString();

            }

            // clear
            {
                sb.Length = 0;
            }

            return ret;

        }

        /// <summary>
        /// Convert string to byte array
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="error">error</param>
        /// <returns>byte array</returns>
        // -------------------------------------------------------------------------------------------
        public static byte[] ConvertStringAsItIsToBytes(
            string text,
            out string error
            )
        {

            string[] split = text.Split(',');

            byte[] ret = new byte[split.Length];

            error = "";

            //
            {

                try
                {

                    for (int i = 0; i < ret.Length; i++)
                    {
                        ret[i] = byte.Parse(split[i]);
                    }

                }

                catch (Exception e)
                {
                    ret = new byte[0];
                    error = e.Message;
                }

            }

            return ret;

        }

        /// <summary>
        /// Check if layer-mask contains the layer of the object
        /// </summary>
        /// <param name="layerMask">LayerMask</param>
        /// <param name="gameObject">GameObject</param>
        /// <returns></returns>
        // -------------------------------------------------------------------------------------------
        public static bool CheckLayerMatches(
            LayerMask layerMask,
            GameObject gameObject
            )
        {

            return
                gameObject ?
                layerMask == (layerMask | (1 << gameObject.layer)) :
                false
                ;

        }

        /// <summary>
        /// Destroy an object (Destroy if runtime, DestroyImmediate if not)
        /// </summary>
        /// <param name="obj">object</param>
        // -------------------------------------------------------------------------------------------
        public static void DestroyObject(UnityEngine.Object obj)
        {

            if (Application.isPlaying)
            {
                UnityEngine.Object.Destroy(obj);
            }

            else
            {
                UnityEngine.Object.DestroyImmediate(obj);
            }

        }

    }

}
