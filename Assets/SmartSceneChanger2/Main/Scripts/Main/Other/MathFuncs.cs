using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static functions for Math
    /// </summary>
    public static class MathFuncs
    {

        /// <summary>
        /// Calculate count to converge
        /// </summary>
        /// <param name="lerp01">lerp value as 01</param>
        /// <param name="epsilon">epsilon</param>
        /// <param name="limitCount">limit count</param>
        /// <returns>count</returns>
        // -------------------------------------------------------------------------------------------
        public static int CalcConvergenceLerpCount(
            float lerp01,
            float epsilon = 0.00001f,
            int limitCount = 10000
            )
        {

            int count = 0;

            float result = 1.0f;

            while (count++ < limitCount)
            {

                result = Mathf.Lerp(result, 0.0f, lerp01);

                if (result <= epsilon)
                {
                    break;
                }

            }

            return count;

        }

        /// <summary>
        /// Convert ms to kmh
        /// </summary>
        /// <returns>kmh</returns>
        // -------------------------------------------------------------------------------------------
        public static float ConvertMsToKmh(float ms)
        {
            return ms * 3.6f;
        }

        /// <summary>
        /// Convert kmh to ms
        /// </summary>
        /// <returns>ms</returns>
        // -------------------------------------------------------------------------------------------
        public static float ConvertKmhToMs(float kmh)
        {
            return kmh / 3.6f;
        }

        /// <summary>
        /// Remap
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="fromA">fromA</param>
        /// <param name="toA">toA</param>
        /// <param name="fromB">fromB</param>
        /// <param name="toB">toB</param>
        /// <returns>value</returns>
        // -------------------------------------------------------------------------------------------
        public static float Remap(
            float value,
            float fromA,
            float toA,
            float fromB,
            float toB
            )
        {
            return Mathf.Lerp(fromB, toB, Mathf.InverseLerp(fromA, toA, value));
        }

        /// <summary>
        /// -(x - 1)~2 + 1
        /// </summary>
        /// <param name="x">x</param>
        /// <returns>y</returns>
        // -------------------------------------------------------------------------------------------
        public static float CalcTweenTypeA(float x)
        {
            return 1 - ((x - 1) * (x - 1));
        }

    }

}
