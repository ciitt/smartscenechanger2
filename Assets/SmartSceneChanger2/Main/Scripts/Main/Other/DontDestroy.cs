﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// DontDestroyOnLoad
    /// </summary>
    public class DontDestroy : MonoBehaviour
    {

        /// <summary>
        /// Awake()
        /// </summary>
        void Awake()
        {
            DontDestroyOnLoad(this.transform.root.gameObject);
        }

    }

}
