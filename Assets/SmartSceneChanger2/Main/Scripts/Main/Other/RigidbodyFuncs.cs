using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Static functions for Rigidbody
    /// </summary>
    public static class RigidbodyFuncs
    {

        /// <summary>
        /// Get speed as kilometer per hour
        /// </summary>
        /// <param name="rigidbody">Rigidbody</param>
        /// <returns>kilometer per hour</returns>
        // -------------------------------------------------------------------------------------------
        public static float GetSpeedKmh(Rigidbody rigidbody)
        {
            return MathFuncs.ConvertMsToKmh(GetSpeedMs(rigidbody));
        }

        /// <summary>
        /// Get speed as meter per second
        /// </summary>
        /// <param name="rigidbody">Rigidbody</param>
        /// <returns>meter per second</returns>
        // -------------------------------------------------------------------------------------------
        public static float GetSpeedMs(Rigidbody rigidbody)
        {
            return rigidbody ? rigidbody.velocity.magnitude : 0.0f;
        }

        /// <summary>
        /// Get speed along forward as meter per second
        /// </summary>
        /// <param name="rigidbody">Rigidbody</param>
        /// <returns>meter per second</returns>
        // -------------------------------------------------------------------------------------------
        public static float GetForwardSignedSpeedMs(Rigidbody rigidbody)
        {

            float ret = 0.0f;

            if (rigidbody)
            {
                ret = Vector3.Dot(rigidbody.velocity, rigidbody.transform.forward);
            }

            return ret;

        }

    }

}
