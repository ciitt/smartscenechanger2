using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Singleton
    /// </summary>
    /// <typeparam name="T">class</typeparam>
    public abstract class Singleton<T> where T : Singleton<T>, new()
    {

        /// <summary>
        /// Singleton instance
        /// </summary>
        static T instance = null;

        /// <summary>
        /// static T instance
        /// </summary>
        public static T Instance
        {

            get
            {

                if (instance == null)
                {
                    instance = new T();
                }

                return instance;

            }

        }

    }

}
