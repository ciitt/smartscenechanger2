﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Csv parser
    /// </summary>
    public class CsvParser
    {

        /// <summary>
        /// StringBuilder
        /// </summary>
        static StringBuilder _sb = null;

        /// <summary>
        /// Cell list
        /// </summary>
        static List<List<string>> _cellList = null;

        /// <summary>
        /// Header dictionary
        /// </summary>
        static Dictionary<int, string> _headerDict = null;

        // ------------------------------------------------------------------------------------------------

        /// <summary>
        /// StringBuilder
        /// </summary>
        static StringBuilder sbInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _sb); }
        }

        /// <summary>
        /// Cell list
        /// </summary>
        static List<List<string>> cellListInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _cellList); }
        }

        /// <summary>
        /// Header dictionary
        /// </summary>
        static Dictionary<int, string> headerDictInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _headerDict); }
        }

        /// <summary>
        /// Check if all the col count are the same 
        /// </summary>
        /// <param name="cellList">list to check</param>
        /// <returns>valid</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool CheckCellListColCount(List<List<string>> cellList)
        {

            bool ret = true;

            if (cellList.Count > 0)
            {

                int colCount = cellList[0].Count;

                foreach (var row in cellList)
                {

                    if (row.Count != colCount)
                    {
                        ret = false;
                        break;
                    }

                }

            }

            return ret;

        }

        /// <summary>
        /// Get cells from csv text
        /// </summary>
        /// <param name="csv">csv string</param>
        /// <param name="destCellList">destination</param>
        /// <param name="punctuation">punctuation</param>
        /// <returns>success</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool Parse(
            string csv,
            List<List<string>> destCellList,
            char punctuation = ','
            )
        {

            StringBuilder sb = sbInstance;

            // clear
            {
                sb.Length = 0;
                destCellList.Clear();
            }

            // parse
            {

                List<string> oneRow = new List<string>();

                int totalQuoteCounter = 0;
                int consecutiveQuoteCounter = 0;

                foreach (char c in csv)
                {

                    if (c == '\"')
                    {

                        consecutiveQuoteCounter++;

                        if (consecutiveQuoteCounter % 2 == 0)
                        {
                            sb.Append(c);
                        }

                        totalQuoteCounter++;

                        continue;

                    }

                    // ---------------

                    consecutiveQuoteCounter = 0;

                    // ---------------

                    if (c == punctuation && totalQuoteCounter % 2 == 0)
                    {
                        oneRow.Add(sb.ToString());
                        sb.Length = 0;
                    }

                    else if (c == '\n' && totalQuoteCounter % 2 == 0)
                    {
                        oneRow.Add(sb.ToString());
                        sb.Length = 0;
                        destCellList.Add(oneRow);
                        oneRow = new List<string>();
                    }

                    else if (c != '\r')
                    {
                        sb.Append(c);
                    }

                }

            }

            // clear
            {
                sb.Length = 0;
            }

            return CheckCellListColCount(destCellList);

        }

        /// <summary>
        /// Get a dictionary organized by the top headers from csv text
        /// </summary>
        /// <param name="csv">csv string</param>
        /// <param name="destColDict">destination</param>
        /// <param name="punctuation">punctuation</param>
        /// <returns>success</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool Parse(
            string csv,
            Dictionary<string, List<string>> destColDict,
            char punctuation = ','
            )
        {

            StringBuilder sb = sbInstance;
            List<List<string>> cellList = cellListInstance;
            Dictionary<int, string> headerDict =headerDictInstance;

            // clear
            {

                sb.Length = 0;
                cellList.Clear();
                headerDict.Clear();

                destColDict.Clear();

            }

            // parse
            {

                // Parse
                {

                    bool valid = Parse(
                        csv,
                        cellList,
                        punctuation
                        );

                    if (!valid)
                    {
                        return false;
                    }

                }

                //
                {

                    if (cellList.Count > 0)
                    {

                        int rowCount = cellList.Count;
                        int colCount = cellList[0].Count;

                        int rowIndex = 0;
                        int colIndex = 0;

                        List<string> list = null;

                        for (colIndex = 0; colIndex < colCount; colIndex++)
                        {

                            list = new List<string>();

                            for (rowIndex = 1; rowIndex < rowCount; rowIndex++)
                            {
                                list.Add(cellList[rowIndex][colIndex]);
                            }

                            destColDict.Add(cellList[0][colIndex], list);

                        }

                    }

                }

            }

            // clear
            {
                sb.Length = 0;
                cellList.Clear();
                headerDict.Clear();
            }

            return true;

        }

        /// <summary>
        /// Get a dictionary organized by the top and left headers from csv text
        /// </summary>
        /// <param name="csv">csv string</param>
        /// <param name="destDict">destination</param>
        /// <param name="punctuation">punctuation</param>
        /// <returns>success</returns>
        // ------------------------------------------------------------------------------------------------
        public static bool Parse(
            string csv,
            Dictionary<(string, string), string> destDict,
            char punctuation = ','
            )
        {

            StringBuilder sb = sbInstance;
            List<List<string>> cellList = cellListInstance;

            // clear
            {

                sb.Length = 0;
                cellList.Clear();

                destDict.Clear();

            }

            // parse
            {

                // Parse
                {

                    bool valid = Parse(
                        csv,
                        cellList,
                        punctuation
                        );

                    if (!valid)
                    {
                        return false;
                    }

                }

                //
                {

                    if (cellList.Count > 0)
                    {

                        int rowCount = cellList.Count;
                        int colCount = cellList[0].Count;

                        int rowIndex = 0;
                        int colIndex = 0;

                        string keyInRow0 = "";
                        string keyInCol0 = "";

                        (string, string) key = ("", "");

                        for (rowIndex = 1; rowIndex < rowCount; rowIndex++)
                        {

                            colCount = cellList[rowIndex].Count;

                            keyInCol0 = cellList[rowIndex][0];

                            for (colIndex = 1; colIndex < colCount; colIndex++)
                            {

                                keyInRow0 = cellList[0][colIndex];

                                key = (keyInRow0, keyInCol0);

                                if (!destDict.ContainsKey(key))
                                {
                                    destDict.Add(key, cellList[rowIndex][colIndex]);
                                }

                                else
                                {
                                    Debug.LogErrorFormat("Duplicated key == {0} : {1}", keyInRow0, keyInCol0);
                                }

                            }

                        }

                    }

                }

            }

            // clear
            {
                sb.Length = 0;
                cellList.Clear();
            }

            return true;

        }

    }

}
