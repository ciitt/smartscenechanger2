using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to lookat
    /// </summary>
    public abstract class ObjectLookat : MonoBehaviour
    {

        /// <summary>
        /// Reference to target to lookat
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to target to lookat")]
        protected Transform m_refTarget = null;

        /// <summary>
        /// Speed
        /// </summary>
        [SerializeField]
        [Tooltip("Speed")]
        [Range(0.01f, 1.0f)]
        protected float m_speed = 0.1f;

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to target to lookat
        /// </summary>
        public Transform refTarget
        {
            get { return this.m_refTarget; }
            set { this.m_refTarget = value; }
        }

        /// <summary>
        /// Speed
        /// </summary>
        public float speed
        {
            get { return this.m_speed; }
            set { this.m_speed = value; }
        }

        /// <summary>
        /// Flag to follow quickly once
        /// </summary>
        public bool quickOnce { get; set; } = false;

        /// <summary>
        /// Lookat
        /// </summary>
        /// <param name="speed">speed</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void Lookat(float speed)
        {

            if (this.m_refTarget)
            {

                Vector3 toward = this.m_refTarget.position - this.transform.position;

                if (toward.sqrMagnitude > 0.0f)
                {

                    Quaternion rot = Quaternion.LookRotation(toward);

                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rot, speed);

                }

            }

        }

    }

}
