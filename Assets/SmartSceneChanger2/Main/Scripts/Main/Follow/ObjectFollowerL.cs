using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to follow an object
    /// </summary>
    [ExecuteAlways]
    public class ObjectFollowerL : ObjectFollower
    {

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // update
            {

                if (
                    this.quickOnce

#if UNITY_EDITOR
                    || !Application.IsPlaying(this.gameObject)
#endif

                    )
                {
                    this.UpdatePositionAndRotation(1.0f, 1.0f);
                }

                else
                {
                    this.UpdatePositionAndRotation(this.m_speedForPos, this.m_speedForRot);
                }

            }

            // reset
            {
                this.quickOnce = false;
            }

        }

    }

}
