using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to lookat
    /// </summary>
    [ExecuteAlways]
    public class ObjectLookatF : ObjectLookat
    {

        /// <summary>
        /// WaitForFixedUpdate
        /// </summary>
        protected WaitForFixedUpdate m_waitForFixedUpdate = new WaitForFixedUpdate();

#if UNITY_EDITOR

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            if (!Application.IsPlaying(this.gameObject))
            {
                this.Lookat(1.0f);
            }

        }

#endif

        /// <summary>
        /// OnEnable
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnEnable()
        {

            if (Application.IsPlaying(this.gameObject))
            {
                StartCoroutine(this.FixedUpdateIE());
            }

        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {

            if (Application.IsPlaying(this.gameObject))
            {
                StopAllCoroutines();
            }

        }

        /// <summary>
        /// FixedUpdate IEnumerator
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator FixedUpdateIE()
        {

            for (; ; )
            {

                // wait
                {
                    yield return this.m_waitForFixedUpdate;
                }

                // update
                {

                    if (this.quickOnce)
                    {
                        this.Lookat(1.0f);
                    }

                    else
                    {
                        this.Lookat(this.m_speed);
                    }

                }

                // reset
                {
                    this.quickOnce = false;
                }

            }

        }

    }

}
