using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to follow an object
    /// </summary>
    public abstract class ObjectFollower : MonoBehaviour
    {

        /// <summary>
        /// Reference to Transform to follow position
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Transform to follow position")]
        protected Transform m_refFollowTargetForPos = null;

        /// <summary>
        /// Reference to Transform to follow rotation
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Transform to follow rotation")]
        protected Transform m_refFollowTargetForRot = null;

        /// <summary>
        /// Speed to follow position
        /// </summary>
        [SerializeField]
        [Tooltip("Speed to follow position")]
        [Range(0.01f, 1.0f)]
        protected float m_speedForPos = 0.1f;

        /// <summary>
        /// Speed to follow rotation
        /// </summary>
        [SerializeField]
        [Tooltip("Speed to follow rotation")]
        [Range(0.01f, 1.0f)]
        protected float m_speedForRot = 0.1f;

        /// <summary>
        /// Constraint max distance (ignored if negative value)
        /// </summary>
        [SerializeField]
        [Tooltip("Constraint max distance (ignored if negative value)")]
        [Range(-1.0f, 100.0f)]
        protected float m_constraintMaxDistance = -1.0f;

        /// <summary>
        /// Temporary position
        /// </summary>
        protected Vector3 m_tempPos = Vector3.zero;

        /// <summary>
        /// Temporary rotation
        /// </summary>
        protected Quaternion m_tempRot = Quaternion.identity;

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to Transform to follow position
        /// </summary>
        public Transform refFollowTargetForPos
        {
            get { return this.m_refFollowTargetForPos; }
            set { this.m_refFollowTargetForPos = value; }
        }

        /// <summary>
        /// Reference to Transform to follow rotation
        /// </summary>
        public Transform refFollowTargetForRot
        {
            get { return this.m_refFollowTargetForRot; }
            set { this.m_refFollowTargetForRot = value; }
        }

        /// <summary>
        /// Speed to follow position
        /// </summary>
        public float speedForPos
        {
            get { return this.m_speedForPos; }
            set { this.m_speedForPos = value; }
        }

        /// <summary>
        /// Speed to follow rotation
        /// </summary>
        public float speedForRot
        {
            get { return this.m_speedForRot; }
            set { this.m_speedForRot = value; }
        }

        /// <summary>
        /// Flag to follow quickly once
        /// </summary>
        public bool quickOnce { get; set; } = false;

        /// <summary>
        /// Calculate position and rotation
        /// </summary>
        /// <param name="speedForPos">speed for position</param>
        /// <param name="speedForRot">speed for rotation</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void CalcPositionAndRotation(
            float speedForPos,
            float speedForRot,
            ref Vector3 pos,
            ref Quaternion rot
            )
        {

            pos =
                this.m_refFollowTargetForPos ?
                Vector3.Lerp(this.transform.position, this.m_refFollowTargetForPos.position, speedForPos) :
                this.transform.position
                ;

            rot =
                this.m_refFollowTargetForRot ?
                Quaternion.Slerp(this.transform.rotation, this.m_refFollowTargetForRot.rotation, speedForRot) :
                this.transform.rotation
                ;

            if (
                this.m_refFollowTargetForPos &&
                this.m_constraintMaxDistance > 0.0f
                )
            {

                float distance = Vector3.Distance(pos, this.m_refFollowTargetForPos.position);

                if (
                    distance > 0.0f &&
                    distance > this.m_constraintMaxDistance
                    )
                {

                    pos =
                        this.m_refFollowTargetForPos.position +
                        ((pos - this.m_refFollowTargetForPos.position).normalized * this.m_constraintMaxDistance)
                        ;

                }

            }

        }

        /// <summary>
        /// Update position and rotation
        /// </summary>
        /// <param name="speedForPos">speed for position</param>
        /// <param name="speedForRot">speed for rotation</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void UpdatePositionAndRotation(
            float speedForPos,
            float speedForRot
            )
        {

            this.CalcPositionAndRotation(
                speedForPos,
                speedForRot,
                ref this.m_tempPos,
                ref this.m_tempRot
                );

            this.transform.SetPositionAndRotation(this.m_tempPos, this.m_tempRot);

        }

    }

}
