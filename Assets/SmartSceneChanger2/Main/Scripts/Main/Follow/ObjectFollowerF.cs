using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to follow an object
    /// </summary>
    [ExecuteAlways]
    public class ObjectFollowerF : ObjectFollower
    {

        /// <summary>
        /// WaitForFixedUpdate
        /// </summary>
        protected WaitForFixedUpdate m_waitForFixedUpdate = new WaitForFixedUpdate();

#if UNITY_EDITOR

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {
            
            if (!Application.IsPlaying(this.gameObject))
            {
                this.UpdatePositionAndRotation(1.0f, 1.0f);
            }

        }

#endif

        /// <summary>
        /// OnEnable
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnEnable()
        {

            if (Application.IsPlaying(this.gameObject))
            {
                StartCoroutine(this.FixedUpdateIE());
            }

        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {

            if (Application.IsPlaying(this.gameObject))
            {
                StopAllCoroutines();
            }

        }

        /// <summary>
        /// FixedUpdate IEnumerator
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator FixedUpdateIE()
        {

            for (; ; )
            {

                // wait
                {
                    yield return this.m_waitForFixedUpdate;
                }

                // update
                {

                    if (this.quickOnce)
                    {
                        this.UpdatePositionAndRotation(1.0f, 1.0f);
                    }

                    else
                    {
                        this.UpdatePositionAndRotation(this.m_speedForPos, this.m_speedForRot);
                    }

                }

                // reset
                {
                    this.quickOnce = false;
                }

            }

        }

    }

}
