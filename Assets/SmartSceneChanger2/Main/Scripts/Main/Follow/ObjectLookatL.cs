using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Script to lookat
    /// </summary>
    [ExecuteAlways]
    public class ObjectLookatL : ObjectLookat
    {

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // update
            {

                if (
                    this.quickOnce

#if UNITY_EDITOR
                    || !Application.IsPlaying(this.gameObject)
#endif

                    )
                {
                    this.Lookat(1.0f);
                }

                else
                {
                    this.Lookat(this.m_speed);
                }

            }

            // reset
            {
                this.quickOnce = false;
            }

        }

    }

}
