using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Startup template
    /// </summary>
    public abstract class StartupTemplate :
        MonoBehaviour,
        SceneChanger.IStartup,
        EventManager<SceneChanger.SceneChangeEvent>.IReceiver
    {

        /// <summary>
        /// Startup function
        /// </summary>
        /// <returns>IEnumerator</returns>
        protected abstract IEnumerator StartupFunction();

        /// <summary>
        /// Receive
        /// </summary>
        /// <param name="eventData">data</param>
        public abstract void Receive(SceneChanger.SceneChangeEvent eventData);

        // ------------------------------------------------------------------
        public virtual IEnumerator Startup(Action finishCallback)
        {

            yield return this.StartupFunction();

            finishCallback();

        }

        /// <summary>
        /// OnEnable
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        protected virtual void OnEnable()
        {
            EventManager<SceneChanger.SceneChangeEvent>.Instance.AddEventReceiver(this);
        }

        /// <summary>
        /// OnDisable
        /// </summary>
        // ----------------------------------------------------------------------------------------------------
        protected virtual void OnDisable()
        {
            EventManager<SceneChanger.SceneChangeEvent>.Instance.RemoveEventReceiver(this);
        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------
        protected virtual void Start()
        {
            SceneChanger.Instance.AddStartup(this);
        }

    }

}
