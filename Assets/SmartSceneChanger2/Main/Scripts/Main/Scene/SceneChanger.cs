using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SSC2
{

    /// <summary>
    /// Scene changer
    /// </summary>
    public class SceneChanger : SingletonMonoBehaviour<SceneChanger>
    {

        /// <summary>
        /// Startup interface
        /// </summary>
        public interface IStartup
        {

            /// <summary>
            /// Startup
            /// </summary>
            /// <param name="finishCallback">callback when finished</param>
            /// <returns>IEnumerator</returns>
            IEnumerator Startup(Action finishCallback);

        }

        /// <summary>
        /// Scene change event
        /// </summary>
        public class SceneChangeEvent
        {

            /// <summary>
            /// Event state
            /// </summary>
            public enum EventState
            {
                Unknown,
                FinishedLoadingSceneSelf,
                FinishedAllStartups,
                FinishedUiOutro,
            }

            /// <summary>
            /// Event state
            /// </summary>
            public EventState eventState { get; set; } = EventState.Unknown;

        }

        /// <summary>
        /// Ui list to show the screen of loading
        /// </summary>
        [SerializeField]
        [Tooltip("Ui list to show the screen of loading")]
        protected KeyAndValue<string, ShowOrHideObject>[] m_loadingUiList = new KeyAndValue<string, ShowOrHideObject>[0];

        /// <summary>
        /// IStartup list
        /// </summary>
        protected HashSet<IStartup> m_startupList = new HashSet<IStartup>();

        /// <summary>
        /// Critical section flag
        /// </summary>
        protected bool m_criticalSection = false;

        // -----------------------------------------------------------------------------------------

        /// <summary>
        /// Normalized progress
        /// </summary>
        public float progress01 { get; protected set; } = 1.0f;

        /// <summary>
        /// Denominator for progress
        /// </summary>
        public int progressDenominator { get; protected set; } = 1;

        /// <summary>
        /// Numerator for progress
        /// </summary>
        public int progressNumerator { get; protected set; } = 0;

        /// <summary>
        /// True if loading any scene
        /// </summary>
        public bool nowLoading { get { return this.progress01 < 1.0f; } }

        /// <summary>
        /// Current key of loading ui
        /// </summary>
        public string currentLoadingUiKey { get; protected set; } = "";

        /// <summary>
        /// Add a startup
        /// </summary>
        /// <param name="startup">startup</param>
        // -----------------------------------------------------------------------------------------
        public virtual void AddStartup(IStartup startup)
        {
            this.m_startupList.Add(startup);
        }

        /// <summary>
        /// Remove a startup
        /// </summary>
        /// <param name="startup">startup</param>
        // -----------------------------------------------------------------------------------------
        public virtual void RemoveStartup(IStartup startup)
        {
            this.m_startupList.Remove(startup);
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR
            this.CheckUiDontDestroyEditorOnly();
#endif

        }

#if UNITY_EDITOR

        /// <summary>
        /// Check if Uis have DontDestroyOnLoad (EditorOnly)
        /// </summary>
        // -----------------------------------------------------------------------------------------
        protected void CheckUiDontDestroyEditorOnly()
        {

            foreach (var val in this.m_loadingUiList)
            {

                if (
                    val.value &&
                    !Funcs.CheckDontDestroy(val.value.gameObject)
                    )
                {
                    Debug.LogWarningFormat("Ui should have DontDestroyOnLoad : {0}", Funcs.CreateHierarchyPath(val.value.transform));
                }

            }

        }

#endif

        /// <summary>
        /// Get loading ui by key
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>ShowOrHideObject</returns>
        // -----------------------------------------------------------------------------------------
        protected virtual ShowOrHideObject GetLoadingUi(string key)
        {

            ShowOrHideObject ret = null;

            KeyAndValue<string, ShowOrHideObject> found =
                KeyAndValue<string, ShowOrHideObject>.Find(key, this.m_loadingUiList);

            if (found != null)
            {
                ret = found.value;
            }

            else if (
                string.IsNullOrEmpty(key) &&
                this.m_loadingUiList.Length > 0
                )
            {
                ret = this.m_loadingUiList[0].value;
            }

            return ret;

        }

        /// <summary>
        /// Load next scene
        /// </summary>
        /// <param name="sceneName">scene name</param>
        // -----------------------------------------------------------------------------------------
        public virtual void LoadNextScene(string sceneName)
        {

            string loadingUiKey = "";

            if (this.m_loadingUiList.Length > 0)
            {
                loadingUiKey = this.m_loadingUiList[0].key;
            }

            this.LoadNextScene(sceneName, loadingUiKey);

        }

        /// <summary>
        /// Load next scene
        /// </summary>
        /// <param name="sceneName">scene name</param>
        /// <param name="loadingUiKey">key of loading ui</param>
        // -----------------------------------------------------------------------------------------
        public virtual void LoadNextScene(
            string sceneName,
            string loadingUiKey
            )
        {

            if (this.m_criticalSection)
            {
                return;
            }

            // ----------------------
            
            // stop
            {
                StopAllCoroutines();
            }

            // LoadNextSceneIE
            {
                StartCoroutine(this.LoadNextSceneIE(sceneName, loadingUiKey));
            }

        }

        /// <summary>
        /// Load next scene
        /// </summary>
        /// <param name="sceneName">scene name</param>
        /// <param name="loadingUiKey">key of loading ui</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------
        protected virtual IEnumerator LoadNextSceneIE(
            string sceneName,
            string loadingUiKey
            )
        {

            ShowOrHideObject showOrHideObject = this.GetLoadingUi(loadingUiKey);

            // ---------------------------

            // hide previous ui
            {

                ShowOrHideObject prevShowOrHideObject = this.GetLoadingUi(this.currentLoadingUiKey);

                if (
                    prevShowOrHideObject &&
                    showOrHideObject != prevShowOrHideObject &&
                    prevShowOrHideObject.showingState != ShowOrHideObject.ShowHideState.Hide
                    )
                {
                    prevShowOrHideObject.ShowOrHide(false, true);
                }

            }

            // init
            {
                this.progress01 = 0.0f;
                this.currentLoadingUiKey = loadingUiKey;
                this.progressNumerator = 0;
                this.progressDenominator = 1;
            }

            // ---------------------------

            EventManager<SceneChangeEvent> sceneChangeEvent = EventManager<SceneChangeEvent>.Instance;

            // ---------------------------

            // ui
            {

                if (showOrHideObject)
                {

                    if (
                        showOrHideObject.showingState != ShowOrHideObject.ShowHideState.Show &&
                        showOrHideObject.showingState != ShowOrHideObject.ShowHideState.ShowTransition
                        )
                    {

                        showOrHideObject.ShowOrHide(true, false);

                        yield return null;

                        while (showOrHideObject.showingState != ShowOrHideObject.ShowHideState.Show)
                        {
                            yield return null;
                        }

                    }

                }

            }
            
            // LoadSceneAsync
            {

                // TODO?
                // Coroutine stops if loaded the scene doesn't exist
                // SceneManager.GetSceneByName(sceneName) only works for loaded scenes

                AsyncOperation ao = SceneManager.LoadSceneAsync(sceneName);

                // m_criticalSection
                {
                    this.m_criticalSection = true;
                }

                // wait
                {

                    while (!ao.isDone)
                    {
                        yield return null;
                    }

                }

                // m_criticalSection
                {
                    this.m_criticalSection = false;
                }

                // progressNumerator
                {
                    this.progressNumerator++;
                }

            }

            // wait Awake and Start
            {
                yield return null;
            }

            // SendEvent
            {
                sceneChangeEvent.value.eventState = SceneChangeEvent.EventState.FinishedLoadingSceneSelf;
                sceneChangeEvent.SendEvent();
                yield return null;
            }

            // startups
            {

                //int startupCount = 0;
                //int finishedCount = 0;

                if (this.m_startupList.Count > 0)
                {

                    void finish()
                    {
                        this.progressNumerator++;
                    }

                    do
                    {

                        // do
                        {

                            foreach (var val in this.m_startupList)
                            {

                                if (val != null)
                                {
                                    this.progressDenominator++;
                                    StartCoroutine(val.Startup(finish));
                                }

                            }

                        }

                        // clear
                        {
                            this.m_startupList.Clear();
                        }

                        // this.progress01
                        {
                            this.progress01 = this.progressNumerator / (float)this.progressDenominator;
                        }

                        // wait
                        {
                            yield return null;
                        }

                    } while (this.progressNumerator < this.progressDenominator);

                }

            }

            // progress01
            {
                this.progress01 = 1.0f;
                yield return null;
            }

            // SendEvent
            {
                sceneChangeEvent.value.eventState = SceneChangeEvent.EventState.FinishedAllStartups;
                sceneChangeEvent.SendEvent();
                yield return null;
            }

            // ui
            {

                if (showOrHideObject)
                {

                    if (
                        showOrHideObject.showingState != ShowOrHideObject.ShowHideState.Hide &&
                        showOrHideObject.showingState != ShowOrHideObject.ShowHideState.HideTransition
                        )
                    {

                        showOrHideObject.ShowOrHide(false, false);

                        yield return null;

                        while (showOrHideObject.showingState != ShowOrHideObject.ShowHideState.Hide)
                        {
                            yield return null;
                        }

                    }
                    
                }

            }

            // SendEvent
            {
                sceneChangeEvent.value.eventState = SceneChangeEvent.EventState.FinishedUiOutro;
                sceneChangeEvent.SendEvent();
                yield return null;
            }

        }

    }

}
