using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Rename children
    /// </summary>
    public class RenameChildren : MonoBehaviour
    {

        /// <summary>
        /// Old text
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Old text")]
        public string oldText { get; private set; } = "";

        /// <summary>
        /// New text
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("New text")]
        public string newText { get; private set; } = "";

#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(RenameChildren), Funcs.CreateHierarchyPath(this.transform));

        }

#endif

    }

}
