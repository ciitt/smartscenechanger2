using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Linear transform
    /// </summary>
    public class LinearTransform : MonoBehaviour
    {

        /// <summary>
        /// Targets to apply
        /// </summary>
        [Flags]
        public enum ApplyTargets
        {
            None = 0,
            X = 1 << 0,
            Y = 1 << 1,
            Z = 1 << 2
        }

        /// <summary>
        /// MinMaxVector3 for scale
        /// </summary>
        [Serializable]
        public class CustomMinMaxVector3 : MinMaxVector3
        {

            /// <summary>
            /// ApplyTargets
            /// </summary>
            public ApplyTargets applyTargets = ApplyTargets.X | ApplyTargets.Y | ApplyTargets.Z;

        }

        /// <summary>
        /// MinMaxVector3 for scale
        /// </summary>
        [Serializable]
        public class ScaleMinMaxVector3 : CustomMinMaxVector3
        {

            /// <summary>
            /// x == y == z
            /// </summary>
            public bool unitScale = false;

        }

        /// <summary>
        /// Seed
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Seed")]
        public int seed { get; private set; } = 0;

        /// <summary>
        /// Position range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Position range")]
        public CustomMinMaxVector3 positionRange { get; private set; } = new CustomMinMaxVector3()
        {
            min = Vector3.zero,
            max = Vector3.one
        };

        /// <summary>
        /// Rotation range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Rotation range")]
        public CustomMinMaxVector3 rotationRange { get; private set; } = new CustomMinMaxVector3()
        {
            min = Vector3.zero,
            max = Vector3.zero
        };

        /// <summary>
        /// Scale range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Scale range")]
        public ScaleMinMaxVector3 scaleRange { get; private set; } = new ScaleMinMaxVector3()
        {
            min = Vector3.one,
            max = Vector3.one
        };

        /// <summary>
        /// Additional position range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Additional position range")]
        public MinMaxVector3 additionalPositionRange { get; private set; } = new MinMaxVector3()
        {
            min = Vector3.zero,
            max = Vector3.zero
        };

        /// <summary>
        /// Additional rotation range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Additional rotation range")]
        public MinMaxVector3 additionalRotationRange { get; private set; } = new MinMaxVector3()
        {
            min = Vector3.zero,
            max = Vector3.zero
        };

        /// <summary>
        /// Additional scale range
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Additional scale range")]
        public MinMaxVector3 additionalScaleRange { get; private set; } = new MinMaxVector3()
        {
            min = Vector3.zero,
            max = Vector3.zero
        };


#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(LinearTransform), Funcs.CreateHierarchyPath(this.transform));

        }

#endif

    }

}
