using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Raycast children
    /// </summary>
    public class RaycastChildren : MonoBehaviour
    {

        /// <summary>
        /// MinMaxFloat with seed
        /// </summary>
        [Serializable]
        public class CustomMinMaxFloat : MinMaxFloat
        {

            /// <summary>
            /// Seed
            /// </summary>
            public int seed  = 0;

        }

        /// <summary>
        /// Target LayerMask
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Scale range")]
        public LayerMask targetLayerMask = 0;

        /// <summary>
        /// Rotate along surface (not perfect)
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Rotate along surface (not perfect)")]
        public bool rotateAlongSurface = false;

        /// <summary>
        /// Direction to raycast
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Direction to raycast")]
        public Vector3 direction = -Vector3.up;

        /// <summary>
        /// Distance of raycast
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Distance of raycast")]
        public float raycastDistance = 1000.0f;

        /// <summary>
        /// Offset range from surface
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Offset range from surface")]
        public CustomMinMaxFloat offsetFromSurfaceRange = new CustomMinMaxFloat()
        {
            min = 0.0f,
            max = 0.0f
        };

#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(RaycastChildren), Funcs.CreateHierarchyPath(this.transform));

        }

#endif

    }

}
