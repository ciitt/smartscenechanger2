using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Distribute on faces
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class MeshAlongLines : MonoBehaviour
    {

        /// <summary>
        /// Source
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Source")]
        public Mesh source { get; private set; } = null;

        /// <summary>
        /// Max mesh loop count
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Max mesh loop count")]
        public int maxMeshLoopCount { get; private set; } = 20;

        /// <summary>
        /// Rotation
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Rotation")]
        public Vector3 rotation { get; private set; } = Vector3.zero;

        /// <summary>
        /// Scale
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Scale")]
        public Vector3 scale { get; private set; } = Vector3.one;

        /// <summary>
        /// Close
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Close")]
        public bool close { get; private set; } = false;

        /// <summary>
        /// Flag to draw lines
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Flag to draw lines")]
        public bool drawLines { get; private set; } = true;

        /// <summary>
        /// RaycastInfoWithFlag
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("RaycastInfoWithFlag")]
        public RaycastInfoWithFlag raycastInfo { get; private set; } = new RaycastInfoWithFlag();

        /// <summary>
        /// Position list
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Position list")]
        public Transform[] positions = new Transform[2];

#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(MeshAlongLines), Funcs.CreateHierarchyPath(this.transform));

        }

        /// <summary>
        /// OnDrawGizmosSelected
        /// </summary>
        // -----------------------------------------------------------------------------------
        void OnDrawGizmosSelected()
        {

            if (
                this.drawLines &&
                this.positions.Length >= 2
                )
            {

                Transform transformA = null;
                Transform transformB = null;

                int size = this.close ? this.positions.Length : this.positions.Length - 1;

                Color color = Color.green;

                float thickness = 3.0f;

                for (int i = 0; i < size; i++)
                {

                    transformA = this.positions[i];
                    transformB = this.positions[(i + 1) % this.positions.Length];

                    if (
                        transformA &&
                        transformB
                        )
                    {

                        UnityEditor.Handles.DrawBezier(
                            transformA.position,
                            transformB.position,
                            Vector3.Lerp(transformA.position, transformB.position, 0.25f),
                            Vector3.Lerp(transformA.position, transformB.position, 0.75f),
                            color,
                            null,
                            thickness
                            );

                    }

                }

            }

        }

#endif

    }

}
