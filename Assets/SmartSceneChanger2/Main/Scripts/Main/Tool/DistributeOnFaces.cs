using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Distribute on faces
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class DistributeOnFaces : MonoBehaviour
    {

        /// <summary>
        /// Distribute type
        /// </summary>
        public enum DistributeType
        {
            Random,
            InOrder
        }

        /// <summary>
        /// Offset range info
        /// </summary>
        [Serializable]
        public class OffsetRangeInfo
        {

            public bool use = false;

            public MinMaxFloat range = new MinMaxFloat()
            {
                min = 0.0f,
                max = 0.0f
            };

        }


        /// <summary>
        /// Seed
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Seed")]
        public int seed { get; private set; } = 0;

        /// <summary>
        /// DistributeType
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("DistributeType")]
        public DistributeType distributeType { get; private set; } = DistributeType.Random;

        /// <summary>
        /// Offset range Y
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Offset range Y")]
        public OffsetRangeInfo offsetRangeY { get; private set; } = new OffsetRangeInfo();

        /// <summary>
        /// Normal range Y
        /// </summary>
        [field: SerializeField]
        [field: Tooltip("Normal range Y")]
        public MinMaxFloat normalRangeY { get; private set; } = new MinMaxFloat()
        {
            min = -1.0f,
            max = 1.0f
        };

#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(DistributeOnFaces), Funcs.CreateHierarchyPath(this.transform));

        }
#endif

    }

}
