using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Shuffle children
    /// </summary>
    public class ShuffleChildren : MonoBehaviour
    {

        ///// <summary>
        ///// Seed
        ///// </summary>
        //[field: SerializeField]
        //[field: Tooltip("Seed")]
        //public int seed { get; private set; } = 0;

#if UNITY_EDITOR

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        void Start()
        {

            Debug.LogWarningFormat("You should remove {0} : {1}", nameof(ShuffleChildren), Funcs.CreateHierarchyPath(this.transform));

        }

#endif

    }

}
