using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Action to show or hide
    /// </summary>
    public class ShowOrHideActionUiTranslate : ShowOrHideActionBase
    {

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to RectTransform")]
        protected RectTransform m_refRectTransform = null;

        /// <summary>
        /// Anchor positions when show
        /// </summary>
        [SerializeField]
        [Tooltip("Anchor positions when show")]
        protected FromTo<Vector3> m_anchorPosShow = new FromTo<Vector3>();

        /// <summary>
        /// Anchor positions when hide
        /// </summary>
        [SerializeField]
        [Tooltip("Anchor positions when hide")]
        protected FromTo<Vector3> m_anchorPosHide = new FromTo<Vector3>();

        /// <summary>
        /// Duration seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Duration seconds")]
        protected float m_durationSeconds = 0.2f;

        /// <summary>
        /// Show
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public override IEnumerator ShowIE(
            bool quick,
            Action finished
            )
        {

            FromTo<Vector3> fromTo = this.m_anchorPosShow;

            // act
            {

                if (!quick)
                {

                    // init
                    {

                        if (this.m_refRectTransform)
                        {
                            this.m_refRectTransform.anchoredPosition = fromTo.from;
                        }

                    }

                    // wait
                    {
                        yield return Funcs.WaitSeconds(this.m_delaySecondsWhenShow);
                    }

                    // act
                    {

                        float timer = 0.0f;

                        while (timer < this.m_durationSeconds)
                        {

                            yield return null;

                            if (this.m_refRectTransform)
                            {
                                this.m_refRectTransform.anchoredPosition = Vector3.Lerp(
                                    fromTo.from,
                                    fromTo.to,
                                    timer / this.m_durationSeconds
                                    );
                            }

                            timer += Time.deltaTime;

                        }

                    }

                }

            }

            // finished
            {

                if (this.m_refRectTransform)
                {
                    this.m_refRectTransform.anchoredPosition = fromTo.to;
                }

                finished?.Invoke();

            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public override IEnumerator HideIE(
            bool quick,
            Action finished
            )
        {

            FromTo<Vector3> fromTo = this.m_anchorPosHide;

            // act
            {

                if (!quick)
                {

                    // init
                    {

                        if (this.m_refRectTransform)
                        {
                            this.m_refRectTransform.anchoredPosition = fromTo.from;
                        }

                    }

                    // wait
                    {
                        yield return Funcs.WaitSeconds(this.m_delaySecondsWhenShow);
                    }

                    // act
                    {

                        float timer = 0.0f;

                        while (timer < this.m_durationSeconds)
                        {

                            yield return null;

                            if (this.m_refRectTransform)
                            {
                                this.m_refRectTransform.anchoredPosition = Vector3.Lerp(
                                    fromTo.from,
                                    fromTo.to,
                                    timer / this.m_durationSeconds
                                    );
                            }

                            timer += Time.deltaTime;

                        }

                    }

                }

            }

            // finished
            {

                if (this.m_refRectTransform)
                {
                    this.m_refRectTransform.anchoredPosition = fromTo.to;
                }

                finished?.Invoke();

            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refRectTransform)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refRectTransform), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
