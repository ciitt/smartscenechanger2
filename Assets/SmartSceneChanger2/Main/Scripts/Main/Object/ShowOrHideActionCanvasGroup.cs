using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Action to show or hide
    /// </summary>
    public class ShowOrHideActionCanvasGroup : ShowOrHideActionBase
    {

        /// <summary>
        /// Reference to CanvasGroup
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to CanvasGroup")]
        protected CanvasGroup m_refCanvasGroup = null;

        /// <summary>
        /// Alpha when show
        /// </summary>
        [SerializeField]
        [Tooltip("Alpha when show")]
        protected FromTo<float> m_alphaShow = new FromTo<float>
        {
            from = 0.0f,
            to = 1.0f
        };

        /// <summary>
        /// Alpha when hide
        /// </summary>
        [SerializeField]
        [Tooltip("Alpha when hide")]
        protected FromTo<float> m_alphaHide = new FromTo<float>
        {
            from = 1.0f,
            to = 0.0f
        };

        /// <summary>
        /// Duration seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Duration seconds")]
        protected float m_durationSeconds = 0.2f;

        /// <summary>
        /// Show
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public override IEnumerator ShowIE(
            bool quick,
            Action finished
            )
        {

            FromTo<float> alphaFromTo = this.m_alphaShow;

            // act
            {

                if (!quick)
                {

                    // init
                    {

                        if (this.m_refCanvasGroup)
                        {
                            this.m_refCanvasGroup.alpha = alphaFromTo.from;
                            this.m_refCanvasGroup.blocksRaycasts = false;
                            this.m_refCanvasGroup.interactable = false;
                        }

                    }

                    // wait
                    {
                        yield return Funcs.WaitSeconds(this.m_delaySecondsWhenShow);
                    }

                    // act
                    {

                        float timer = 0.0f;

                        while (timer < this.m_durationSeconds)
                        {

                            yield return null;

                            if (this.m_refCanvasGroup)
                            {
                                this.m_refCanvasGroup.alpha = Mathf.Lerp(
                                    alphaFromTo.from,
                                    alphaFromTo.to,
                                    timer / this.m_durationSeconds
                                    );
                            }

                            timer += Time.deltaTime;

                        }

                    }

                }

            }

            // finished
            {

                if (this.m_refCanvasGroup)
                {
                    this.m_refCanvasGroup.alpha = alphaFromTo.to;
                    this.m_refCanvasGroup.blocksRaycasts = true;
                    this.m_refCanvasGroup.interactable = true;
                }

                finished?.Invoke();

            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public override IEnumerator HideIE(
            bool quick,
            Action finished
            )
        {

            FromTo<float> alphaFromTo = this.m_alphaHide;

            // act
            {

                if (!quick)
                {

                    // init
                    {

                        if (this.m_refCanvasGroup)
                        {
                            this.m_refCanvasGroup.alpha = alphaFromTo.from;
                            this.m_refCanvasGroup.blocksRaycasts = true;
                            this.m_refCanvasGroup.interactable = true;
                        }

                    }

                    // wait
                    {
                        yield return Funcs.WaitSeconds(this.m_delaySecondsWhenHide);
                    }

                    // act
                    {

                        float timer = 0.0f;

                        while (timer < this.m_durationSeconds)
                        {

                            yield return null;

                            if (this.m_refCanvasGroup)
                            {
                                this.m_refCanvasGroup.alpha = Mathf.Lerp(
                                    alphaFromTo.from,
                                    alphaFromTo.to,
                                    timer / this.m_durationSeconds
                                    );
                            }

                            timer += Time.deltaTime;

                        }

                    }

                }

            }

            // finished
            {

                if (this.m_refCanvasGroup)
                {
                    this.m_refCanvasGroup.alpha = alphaFromTo.to;
                    this.m_refCanvasGroup.blocksRaycasts = false;
                    this.m_refCanvasGroup.interactable = false;
                }

                finished?.Invoke();

            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCanvasGroup)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refCanvasGroup), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
