using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Action to show or hide
    /// </summary>
    public class ShowOrHideActionBase : MonoBehaviour
    {

        /// <summary>
        /// Seconds to delay when show
        /// </summary>
        [SerializeField]
        [Tooltip("Seconds to delay when show")]
        protected float m_delaySecondsWhenShow = 0.0f;

        /// <summary>
        /// Seconds to delay when hide
        /// </summary>
        [SerializeField]
        [Tooltip("Seconds to delay when hide")]
        protected float m_delaySecondsWhenHide = 0.0f;

        /// <summary>
        /// Show
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public virtual IEnumerator ShowIE(
            bool quick,
            Action finished
            )
        {

            // show
            {

                if (!quick)
                {
                    yield return Funcs.WaitSeconds(this.m_delaySecondsWhenShow);
                }

                this.gameObject.SetActive(true);

            }

            // finished
            {
                finished?.Invoke();
            }

        }

        /// <summary>
        /// Hide
        /// </summary>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <param name="finished">callback when finished</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        public virtual IEnumerator HideIE(
            bool quick,
            Action finished
            )
        {

            // show
            {

                if (!quick)
                {
                    yield return Funcs.WaitSeconds(this.m_delaySecondsWhenHide);
                }

                this.gameObject.SetActive(false);

            }

            // finished
            {
                finished?.Invoke();
            }

        }

    }

}
