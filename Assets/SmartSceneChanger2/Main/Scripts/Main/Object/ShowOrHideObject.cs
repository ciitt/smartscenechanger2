﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC2
{

    /// <summary>
    /// Show or hide the object
    /// </summary>
    public class ShowOrHideObject : MonoBehaviour
    {

        /// <summary>
        /// Show or hide state
        /// </summary>
        public enum ShowHideState
        {
            Unknown,
            Show,
            Hide,
            ShowTransition,
            HideTransition
        }

        /// <summary>
        /// Hide at Start
        /// </summary>
        [SerializeField]
        [Tooltip("Hide at Start")]
        protected bool m_hideAtStart = false;

        /// <summary>
        /// ShowOrHideActionBase array
        /// </summary>
        [SerializeField]
        [Tooltip("ShowOrHideActionBase array")]
        protected ShowOrHideActionBase[] m_actionList = new ShowOrHideActionBase[0];

        /// <summary>
        /// Events when show
        /// </summary>
        [SerializeField]
        [Tooltip("Events when show")]
        protected UnityEvent m_eventsWhenShow = null;

        /// <summary>
        /// Events when hide
        /// </summary>
        [SerializeField]
        [Tooltip("Events when hide")]
        protected UnityEvent m_eventsWhenHide = null;

        /// <summary>
        /// Coroutine list
        /// </summary>
        protected List<Coroutine> m_coroutineList = new List<Coroutine>();

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// ShowHideState
        /// </summary>
        public ShowHideState showingState { get; protected set; } = ShowHideState.Unknown;

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // ShowOrHide
            {
                this.ShowOrHide(!this.m_hideAtStart, true);
            }

#if UNITY_EDITOR

            //
            {


                if (this.m_actionList.Length <= 0)
                {
                    Debug.LogWarningFormat("{0}.Length <= 0 : {1}", nameof(this.m_actionList), Funcs.CreateHierarchyPath(this.transform));
                }

                foreach (var val in this.m_actionList)
                {
                    if (!val)
                    {
                        Debug.LogWarningFormat("{0} contains null : {1}", nameof(this.m_actionList), Funcs.CreateHierarchyPath(this.transform));
                    }
                }

            }

#endif

        }

        /// <summary>
        /// Stop and clear coroutines
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void StopAndClearCoroutines()
        {

            CoroutineProxy coroutineProxy = CoroutineProxy.Instance;

            // StopCoroutine
            {

                if (coroutineProxy)
                {

                    foreach (var coroutine in this.m_coroutineList)
                    {

                        if (coroutine != null)
                        {
                            coroutineProxy.StopCoroutine(coroutine);
                        }

                    }

                }

            }

            // clear
            {
                this.m_coroutineList.Clear();
            }

        }

        /// <summary>
        /// Show or hide
        /// </summary>
        /// <param name="show">true to show</param>
        // -----------------------------------------------------------------------------------
        public virtual void ShowOrHide(bool show)
        {
            this.ShowOrHide(show, false);
        }

        /// <summary>
        /// Show or hide
        /// </summary>
        /// <param name="show">true to show</param>
        /// <param name="quick">flag to show or hide quickly</param>
        // -----------------------------------------------------------------------------------
        public virtual void ShowOrHide(
            bool show,
            bool quick
            )

        {

            bool shouldStart = this.showingState == ShowHideState.Unknown;

            if (!shouldStart)
            {

                if (show)
                {

                    if (
                        this.showingState == ShowHideState.Hide ||
                        this.showingState == ShowHideState.HideTransition
                        )
                    {
                        shouldStart = true;
                    }

                }

                else
                {

                    if (
                        this.showingState == ShowHideState.Show ||
                        this.showingState == ShowHideState.ShowTransition
                        )
                    {
                        shouldStart = true;
                    }

                }

            }

            // ----------------------

            if (shouldStart)
            {

                this.StopAndClearCoroutines();

                CoroutineProxy coroutineProxy = CoroutineProxy.Instance;

                if (coroutineProxy)
                {
                    this.m_coroutineList.Add(
                        CoroutineProxy.Instance.StartCoroutine(this.ShowOrHideIE(show, quick))
                        );
                }

            }

        }

        /// <summary>
        /// Show or hide
        /// </summary>
        /// <param name="show">true to show</param>
        /// <param name="quick">flag to show or hide quickly</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        protected virtual IEnumerator ShowOrHideIE(
            bool show,
            bool quick
            )
        {

            int max = 0;
            int finished = 0;

            CoroutineProxy coroutineProxy = CoroutineProxy.Instance;

            // ----------------------------

            // check
            {
                if (!coroutineProxy)
                {
                    yield break;
                }
            }

            // m_isShowing, m_eventsWhenShow
            {

                if (show)
                {
                    this.showingState = ShowHideState.ShowTransition;
                    this.m_eventsWhenShow.Invoke();
                }

                else
                {
                    this.showingState = ShowHideState.HideTransition;
                }

            }

            // StartCoroutine
            {

                void incrementFinishedCounter()
                {
                    finished++;
                }

                if (show)
                {

                    foreach (var val in this.m_actionList)
                    {

                        if (val)
                        {

                            max++;

                            this.m_coroutineList.Add(
                                coroutineProxy.StartCoroutine(val.ShowIE(quick, incrementFinishedCounter))
                                );

                        }

                    }

                }

                else
                {

                    foreach (var val in this.m_actionList)
                    {

                        if (val)
                        {

                            max++;

                            this.m_coroutineList.Add(
                                coroutineProxy.StartCoroutine(val.HideIE(quick, incrementFinishedCounter))
                                );

                        }

                    }

                }

            }

            // wait
            {
                while (finished < max)
                {
                    yield return null;
                }
            }

            // showingState, m_eventsWhenHide
            {

                if (!show)
                {
                    this.showingState = ShowHideState.Hide;
                    this.m_eventsWhenHide.Invoke();
                }

                else
                {
                    this.showingState = ShowHideState.Show;
                }

            }

        }

    }

}
