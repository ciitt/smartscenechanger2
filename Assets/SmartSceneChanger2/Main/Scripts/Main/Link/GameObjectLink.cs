using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// GameObject link
    /// </summary>
    public class GameObjectLink : ObjectLink<GameObject>
    {

    }

}
