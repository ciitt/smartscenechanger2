using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Object link
    /// </summary>
    /// <typeparam name="T">target</typeparam>
    public class ObjectLink<T> : MonoBehaviour where T : UnityEngine.Object
    {

        /// <summary>
        /// Reference to object
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to object")]
        protected T m_refObject = null;

        // -----------------------------------------------------------------------------------
         
        /// <summary>
        /// Reference to object
        /// </summary>
        public T refObject { get { return this.m_refObject; } }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            //
            {
                
                if (!this.m_refObject)
                {
                    Debug.LogWarningFormat("{0} == null : {1}", nameof(this.m_refObject), Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
