﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Bezier
    /// </summary>
    [Serializable]
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Point on bezier
        /// </summary>
        public class PointOnBezier
        {

            /// <summary>
            /// Position
            /// </summary>
            public Vector3 position = Vector3.zero;

            /// <summary>
            /// Tangent direction
            /// </summary>
            public Vector3 tangent = Vector3.forward;

            /// <summary>
            /// Normal right direction
            /// </summary>
            public Vector3 normalRight = Vector3.right;

            /// <summary>
            /// Up direction
            /// </summary>
            public Vector3 up = Vector3.up;

            /// <summary>
            /// Calculate rotation
            /// </summary>
            /// <returns>rotation</returns>
            public Quaternion CalcQuaternion()
            {
                return Quaternion.LookRotation(this.tangent, this.up);
            }

            /// <summary>
            /// Calculate rotation on XZ plane
            /// </summary>
            /// <returns>rotation</returns>
            public Quaternion CalcQuaternionOnPlaneXZ()
            {
                return Quaternion.LookRotation(
                    new Vector3(this.tangent.x, 0.0f, this.tangent.z),
                    Vector3.up
                    );
            }

            /// <summary>
            /// Calculate position
            /// </summary>
            /// <param name="offset">offset</param>
            /// <returns>position</returns>
            public Vector3 CalcOffsetPosition(Vector3 offset)
            {
                return
                    this.position +
                    (this.normalRight * offset.x) +
                    (this.up * offset.y) +
                    (this.tangent * offset.z)
                    ;
            }

        }

        /// <summary>
        /// Point at start
        /// </summary>
        public Vector3 p0 = Vector3.zero;

        /// <summary>
        /// Tangent point for p0
        /// </summary>
        public Vector3 p1 = Vector3.zero;

        /// <summary>
        /// Tangent point for p3
        /// </summary>
        public Vector3 p2 = Vector3.forward;

        /// <summary>
        /// Point at end
        /// </summary>
        public Vector3 p3 = Vector3.forward;

        /// <summary>
        /// Upwards at start
        /// </summary>
        public Vector3 startUpwards = Vector3.up;

        /// <summary>
        /// Upwards at end
        /// </summary>
        public Vector3 endUpwards = Vector3.up;

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float omt = 0.0f;

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float omt2 = 0.0f;

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float t2 = 0.0f;

        // ---------------------------------------------------------------------------------------

        /// <summary>
        /// Temp points
        /// </summary>
        protected static List<Vector3> _tempPoints = null;

        /// <summary>
        /// Temp PointOnBezier for any purposes
        /// </summary>
        protected static PointOnBezier _tempPob = null;

        /// <summary>
        /// Temp points
        /// </summary>
        protected static List<Vector3> tempPointsInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempPoints); }
        }

        /// <summary>
        /// Temp PointOnBezier for any purposes
        /// </summary>
        public static PointOnBezier tempPobInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempPob); }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve() : base()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter
            ) : base()
        {
            this.Update(_p0, _p1, _p2, _p3, _samplingUnitMeter);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        /// <param name="_startUpwards">start upwards</param>
        /// <param name="_endUpwards">end upwards</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter,
            Vector3 _startUpwards,
            Vector3 _endUpwards
            ) : base()
        {
            this.Update(_p0, _p1, _p2, _p3, _samplingUnitMeter, _startUpwards, _endUpwards);
        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public virtual void Update(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter
            )
        {
            this.Update(_p0, _p1, _p2, _p3, _samplingUnitMeter, Vector3.up, Vector3.up);
        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        /// <param name="_startUpwards">start upwards</param>
        /// <param name="_endUpwards">end upwards</param>
        /// <param name="samplingMax">max point count to sample</param>
        // ---------------------------------------------------------------------------------------
        public virtual void Update(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter,
            Vector3 _startUpwards,
            Vector3 _endUpwards,
            int samplingMax = 100
            )
        {

            // set
            {
                this.p0 = _p0;
                this.p1 = _p1;
                this.p2 = _p2;
                this.p3 = _p3;
                this.startUpwards = _startUpwards;
                this.endUpwards = _endUpwards;
            }

            // ---------------------

            List<Vector3> tempPoints = tempPointsInstance;

            // ---------------------

            // tempPoints
            {

                // clear tempPoints
                {
                    tempPoints.Clear();
                }

                // set
                {

                    float distance =
                        Vector3.Distance(this.p0, this.p1) +
                        Vector3.Distance(this.p1, this.p2) +
                        Vector3.Distance(this.p2, this.p3)
                        ;

                    int sampling =
                        (_samplingUnitMeter > 0.0f) ?
                        Mathf.Max(2, Mathf.RoundToInt(distance / _samplingUnitMeter)) :
                        2
                        ;

                    if (sampling > samplingMax)
                    {

                        Debug.LogWarningFormat(
                            "Too many sampling points : {0} : {1} : {2} : {3} : {4}",
                            sampling,
                            this.p0,
                            this.p1,
                            this.p2,
                            this.p3
                            );

                        sampling = samplingMax;

                    }

                    float sampling_t01 = 0.0f;

                    for (int i = 0; i < sampling; i++)
                    {

                        sampling_t01 = i / (float)(sampling - 1);

                        tempPoints.Add(this.FindRawBezierPoint(sampling_t01));

                    }

                }

            }

            // base.update
            {
                base.Update(tempPoints, _samplingUnitMeter);
            }

            // clear tempPoints
            {
                tempPoints.Clear();
            }

        }

        /// <summary>
        /// Find a detail info on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindRawBezierPoint(
            float t01,
            PointOnBezier dest
            )
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            dest.position = this.FindRawBezierPoint(t01);
            dest.tangent = this.FindRawBezierTangent(t01);
            dest.normalRight = this.FindRawBezierRightNormal(dest.tangent, t01);
            dest.up = this.FindRawBezierUpVector(dest.tangent, dest.normalRight);

        }

        /// <summary>
        /// Find a detail info on a bezier curve
        /// </summary>
        /// <param name="meter">meter</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindRawBezierPointByMeter(
            float meter,
            PointOnBezier dest
            )
        {

            this.FindRawBezierPoint(
                this.ConvertSampledMeterToNormalized01(meter),
                dest
                );

        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindSampledBezierPointByNormalizedValue(
            float t01,
            PointOnBezier dest
            )
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            dest.position = this.FindSampledPointByNormalizeValue(t01);
            dest.tangent = this.FindRawBezierTangent(t01);

            dest.normalRight = this.FindRawBezierRightNormal(dest.tangent, t01);
            dest.up = this.FindSampledUpVector(t01);

        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindSampledBezierPointByMeter(
            float targetMeter,
            PointOnBezier dest
            )
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            float t01 =
                (this.sampledTotalMeterLength > 0) ?
                Mathf.Clamp01(targetMeter / this.sampledTotalMeterLength) :
                0.0f
                ;

            this.FindSampledBezierPointByNormalizedValue(t01, dest);

        }

        /// <summary>
        /// Convert sampled meter to normalized value
        /// </summary>
        /// <param name="meter">meter position</param>
        /// <returns>normalized value</returns>
        // -------------------------------------------------------------------------------------------
        public virtual float ConvertSampledMeterToNormalized01(float meter)
        {
            return this.ConvertSampledMeterToNormalized01(meter, false);
        }

        /// <summary>
        /// Convert sampled meter to normalized value
        /// </summary>
        /// <param name="meter">meter position</param>
        /// <param name="clamp">clamp the value</param>
        /// <returns>normalized value</returns>
        // -------------------------------------------------------------------------------------------
        public virtual float ConvertSampledMeterToNormalized01(
            float meter,
            bool clamp
            )
        {

            float ret =
                (this.sampledTotalMeterLength > 0) ?
                meter / this.sampledTotalMeterLength :
                0
                ;

            if (clamp)
            {
                ret = Mathf.Clamp01(ret);
            }

            return ret;

        }

        /// <summary>
        /// Find a point on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>point</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 FindRawBezierPoint(float t01)
        {

            omt = 1.0f - t01;
            omt2 = omt * omt;
            t2 = t01 * t01;

            return
                (this.p0 * (omt2 * omt)) +
                (this.p1 * (3f * omt2 * t01)) +
                (this.p2 * (3f * omt * t2)) +
                (this.p3 * (t2 * t01))
                ;

        }

        /// <summary>
        /// Find a tangent on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>tangent</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 FindRawBezierTangent(float t01)
        {

            Vector3 ret = Vector3.zero;

            if (t01 <= 0.0f)
            {

                Vector3 p0p1 = this.p1 - this.p0;

                if (p0p1.sqrMagnitude > 0.0f)
                {
                    ret = p0p1.normalized;
                }

            }

            else if (t01 >= 1.0f)
            {

                Vector3 p2p3 = this.p3 - this.p2;

                if (p2p3.sqrMagnitude > 0.0f)
                {
                    ret = p2p3.normalized;
                }

            }

            if (ret.sqrMagnitude <= 0.0f)
            {

                omt = 1.0f - t01;
                omt2 = omt * omt;
                t2 = t01 * t01;

                ret =
                    (
                    (this.p0 * (-omt2)) +
                    (this.p1 * (3 * omt2 - 2 * omt)) +
                    (this.p2 * (-3 * t2 + 2 * t01)) +
                    (this.p3 * (t2))
                    )
                    .normalized
                    ;

            }

            return ret;

        }

        /// <summary>
        /// Find a right normal on a bezier curve
        /// </summary>
        /// <param name="tangent">tangent</param>
        /// <param name="t01">normalized t</param>
        /// <returns>right normal</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 FindRawBezierRightNormal(
            Vector3 tangent,
            float t01
            )
        {

            if (tangent.sqrMagnitude <= 0.0f)
            {
                return Vector3.zero;
            }

            return
                (tangent.sqrMagnitude <= 0.0f) ?
                Vector3.zero :
                Quaternion.LookRotation(tangent, Vector3.Lerp(this.startUpwards, this.endUpwards, t01)) * Vector3.right
                ;

        }

        /// <summary>
        /// Find a up vector on a bezier curve
        /// </summary>
        /// <param name="tangent">tangent</param>
        /// <param name="normalRight">normalRight</param>
        /// <returns>up vector</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 FindRawBezierUpVector(
            Vector3 tangent,
            Vector3 normalRight
            )
        {
            return Vector3.Cross(tangent, normalRight);
        }

        /// <summary>
        /// Find sampled up vector
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>up vector</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 FindSampledUpVector(float t01)
        {
            return Vector3.Lerp(this.startUpwards, this.endUpwards, t01).normalized;
        }

        /// <summary>
        /// Create offset bezier
        /// </summary>
        /// <param name="offsetAtStart">offset at start</param>
        /// <param name="offsetAtEnd">offset at end</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void CreateOffsetBezier(
            Vector3 offsetAtStart,
            Vector3 offsetAtEnd,
            EquidistantBezierCurve dest
            )
        {

            if (dest == null)
            {
                return;
            }

            // --------------------------

            float thisDistanceP0toP3 = (this.p0 - this.p3).magnitude;

            float scale = 0.0f;

            Vector3 destP0 = Vector3.zero;
            Vector3 destP3 = Vector3.zero;

            // --------------------------

            // destP0, destP3
            {

                // destP0
                {
                    this.FindSampledBezierPointByNormalizedValue(0.0f, tempPobInstance);
                    destP0 = tempPobInstance.CalcOffsetPosition(offsetAtStart);
                }

                // destP3
                {
                    this.FindSampledBezierPointByNormalizedValue(1.0f, tempPobInstance);
                    destP3 = tempPobInstance.CalcOffsetPosition(offsetAtEnd);
                }

            }

            // scale
            {

                float destDistanceP0toP3 = (destP3 - destP0).magnitude;

                scale =
                    (!Mathf.Approximately(thisDistanceP0toP3, 0.0f)) ?
                    destDistanceP0toP3 / thisDistanceP0toP3 :
                    0.0f
                    ;

            }

            // update
            {

                Vector3 destP1 = destP0 + ((this.p1 - this.p0) * scale);
                Vector3 destP2 = destP3 + ((this.p2 - this.p3) * scale);

                dest.Update(
                    destP0,
                    destP1,
                    destP2,
                    destP3,
                    this.samplingUnitMeter,
                    this.startUpwards,
                    this.endUpwards
                    );

            }

        }

        /// <summary>
        /// Get the action to find the point by normalized value
        /// </summary>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <returns>action</returns>
        // -------------------------------------------------------------------------------------------
        public Action<float, PointOnBezier> GetFindPointByNormalizedValueAction(bool useSampledPoints)
        {
            return useSampledPoints ? this.FindSampledBezierPointByNormalizedValue : this.FindRawBezierPoint;
        }

        /// <summary>
        /// Find lattice points by meter
        /// </summary>
        /// <param name="meterFrom">target meter from</param>
        /// <param name="meterTo">target meter to</param>
        /// <param name="sizeMeterXY">size meter XY</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="posNNN">position at NNN</param>
        /// <param name="posPNN">position at PNN</param>
        /// <param name="posNNP">position at NNP</param>
        /// <param name="posPNP">position at PNP</param>
        /// <param name="posNPN">position at NPN</param>
        /// <param name="posPPN">position at PPN</param>
        /// <param name="posNPP">position at NPP</param>
        /// <param name="posPPP">position at PPP</param>
        // -------------------------------------------------------------------------------------------
        public void FindLatticePoints(
            float meterFrom,
            float meterTo,
            Vector2 sizeMeterXY,
            bool useSampledPoints,
            out Vector3 posNNN,
            out Vector3 posPNN,
            out Vector3 posNNP,
            out Vector3 posPNP,
            out Vector3 posNPN,
            out Vector3 posPPN,
            out Vector3 posNPP,
            out Vector3 posPPP
            )
        {

            this.FindLatticePointsN(
                this.ConvertSampledMeterToNormalized01(meterFrom),
                this.ConvertSampledMeterToNormalized01(meterTo),
                sizeMeterXY,
                useSampledPoints,
                out posNNN,
                out posPNN,
                out posNNP,
                out posPNP,
                out posNPN,
                out posPPN,
                out posNPP,
                out posPPP
                );

        }

        /// <summary>
        /// Get the action to find the point by meter
        /// </summary>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <returns>action</returns>
        // -------------------------------------------------------------------------------------------
        protected Action<float, PointOnBezier> GetFindPointByMeterAction(bool useSampledPoints)
        {
            return useSampledPoints ? this.FindSampledBezierPointByMeter : this.FindRawBezierPointByMeter;
        }

        /// <summary>
        /// Find lattice points by normalized value
        /// </summary>
        /// <param name="from01">target normalized at start</param>
        /// <param name="to01">target normalized at end</param>
        /// <param name="sizeMeterXY">size meter XY</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="posNNN">position at NNN</param>
        /// <param name="posPNN">position at PNN</param>
        /// <param name="posNNP">position at NNP</param>
        /// <param name="posPNP">position at PNP</param>
        /// <param name="posNPN">position at NPN</param>
        /// <param name="posPPN">position at PPN</param>
        /// <param name="posNPP">position at NPP</param>
        /// <param name="posPPP">position at PPP</param>
        // -------------------------------------------------------------------------------------------
        public void FindLatticePointsN(
            float from01,
            float to01,
            Vector2 sizeMeterXY,
            bool useSampledPoints,
            out Vector3 posNNN,
            out Vector3 posPNN,
            out Vector3 posNNP,
            out Vector3 posPNP,
            out Vector3 posNPN,
            out Vector3 posPPN,
            out Vector3 posNPP,
            out Vector3 posPPP
            )
        {

            float halfSizeMeterX = sizeMeterXY.x * 0.5f;

            float sign = (from01 < to01) ? 1 : -1;

            Action<float, PointOnBezier> act = this.GetFindPointByNormalizedValueAction(useSampledPoints);

            // from01
            {

                act(from01, tempPobInstance);

                posNNN =
                    tempPobInstance.position -
                    (tempPobInstance.normalRight * halfSizeMeterX * sign);
                posNPN = posNNN + (tempPobInstance.up * sizeMeterXY.y);

                posPNN =
                    tempPobInstance.position +
                    (tempPobInstance.normalRight * halfSizeMeterX * sign);
                posPPN = posPNN + (tempPobInstance.up * sizeMeterXY.y);

            }

            // to01
            {

                act(to01, tempPobInstance);

                posNNP =
                    tempPobInstance.position -
                    (tempPobInstance.normalRight * halfSizeMeterX * sign);
                posNPP = posNNP + (tempPobInstance.up * sizeMeterXY.y);

                posPNP =
                    tempPobInstance.position +
                    (tempPobInstance.normalRight * halfSizeMeterX * sign);
                posPPP = posPNP + (tempPobInstance.up * sizeMeterXY.y);

            }

        }

    }

}

