﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    public partial class ConsecutiveEquidistantBezierCurves
    {

        /// <summary>
        /// Draw debug bezier line (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        /// <param name="width">line width</param>
        // ------------------------------------------------------------------------------------------
        public void DrawBezierLineEditorOnly(Color color, float width)
        {

            foreach (var val in this.bezieList)
            {
                val.DrawBezierLineEditorOnly(color, width);
            }

        }

        /// <summary>
        /// Draw debug bezier position info (EditorOnly)
        /// </summary>
        /// <param name="position01">normalized position</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="handleWidth">width of DrawAAPolyLine</param>
        // ------------------------------------------------------------------------------------------
        public void DrawHandleOnBezierEditorOnly(
            float position01,
            bool useSampledPoints,
            float handleWidth
            )
        {

            float t01 = 0.0f;
            EquidistantBezierCurve bezier = null;

            if (this.FindTargetBezier(this.sampledAllTotalMeterLength * position01, ref bezier, ref t01))
            {
                bezier.DrawHandleOnBezierEditorOnly(t01, useSampledPoints, handleWidth);
            }

        }

    }

}

#endif
