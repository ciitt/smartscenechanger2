﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Equidistant point list
    /// </summary>
    [Serializable]
    public class EquidistantPointList
    {

        /// <summary>
        /// Sampling unit meter
        /// </summary>
        public float samplingUnitMeter = 0.0f;

        /// <summary>
        /// Sampled point list
        /// </summary>
        public List<Vector3> sampledPoints = new List<Vector3>();

        /// <summary>
        /// Sampled total meter length
        /// </summary>
        public float sampledTotalMeterLength = 0.0f;

        /// <summary>
        /// Length list for sampling
        /// </summary>
        public List<float> sampledLengthList = new List<float>();

        // ---------------------------------------------------------------------------------------

        /// <summary>
        /// Sampled point count
        /// </summary>
        public int sampledPointCount { get { return this.sampledPoints.Count; } }

        /// <summary>
        /// Constructor
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public EquidistantPointList()
        {

        }

        /// <summary>
        /// Is valid parameters
        /// </summary>
        /// <returns>yes</returns>
        // ---------------------------------------------------------------------------------------
        protected virtual bool isValidParameters()
        {
            return (
                this.sampledPointCount > 1 &&
                this.sampledLengthList.Count > 0 &&
                this.sampledLengthList.Count + 1 == this.sampledPointCount
                );
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_points">point list</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantPointList(
            IList<Vector3> _points,
            float _samplingUnitMeter
            )
        {

            // init
            {
                this.Update(_points, _samplingUnitMeter);
            }

        }

        /// <summary>
        /// Find a sampled point by normalized value
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>point</returns>
        // ---------------------------------------------------------------------------------------
        public virtual Vector3 FindSampledPointByNormalizeValue(float t01)
        {
            return this.FindSampledPointByMeter(this.sampledTotalMeterLength * t01);
        }

        /// <summary>
        /// Find a sampled point by meter value
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <returns>point</returns>
        // ---------------------------------------------------------------------------------------
        public virtual Vector3 FindSampledPointByMeter(float targetMeter)
        {

            if (!this.isValidParameters())
            {
                return Vector3.zero;
            }

            // -----------------------------

            float t01 = (this.sampledTotalMeterLength > 0.0f) ? Mathf.Clamp01(targetMeter / this.sampledTotalMeterLength) : 0.0f;

            int size = this.sampledLengthList.Count;

            float point_t01 = 0.0f;

            int i = Mathf.RoundToInt(t01 * (size - 1));

            float baseLength = this.sampledLengthList[i];

            // -----------------------------

            if (targetMeter <= this.sampledLengthList[0])
            {

                point_t01 = Mathf.InverseLerp(
                    0.0f,
                    this.sampledLengthList[0],
                    targetMeter
                    );

                return Vector3.Lerp(this.sampledPoints[0], this.sampledPoints[1], point_t01);

            }

            if (targetMeter > this.sampledTotalMeterLength)
            {
                return this.sampledPoints[this.sampledPoints.Count - 1];
            }

            // -----------------------------

            if (targetMeter <= baseLength)
            {

                float length1 = 0.0f;
                float length2 = 0.0f;

                for (; i >= 1; i--)
                {

                    length1 = this.sampledLengthList[i - 1];
                    length2 = this.sampledLengthList[i];

                    if (length1 <= targetMeter && targetMeter <= length2)
                    {

                        point_t01 = Mathf.InverseLerp(
                            length1,
                            length2,
                            targetMeter
                            );

                        return Vector3.Lerp(this.sampledPoints[i], this.sampledPoints[i + 1], point_t01);

                    }

                }

            }

            else
            {

                i = Mathf.Max(1, i);

                float length1 = 0.0f;
                float length2 = 0.0f;

                for (; i < size; i++)
                {

                    length1 = this.sampledLengthList[i - 1];
                    length2 = this.sampledLengthList[i];

                    if (targetMeter <= this.sampledLengthList[i])
                    {

                        point_t01 = Mathf.InverseLerp(
                            length1,
                            length2,
                            targetMeter
                            );

                        return Vector3.Lerp(this.sampledPoints[i], this.sampledPoints[i + 1], point_t01);

                    }

                }

            }

            return Vector3.zero;

        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_points">point list</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public virtual void Update(
            IList<Vector3> _points,
            float _samplingUnitMeter
            )
        {

            // clear
            {
                this.sampledPoints.Clear();
                this.sampledLengthList.Clear();
                this.sampledTotalMeterLength = 0.0f;
            }

            // samplingUnitMeter
            {
                this.samplingUnitMeter = Mathf.Max(0.0f, _samplingUnitMeter);
            }

            // check
            {

                if (
                    _points.Count <= 1 ||
                    this.samplingUnitMeter <= 0.0f
                    )
                {
                    return;
                }

            }

            // --------------------------

            float totalMeterLength = 0.0f;

            int size = _points.Count;

            // --------------------------

            // totalMeterLength
            {

                for (int i = 0; i < size - 1; i++)
                {
                    totalMeterLength += Vector3.Distance(_points[i], _points[i + 1]);
                }

            }

            // sample
            {

                int sampling =
                    (this.samplingUnitMeter > 0.0f) ?
                    Mathf.Max(2, Mathf.RoundToInt(totalMeterLength / this.samplingUnitMeter)) :
                    2
                    ;

                float targetMeter = 0.0f;

                float sampling_t01 = 0.0f;
                float point_t01 = 0.0f;

                int j = 0;

                float currTotalMeterLength = 0.0f;
                float prevTotalMeterLength = 0.0f;

                // -----------------------

                // sampledPoints
                {

                    // first
                    {
                        this.sampledPoints.Add(_points[0]);
                    }

                    // middle
                    {

                        for (int i = 1; i < sampling - 1; i++)
                        {

                            sampling_t01 = i / (float)(sampling - 1);

                            targetMeter = totalMeterLength * sampling_t01;

                            while (currTotalMeterLength < targetMeter)
                            {
                                prevTotalMeterLength = currTotalMeterLength;
                                currTotalMeterLength += Vector3.Distance(_points[j], _points[j + 1]);
                                j++;
                            }

                            point_t01 = Mathf.InverseLerp(
                                prevTotalMeterLength,
                                currTotalMeterLength,
                                targetMeter
                                );

                            // if (j >= 0)
                            this.sampledPoints.Add(Vector3.Lerp(_points[j - 1], _points[j], point_t01));

                        }

                    }

                    // last
                    {
                        this.sampledPoints.Add(_points[size - 1]);
                    }

                }

                // sampledLengthList
                {

                    size = this.sampledPoints.Count;

                    for (int i = 0; i < size - 1; i++)
                    {
                        this.sampledTotalMeterLength += Vector3.Distance(this.sampledPoints[i], this.sampledPoints[i + 1]);
                        this.sampledLengthList.Add(this.sampledTotalMeterLength);
                    }

                }

            }

        }

        /// <summary>
        /// Find sampled points
        /// </summary>
        /// <param name="dest">return</param>
        /// <param name="numberOfCuts">number of cuts (val > 1)</param>
        // ---------------------------------------------------------------------------------------
        public void FindSampledPoints(
            List<Vector3> dest,
            int numberOfCuts
            )
        {

            if (numberOfCuts <= 1)
            {
                return;
            }

            // -------------------

            dest.Clear();

            float lerpVal = 0.0f;

            // -------------------

            for (int i = 0; i < numberOfCuts; i++)
            {

                lerpVal = i / (float)(numberOfCuts - 1);

                dest.Add(this.FindSampledPointByNormalizeValue(lerpVal));

            }

        }

        /// <summary>
        /// Find a closest position from pointP
        /// </summary>
        /// <param name="pointP">pointP</param>
        /// <param name="position">position</param>
        /// <param name="from">from</param>
        /// <param name="to">to</param>
        // ---------------------------------------------------------------------------------------
        public void FindClosestPositionOnLines(
            Vector3 pointP,
            out Vector3 position,
            out Vector3 from,
            out Vector3 to
            )
        {

            position = Vector3.zero;
            from = Vector3.zero;
            to = Vector3.zero;

            // --------------

            Vector3 pointA = Vector3.zero;
            Vector3 pointB = Vector3.zero;

            Vector3 vecAtoB = Vector3.zero;
            Vector3 vecAtoP = Vector3.zero;

            float dot = 0.0f;
            float currentMinSqrDistance = float.MaxValue;

            Vector3 tempPosition = Vector3.zero;
            float tempSqrDistance = 0.0f;

            // --------------

            for (int i = 0; i < this.sampledPointCount - 1; i++)
            {

                pointA = this.sampledPoints[i];
                pointB = this.sampledPoints[i + 1];

                vecAtoB = pointB - pointA;
                vecAtoP = pointP - pointA;

                dot = Mathf.Clamp01(Vector3.Dot(vecAtoB, vecAtoP) / vecAtoB.sqrMagnitude);

                tempPosition = Vector3.Lerp(pointA, pointB, dot);

                tempSqrDistance = (pointP - tempPosition).sqrMagnitude;

                if (currentMinSqrDistance > tempSqrDistance)
                {
                    currentMinSqrDistance = tempSqrDistance;
                    position = tempPosition;
                    from = pointA;
                    to = pointB;
                }

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// Draw lines (EditorOnly)
        /// </summary>
        /// <param name="color">color</param>
        /// <param name="thickness">thickness</param>
        // ---------------------------------------------------------------------------------------
        public virtual void DrawLinesEditorOnly(
            Color color,
            float thickness = 3.0f
            )
        {

            // check
            {
                if (this.sampledPoints.Count <= 1)
                {
                    return;
                }
            }

            // -------------------------

            // draw
            {

                Vector3 posA = Vector3.zero;
                Vector3 posB = Vector3.zero;

                for (int i = 0; i < this.sampledPoints.Count - 1; i++)
                {

                    posA = this.sampledPoints[i];
                    posB = this.sampledPoints[i + 1];

                    UnityEditor.Handles.DrawBezier(
                        posA,
                        posB,
                        posA,
                        posB,
                        color,
                        null,
                        thickness
                        );

                }

            }

        }

#endif

    }

}
