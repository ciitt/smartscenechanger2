using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    public partial class EquidistantBezierCurve
    {

        /// <summary>
        /// Create the collidert by 2 beziers
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="startMeterA">start meter point on bezier A</param>
        /// <param name="endMeterA">end meter point on bezier A</param>
        /// <param name="startMeterB">start meter point on bezier B</param>
        /// <param name="endMeterB">end meter point on bezier B</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="markNoLogerReadable">markNoLogerReadable</param>
        // -------------------------------------------------------------------------------------------
        public static void CreateRoadCollider(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform = null,
            int numberOfCutsU = 2,
            int numberOfCutsV = -1,
            bool markNoLogerReadable = false
            )
        {

            // check
            {
                if (
                    bezierA == null ||
                    bezierB == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            startMeterA = Mathf.Clamp(startMeterA, 0, bezierA.sampledTotalMeterLength);
            startMeterB = Mathf.Clamp(startMeterB, 0, bezierB.sampledTotalMeterLength);
            endMeterA = Mathf.Clamp(endMeterA, 0, bezierA.sampledTotalMeterLength);
            endMeterB = Mathf.Clamp(endMeterB, 0, bezierB.sampledTotalMeterLength);

            numberOfCutsU = Mathf.Max(2, numberOfCutsU);
            numberOfCutsV = CalcClampedNumberOfCutsVIfNeeded(bezierA, bezierB, numberOfCutsV);

            int vertexCount = numberOfCutsU * numberOfCutsV;
            int triangleCount = 6 * (numberOfCutsU - 1) * (numberOfCutsV - 1);

            MeshFuncs.MeshDataLists meshDataLists = MeshFuncs.meshDataListsInstance.Clear(vertexCount, triangleCount);

            // ------------------------

            // vertices, uvs
            {

                Vector3 posA = Vector3.zero;
                Vector3 posB = Vector3.zero;

                int v = 0;
                int u = 0;

                int index = 0;

                float lerpX = 0.0f;
                float lerpY = 0.0f;

                float meterA = 0.0f;
                float meterB = 0.0f;

                float normalizedMeterA = 0.0f;
                float normalizedMeterB = 0.0f;

                for (v = 0; v < numberOfCutsV; v++)
                {

                    lerpY = v / (float)(numberOfCutsV - 1);

                    meterA = Mathf.Lerp(startMeterA, endMeterA, lerpY);
                    meterB = Mathf.Lerp(startMeterB, endMeterB, lerpY);

                    normalizedMeterA = bezierA.ConvertSampledMeterToNormalized01(meterA);
                    normalizedMeterB = bezierB.ConvertSampledMeterToNormalized01(meterB);

                    posA = (useSampledPoints) ? bezierA.FindSampledPointByMeter(meterA) : bezierA.FindRawBezierPoint(normalizedMeterA);
                    posB = (useSampledPoints) ? bezierB.FindSampledPointByMeter(meterB) : bezierB.FindRawBezierPoint(normalizedMeterB);

                    if (baseTransform)
                    {
                        posA = baseTransform.InverseTransformPoint(posA);
                        posB = baseTransform.InverseTransformPoint(posB);
                    }

                    for (u = 0; u < numberOfCutsU; u++)
                    {

                        lerpX = u / (float)(numberOfCutsU - 1);

                        meshDataLists.vertices[index++] = Vector3.Lerp(posA, posB, lerpX);

                    }

                }

            }

            // triangles
            {

                Vector3 dirA = (meshDataLists.vertices[1] - meshDataLists.vertices[0]).normalized;
                Vector3 dirB =
                    (bezierA.sampledTotalMeterLength > 0.0001f) ?
                    (meshDataLists.vertices[numberOfCutsU] - meshDataLists.vertices[0]).normalized :
                    (meshDataLists.vertices[numberOfCutsU + numberOfCutsV - 1] - meshDataLists.vertices[numberOfCutsU - 1]).normalized
                    ;

                bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                int trianglesCounter = 0;

                int baseIndex = 0;

                int v = 0;
                int u = 0;

                if (!flip)
                {

                    for (v = 0; v < numberOfCutsV - 1; v++)
                    {

                        baseIndex = (v * numberOfCutsU);

                        for (u = 0; u < numberOfCutsU - 1; u++)
                        {

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU + 1;

                        }

                    }

                }

                else
                {

                    for (v = 0; v < numberOfCutsV - 1; v++)
                    {

                        baseIndex = (v * numberOfCutsU);

                        for (u = 0; u < numberOfCutsU - 1; u++)
                        {

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU + 1;

                        }

                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.SetVertices(meshDataLists.vertices);
                dest.SetTriangles(meshDataLists.triangles, 0);

                dest.RecalculateBounds();

                dest.UploadMeshData(markNoLogerReadable);

            }

            // clear
            {
                meshDataLists.Clear();
            }

        }

        /// <summary>
        /// Calculate uv y at end
        /// </summary>
        /// <param name="targetMeterLength">target meter length</param>
        /// <param name="repeatUvMeterV">repeat uv meter along v</param>
        /// <returns>uv y</returns>
        // -------------------------------------------------------------------------------------------
        static float CalcEndUvY(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            float repeatUvMeterV
            )
        {

            float lengthA = (bezierA != null) ? bezierA.sampledTotalMeterLength : 1.0f;
            float lengthB = (bezierB != null) ? bezierB.sampledTotalMeterLength : 1.0f;

            float ave = (lengthA + lengthB) * 0.5f;

            return
                !Mathf.Approximately(repeatUvMeterV, 0.0f) ?
                Mathf.Max(1, Mathf.RoundToInt(ave / repeatUvMeterV)) :
                1.0f
                ;

        }

        /// <summary>
        /// Calculate clamped number of cuts v
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="numberOfCutsV">input</param>
        /// <returns>cuts</returns>
        // -------------------------------------------------------------------------------------------
        static int CalcClampedNumberOfCutsVIfNeeded(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            int numberOfCutsV
            )
        {

            if (numberOfCutsV >= 2)
            {
                return numberOfCutsV;
            }

            int countA = (bezierA != null) ? bezierA.sampledPointCount : 2;
            int countB = (bezierB != null) ? bezierB.sampledPointCount : 2;

            return Mathf.Clamp(Mathf.Max(countA, countB), 2, 100);

        }

        /// <summary>
        /// Create simple road by 2 beziers
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="startMeterA">start meter point on bezier A</param>
        /// <param name="endMeterA">end meter point on bezier A</param>
        /// <param name="startMeterB">start meter point on bezier B</param>
        /// <param name="endMeterB">end meter point on bezier B</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="generateLightmapUv">true to generate lightmap uv (uv2.xy)</param>
        /// <param name="markNoLogerReadable">markNoLogerReadable</param>
        // -------------------------------------------------------------------------------------------
        public static void CreateSimpleRoad(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            float repeatUvMeterV,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform = null,
            int numberOfCutsU = 2,
            int numberOfCutsV = -1,
            bool generateLightmapUv = false,
            bool markNoLogerReadable = false
            )
        {

            // check
            {
                if (
                    bezierA == null ||
                    bezierB == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            startMeterA = Mathf.Clamp(startMeterA, 0, bezierA.sampledTotalMeterLength);
            startMeterB = Mathf.Clamp(startMeterB, 0, bezierB.sampledTotalMeterLength);
            endMeterA = Mathf.Clamp(endMeterA, 0, bezierA.sampledTotalMeterLength);
            endMeterB = Mathf.Clamp(endMeterB, 0, bezierB.sampledTotalMeterLength);

            numberOfCutsU = Mathf.Max(2, numberOfCutsU);
            numberOfCutsV = CalcClampedNumberOfCutsVIfNeeded(bezierA, bezierB, numberOfCutsV);

            int vertexCount = numberOfCutsU * 2 * (numberOfCutsV - 1);
            int triangleCount = 6 * (numberOfCutsU - 1) * (numberOfCutsV - 1);

            // uv  = normalized uvX of 4 vertices, normalized uvY of 4 vertices, distanceFromLeft, distanceFromBottom
            // uv2 = lightmapUv.xy, currUvY, nextUvY
            // uv3 = distance0010, distance0111, distance0001, distance1011
            // uv4 = normalized uvY based on all vertices, unused, unused, unused

            MeshFuncs.MeshDataLists meshDataLists = MeshFuncs.meshDataListsInstance.Clear(vertexCount, triangleCount);

            float bezierLengthA = Mathf.Abs(endMeterA - startMeterA);
            float bezierLengthB = Mathf.Abs(endMeterB - startMeterB);

            float endUvY = CalcEndUvY(bezierA, bezierB, repeatUvMeterV);

            // ------------------------

            // vertices, uvs, normals
            {

                Vector3 pos00 = Vector3.zero;
                Vector3 pos10 = Vector3.zero;
                Vector3 pos01 = Vector3.zero;
                Vector3 pos11 = Vector3.zero;

                Vector3 normal00 = Vector3.zero;
                Vector3 normal10 = Vector3.zero;
                Vector3 normal01 = Vector3.zero;
                Vector3 normal11 = Vector3.zero;

                float distance0010 = 0.0f;
                float distance0111 = 0.0f;
                float distance0001 = 0.0f;
                float distance1011 = 0.0f;

                Vector4 distances = Vector4.zero;

                int v = 0;
                int u = 0;

                int baseIndex = 0;

                float lerpX = 0.0f;

                Vector3 tempA = Vector3.zero;
                Vector3 tempB = Vector3.zero;

                float meter01 = 0.0f;
                float meter11 = 0.0f;

                float normalizedMeter01 = 0.0f;
                float normalizedMeter11 = 0.0f;

                // first
                {

                    float thisStartMeterFromAs01 = bezierA.ConvertSampledMeterToNormalized01(startMeterA);
                    float otherStartMeterFromAs01 = bezierB.ConvertSampledMeterToNormalized01(startMeterB);

                    pos00 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(startMeterA) : bezierA.FindRawBezierPoint(thisStartMeterFromAs01);
                    pos10 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(startMeterB) : bezierB.FindRawBezierPoint(otherStartMeterFromAs01);

                    normal00 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, thisStartMeterFromAs01).normalized;
                    normal10 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, otherStartMeterFromAs01).normalized;

                    if (baseTransform)
                    {
                        pos00 = baseTransform.InverseTransformPoint(pos00);
                        pos10 = baseTransform.InverseTransformPoint(pos10);
                    }

                }

                // 
                {

                    float currLerpValV = 0.0f;
                    float nextLerpValV = 0.0f;

                    for (v = 0; v < numberOfCutsV - 1; v++)
                    {

                        currLerpValV = v / (float)(numberOfCutsV - 1);
                        nextLerpValV = (v + 1) / (float)(numberOfCutsV - 1);

                        meter01 = Mathf.Lerp(startMeterA, endMeterA, nextLerpValV);
                        meter11 = Mathf.Lerp(startMeterB, endMeterB, nextLerpValV);

                        normalizedMeter01 = bezierA.ConvertSampledMeterToNormalized01(meter01);
                        normalizedMeter11 = bezierB.ConvertSampledMeterToNormalized01(meter11);

                        pos01 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(meter01) : bezierA.FindRawBezierPoint(normalizedMeter01);
                        pos11 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(meter11) : bezierB.FindRawBezierPoint(normalizedMeter11);

                        if (baseTransform)
                        {
                            pos01 = baseTransform.InverseTransformPoint(pos01);
                            pos11 = baseTransform.InverseTransformPoint(pos11);
                        }

                        normal01 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, normalizedMeter01).normalized;
                        normal11 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, normalizedMeter11).normalized;

                        distance0010 = Vector3.Distance(pos00, pos10);
                        distance0111 = Vector3.Distance(pos01, pos11);
                        distance0001 = Vector3.Distance(pos00, pos01);
                        distance1011 = Vector3.Distance(pos10, pos11);

                        baseIndex = (v * numberOfCutsU * 2);

                        distances = new Vector4(
                            distance0010,
                            distance0111,
                            distance0001,
                            distance1011
                            );

                        for (u = 0; u < numberOfCutsU; u++)
                        {

                            lerpX = u / (float)(numberOfCutsU - 1);

                            tempA = Vector3.Lerp(pos00, pos10, lerpX);
                            tempB = Vector3.Lerp(pos01, pos11, lerpX);

                            meshDataLists.vertices[baseIndex + u] = tempA;
                            meshDataLists.vertices[baseIndex + u + numberOfCutsU] = tempB;

                            meshDataLists.uv[baseIndex + u] = new Vector4(
                                lerpX,
                                0.0f,
                                distance0010 * lerpX,
                                0.0f
                                );

                            meshDataLists.uv[baseIndex + u + numberOfCutsU] = new Vector4(
                                lerpX,
                                1.0f,
                                distance0111 * lerpX,
                                Mathf.Lerp(distance0001, distance1011, lerpX)
                                );

                            meshDataLists.uv2[baseIndex + u] = new Vector4(0, 0, currLerpValV * endUvY, nextLerpValV * endUvY);
                            meshDataLists.uv2[baseIndex + u + numberOfCutsU] = new Vector4(0, 0, currLerpValV * endUvY, nextLerpValV * endUvY);

                            meshDataLists.uv3[baseIndex + u] = distances;
                            meshDataLists.uv3[baseIndex + u + numberOfCutsU] = distances;

                            meshDataLists.uv4[baseIndex + u] = new Vector4(currLerpValV, 0, 0, 0);
                            meshDataLists.uv4[baseIndex + u + numberOfCutsU] = new Vector4(nextLerpValV, 0, 0, 0);

                            meshDataLists.normals[baseIndex + u] = Vector3.Lerp(normal00, normal10, lerpX).normalized;
                            meshDataLists.normals[baseIndex + u + numberOfCutsU] = Vector3.Lerp(normal01, normal11, lerpX).normalized;

                        }

                        // swap
                        {

                            pos00 = pos01;
                            pos10 = pos11;

                            normal00 = normal01;
                            normal10 = normal11;

                        }

                    }

                }

            }

            // triangles
            {

                Vector3 dirA = (meshDataLists.vertices[1] - meshDataLists.vertices[0]).normalized;
                Vector3 dirB =
                    (bezierA.sampledTotalMeterLength > 0.0001f) ?
                    (meshDataLists.vertices[numberOfCutsU] - meshDataLists.vertices[0]).normalized :
                    (meshDataLists.vertices[numberOfCutsU + numberOfCutsU - 1] - meshDataLists.vertices[numberOfCutsU - 1]).normalized
                    ;

                bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                int trianglesCounter = 0;

                int baseIndex = 0;

                int v = 0;
                int u = 0;

                if (!flip)
                {

                    for (v = 0; v < numberOfCutsV - 1; v++)
                    {

                        baseIndex = (v * numberOfCutsU * 2);

                        for (u = 0; u < numberOfCutsU - 1; u++)
                        {

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU + 1;

                        }

                    }

                }

                else
                {

                    for (v = 0; v < numberOfCutsV - 1; v++)
                    {

                        baseIndex = (v * numberOfCutsU * 2);

                        for (u = 0; u < numberOfCutsU - 1; u++)
                        {

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;

                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + 1;
                            meshDataLists.triangles[trianglesCounter++] = baseIndex + u + numberOfCutsU + 1;

                        }

                    }

                }

            }

            // calcRoadLightmapUv
            {
                if (generateLightmapUv)
                {
                    CalcRoadLightmapUv(meshDataLists.vertices, meshDataLists.uv2);
                }
            }

            // set
            {

                dest.Clear();

                dest.SetVertices(meshDataLists.vertices);
                dest.SetUVs(0, meshDataLists.uv);
                dest.SetUVs(1, meshDataLists.uv2);
                dest.SetUVs(2, meshDataLists.uv3);
                dest.SetUVs(3, meshDataLists.uv4);
                dest.SetTriangles(meshDataLists.triangles, 0);
                dest.SetNormals(meshDataLists.normals);

                dest.RecalculateBounds();

                dest.UploadMeshData(markNoLogerReadable);

            }

            // clear
            {
                meshDataLists.Clear();
            }

        }

        /// <summary>
        /// Create road by 2 beziers
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="startMeterA">start meter point on bezier A</param>
        /// <param name="endMeterA">end meter point on bezier A</param>
        /// <param name="startMeterB">start meter point on bezier B</param>
        /// <param name="endMeterB">end meter point on bezier B</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="generateLightmapUv">true to generate lightmap uv (uv2.xy)</param>
        [Obsolete]
        // -------------------------------------------------------------------------------------------
        public static void CreateComplexRoad(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            float repeatUvMeterV,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform = null,
            int numberOfCutsV = -1,
            bool generateLightmapUv = false
            )
        {

            // check
            {
                if (
                    bezierA == null ||
                    bezierB == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            startMeterA = Mathf.Max(0, startMeterA);
            startMeterB = Mathf.Max(0, startMeterB);

            endMeterA = Mathf.Min(endMeterA, bezierA.sampledTotalMeterLength);
            endMeterB = Mathf.Min(endMeterB, bezierB.sampledTotalMeterLength);

            numberOfCutsV = CalcClampedNumberOfCutsVIfNeeded(bezierA, bezierB, numberOfCutsV);

            int vertexCount = (numberOfCutsV - 1) * 4;
            int triangleCount = (numberOfCutsV - 1) * 6;

            // uv = pos00.xyz, pos11.x
            // uv2 = lightmapUv.xy, width00to10, width01to11
            // uv3 = pos10.xyz, pos11.y
            // uv4 = pos01.xyz, pos11.z
            // colors32 = currLerpValV, nextLerpValV, unused, 1 / uvScaleY

            MeshFuncs.MeshDataLists meshDataLists = MeshFuncs.meshDataListsInstance.Clear(vertexCount, triangleCount);

            float bezierLengthA = Mathf.Abs(endMeterA - startMeterA);
            float bezierLengthB = Mathf.Abs(endMeterB - startMeterB);

            // ------------------------

            // vertices, uvs (straight along z)
            {

                int trianglesCounter = 0;

                float currLerpValV = 0.0f;
                float nextLerpValV = 0.0f;

                float endUvY = CalcEndUvY(bezierA, bezierB, repeatUvMeterV);

                Color32 vertexColor = new Color32(0, 0, 0, 0);

                Vector3 localPos00 = Vector3.zero;
                Vector3 localPos10 = Vector3.zero;
                Vector3 localPos01 = Vector3.zero;
                Vector3 localPos11 = Vector3.zero;

                Vector3 worldPos00 = Vector3.zero;
                Vector3 worldPos10 = Vector3.zero;
                Vector3 worldPos01 = Vector3.zero;
                Vector3 worldPos11 = Vector3.zero;

                Vector3 normal00 = Vector3.zero;
                Vector3 normal10 = Vector3.zero;
                Vector3 normal01 = Vector3.zero;
                Vector3 normal11 = Vector3.zero;

                float meter01 = 0.0f;
                float meter11 = 0.0f;

                float width00to10 = 0.0f;
                float width01to11 = 0.0f;

                bool posFlip = (startMeterA > endMeterA);

                // first
                {

                    float startMeterA01 = bezierA.ConvertSampledMeterToNormalized01(startMeterA);
                    float startMeterB01 = bezierB.ConvertSampledMeterToNormalized01(startMeterB);

                    worldPos00 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(startMeterA) : bezierA.FindRawBezierPoint(startMeterA01);
                    worldPos10 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(startMeterB) : bezierB.FindRawBezierPoint(startMeterB01);

                    localPos00 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos00) : worldPos00;
                    localPos10 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos10) : worldPos10;

                    normal00 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, startMeterA01).normalized;
                    normal10 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, startMeterB01).normalized;

                    width00to10 = Vector3.Distance(worldPos00, worldPos10);

                }

                // vertices, uvs, normals
                {

                    int i = 0;

                    float normalizedMeter01 = 0.0f;
                    float normalizedMeter11 = 0.0f;

                    for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                    {

                        currLerpValV = v / (float)(numberOfCutsV - 1);
                        nextLerpValV = (v + 1) / (float)(numberOfCutsV - 1);

                        meter01 = Mathf.Lerp(startMeterA, endMeterA, nextLerpValV);
                        meter11 = Mathf.Lerp(startMeterB, endMeterB, nextLerpValV);

                        normalizedMeter01 = bezierA.ConvertSampledMeterToNormalized01(meter01);
                        normalizedMeter11 = bezierB.ConvertSampledMeterToNormalized01(meter11);

                        worldPos01 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(meter01) : bezierA.FindRawBezierPoint(normalizedMeter01);
                        worldPos11 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(meter11) : bezierB.FindRawBezierPoint(normalizedMeter11);

                        localPos01 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos01) : worldPos01;
                        localPos11 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos11) : worldPos11;

                        normal01 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, normalizedMeter01).normalized;
                        normal11 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, normalizedMeter11).normalized;

                        width01to11 = Vector3.Distance(worldPos01, worldPos11);

                        // set
                        {

                            meshDataLists.vertices[index + 0] = localPos00;
                            meshDataLists.vertices[index + 1] = localPos10;
                            meshDataLists.vertices[index + 2] = localPos01;
                            meshDataLists.vertices[index + 3] = localPos11;

                            if (!posFlip)
                            {

                                vertexColor.r = (byte)(currLerpValV * 255);
                                vertexColor.g = (byte)(nextLerpValV * 255);
                                vertexColor.b = 0;
                                vertexColor.a = (byte)((1.0f / endUvY) * 255);

                                for (i = 0; i < 4; i++)
                                {
                                    meshDataLists.uv[index + i] = new Vector4(worldPos00.x, worldPos00.y, worldPos00.z, worldPos11.x);
                                    meshDataLists.uv2[index + i] = new Vector4(0, 0, width00to10, width01to11);
                                    meshDataLists.uv3[index + i] = new Vector4(worldPos10.x, worldPos10.y, worldPos10.z, worldPos11.y);
                                    meshDataLists.uv4[index + i] = new Vector4(worldPos01.x, worldPos01.y, worldPos01.z, worldPos11.z);
                                }

                            }

                            else
                            {

                                vertexColor.g = (byte)(currLerpValV * 255);
                                vertexColor.r = (byte)(nextLerpValV * 255);
                                vertexColor.b = 0;
                                vertexColor.a = (byte)((1.0f / endUvY) * 255);

                                for (i = 0; i < 4; i++)
                                {
                                    meshDataLists.uv[index + i] = new Vector4(worldPos01.x, worldPos01.y, worldPos01.z, worldPos10.x);
                                    meshDataLists.uv2[index + i] = new Vector4(0, 0, width01to11, width00to10);
                                    meshDataLists.uv3[index + i] = new Vector4(worldPos11.x, worldPos11.y, worldPos11.z, worldPos10.y);
                                    meshDataLists.uv4[index + i] = new Vector4(worldPos00.x, worldPos00.y, worldPos00.z, worldPos10.z);
                                }

                            }

                            meshDataLists.normals[index + 0] = normal00;
                            meshDataLists.normals[index + 1] = normal10;
                            meshDataLists.normals[index + 2] = normal01;
                            meshDataLists.normals[index + 3] = normal11;

                            meshDataLists.color32[index + 0] = vertexColor;
                            meshDataLists.color32[index + 1] = vertexColor;
                            meshDataLists.color32[index + 2] = vertexColor;
                            meshDataLists.color32[index + 3] = vertexColor;

                        }

                        // swap
                        {

                            //meter00 = meter01;
                            //meter10 = meter11;

                            localPos00 = localPos01;
                            localPos10 = localPos11;

                            worldPos00 = worldPos01;
                            worldPos10 = worldPos11;

                            normal00 = normal01;
                            normal10 = normal11;

                            width00to10 = width01to11;

                        }

                    }

                }

                // triangles
                {

                    Vector3 dirA = (meshDataLists.vertices[1] - meshDataLists.vertices[0]).normalized;
                    Vector3 dirB =
                        (bezierA.sampledTotalMeterLength > 0.0001f) ?
                        (meshDataLists.vertices[2] - meshDataLists.vertices[0]).normalized :
                        (meshDataLists.vertices[3] - meshDataLists.vertices[1]).normalized
                        ;

                    bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                    if (!flip)
                    {

                        for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                        {

                            meshDataLists.triangles[trianglesCounter++] = index + 0;
                            meshDataLists.triangles[trianglesCounter++] = index + 2;
                            meshDataLists.triangles[trianglesCounter++] = index + 1;

                            meshDataLists.triangles[trianglesCounter++] = index + 1;
                            meshDataLists.triangles[trianglesCounter++] = index + 2;
                            meshDataLists.triangles[trianglesCounter++] = index + 3;

                        }

                    }

                    else
                    {

                        for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                        {

                            meshDataLists.triangles[trianglesCounter++] = index + 0;
                            meshDataLists.triangles[trianglesCounter++] = index + 1;
                            meshDataLists.triangles[trianglesCounter++] = index + 2;

                            meshDataLists.triangles[trianglesCounter++] = index + 1;
                            meshDataLists.triangles[trianglesCounter++] = index + 3;
                            meshDataLists.triangles[trianglesCounter++] = index + 2;

                        }

                    }

                }

            }

            // calcRoadLightmapUv
            {
                if (generateLightmapUv)
                {
                    CalcRoadLightmapUv(meshDataLists.vertices, meshDataLists.uv2);
                }
            }

            // set
            {

                dest.Clear();

                dest.SetVertices(meshDataLists.vertices);
                dest.SetUVs(0, meshDataLists.uv);
                dest.SetUVs(1, meshDataLists.uv2);
                dest.SetUVs(2, meshDataLists.uv3);
                dest.SetUVs(3, meshDataLists.uv4);
                dest.SetTriangles(meshDataLists.triangles, 0);
                dest.SetNormals(meshDataLists.normals);
                dest.SetColors(meshDataLists.color32);

                dest.RecalculateBounds();

                dest.UploadMeshData(false);

            }

            // clear
            {
                meshDataLists.Clear();
            }

        }

        /// <summary>
        /// Create road by 2 beziers
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="startMeterA">start meter point on bezier A</param>
        /// <param name="endMeterA">end meter point on bezier A</param>
        /// <param name="startMeterB">start meter point on bezier B</param>
        /// <param name="endMeterB">end meter point on bezier B</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="generateLightmapUv">true to generate lightmap uv (uv2.xy)</param>
        /// <param name="markNoLogerReadable">markNoLogerReadable</param>
        // -------------------------------------------------------------------------------------------
        public static void CreateComplexRoad2(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            float repeatUvMeterV,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform = null,
            int numberOfCutsV = -1,
            int numberOfCutsU = 2,
            bool generateLightmapUv = false,
            bool markNoLogerReadable = false
            )
        {

            // check
            {

                if (
                    bezierA == null ||
                    bezierB == null ||
                    !dest
                    )
                {
                    return;
                }

            }

            // -----------------------

            startMeterA = Mathf.Max(0, startMeterA);
            startMeterB = Mathf.Max(0, startMeterB);

            endMeterA = Mathf.Min(endMeterA, bezierA.sampledTotalMeterLength);
            endMeterB = Mathf.Min(endMeterB, bezierB.sampledTotalMeterLength);
            
            numberOfCutsV = CalcClampedNumberOfCutsVIfNeeded(bezierA, bezierB, numberOfCutsV);
            numberOfCutsU = Mathf.Clamp(numberOfCutsU, 2, 11);

            //int vertexCount = (numberOfCutsV - 1) * 4;
            //int triangleCount = (numberOfCutsV - 1) * 6;

            int vertexCount = (numberOfCutsV - 1) * numberOfCutsU * 2;
            int triangleCount = (numberOfCutsV - 1) * (numberOfCutsU - 1) * 6;

            // uv = pos00.2, pos10.2
            // uv2 = lightmapUv.xy, width00to10, width01to11
            // uv3 = pos01.2, pos11.2
            // uv4 = currLerpValV, nextLerpValV, uvScaleY, unused
            // colors32 = scale.xyz, unused

            MeshFuncs.MeshDataLists meshDataLists = MeshFuncs.meshDataListsInstance.Clear(vertexCount, triangleCount);

            float bezierLengthA = Mathf.Abs(endMeterA - startMeterA);
            float bezierLengthB = Mathf.Abs(endMeterB - startMeterB);

            float calcArea(
                Vector3 pos00,
                Vector3 pos10,
                Vector3 pos01,
                Vector3 pos11
                )
            {

                Vector3 vecA = pos01 - pos00;
                Vector3 vecB = pos11 - pos00;
                Vector3 vecC = pos10 - pos00;

                float areaA = Mathf.Abs(VectorFuncs.Wedge(vecA, vecB));
                float areaB = Mathf.Abs(VectorFuncs.Wedge(vecC, vecB));

                return areaA + areaB;

            }

            Vector3 calcScale(
                Vector3 pos00,
                Vector3 pos10,
                Vector3 pos01,
                Vector3 pos11
                )
            {

                Vector3 ret = Vector3.zero;

                Vector3 scale011 = new Vector3(0, 1, 1);
                Vector3 scale101 = new Vector3(1, 0, 1);
                Vector3 scale110 = new Vector3(1, 1, 0);

                float area011 = calcArea(
                    Vector3.Scale(pos00, scale011),
                    Vector3.Scale(pos10, scale011),
                    Vector3.Scale(pos01, scale011),
                    Vector3.Scale(pos11, scale011)
                    );

                float area101 = calcArea(
                    Vector3.Scale(pos00, scale101),
                    Vector3.Scale(pos10, scale101),
                    Vector3.Scale(pos01, scale101),
                    Vector3.Scale(pos11, scale101)
                    );

                float area110 = calcArea(
                    Vector3.Scale(pos00, scale110),
                    Vector3.Scale(pos10, scale110),
                    Vector3.Scale(pos01, scale110),
                    Vector3.Scale(pos11, scale110)
                    );

                if (area011 >= area101 && area011 >= area110)
                {
                    ret = scale011;
                }

                else if (area101 >= area011 && area101 >= area110)
                {
                    ret = scale101;
                }

                else
                {
                    ret = scale110;
                }

                return ret;

            }

            Vector2 convertPos(
                Vector3 pos,
                Vector3 scale
                )
            {

                Vector2 ret = Vector2.zero;

                if (scale.x <= 0.0f)
                {
                    ret.Set(pos.y, pos.z);
                }

                else if (scale.y <= 0.0f)
                {
                    ret.Set(pos.x, pos.z);
                }

                else
                {
                    ret.Set(pos.x, pos.y);
                }

                return ret;

            }

            // ------------------------

            // vertices, uvs (straight along z)
            {

                int trianglesCounter = 0;

                float currLerpValV = 0.0f;
                float nextLerpValV = 0.0f;

                float endUvY = CalcEndUvY(bezierA, bezierB, repeatUvMeterV);

                Color32 vertexColor = new Color32(0, 0, 0, 0);

                Vector3 localPos00 = Vector3.zero;
                Vector3 localPos10 = Vector3.zero;
                Vector3 localPos01 = Vector3.zero;
                Vector3 localPos11 = Vector3.zero;

                Vector3 worldPos00 = Vector3.zero;
                Vector3 worldPos10 = Vector3.zero;
                Vector3 worldPos01 = Vector3.zero;
                Vector3 worldPos11 = Vector3.zero;

                Vector2 pos00_2 = Vector2.zero;
                Vector2 pos10_2 = Vector2.zero;
                Vector2 pos01_2 = Vector2.zero;
                Vector2 pos11_2 = Vector2.zero;

                Vector3 normal00 = Vector3.zero;
                Vector3 normal10 = Vector3.zero;
                Vector3 normal01 = Vector3.zero;
                Vector3 normal11 = Vector3.zero;

                Vector3 scale = Vector3.one;

                float meter01 = 0.0f;
                float meter11 = 0.0f;

                float width00to10 = 0.0f;
                float width01to11 = 0.0f;

                bool posFlip = (startMeterA > endMeterA);

                // first
                {

                    float startMeterA01 = bezierA.ConvertSampledMeterToNormalized01(startMeterA);
                    float startMeterB01 = bezierB.ConvertSampledMeterToNormalized01(startMeterB);

                    worldPos00 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(startMeterA) : bezierA.FindRawBezierPoint(startMeterA01);
                    worldPos10 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(startMeterB) : bezierB.FindRawBezierPoint(startMeterB01);

                    normal00 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, startMeterA01).normalized;
                    normal10 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, startMeterB01).normalized;

                    localPos00 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos00) : worldPos00;
                    localPos10 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos10) : worldPos10;

                }

                // vertices, uvs, normals
                {

                    int u = 0;
                    int v = 0;

                    int index = 0;

                    int inc = numberOfCutsU * 2;

                    float normalizedMeter01 = 0.0f;
                    float normalizedMeter11 = 0.0f;

                    for (v = 0, index = 0; v < numberOfCutsV - 1; v++, index += inc)
                    {

                        currLerpValV = v / (float)(numberOfCutsV - 1);
                        nextLerpValV = (v + 1) / (float)(numberOfCutsV - 1);

                        meter01 = Mathf.Lerp(startMeterA, endMeterA, nextLerpValV);
                        meter11 = Mathf.Lerp(startMeterB, endMeterB, nextLerpValV);

                        normalizedMeter01 = bezierA.ConvertSampledMeterToNormalized01(meter01);
                        normalizedMeter11 = bezierB.ConvertSampledMeterToNormalized01(meter11);

                        worldPos01 = (useSampledPoints) ? bezierA.FindSampledPointByMeter(meter01) : bezierA.FindRawBezierPoint(normalizedMeter01);
                        worldPos11 = (useSampledPoints) ? bezierB.FindSampledPointByMeter(meter11) : bezierB.FindRawBezierPoint(normalizedMeter11);

                        normal01 = Vector3.Lerp(bezierA.startUpwards, bezierA.endUpwards, normalizedMeter01).normalized;
                        normal11 = Vector3.Lerp(bezierB.startUpwards, bezierB.endUpwards, normalizedMeter11).normalized;

                        localPos01 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos01) : worldPos01;
                        localPos11 = (baseTransform) ? baseTransform.InverseTransformPoint(worldPos11) : worldPos11;

                        //
                        {

                            scale = calcScale(localPos00, localPos10, localPos01, localPos11);

                            pos00_2 = convertPos(worldPos00, scale);
                            pos10_2 = convertPos(worldPos10, scale);
                            pos01_2 = convertPos(worldPos01, scale);
                            pos11_2 = convertPos(worldPos11, scale);

                            width00to10 = Vector2.Distance(pos00_2, pos10_2);
                            width01to11 = Vector2.Distance(pos01_2, pos11_2);

                            vertexColor.r = (byte)(scale.x * 255);
                            vertexColor.g = (byte)(scale.y * 255);
                            vertexColor.b = (byte)(scale.z * 255);

                        }

                        //
                        {

                            for (u = 0; u < numberOfCutsU; u++)
                            {

                                float lerpX = u / (float)(numberOfCutsU - 1);

                                meshDataLists.vertices[index + u] = Vector3.Lerp(localPos00, localPos10, lerpX);
                                meshDataLists.vertices[index + u + numberOfCutsU] = Vector3.Lerp(localPos01, localPos11, lerpX);

                                meshDataLists.normals[index + u] = Vector3.Lerp(normal00, normal10, lerpX);
                                meshDataLists.normals[index + u + numberOfCutsU] = Vector3.Lerp(normal01, normal11, lerpX);

                                meshDataLists.color32[index + u] = vertexColor;
                                meshDataLists.color32[index + u + numberOfCutsU] = vertexColor;

                                if (!posFlip)
                                {

                                    meshDataLists.uv[index + u] = new Vector4(pos00_2.x, pos00_2.y, pos10_2.x, pos10_2.y);
                                    meshDataLists.uv2[index + u] = new Vector4(0, 0, width00to10, width01to11);
                                    meshDataLists.uv3[index + u] = new Vector4(pos01_2.x, pos01_2.y, pos11_2.x, pos11_2.y);
                                    meshDataLists.uv4[index + u] = new Vector4(currLerpValV, nextLerpValV, endUvY, 0);

                                    meshDataLists.uv[index + u + numberOfCutsU] = new Vector4(pos00_2.x, pos00_2.y, pos10_2.x, pos10_2.y);
                                    meshDataLists.uv2[index + u + numberOfCutsU] = new Vector4(0, 0, width00to10, width01to11);
                                    meshDataLists.uv3[index + u + numberOfCutsU] = new Vector4(pos01_2.x, pos01_2.y, pos11_2.x, pos11_2.y);
                                    meshDataLists.uv4[index + u + numberOfCutsU] = new Vector4(currLerpValV, nextLerpValV, endUvY, 0);

                                }

                                else
                                {

                                    meshDataLists.uv[index + u] = new Vector4(pos01_2.x, pos01_2.y, pos11_2.x, pos11_2.y);
                                    meshDataLists.uv2[index + u] = new Vector4(0, 0, width01to11, width00to10);
                                    meshDataLists.uv3[index + u] = new Vector4(pos00_2.x, pos00_2.y, pos10_2.x, pos10_2.y);
                                    meshDataLists.uv4[index + u] = new Vector4(nextLerpValV, currLerpValV, endUvY, 0);

                                    meshDataLists.uv[index + u + numberOfCutsU] = new Vector4(pos01_2.x, pos01_2.y, pos11_2.x, pos11_2.y);
                                    meshDataLists.uv2[index + u + numberOfCutsU] = new Vector4(0, 0, width01to11, width00to10);
                                    meshDataLists.uv3[index + u + numberOfCutsU] = new Vector4(pos00_2.x, pos00_2.y, pos10_2.x, pos10_2.y);
                                    meshDataLists.uv4[index + u + numberOfCutsU] = new Vector4(nextLerpValV, currLerpValV, endUvY, 0);

                                }

                            }

                        }

                        // swap
                        {

                            //meter00 = meter01;
                            //meter10 = meter11;

                            localPos00 = localPos01;
                            localPos10 = localPos11;

                            worldPos00 = worldPos01;
                            worldPos10 = worldPos11;

                            normal00 = normal01;
                            normal10 = normal11;

                            width00to10 = width01to11;

                        }

                    }

                }

                // triangles
                {

                    Vector3 dirA = (meshDataLists.vertices[1] - meshDataLists.vertices[0]).normalized;
                    Vector3 dirB =
                        (bezierA.sampledTotalMeterLength > 0.0001f) ?
                        (meshDataLists.vertices[numberOfCutsU] - meshDataLists.vertices[0]).normalized :
                        (meshDataLists.vertices[numberOfCutsU + 1] - meshDataLists.vertices[1]).normalized
                        ;

                    bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                    int u = 0;
                    int v = 0;

                    int index = 0;

                    int inc = numberOfCutsU * 2;

                    if (!flip)
                    {
                        
                        for (v = 0, index = 0; v < numberOfCutsV - 1; v++, index += inc)
                        {

                            for (u = 0; u < numberOfCutsU - 1; u++)
                            {

                                meshDataLists.triangles[trianglesCounter++] = index + u + 0;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU;
                                meshDataLists.triangles[trianglesCounter++] = index + u + 1;

                                meshDataLists.triangles[trianglesCounter++] = index + u + 1;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU + 1;

                            }

                        }

                    }

                    else
                    {

                        for (v = 0, index = 0; v < numberOfCutsV - 1; v++, index += inc)
                        {

                            for (u = 0; u < numberOfCutsU - 1; u++)
                            {

                                meshDataLists.triangles[trianglesCounter++] = index + u + 0;
                                meshDataLists.triangles[trianglesCounter++] = index + u + 1;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU;

                                meshDataLists.triangles[trianglesCounter++] = index + u + 1;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU + 1;
                                meshDataLists.triangles[trianglesCounter++] = index + u + numberOfCutsU;

                            }

                        }

                    }

                }

            }

            // calcRoadLightmapUv
            {
                if (generateLightmapUv)
                {
                    CalcRoadLightmapUv(meshDataLists.vertices, meshDataLists.uv2);
                }
            }

            // set
            {

                dest.Clear();

                dest.SetVertices(meshDataLists.vertices);
                dest.SetUVs(0, meshDataLists.uv);
                dest.SetUVs(1, meshDataLists.uv2);
                dest.SetUVs(2, meshDataLists.uv3);
                dest.SetUVs(3, meshDataLists.uv4);
                dest.SetTriangles(meshDataLists.triangles, 0);
                dest.SetNormals(meshDataLists.normals);
                dest.SetColors(meshDataLists.color32);

                dest.RecalculateBounds();

                dest.UploadMeshData(markNoLogerReadable);

            }

            // clear
            {
                meshDataLists.Clear();
            }

        }

        /// <summary>
        /// Calculate lightmap uvs for a road
        /// </summary>
        /// <param name="vertices">vertices</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        static void CalcRoadLightmapUv(
            List<Vector3> vertices,
            List<Vector4> dest
            )
        {

            int size = vertices.Count;

            if (
                size != dest.Count ||
                size <= 3
                )
            {
                // Debug.LogError("vertices.Length != dest.Count in EquidistantBezierCurve.calcLightmapUv");
                return;
            }

            // -------------------

            float minX = 1000000.0f;
            float minZ = 1000000.0f;
            float maxX = -1000000.0f;
            float maxZ = -1000000.0f;

            // -------------------

            // calc
            {

                Vector3 pos00 = vertices[0];
                Vector3 pos10 = vertices[1];
                Vector3 pos01 = Vector3.zero;
                Vector3 pos11 = Vector3.zero;

                Vector3 currPosOnL = Vector3.zero;
                Vector3 currPosOnR = new Vector3((pos10 - pos00).magnitude, 0, 0);
                Vector3 nextPosOnL = Vector3.zero;
                Vector3 nextPosOnR = Vector3.zero;

                Vector3 vec00To10 = Vector3.right;
                Vector3 vec00To01 = Vector3.forward;
                Vector3 vec10To11 = Vector3.forward;

                Vector3 dir00To10 = Vector3.right;
                Vector3 dir00To01 = Vector3.forward;
                Vector3 dir10To11 = Vector3.forward;

                float distance00to01 = 0.0f;
                float distance10to11 = 0.0f;

                float angle00To01 = 0.0f;
                float angle10To11 = 0.0f;

                //
                {
                    minX = Mathf.Min(currPosOnL.x, currPosOnR.x, minX);
                    maxX = Mathf.Max(currPosOnL.x, currPosOnR.x, maxX);
                    minZ = Mathf.Min(currPosOnL.z, currPosOnR.z, minZ);
                    maxZ = Mathf.Max(currPosOnL.z, currPosOnR.z, maxZ);
                }

                for (int i = 0; i < size; i += 4)
                {

                    pos00 = vertices[i + 0];
                    pos10 = vertices[i + 1];
                    pos01 = vertices[i + 2];
                    pos11 = vertices[i + 3];

                    vec00To10 = (pos10 - pos00);
                    vec00To01 = (pos01 - pos00);
                    vec10To11 = (pos11 - pos10);

                    angle00To01 = Vector3.SignedAngle(vec00To10, vec00To01, Vector3.up);
                    angle10To11 = Vector3.SignedAngle(vec00To10, vec10To11, Vector3.up);

                    distance00to01 = (pos01 - pos00).magnitude;
                    distance10to11 = (pos11 - pos10).magnitude;

                    dir00To10 = (currPosOnR - currPosOnL).normalized;
                    dir00To01 = Quaternion.AngleAxis(angle00To01, Vector3.up) * dir00To10;
                    dir10To11 = Quaternion.AngleAxis(angle10To11, Vector3.up) * dir00To10;

                    nextPosOnL = currPosOnL + (distance00to01 * dir00To01);
                    nextPosOnR = currPosOnR + (distance10to11 * dir10To11);

                    // --------------------

                    dest[i + 0] = new Vector4(currPosOnL.x, currPosOnL.z, dest[i + 0].z, dest[i + 0].w);
                    dest[i + 1] = new Vector4(currPosOnR.x, currPosOnR.z, dest[i + 1].z, dest[i + 1].w);
                    dest[i + 2] = new Vector4(nextPosOnL.x, nextPosOnL.z, dest[i + 2].z, dest[i + 2].w);
                    dest[i + 3] = new Vector4(nextPosOnR.x, nextPosOnR.z, dest[i + 3].z, dest[i + 3].w);

                    minX = Mathf.Min(nextPosOnL.x, nextPosOnR.x, minX);
                    maxX = Mathf.Max(nextPosOnL.x, nextPosOnR.x, maxX);
                    minZ = Mathf.Min(nextPosOnL.z, nextPosOnR.z, minZ);
                    maxZ = Mathf.Max(nextPosOnL.z, nextPosOnR.z, maxZ);

                    // --------------------

                    currPosOnL = nextPosOnL;
                    currPosOnR = nextPosOnR;

                }

            }

            // offset and scale
            {

                float diffX = (maxX - minX);
                float diffZ = (maxZ - minZ);

                if (
                    diffX > 0.0f &&
                    diffZ > 0.0f
                    )
                {

                    float offsetX = -minX;
                    float offsetZ = -minZ;
                    float scaleX = 1.0f / diffX;
                    float scaleZ = 1.0f / diffZ;

                    float minScale = Mathf.Min(scaleX, scaleZ);

                    Vector4 temp = Vector4.zero;

                    for (int i = 0; i < size; i++)
                    {

                        temp = dest[i];

                        dest[i] = new Vector4(
                            (temp.x + offsetX) * minScale,
                            (temp.y + offsetZ) * minScale,
                            temp.z,
                            temp.w
                            );

                    }

                }

            }

        }

    }

}

