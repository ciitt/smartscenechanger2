using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace SSC2
{

    public partial class EquidistantBezierCurve
    {

        /// <summary>
        /// Up direction type
        /// </summary>
        public enum UpDirType
        {
            BezierUp,
            WorldY
        }

        /// <summary>
        /// Temp PointOnBezier for deform
        /// </summary>
        static PointOnBezier _tempPobForDeform1 = null;

        /// <summary>
        /// Temp PointOnBezier for deform
        /// </summary>
        static PointOnBezier _tempPobForDeform2 = null;

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Temp PointOnBezier for deform
        /// </summary>
        static PointOnBezier tempPobForDeformIntance1
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempPobForDeform1); }
        }

        /// <summary>
        /// Temp PointOnBezier for deform
        /// </summary>
        static PointOnBezier tempPobForDeformIntance2
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempPobForDeform2); }
        }

        /// <summary>
        /// Deform mesh along this bezier
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="forwardAxis">forward axis to deform along</param>
        /// <param name="startMeterA">offset meter at start for bezierA</param>
        /// <param name="endMeterA">offset meter at end for bezierA</param>
        /// <param name="startMeterB">offset meter at start for bezierB</param>
        /// <param name="endMeterB">offset meter at end for bezierB</param>
        /// <param name="sourceAndDest"></param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="baseTransform">base transform</param>
        // -------------------------------------------------------------------------------------------
        public static void DeformMeshAlongBezier(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            Axis forwardAxis,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            Mesh sourceAndDest,
            bool useSampledPoints,
            Transform baseTransform
            )
        {

            DeformMeshAlongBezier(
                bezierA,
                bezierB,
                forwardAxis,
                startMeterA,
                endMeterA,
                startMeterB,
                endMeterB,
                sourceAndDest,
                useSampledPoints,
                baseTransform,
                UpDirType.BezierUp,
                null
                );

        }

        /// <summary>
        /// Deform mesh along this bezier
        /// </summary>
        /// <param name="bezierA">bezier A</param>
        /// <param name="bezierB">bezier B</param>
        /// <param name="forwardAxis">forward axis to deform along</param>
        /// <param name="startMeterA">offset meter at start for bezierA</param>
        /// <param name="endMeterA">offset meter at end for bezierA</param>
        /// <param name="startMeterB">offset meter at start for bezierB</param>
        /// <param name="endMeterB">offset meter at end for bezierB</param>
        /// <param name="sourceAndDest"></param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="baseTransform">base transform</param>
        /// <param name="upDirType">UpDirType</param>
        /// <param name="raycastInfo">raycast info</param>
        // -------------------------------------------------------------------------------------------
        public static void DeformMeshAlongBezier(
            EquidistantBezierCurve bezierA,
            EquidistantBezierCurve bezierB,
            Axis forwardAxis,
            float startMeterA,
            float endMeterA,
            float startMeterB,
            float endMeterB,
            Mesh sourceAndDest,
            bool useSampledPoints,
            Transform baseTransform,
            UpDirType upDirType,
            RaycastInfo raycastInfo
            )
        {

            // check
            {

                if (
                    bezierA == null ||
                    bezierB == null ||
                    !MeshFuncs.CheckMeshReadable(sourceAndDest)
                    )
                {
                    return;
                }

            }

            // -----------------------

            Vector3[] vertices = sourceAndDest.vertices;

            Bounds bounds = sourceAndDest.bounds;

            Vector3 sourceBoundsSize = bounds.size;
            Vector3 sourceBoundsMin = bounds.min;
            Vector3 sourceBoundsMax = bounds.max;

            float length = 0.0f;

            startMeterA = Mathf.Clamp(startMeterA, 0, bezierA.sampledTotalMeterLength);
            startMeterB = Mathf.Clamp(startMeterB, 0, bezierB.sampledTotalMeterLength);

            endMeterA = Mathf.Clamp(endMeterA, 0, bezierA.sampledTotalMeterLength);
            endMeterB = Mathf.Clamp(endMeterB, 0, bezierB.sampledTotalMeterLength);

            // -----------------------

            // length
            {

                if (forwardAxis == Axis.X)
                {
                    length += sourceBoundsSize.x;
                }

                else if (forwardAxis == Axis.Y)
                {
                    length += sourceBoundsSize.y;
                }

                else
                {
                    length += sourceBoundsSize.z;
                }

            }

            // check
            {

                if (length <= 0.0f)
                {
                    return;
                }

            }

            // -----------------------

            Action<float, EquidistantBezierCurve.PointOnBezier> actA = bezierA.GetFindPointByNormalizedValueAction(useSampledPoints);
            Action<float, EquidistantBezierCurve.PointOnBezier> actB = bezierB.GetFindPointByNormalizedValueAction(useSampledPoints);

            float start01A = bezierA.ConvertSampledMeterToNormalized01(startMeterA);
            float end01A = bezierA.ConvertSampledMeterToNormalized01(endMeterA);
            float start01B = bezierB.ConvertSampledMeterToNormalized01(startMeterB);
            float end01B = bezierB.ConvertSampledMeterToNormalized01(endMeterB);

            void func(
                Vector3 vertex,
                ref float _lerpValForward,
                ref float _lerpValRight,
                ref float _upVal
                )
            {

                if (forwardAxis == Axis.X)
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMin.x, sourceBoundsMax.x, vertex.x);
                    _lerpValRight = Mathf.InverseLerp(sourceBoundsMax.z, sourceBoundsMin.z, vertex.z);
                    _upVal = vertex.y;
                }

                else if (forwardAxis == Axis.Y)
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMin.y, sourceBoundsMax.y, vertex.y);
                    _lerpValRight = Mathf.InverseLerp(sourceBoundsMin.x, sourceBoundsMax.x, vertex.x);
                    _upVal = -vertex.z;
                }

                else
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMin.z, sourceBoundsMax.z, vertex.z);
                    _lerpValRight = Mathf.InverseLerp(sourceBoundsMin.x, sourceBoundsMax.x, vertex.x);
                    _upVal = vertex.y;
                }

            }

            // -----------------------

            // vertices
            {

                Vector3 vertex = Vector3.zero;
                Vector3 upDir = Vector3.up;

                float lerpValForward = 0.0f;
                float lerpValRight = 0.0f;
                float upVal = 0.0f;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                for (int i = 0; i < vertices.Length; i++)
                {

                    // func
                    {
                        func(vertices[i], ref lerpValForward, ref lerpValRight, ref upVal);
                    }

                    // tempPointOnBezier
                    {

                        actA(Mathf.Lerp(start01A, end01A, lerpValForward), tempPobForDeformIntance1);
                        actB(Mathf.Lerp(start01B, end01B, lerpValForward), tempPobForDeformIntance2);
                    }

                    // vertices
                    {

                        vertex = Vector3.Lerp(tempPobForDeformIntance1.position, tempPobForDeformIntance2.position, lerpValRight);

                        upDir =
                            (upDirType == UpDirType.BezierUp) ?
                            Vector3.Lerp(tempPobForDeformIntance1.up, tempPobForDeformIntance2.up, lerpValRight).normalized :
                            Vector3.up
                            ;

                        if (
                            raycastInfo != null &&
                            raycastInfo.raycastMaxDistance > 0.0f
                            )
                        {

                            ray.origin = vertex + (upDir * raycastInfo.raycastMaxDistance);
                            ray.direction = -upDir;

                            if (Physics.Raycast(ray, out hit, raycastInfo.raycastMaxDistance * 2, raycastInfo.targetLayers))
                            {
                                vertex = hit.point + (upDir * raycastInfo.offsetFromHitPosition);
                            }

                        }

                        vertex = vertex + (upDir * upVal);
                        
                        vertices[i] = (baseTransform) ? baseTransform.InverseTransformPoint(vertex) : vertex;

                    }

                }

            }

            // vertices
            {
                sourceAndDest.vertices = vertices;
            }

        }

        /// <summary>
        /// Deform mesh along this bezier
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="forwardAxis">axis to deform along</param>
        /// <param name="spaceMeterAtStart">space meter at start</param>
        /// <param name="spaceMeterAtEnd">space meter at end</param>
        /// <param name="baseTransform">base transform</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        // -------------------------------------------------------------------------------------------
        public void DeformMeshAlongBezier(
            Mesh sourceAndDest,
            Axis forwardAxis,
            float spaceMeterAtStart,
            float spaceMeterAtEnd,
            Transform baseTransform,
            bool useSampledPoints
            )
        {

            this.DeformMeshAlongBezier(
                sourceAndDest,
                forwardAxis,
                spaceMeterAtStart,
                spaceMeterAtEnd,
                baseTransform,
                useSampledPoints,
                UpDirType.BezierUp,
                null
                );

        }

        /// <summary>
        /// Deform mesh along this bezier
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="forwardAxis">axis to deform along</param>
        /// <param name="spaceMeterAtStart">space meter at start</param>
        /// <param name="spaceMeterAtEnd">space meter at end</param>
        /// <param name="baseTransform">base transform</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="upDirType">UpDirType</param>
        /// <param name="raycastInfo">raycast info</param>
        // -------------------------------------------------------------------------------------------
        public void DeformMeshAlongBezier(
            Mesh sourceAndDest,
            Axis forwardAxis,
            float spaceMeterAtStart,
            float spaceMeterAtEnd,
            Transform baseTransform,
            bool useSampledPoints,
            UpDirType upDirType,
            RaycastInfo raycastInfo
            )
        {

            // check
            {

                if (
                    this.sampledTotalMeterLength <= 0.0f ||
                    !sourceAndDest
                    )
                {
                    return;
                }

                if (!sourceAndDest.isReadable)
                {
                    Debug.LogWarningFormat("The mesh is not readable : {0}", sourceAndDest.name);
                    return;
                }

            }

            // -----------------------

            Vector3[] vertices = sourceAndDest.vertices;

            Bounds bounds = sourceAndDest.bounds;

            Vector3 sourceBoundsSize = bounds.size;
            Vector3 sourceBoundsMin = bounds.min;
            Vector3 sourceBoundsMax = bounds.max;

            // -----------------------

            Action<float, EquidistantBezierCurve.PointOnBezier> act = this.GetFindPointByNormalizedValueAction(useSampledPoints);

            void func(
                Vector3 vertex,
                ref float _lerpValForward,
                ref float _normalRightVal,
                ref float _upVal
                )
            {

                if (forwardAxis == Axis.X)
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMax.x, sourceBoundsMin.x, vertex.x);
                    _normalRightVal = -vertex.z;
                    _upVal = vertex.y;
                }

                else if (forwardAxis == Axis.Y)
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMin.y, sourceBoundsMax.y, vertex.y);
                    _normalRightVal = vertex.x;
                    _upVal = -vertex.z;
                }

                else
                {
                    _lerpValForward = Mathf.InverseLerp(sourceBoundsMin.z, sourceBoundsMax.z, vertex.z);
                    _normalRightVal = vertex.x;
                    _upVal = vertex.y;
                }

            }

            // -----------------------

            // vertices
            {

                float lerpValForward = 0.0f;
                float normalRightVal = 0.0f;
                float upVal = 0.0f;

                float start01 = this.ConvertSampledMeterToNormalized01(spaceMeterAtStart);
                float end01 = 1.0f - this.ConvertSampledMeterToNormalized01(spaceMeterAtEnd);

                Vector3 vertex = Vector3.zero;

                Vector3 up = Vector3.up;
                Vector3 normalRight = Vector3.right;

                Vector3 basePos = Vector3.zero;

                Ray ray = new Ray(Vector3.zero, -Vector3.up);
                RaycastHit hit = new RaycastHit();

                for (int i = 0; i < vertices.Length; i++)
                {

                    // func
                    {
                        func(vertices[i], ref lerpValForward, ref normalRightVal, ref upVal);
                    }

                    // tempPointOnBezier
                    {
                        act(Mathf.Lerp(start01, end01, lerpValForward), tempPobInstance);
                    }

                    // dir
                    {

                        if (upDirType == UpDirType.BezierUp)
                        {
                            up = tempPobInstance.up;
                            normalRight = tempPobInstance.normalRight;
                        }

                        else
                        {
                            up = Vector3.up;
                            normalRight.Set(tempPobInstance.normalRight.x, 0.0f, tempPobInstance.normalRight.z);
                            normalRight.Normalize();
                        }

                    }

                    // basePos
                    {

                        basePos = tempPobInstance.position;

                        if (
                            raycastInfo != null &&
                            raycastInfo.raycastMaxDistance > 0.0f
                            )
                        {

                            ray.origin = basePos + (up * raycastInfo.raycastMaxDistance);
                            ray.direction = -up;

                            if (Physics.Raycast(ray, out hit, raycastInfo.raycastMaxDistance * 2, raycastInfo.targetLayers))
                            {
                                basePos = hit.point + (up * raycastInfo.offsetFromHitPosition);
                            }

                        }

                    }

                    // vertices
                    {

                        vertex =
                            basePos +
                            (normalRight * normalRightVal) +
                            (up * upVal)
                            ;

                        vertices[i] = (baseTransform) ? baseTransform.InverseTransformPoint(vertex) : vertex;

                    }

                }

            }

            // vertices
            {
                sourceAndDest.vertices = vertices;
            }

        }

        /// <summary>
        /// Create straight bezier
        /// </summary>
        /// <param name="from">from</param>
        /// <param name="to">to</param>
        /// <param name="startUpwards">start upwards</param>
        /// <param name="endUpwards">end upwards</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public static void CreateStraightBezier(
            Vector3 from,
            Vector3 to,
            Vector3 startUpwards,
            Vector3 endUpwards,
            EquidistantBezierCurve dest
            )
        {

            dest?.Update(
                from,
                Vector3.Lerp(from, to, 0.25f),
                Vector3.Lerp(from, to, 0.75f),
                to,
                Vector3.Distance(from, to),
                startUpwards,
                endUpwards
                );

        }

    }

}
