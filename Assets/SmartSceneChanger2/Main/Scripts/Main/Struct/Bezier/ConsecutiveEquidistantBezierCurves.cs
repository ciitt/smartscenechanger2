﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Consecutive EquidistantBezierCurve
    /// </summary>
    public partial class ConsecutiveEquidistantBezierCurves
    {

        /// <summary>
        /// EquidistantBezierCurve list
        /// </summary>
        public List<EquidistantBezierCurve> bezieList = new List<EquidistantBezierCurve>();

        /// <summary>
        /// Sampled all total meter length
        /// </summary>
        public float sampledAllTotalMeterLength = 0.0f;

        /// <summary>
        /// Temp position list
        /// </summary>
        protected static List<Vector3> _tempPositionList = null;

        /// <summary>
        /// Temp upward list
        /// </summary>
        protected static List<Vector3> _tempUpwardList = null;

        // ---------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Temp position list
        /// </summary>
        protected static List<Vector3> tempPositionListInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempPositionList); }
        }

        /// <summary>
        /// Temp upward list
        /// </summary>
        protected static List<Vector3> tempUpwardListInstance
        {
            get { return Funcs.CreateInstanceIfNeeded(ref _tempUpwardList); }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------
        public ConsecutiveEquidistantBezierCurves()
        {
            
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_bezieList">bezieList</param>
        // ---------------------------------------------------------------------------------------------------------
        public ConsecutiveEquidistantBezierCurves(IList<EquidistantBezierCurve> _bezieList)
        {
            this.SetBezierList(_bezieList);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="basePointList">base point list</param>
        /// <param name="samplingUnitMeter">sampling unit meter</param>
        /// <param name="close">close</param>
        /// <param name="tangentScale">tangent scale</param>
        // ---------------------------------------------------------------------------------------------------------
        public ConsecutiveEquidistantBezierCurves(
            IList<Vector3> basePointList,
            float samplingUnitMeter,
            bool close,
            float tangentScale = 0.25f
            )
        {
            this.SetPointList(basePointList, samplingUnitMeter, close, tangentScale);
        }

        /// <summary>
        /// Set bezier list
        /// </summary>
        /// <param name="_bezieList">bezieList</param>
        // ---------------------------------------------------------------------------------------------------------
        public virtual void SetBezierList(IList<EquidistantBezierCurve> _bezieList)
        {

            // bezieList
            {
                this.bezieList.Clear();
                this.bezieList.AddRange(_bezieList);
            }

            // init
            {
                this.Init();
            }

        }

        /// <summary>
        /// Set point list
        /// </summary>
        /// <param name="basePointList">base point list</param>
        /// <param name="samplingUnitMeter">sampling unit meter</param>
        /// <param name="close">close</param>
        /// <param name="tangentScale">tangent scale</param>
        // ---------------------------------------------------------------------------------------------------------
        public virtual void SetPointList(
            IList<Vector3> basePointList,
            float samplingUnitMeter,
            bool close,
            float tangentScale = 1.0f
            )
        {

            List<Vector3> tempPositionList = tempPositionListInstance;
            List<Vector3> tempUpwardList = tempUpwardListInstance;

            // clear
            {
                tempPositionList.Clear();
                tempUpwardList.Clear();
            }

            // add
            {
                for (int i = 0; i < basePointList.Count; i++)
                {
                    tempUpwardList.Add(Vector3.up);
                }
            }

            // SetPointList
            {
                this.SetPointList(basePointList, tempUpwardList, samplingUnitMeter, close, tangentScale);
            }

            // clear
            {
                tempPositionList.Clear();
                tempUpwardList.Clear();
            }

        }

        /// <summary>
        /// Set point list
        /// </summary>
        /// <param name="transformPointList">transform point list</param>
        /// <param name="samplingUnitMeter">sampling unit meter</param>
        /// <param name="close">close</param>
        /// <param name="tangentScale">tangent scale</param>
        // ---------------------------------------------------------------------------------------------------------
        public virtual void SetPointList(
            IList<Transform> transformPointList,
            float samplingUnitMeter,
            bool close,
            float tangentScale = 1.0f
            )
        {

            List<Vector3> tempPositionList = tempPositionListInstance;
            List<Vector3> tempUpwardList = tempUpwardListInstance;

            // clear
            {
                tempPositionList.Clear();
                tempUpwardList.Clear();
            }

            // add
            {

                Transform trans = null;

                for (int i = 0; i < transformPointList.Count; i++)
                {

                    trans = transformPointList[i];

                    if (trans)
                    {
                        tempPositionList.Add(trans.position);
                        tempUpwardList.Add(trans.up);
                    }

                }

            }

            // SetPointList
            {
                this.SetPointList(tempPositionList, tempUpwardList, samplingUnitMeter, close, tangentScale);
            }

            // clear
            {
                tempPositionList.Clear();
                tempUpwardList.Clear();
            }

        }

        /// <summary>
        /// Set point list
        /// </summary>
        /// <param name="basePointList">base point list</param>
        /// <param name="upwardList">upward direction list</param>
        /// <param name="samplingUnitMeter">sampling unit meter</param>
        /// <param name="close">close</param>
        /// <param name="tangentScale">tangent scale</param>
        // ---------------------------------------------------------------------------------------------------------
        public virtual void SetPointList(
            IList<Vector3> basePointList,
            IList<Vector3> upwardList,
            float samplingUnitMeter,
            bool close,
            float tangentScale = 1.0f
            )
        {

            int minSize = Mathf.Min(basePointList.Count, upwardList.Count);

            // ---------------------------

            if (minSize <= 1 || samplingUnitMeter <= 0.0f)
            {
                this.bezieList.Clear();
                return;
            }

            // ---------------------------

            // bezieList
            {

                int bezierCount = (close) ? minSize : minSize - 1;

                // remove
                {
                    for (int i = this.bezieList.Count - 1; i >= bezierCount; i--)
                    {
                        this.bezieList.RemoveAt(i);
                    }
                }

                // add
                {

                    int count = Mathf.Abs(this.bezieList.Count - bezierCount);

                    for (int i = 0; i < count; i++)
                    {
                        this.bezieList.Add(new EquidistantBezierCurve());
                    }

                }

            }

            // ---------------------------

            Vector3 prev = (close) ? basePointList[minSize - 1] : basePointList[0];
            Vector3 curr = basePointList[0];
            Vector3 next1 = basePointList[1];
            Vector3 next2 = Vector3.zero;

            Vector3 currDir = (next1 != prev) ? (next1 - prev).normalized : Vector3.zero;
            Vector3 nextDir = Vector3.zero;

            Vector3 currUpward = upwardList[0];
            Vector3 nextUpward = Vector3.up;

            int targetSize = (close) ? minSize : minSize - 1;

            // ---------------------------

            for (int i = 0; i < targetSize; i++)
            {

                // set
                {

                    curr = basePointList[i];
                    next1 = basePointList[(close) ? (i + 1) % minSize : Mathf.Min(i + 1, minSize - 1)];
                    next2 = basePointList[(close) ? (i + 2) % minSize : Mathf.Min(i + 2, minSize - 1)];

                    nextDir = (next2 != curr) ? (next2 - curr).normalized : Vector3.zero;

                    nextUpward = upwardList[(close) ? (i + 1) % minSize : Mathf.Min(i + 1, minSize - 1)];

                }

                // update
                {
                    this.bezieList[i].Update(
                        curr,
                        curr + (currDir * tangentScale),
                        next1 - (nextDir * tangentScale),
                        next1,
                        samplingUnitMeter,
                        currUpward,
                        nextUpward
                        );
                }

                // prev
                {
                    prev = curr;
                    currDir = nextDir;
                    currUpward = nextUpward;
                }

            }

            // init
            {
                this.Init();
            }

        }

        /// <summary>
        /// Initialize
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------
        public virtual void Init()
        {

            // sampledAllTotalMeterLength
            {

                this.sampledAllTotalMeterLength = 0.0f;

                foreach (var val in this.bezieList)
                {
                    this.sampledAllTotalMeterLength += val.sampledTotalMeterLength;
                }

            }

        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindSampledBezierPointByNormalizedValue(
            float t01,
            EquidistantBezierCurve.PointOnBezier dest
            )
        {
            this.FindSampledBezierPointByMeter(this.sampledAllTotalMeterLength * t01, dest);
        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void FindSampledBezierPointByMeter(
            float targetMeter,
            EquidistantBezierCurve.PointOnBezier dest
            )
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            EquidistantBezierCurve bezier = null;
            float t01 = 0.0f;

            if (this.FindTargetBezier(targetMeter, ref bezier, ref t01))
            {
                bezier.FindSampledBezierPointByNormalizedValue(t01, dest);
            }

        }

        /// <summary>
        /// Find target bezier
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <param name="dest">destination</param>
        /// <returns>found</returns>
        // -------------------------------------------------------------------------------------------
        public virtual bool FindTargetBezier(
            float targetMeter,
            ref EquidistantBezierCurve dest
            )
        {

            float t01 = 0.0f;

            return this.FindTargetBezier(targetMeter, ref dest, ref t01);

        }

        /// <summary>
        /// Find target bezier
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <param name="dest">destination</param>
        /// <param name="t01">normalized t</param>
        /// <returns>found</returns>
        // -------------------------------------------------------------------------------------------
        public virtual bool FindTargetBezier(
            float targetMeter,
            ref EquidistantBezierCurve dest,
            ref float t01
            )
        {

            if (this.bezieList.Count <= 0)
            {
                return false;
            }

            // -------------------

            if (targetMeter <= 0.0f)
            {
                dest = this.bezieList[0];
                t01 = 0.0f;
                return true;
            }

            else if (targetMeter >= this.sampledAllTotalMeterLength)
            {
                dest = this.bezieList[this.bezieList.Count - 1];
                t01 = 1.0f;
                return true;
            }

            // -------------------

            float currentTotalMeter = 0.0f;

            // -------------------

            foreach (var val in this.bezieList)
            {

                if (val.sampledTotalMeterLength <= 0.0f)
                {
                    continue;
                }

                // ----------------

                // found
                {
                    if (currentTotalMeter + val.sampledTotalMeterLength >= targetMeter)
                    {
                        dest = val;
                        t01 = Mathf.Clamp01((targetMeter - currentTotalMeter) / val.sampledTotalMeterLength);
                        return true;
                    }
                }

                // currentTotalMeter
                {
                    currentTotalMeter += val.sampledTotalMeterLength;
                }

            }

            return false;

        }

        /// <summary>
        /// Find a closest position from pointP
        /// </summary>
        /// <param name="pointP">pointP</param>
        /// <param name="position">position</param>
        /// <param name="from">from</param>
        /// <param name="to">to</param>
        // ---------------------------------------------------------------------------------------
        public void FindClosestPositionOnLines(
            Vector3 pointP,
            out Vector3 position,
            out Vector3 from,
            out Vector3 to
            )
        {

            position = Vector3.zero;
            from = Vector3.zero;
            to = Vector3.zero;

            // --------------

            Vector3 tempPosition = Vector3.zero;
            Vector3 tempFrom = Vector3.zero;
            Vector3 tempTo = Vector3.zero;

            float currentMinSqrDistance = float.MaxValue;
            float tempSqrDistance = 0.0f;

            // --------------

            for (int i = 0; i < this.bezieList.Count; i++)
            {

                this.bezieList[i].FindClosestPositionOnLines(
                    pointP,
                    out tempPosition,
                    out tempFrom,
                    out tempTo
                    );

                tempSqrDistance = (pointP - tempPosition).sqrMagnitude;

                if (currentMinSqrDistance > tempSqrDistance)
                {
                    currentMinSqrDistance = tempSqrDistance;
                    position = tempPosition;
                    from = tempFrom;
                    to = tempTo;
                }

            }

        }

    }

}
