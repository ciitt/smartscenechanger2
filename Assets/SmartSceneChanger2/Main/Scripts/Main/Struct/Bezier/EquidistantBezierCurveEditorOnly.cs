#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC2
{

    public partial class EquidistantBezierCurve
    {

        /// <summary>
        /// Draw the bezier line (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        /// <param name="width">line width</param>
        // ------------------------------------------------------------------------------------------
        public void DrawBezierLineEditorOnly(
            Color color,
            float width
            )
        {

            Handles.DrawBezier(
                this.p0,
                this.p3,
                this.p1,
                this.p2,
                color,
                null,
                width
                );

        }

        /// <summary>
        /// Draw the handle on the bezier (EditorOnly)
        /// </summary>
        /// <param name="position01">normalized position</param>
        /// <param name="useSampledPoints">flag to use sampled points</param>
        /// <param name="handleWidth">width of DrawAAPolyLine</param>
        // ------------------------------------------------------------------------------------------
        public void DrawHandleOnBezierEditorOnly(
            float position01,
            bool useSampledPoints,
            float handleWidth
            )
        {

            float lengthScale = 5.0f;

            // ------------------------

            // 
            {
                this.GetFindPointByNormalizedValueAction(useSampledPoints)(position01, tempPobInstance);
            }

            // tangent
            {

                Handles.color = Handles.zAxisColor;

                Handles.DrawAAPolyLine(
                    handleWidth,
                    tempPobInstance.position,
                    tempPobInstance.position + (tempPobInstance.tangent * lengthScale)
                    );

            }

            // normal
            {

                Handles.color = Handles.xAxisColor;

                Handles.DrawAAPolyLine(
                    handleWidth,
                    tempPobInstance.position,
                    tempPobInstance.position + (tempPobInstance.normalRight * lengthScale)
                    );

            }

            // up
            {

                Handles.color = Handles.yAxisColor;

                Handles.DrawAAPolyLine(
                    handleWidth,
                    tempPobInstance.position,
                    tempPobInstance.position + (tempPobInstance.up * lengthScale)
                    );

            }

        }

    }

}

#endif
