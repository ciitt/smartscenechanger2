﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Shader property
    /// </summary>
    public class ShaderProperty
    {

        /// <summary>
        /// Property id
        /// </summary>
        public int id = -1;

        /// <summary>
        /// Shader property
        /// </summary>
        /// <param name="propertyName">propertyName</param>
        public ShaderProperty(string propertyName)
        {
            this.id = Shader.PropertyToID(propertyName);
        }

    }

}
