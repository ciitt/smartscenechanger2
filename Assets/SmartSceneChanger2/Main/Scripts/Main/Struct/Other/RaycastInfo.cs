using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Raycast info
    /// </summary>
    [Serializable]
    public class RaycastInfo
    {

        /// <summary>
        /// Max distance
        /// </summary>
        [Tooltip("Max distance")]
        [Range(0.0f, 1000.0f)]
        public float raycastMaxDistance = 10.0f;

        /// <summary>
        /// Offset from hit position
        /// </summary>
        [Tooltip("Offset from hit position")]
        public float offsetFromHitPosition = 0.0f;

        /// <summary>
        /// Target layers to raycast
        /// </summary>
        [Tooltip("Target layers to raycast")]
        public LayerMask targetLayers;

    }

    /// <summary>
    /// Raycast info with a flag to use or not
    /// </summary>
    [Serializable]
    public class RaycastInfoWithFlag : RaycastInfo
    {

        /// <summary>
        /// Flag to use to raycast
        /// </summary>
        [Tooltip("Flag to use raycast")]
        public bool useRaycast;

    }

}
