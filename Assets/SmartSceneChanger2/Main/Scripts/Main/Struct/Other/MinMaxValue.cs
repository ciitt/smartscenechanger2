﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Min and max value
    /// </summary>
    /// <typeparam name="T">template</typeparam>
    [Serializable]
    public abstract class MinMaxValue<T>
    {

        /// <summary>
        /// Min value
        /// </summary>
        public T min;

        /// <summary>
        /// Max valuue
        /// </summary>
        public T max;

        /// <summary>
        /// Get random value between min and max
        /// </summary>
        /// <returns>random value</returns>
        public abstract T GetRandomValue();

    }

    /// <summary>
    /// MinMaxValue of float
    /// </summary>
    [Serializable]
    public class MinMaxFloat : MinMaxValue<float>
    {
        public override float GetRandomValue() { return UnityEngine.Random.Range(this.min, this.max); }
    }

    /// <summary>
    /// MinMaxValue of Vector2
    /// </summary>
    [Serializable]
    public class MinMaxVector2 : MinMaxValue<Vector2>
    {
        public override Vector2 GetRandomValue()
        {
            return new Vector2(
                UnityEngine.Random.Range(this.min.x, this.max.x),
                UnityEngine.Random.Range(this.min.y, this.max.y)
                );
        }
    }

    /// <summary>
    /// MinMaxValue of Vector3
    /// </summary>
    [Serializable]
    public class MinMaxVector3 : MinMaxValue<Vector3>
    {
        public override Vector3 GetRandomValue()
        {
            return new Vector3(
                UnityEngine.Random.Range(this.min.x, this.max.x),
                UnityEngine.Random.Range(this.min.y, this.max.y),
                UnityEngine.Random.Range(this.min.z, this.max.z)
                );
        }
    }

    /// <summary>
    /// MinMaxValue of 4
    /// </summary>
    [Serializable]
    public class MinMaxVector4 : MinMaxValue<Vector4>
    {
        public override Vector4 GetRandomValue()
        {
            return new Vector4(
                UnityEngine.Random.Range(this.min.x, this.max.x),
                UnityEngine.Random.Range(this.min.y, this.max.y),
                UnityEngine.Random.Range(this.min.z, this.max.z),
                UnityEngine.Random.Range(this.min.w, this.max.w)
                );
        }
    }

}
