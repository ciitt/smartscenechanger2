using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SSC2
{

    /// <summary>
    /// User progress data
    /// </summary>
    [Serializable]
    public class UserProgressData
    {

        /// <summary>
        /// Event state
        /// </summary>
        public enum EventState
        {
            Unknown,
            Save,
            Load
        }

        /// <summary>
        /// Scene name
        /// </summary>
        public string sceneName = "";

        /// <summary>
        /// Version
        /// </summary>
        public int version = 1;

        /// <summary>
        /// yyyyMMddHHmmss when saved the data
        /// </summary>
        [SerializeField]
        protected string yyyyMMddHHmmss = "";

        /// <summary>
        /// Key and data list
        /// </summary>
        [SerializeField]
        protected List<KeyAndValue<string, string>> keyAndDataList = new List<KeyAndValue<string, string>>();

        /// <summary>
        /// Key and data dictionary
        /// </summary>
        protected Dictionary<string, string> keyAndDataDict = new Dictionary<string, string>();

        // -------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Event state
        /// </summary>
        public EventState eventState { get; set; } = EventState.Unknown;

        /// <summary>
        /// yyyyMMddHHmmss when saved the data
        /// </summary>
        protected DateTime _yyyyMMddHHmmssDateTime = DateTime.MinValue;

        /// <summary>
        /// yyyyMMddHHmmss when saved the data
        /// </summary>
        public DateTime yyyyMMddHHmmssDateTime
        {

            get
            {

                if (this._yyyyMMddHHmmssDateTime <= DateTime.MinValue)
                {
                    Funcs.ConvertStringToDateTime(this.yyyyMMddHHmmss, ref this._yyyyMMddHHmmssDateTime);
                }

                return this._yyyyMMddHHmmssDateTime;

            }

        }

        /// <summary>
        /// Overwrite the instance from json
        /// </summary>
        /// <param name="json">json</param>
        /// <returns>error</returns>
        // -------------------------------------------------------------------------------------------------------
        public virtual string OverwriteFromJson(string json)
        {

            string error = "";

            // FromJsonOverwrite
            {

                try
                {

                    if (!string.IsNullOrEmpty(json))
                    {
                        JsonUtility.FromJsonOverwrite(json, this);
                    }

                }

                catch (Exception e)
                {
                    error = e.Message;
                }

            }

            // keyAndDataDict
            {
                KeyAndValue<string, string>.ListToDictionary(this.keyAndDataList, this.keyAndDataDict);
                this.keyAndDataList.Clear();
            }

            return error;

        }

        /// <summary>
        /// Clear
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        public virtual void Clear()
        {

            this.sceneName = "";
            this.yyyyMMddHHmmss = "";

            this._yyyyMMddHHmmssDateTime = DateTime.MinValue;

            this.keyAndDataList.Clear();
            this.keyAndDataDict.Clear();

        }

        /// <summary>
        /// Convert the instance to json
        /// </summary>
        /// <param name="dateTime">date time</param>
        /// <returns>json</returns>
        // -------------------------------------------------------------------------------------------------------
        public virtual string ConvertToJson(DateTime dateTime)
        {

            // keyAndDataDict
            {
                KeyAndValue<string, string>.DictionaryToList(this.keyAndDataDict, this.keyAndDataList);
            }

            // sceneName, yyyyMMddHHmmss
            {
                this.sceneName = SceneManager.GetActiveScene().name;
                Funcs.ConvertDateTimeToString(dateTime, ref this.yyyyMMddHHmmss);
            }

            // ------------------

            string ret = JsonUtility.ToJson(this);

            // ------------------

            // clear
            {
                this.keyAndDataList.Clear();
            }

            return ret;

        }

        /// <summary>
        /// Load into the object
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="obj">object</param>
        // -------------------------------------------------------------------------------------------------------
        public virtual void LoadInto(
            string key,
            object obj
            )
        {

            if (obj != null)
            {

                string json = this.GetData(key);

                if (!string.IsNullOrEmpty(json))
                {
                    JsonUtility.FromJsonOverwrite(json, obj);
                }

            }

        }

        /// <summary>
        /// Add data
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="obj">object</param>
        // -------------------------------------------------------------------------------------------------------
        public virtual void AddData(
            string key,
            object obj
            )
        {

            if (obj != null)
            {

                string json = JsonUtility.ToJson(obj);

                this.AddData(key, json);

            }

        }

        /// <summary>
        /// Add data
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="data">data</param>
        // -------------------------------------------------------------------------------------------------------
        public virtual void AddData(
            string key,
            string data
            )
        {

            if (string.IsNullOrEmpty(key))
            {
                Debug.LogError("Empty key");
                return;
            }

            // ------------------

            if (this.keyAndDataDict.ContainsKey(key))
            {
#if UNITY_EDITOR
                Debug.LogErrorFormat("Key already exists : {0}", key);
#endif
            }

            this.keyAndDataDict[key] = data;

        }

        /// <summary>
        /// Get data
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>found data or empty</returns>
        // -------------------------------------------------------------------------------------------------------
        public virtual string GetData(string key)
        {

            string ret = "";

            if (this.keyAndDataDict.ContainsKey(key))
            {
                ret = this.keyAndDataDict[key];
            }

            return ret;

        }

    }

}
