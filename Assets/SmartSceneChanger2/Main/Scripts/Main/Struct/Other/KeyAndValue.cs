using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Key and value
    /// </summary>
    /// <typeparam name="TKey">key</typeparam>
    /// <typeparam name="TValue">value</typeparam>
    [Serializable]
    public class KeyAndValue<TKey, TValue>
    {

        /// <summary>
        /// Key
        /// </summary>
        [Tooltip("Key")]
        public TKey key = default(TKey);

        /// <summary>
        /// Value
        /// </summary>
        [Tooltip("Value")]
        public TValue value = default(TValue);

        /// <summary>
        /// Find by key
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="list">list to iterate</param>
        /// <returns>found or null</returns>
        // ------------------------------------------------------------------------------------------------------
        public static KeyAndValue<TKey, TValue> Find(
            TKey key,
            IEnumerable<KeyAndValue<TKey, TValue>> list
            )
        {

            EqualityComparer<TKey> equalityComparer = EqualityComparer<TKey>.Default;

            foreach (var val in list)
            {
                if (equalityComparer.Equals(val.key, key))
                {
                    return val;
                }
            }

            return null;

        }

        /// <summary>
        /// Convert list to dictionary
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="dest">destination</param>
        /// <returns>no duplicated key</returns>
        // ------------------------------------------------------------------------------------------------------
        public static bool ListToDictionary(
            List<KeyAndValue<TKey, TValue>> list,
            Dictionary<TKey, TValue> dest
            )
        {

            bool ret = true;

            // clear
            {
                dest.Clear();
            }

            // add
            {

                foreach (var val in list)
                {

                    if (dest.ContainsKey(val.key))
                    {
                        ret = false;
                    }

                    else
                    {
                        dest.Add(val.key, val.value);
                    }

                }

            }

            return ret;

        }

        /// <summary>
        /// Convert list to dictionary
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="dest">destination</param>
        /// <returns>no duplicated key</returns>
        // ------------------------------------------------------------------------------------------------------
        public static void DictionaryToList(
            Dictionary<TKey, TValue> dict,
            List<KeyAndValue<TKey, TValue>> dest
            )
        {

            // recycle
            {
                Funcs.RecycleList(dest, dict.Count);
            }

            // add
            {

                int index = 0;

                foreach (var kv in dict)
                {
                    dest[index++].Set(kv);
                }

            }

        }

        /// <summary>
        /// Set
        /// </summary>
        /// <param name="kv">key and value</param>
        // ------------------------------------------------------------------------------------------------------
        public void Set(KeyValuePair<TKey, TValue> kv)
        {
            this.key = kv.Key;
            this.value = kv.Value;
        }

    }

}
