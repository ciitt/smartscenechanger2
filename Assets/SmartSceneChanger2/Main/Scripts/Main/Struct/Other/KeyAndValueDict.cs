using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Key and value dictionary
    /// </summary>
    /// <typeparam name="TKey">key</typeparam>
    /// <typeparam name="TValue">value</typeparam>
    [Serializable]
    public class KeyAndValueDict<TKey, TValue>
    {

        /// <summary>
        /// List
        /// </summary>
        [SerializeField]
        protected List<KeyAndValue<TKey, TValue>> _list = new List<KeyAndValue<TKey, TValue>>();

        /// <summary>
        /// Dictionary
        /// </summary>
        protected Dictionary<TKey, TValue> _dict = new Dictionary<TKey, TValue>();

        // ------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Dictionary
        /// </summary>
        public Dictionary<TKey, TValue> dict
        {

            get
            {

                if (
                    this._list.Count > 0 &&
                    this._dict.Count <= 0
                    )
                {
                    KeyAndValue<TKey, TValue>.ListToDictionary(this._list, this._dict);
                }

                return this._dict;

            }

        }

    }

}
