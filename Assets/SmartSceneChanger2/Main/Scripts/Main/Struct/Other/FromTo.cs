using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// From to
    /// </summary>
    /// <typeparam name="T">T</typeparam>
    [Serializable]
    public class FromTo<T>
    {

        /// <summary>
        /// From
        /// </summary>
        [Tooltip("From")]
        public T from = default(T);

        /// <summary>
        /// To
        /// </summary>
        [Tooltip("To")]
        public T to = default(T);

    }

}
