using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// Values for lerp
    /// </summary>
    /// <typeparam name="T">template</typeparam>
    public abstract class LerpValues<T>
    {

        /// <summary>
        /// Current value
        /// </summary>
        public T current;

        /// <summary>
        /// Target valuue
        /// </summary>
        public T target;

        /// <summary>
        /// Lerp
        /// </summary>
        /// <param name="t">t</param>
        public abstract void Lerp(float t);

    }

    /// <summary>
    /// LerpValues of float
    /// </summary>
    public class FloatLerpValues : LerpValues<float>
    {

        public override void Lerp(float t)
        {
            this.current = Mathf.Lerp(this.current, this.target, t);
        }

    }

}
