using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC2
{

    /// <summary>
    /// String and string
    /// </summary>
    [Serializable]
    public class StringAndString : KeyAndValue<string, string>
    {

    }

}
